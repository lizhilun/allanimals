<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>실종 동물 | 조회</title>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

<script src="https://kit.fontawesome.com/da4b1b94bb.js" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>

</head>
<body>
	<jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
	<jsp:include page="/WEB-INF/views/menubar/sidebar.jsp"/>
	
 <div class="container">
    <div class="features">
        <div class="content">
          <div class="page-title">
            <h3>실종 동물</h3>
            </div>

        <!-- 메뉴 선택 -->
        <div class="menu">
          <c:if test="${ !empty sessionScope.loginMember }">
          <div class="write">
            <button class="btnMenu btn1" onclick="addForm()">글쓰기</button>
            </div>
            </c:if>
				<script type="text/javascript">
					function addForm() {
						location.href="${ pageContext.servletContext.contextPath }/disappear/insert";
					}
				</script>
          <div class="mine">
          	<c:if test="${ !empty sessionScope.loginMember }">
            <button class="btnMenu btn2" onclick="location.href='${ pageContext.servletContext.contextPath }/disappear/list?type=myT'">나의 글</button>
            <!-- &&login=Y -->
            </c:if>
          </div>
        </div>

        <!-- 게시판 검색창 -->
        <div id="board-search">
          <div class="content">
            <div class="search-window">
              <form action="${ pageContext.servletContext.contextPath }/disappear/list">
                <div class="search-wrap">
                  <label for="search" class="blind"></label>
                  <!-- <input type="search" id="searchValue" name="searchValue" placeholder="원하시는 글의 제목을 입력해주세요." value="<c:out value="${ requestScope.selectCriteria.searchValue }"/>"/>-->
                  <button type="submit" class="btn btn-dark" >검색</button>
                </div>
              </form>
            </div>
          </div>
        </div>

          <ul class="clearfix" >
        <c:forEach items="${ requestScope.disappearList }" var="disappear">
            <li>
              <div class="item">
                <div class="thumb">
                <!-- 사진 -->
                  <a onclick="href='${ pageContext.servletContext.contextPath }/disappear/content?disNum='${ disappear.disNum }">
                  <img src="<c:out value='/allanimals/${ disappear.thumPath }'/>" alt="1"></a> <!-- 동물 사진을 넣어주세요. -->
                </div>
                <div class="desc">
                <input type="hidden" name="introNum" value="${ disappear.disNum }">
                  <!-- 제목 -->
                  <h5><a onclick="href='${ pageContext.servletContext.contextPath }/disappear/content?disNum=${ disappear.disNum }'"><c:out value="제목입니다."/></a></h5>
                  <!-- 내용 -->
                  <p><a onclick="href='${ pageContext.servletContext.contextPath }/disappear/content?disNum=${ disappear.disNum }'"><c:out value="${ disappear.disCont }" escapeXml="false"/></a></p>
                  <div class="detail">
                  <!-- 날짜 -->
                  <span><c:out value="${ disappear.disTime }"/></span>
                  <!-- 아이디 -->
                  <span><c:out value="${ disappear.memId }"/></span>

                  </div>
                </div>
              </div>
            </li>
		</c:forEach>
          </ul>
		
		</div>
      </div>

    
      <div class="pagination">
      <jsp:include page="../common/paging.jsp"/>
      </div> 
  </div>
	<jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>
	
	</body>
	</html>
	
	
	
	
	