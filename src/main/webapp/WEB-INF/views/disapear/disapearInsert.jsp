<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>실종 동물 | 게시글 수정</title>
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/allDaily/template.css">

	
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
    <style>
      * {
        margin: 0 auto;
      } 
      
      .container{
        height: 1600px;
      }
      #pName {
        margin-top: -100px;
        height: 120px;
      }

      .nameBox{
        width: 800px;
        height: 50px;
        margin-bottom: 30px;
        padding: 0 0 0 15px;
        font-size: 21px;
      }
      
      .petName{
        width: 300px;
        height: 50px;
        margin-bottom: 30px;
        padding: 0 0 0 15px;
        font-size: 21px;
      }
      

      #table{  position:absolute;
      left: 30%;
      top:300px;
      width: 800px; height: 630px;}

      #btn {
        float: right;
        margin-top: 40px;
      }

      #btn button {
        background-color: white; 
        color: black; 
        border: 2px solid #DAE5D0;
        text-decoration: none;
        color: black;
        width: 100px;
        height: 40px;
        font-size: 16px;
        margin: 4px 2px;
        text-decoration: none;
        cursor: pointer;
        padding: 0px;
        text-align: center;
      }

      #btn button:hover {
        background-color: #DAE5D0;
        color: white;
      }

      input[type=text] {
        margin-left: 0px !important;
        margin-top:  0px !important;
        width: 537px !important;
      }

      .note-btn-primary {
        margin-right: 10px !important;
        margin-top: -25px !important;
      }

      /* 검색창 */
      input[type=text] {
        width: auto;
        box-sizing: border-box;
        border: 1px solid #000;
        border-radius: 4px;
        font-size: 16px;
        color: #000;
        background-color: white;
        /* background-image: url('searchicon.png'); */
        /* background-position: 10px 10px;  */
        background-repeat: no-repeat;
        padding: 8px 20px 8px 40px;
        -webkit-transition: width 0.4s ease-in-out;
        transition: width 0.4s ease-in-out;
        margin-left: -80px;
        margin-bottom: 80px;
        margin-top: 50px;
      }
      
      input[type=text]:focus {
        width: 200px;
      }
    </style>


</head>
<body>
<jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
<jsp:include page="/WEB-INF/views/menubar/sidebar.jsp"/>

    <div class="container">

      <div id="table">
        <div id="pName"><h1>실종 동물 | 게시글 수정</h1></div>
		<form name="addDisappear" id="addDisappear" action="${ pageContext.servletContext.contextPath }/update/list" method="post">
  
        <div>
        <input type="email" class="nameBox" name="disTitle" placeholder="글제목을 입력하세요." value="제목입니다.	" > 
        </div>
        <textarea id="summernote" name="disCont">
      	<c:out value="${ requestScope.thumbnail.disCont }" escapeXml= "false"/>
        </textarea>
		<input type="hidden" name="disNum" value="${ requestScope.thumbnail.disNum }">
		</form>

        <div id="btn">
          <button onclick="insert()">작성하기</button>
          <script type="text/javascript">
              function insert() {
            	  var cont = $("#disCont").text();
            	  $("#Cont").val(cont);
            	  
            	  $("#addDisappear").submit();
              }
          </script>
        </div>
      </div>
      <script>
        $(document).ready(function() {
            $('#summernote').summernote({
    
                height:600 , 
              minHeight: 600,             // set minimum height of editor
                maxHeight: 600,             // set maximum height of editor
                 focus: true   ,             
                toolbar: [
                    
    
              // [groupName, [list of button]]
              ['fontname', ['fontname']],
              ['fontsize', ['fontsize']],
              ['style', ['bold', 'italic', 'underline','strikethrough', 'clear']],
              ['color', ['forecolor','color']],
              ['table', ['table']],
              ['para', ['ul', 'ol', 'paragraph']],
              ['height', ['height']],
              ['insert',['picture']],
              ['view', ['fullscreen', 'help']]
            ],
          fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New','맑은 고딕','궁서','굴림체','굴림','돋움체','바탕체'],
          fontSizes: ['8','9','10','11','12','14','16','18','20','22','24','28','30','36','50','72']
                
            });
            
    
        });
       
      </script>
      <script>
		function loadImg(value, num) {
			if (value.files && value.files[0]) {
				const reader = new FileReader();
				reader.onload = function(e) {
					switch(num){
					case 1:
						document.getElementById("introFile1").src = e.target.result;
						break;
					case 2:
						document.getElementById("introFile2").src = e.target.result;
						break;
					case 3:
						document.getElementById("introFile3").src = e.target.result;
						break;
					}
				}
				reader.readAsDataURL(value.files[0]);
			}
		}
		
	  </script>
      
      
    </div>

<jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>


</body>
</html>