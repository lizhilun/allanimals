<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />

    <title>Document</title>
    
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/header.css?ver=1" type="text/css">
    <script src="http://code.jquery.com/jquery-3.6.0.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
    <script src=" https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.18/lang/summernote-ko-KR.min.js"></script>
</head>
<body>
<c:if test="${ empty sessionScope.loginMember }">
   <div class="header">
      <a href="${ pageContext.servletContext.contextPath }/main"
        ><img
          src="${ pageContext.servletContext.contextPath }/resources/upload/images/KakaoTalk_20220713_121409010.png"
          alt="logo"
        />
      </a>
	
      <ul class="headerMenu">
        <li><a href="${ pageContext.servletContext.contextPath }/member/login" class="headmenu">로그인</a></li>
        <li><a href="${ pageContext.servletContext.contextPath }/member/regist" class="headmenu">회원가입</a></li>
      </ul>
    </div>
    </c:if>
    <c:if test="${ !empty sessionScope.loginMember }">
   <div class="header">
      <a href="${ pageContext.servletContext.contextPath }/main"
        ><img
          src="${ pageContext.servletContext.contextPath }/resources/upload/images/KakaoTalk_20220713_121409010.png"
          alt="logo"
        />
      </a>
	
      <ul class="headerMenu">
        <li><a href="${ pageContext.servletContext.contextPath }/member/userInfo" class="headmenu">마이페이지</a></li>
        <li><a href="${ pageContext.servletContext.contextPath }/member/logout" class="headmenu">로그아웃</a></li>
      </ul>
    </div>
    </c:if>
    <!--header end-->
</body>
</html>