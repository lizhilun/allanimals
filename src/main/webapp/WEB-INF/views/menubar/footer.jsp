<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
 <meta charset="UTF-8">
 <title>Document</title>
 <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/footer.css?ver=1" type="text/css">

</head>
<body>
<!--footer-->
    <div class="footer">
        <h1>
          사단법인 all animal | 대표 나야나 | 고유번호 623-82-00401 <br />| 주소
          서울시 종로구 | TEL 070-8802-8800 | E-mail allanimal@naver.com<br />
          국민권익위 https://www.acrc.go.kr/ | 국세청 https://www.nts.go.kr/<br />
        </h1>
        <img class="centerlogo"
            src="${ pageContext.servletContext.contextPath }/resources/upload/images/linelesLogo.png"
            alt="centerlogo"
          />
          <h2>Copyright ⓒ allanimal | Design by allanimal | Hosting by I'MWEB</h2>
      </div>
      <!--footer end -->
</body>
</html>