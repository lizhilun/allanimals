<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
   <meta charset="UTF-8" />
    <title>Document</title>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/sidebar.css?ver=1" type="text/css">
<!--      <script src="http://code.jquery.com/jquery-3.6.0.min.js"></script> -->
</head>
<body>
<input type="checkbox" id="menuicon" />
    <!--checked 속성은 버튼을 누르지 않고도 사이드바가 나온 상태로 화면에 띄움-->
    <label for="menuicon">
      <span></span>
      <span></span>
      <span></span>
      <!-- span 이 있어야 버튼이 나옵니다-->
    </label>
    <div class="sidebar">
      <!--sidebar menu-->
      <a href="${ pageContext.servletContext.contextPath }/main"
        ><img
          src="${ pageContext.servletContext.contextPath }/resources/upload/images/KakaoTalk_20220713_121409010.png"
          alt="logo"
        />
      </a>

      <div class="accordianMenu">
        <button class="accordion" onclick="location.href='${ pageContext.servletContext.contextPath }/main'">
          메인화면으로
        </button>
         <button class="accordion" onclick="location.href='${ pageContext.servletContext.contextPath }/intro'">유기동물 소개글</button>
        <button class="accordion" onclick="location.href='${ pageContext.servletContext.contextPath }/daily'">동물들의 일상</button>
        <button class="accordion">동물들 입양 후기</button>
        <button class="accordion">실종 동물 게시판</button>
        <div class="panel">
          <li><a href="${ pageContext.servletContext.contextPath }/disappear/list" class="accmenu">잃어버린 동물</a></li>
          <li><a href="${ pageContext.servletContext.contextPath }/rescue/list" class="accmenu">내가 구조한 동물</a></li>
        </div>
        <button class="accordion">공지&문의</button>
        <div class="panel">
          <li><a href="${ pageContext.servletContext.contextPath }/board/list" class="accmenu">공지사항 조회</a></li>
          <li><a href="${ pageContext.servletContext.contextPath }/board/inquirylist" class="accmenu">문의사항</a></li>
        </div>
        <button class="accordion" onclick="location.href='#'">후원하기</button>
        <div class="panel">
          <li><a href="${ pageContext.servletContext.contextPath }/donation/main" class="accmenu">후원하기</a></li>
          <li><a href="${ pageContext.servletContext.contextPath }/donation/sub" class="accmenu">정기 후원하기</a></li>
        </div>
        <button
        class="help-btn"
        type="submit"
        onclick="location.href='${ pageContext.servletContext.contextPath }/rescueRequest';">
        Help!
      </button>
      </div>

      <!-- accordion menu javascript -->
      <script>
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
          acc[i].addEventListener("click", function () {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
              panel.style.maxHeight = null;
            } else {
              panel.style.maxHeight = panel.scrollHeight + "px";
            }
          });
        }
      </script>
      <!-- accordion menu javascript end -->
    </div>
    <!--sidebar menu end-->
</body>
</html>