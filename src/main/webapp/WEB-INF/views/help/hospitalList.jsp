<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
       <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/help/hospitalList.css">
<script src="http://code.jquery.com/jquery-3.6.0.min.js"></script>
 <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<!-- 맵관련 -->
<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=29afa9012e3f54296163227e207c6ab3&libraries=services,clusterer,drawing"></script>
<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=29afa9012e3f54296163227e207c6ab3"></script>
</head>
<body>
 	<jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
	<jsp:include page="/WEB-INF/views/help/helpSidebar.jsp"/>
	<container>
	
	 <div class="container">
	 <div class="page-title">
      <h3>지도</h3>
      </div>
	   <div id="map" style="width:80%;height:350px;" class="maparea"></div>
      <div class="page-title">
      <h3>동물병원목록</h3>
      
      </div>
       

<script>


var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
    mapOption = {
        center: new kakao.maps.LatLng(37.5702, 126.9829), // 지도의 중심좌표
        level: 3 // 지도의 확대 레벨
    };  

// 지도를 생성합니다    
var map = new kakao.maps.Map(mapContainer, mapOption); 

var positionList = [];

function onMap(name,addre){
	
	this.onload = function(){
		geocoder.addressSearch(addre, function(result, status) {
		    // 정상적으로 검색이 완료됐으면 
		     if (status === kakao.maps.services.Status.OK) {

		        var coords = new kakao.maps.LatLng(result[0].y, result[0].x);
				
		        positionList.push(coords,name);
		    } 
		});
	}
	
}
/* window.onload =  function(){
	
	for (var i = 0; i < positionList.length; i ++) {
	    
	    // 마커를 생성합니다
	    var marker = new kakao.maps.Marker({
	        map: map, // 마커를 표시할 지도
	        position: positionList[2i], // 마커를 표시할 위치
	        title : positionList[2i-1], // 마커의 타이틀, 마커에 마우스를 올리면 타이틀이 표시됩니다
	    });
	
} */


/* var position = */
/* 

function onMap(mapList){

	
	this.onclick = function(){

		
		var geocoder = new kakao.maps.services.Geocoder();

for(i in mapList){
		// 주소로 좌표를 검색합니다
		geocoder.addressSearch(mapList[i].addre, function(result, status) {

		    // 정상적으로 검색이 완료됐으면 
		     if (status === kakao.maps.services.Status.OK) {

		        var coords = new kakao.maps.LatLng(result[0].y, result[0].x);

		        // 결과값으로 받은 위치를 마커로 표시합니다
		        var marker = new kakao.maps.Marker({
		            map: map,
		            position: coords
		        });

		        // 인포윈도우로 장소에 대한 설명을 표시합니다
		        var infowindow = new kakao.maps.InfoWindow({
		            content: '<div style="font-size:0.7rem; width:200px;text-align:center;padding:0.5px 0;">업체명 : ' + mapList[i].name ,
		            removable: true
		        });
		        kakao.maps.event.addListener(marker, 'click', function() {
		            infowindow.open(map, marker);  
		      });

		        // 지도의 중심을 결과값으로 받은 위치로 이동시킵니다
		      map.setCenter(coords);
		    } 
		});    
		
	}
	}
	
}
 
 */

//
var search;
var name;
var phone;
function searchAddre(na,addre,ph){
	
	this.onclick = function() {
		search = addre;
		name = na;
		phone = ph;
		
		// 주소-좌표 변환 객체를 생성합니다
		var geocoder = new kakao.maps.services.Geocoder();

		// 주소로 좌표를 검색합니다
		geocoder.addressSearch(search, function(result, status) {

		    // 정상적으로 검색이 완료됐으면 
		     if (status === kakao.maps.services.Status.OK) {

		        var coords = new kakao.maps.LatLng(result[0].y, result[0].x);

		        // 결과값으로 받은 위치를 마커로 표시합니다
		        var marker = new kakao.maps.Marker({
		            map: map,
		            position: coords
		        });

		        // 인포윈도우로 장소에 대한 설명을 표시합니다
		        var infowindow = new kakao.maps.InfoWindow({
		            content: '<div style="font-size:0.7rem; width:200px;text-align:center;padding:0.5px 0;">업체명 : ' + name + '</div>' + '<br>' +
		           			 '<div style="font-size:0.7rem; width:200px;text-align:center;padding:0.5px 0;">주소 : ' + search + '</div>' + '<br>' +
		           			 '<div style="font-size:0.7rem; width:200px;text-align:center;padding:0.5px 0;">연락처 : ' + phone + '</div>'
		        });
		        infowindow.open(map, marker);

		        // 지도의 중심을 결과값으로 받은 위치로 이동시킵니다
		        map.setCenter(coords);
		    } 
		});    
	}
	
}

</script>


        <!-- 게시판 검색창 -->
        <div id="board-search">
          <div class="content">
            <div class="search-window">
              <form id="loginForm" method="get"  action="${ pageContext.servletContext.contextPath }/help/hospitallist">
                  <input type="hidden" name="currentPage" value="1">
			    <select id="searchCondition" name="searchCondition">
			    	<option value="option" ${ requestScope.selectCriteria.searchCondition eq "option"? "selected": "" }>▼선택</option>
					<option value="hosName" ${ requestScope.selectCriteria.searchCondition eq "hosName"? "selected": "" }>병원이름</option>
					<option value="addre" ${ requestScope.selectCriteria.searchCondition eq "addre"? "selected": "" }>지역(구)</option>
					<option value="time" ${ requestScope.selectCriteria.searchCondition eq "time"? "selected": "" }>영업시간</option>
				</select>
				<div class="search-wrap">
				<label for="search" class="blind"></label>
                 <input type="search" id="searchValue" name="searchValue" value="<c:out value="${ requestScope.selectCriteria.searchValue }"/>" placeholder="검색어를 입력해주세요">
                  <button type="submit" class="btn btn-dark">검색</button>
                </div>
              </form>
            </div>
          </div>
        </div>
             
        <!-- 게시판 글 목록 -->
        <div id="board-list">
          <div class="content">
            <table class="board-table">
            <thead>
              <tr>
                <th scope="col" colspan="1">병원이름</th>
                <th scope="col" colspan="2">주소</th>
                <th scope="col" colspan="1">영업시간</th>
                <th scope="col" colspan="1">전화번호</th>
              </tr>
            </thead>

            <tbody>
               <c:forEach var="board" items="${ requestScope.boardList }">
              <%--  <input type="hidden" id="data1" value="${ board.name }" >
               <input type="hidden" id="data2" value="${ board.addre }" > --%>
               <input type="hidden" onload="onMap('${ board.name }','${ board.addre }')" >
			<tr>
				<td scope="col" colspan="1"><c:out value="${ board.name }"/></td>
				<td scope="col" colspan="2">
				<a style="cursor:pointer;" onclick="searchAddre('${ board.name }','${ board.addre }','${ board.phone }')"><c:out value="${ board.addre }"/></a></td>
				<td scope="col" colspan="1"><c:out value="${ board.time }"/></td>
				<td scope="col" colspan="1"><c:out value="${ board.phone }"/></td>
			</tr>
			</c:forEach>
            </tbody>
            </table>
          </div>
        </div>         
	<script>
	
	</script>
      <!-- 페이지네이션 -->

      <div class="pagination">
       <jsp:include page="../common/paging.jsp"/>
      </div>
  </div>
	
	
	
	
	
	</container>
	<jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>
	 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>