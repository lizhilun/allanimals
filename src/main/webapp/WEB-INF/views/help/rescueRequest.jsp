<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/button.css" />
 <script src="http://code.jquery.com/jquery-3.6.0.min.js"></script>
    <style>
    .container {position: relative; height: auto; }       
    #box {width: 1500px; height: auto; background-color: #f9ebc8; padding: 50px; position: fix;left: 10%;}  
    #select { width: 450px;}  
     
    #box2 { width: 1400px; background-color: #fff;padding: 40px;}
    .table {width: 1000px; height: auto; line-height: 40px; border-collapse: separate;}
    .footer {margin-top: 200px;}
    
    </style>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  </head>
   <script>
		$(function(){
			let check = '${ requestScope.type }';
			let select = $("#select option");
			
			for(let i = 0; i < select.length; i++){
				
				if(select[i].value == check){
					$(select[i]).prop("selected", true);
					 
					break;
				}
			}
			//alert(check);
		});
	</script> 
  <body>

	<jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
    <jsp:include page="/WEB-INF/views/help/helpSidebar.jsp"/>

    <!-- 이 밑에서부터 작업하시면 됩니다.-->
    <div class="container" style="margin-top: 100px">
     
        <div id="box">
          <select id="select" class="form-select form-select-lg mb-3 col-md-8 offset-md-8" aria-label=".form-select-lg" onchange="testChange(this)">
            <option selected>지역</option>
            <c:forEach var="list" items="${ areaList }">
          	  <option value="${ list.helpArea }"><c:out value='${ list.helpArea }'/></option>            
            </c:forEach>
          </select>
          <div id="box2">
            <table class="table">
                <thead>
                  <tr>
                    <th scope="col">관할구역</th>
                    <th scope="col">보호센터명</th>
                    <th scope="col">전화번호</th>
                    <th scope="col">보호센터 주소</th>
                  </tr>
                </thead>
               <tbody>
                <c:if test="${ testChange == '지역' }">
				<c:forEach items="${ resList }" var="res">
                  <tr>
                    <th scope="row"><c:out value="${ res.helpArea }"/></th>
                    <td><c:out value="${ res.helpName }"/></td>
                    <td><c:out value="${ res.helpPhone }"/></td>
                    <td><c:out value="${ res.helpAddre }"/></td>
                  </tr>
                </c:forEach>  
                </c:if>  
                 <c:if test="${ testChange == res.helpArea }">
				<c:forEach items="${ resList }" var="res">
                  <tr>
                    <th scope="row"><c:out value="${ res.helpArea }"/></th>
                    <td><c:out value="${ res.helpName }"/></td>
                    <td><c:out value="${ res.helpPhone }"/></td>
                    <td><c:out value="${ res.helpAddre }"/></td>
                  </tr>
                </c:forEach>  
                </c:if> 
                </tbody> 
              </table>
              
          </div>  
        </div>

    </div><!-- container close -->

   <jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>
   
   <script>
   
   	function testChange(obj){
   		
      let idx = $("#select option:selected").val();
      location.href="rescueRequest?type="+idx;
      
   	} 
   	
   </script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>
