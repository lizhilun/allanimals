<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<script>
	(function(){
		const successCode = "${ requestScope.successCode }";
		
		let successMessage = "";
		let movePath = "";
		
		switch(successCode){

			case "insertReview" : 
				successMessage = "후기 등록에 성공하셨습니다!";
				movePath = "${ pageContext.servletContext.contextPath }/review/list";
				break;
			case "insertMember" : 
				successMessage = "회원 가입에 성공하셨습니다!";
				movePath = "${ pageContext.servletContext.contextPath }";
				break;
			case "updateMember" : 
				successMessage = "회원 정보 변경에 성공하셨습니다!";
				movePath = "${ pageContext.servletContext.contextPath }/revise";
				break;
			case "deleteMember" : 
				successMessage = "회원 탈퇴에 성공하셨습니다!";
				movePath = "${ pageContext.servletContext.contextPath }/index.jsp";
				break;
			case "deleteHelp" : 
				successMessage = "정보 삭제에 성공하셨습니다!";
				movePath = "${ pageContext.servletContext.contextPath }/admin/main";
				break;
			case "updateHelp" : 
				successMessage = "정보 수정에 성공하셨습니다!";
				movePath = "${ pageContext.servletContext.contextPath }/admin/main";
				break;
			case "insertNotice" : 
				successMessage = "공지사항 등록에 성공하셨습니다!";
				movePath = "${ pageContext.servletContext.contextPath }/notice/list";

			case "updateApply" : 
				successMessage = "신청서 수정에 성공하셨습니다!";
				movePath = "${ pageContext.servletContext.contextPath }/apply3";

				break;
			case "complteAdopt" : 
				successMessage = "입양완료 처리에 성공하셨습니다!";
				movePath = "${ pageContext.servletContext.contextPath }/protection?type=protect";
				break;

			case "insertHelp" : 
				successMessage = "등록에 성공하셨습니다!";
				movePath = "${ pageContext.servletContext.contextPath }/admin/main";
				break;
			case "insertThumbnail" : 
				successMessage = "썸네일 게시판 등록에 성공하셨습니다!";
				movePath = "${ pageContext.servletContext.contextPath }/thumbnail/list";

			case "machingMem" : 
				successMessage = "회원정보 조회에 성공하셨습니다!";
				movePath = "${ pageContext.servletContext.contextPath }/matching?memId=${ requestScope.memId }&appNum=${ requestScope.appNum }";

				break;
			case "checkPass" : 
				successMessage = "비밀번호 확인에 성공하셨습니다";
				movePath = "${ pageContext.servletContext.contextPath }/check/pwd";
				break;	
			case "introWrite" : 
				successMessage = "게시글 등록에 성공했습니다!";
				movePath = "${ pageContext.servletContext.contextPath }/intro";
				break;	
			case "deleteMaching" : 
				successMessage = "입양취소에 성공하셨습니다!";
				movePath = "${ pageContext.servletContext.contextPath }/protection?type=protect";
				break;
			case "machingSuccess" : 
				successMessage = "입양매칭에 성공하셨습니다!";
				movePath = "${ pageContext.servletContext.contextPath }/protection2?aniNum=${ requestScope.aniNum }";
				break;

		}

		alert(successMessage);
		
		location.replace(movePath);
	})();
	</script>
</body>
</html>