<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/menu/menu.css "/>
</head>
 <body>
    <div class="leftside">
      <div class="header">
        <a href="${ pageContext.servletContext.contextPath }/main"
          ><img class="logo" src="${ pageContext.servletContext.contextPath }/resources/upload/images/semiLogo2.PNG" alt=""
        /></a>
      </div>
      <div class="footer">
        <button
          class="help-btn"
          type="submit"
          onclick="location.href='${ pageContext.servletContext.contextPath }/rescueRequest';"
        >
          Help!
        </button>
      </div>
    </div>
    <div class="rightside">
      <a class="mainmenu" href="#">메뉴</a>
      <div class="list">
        <ul>
          <li><a href="${ pageContext.servletContext.contextPath }/main">메인화면으로</a></li>
          <li>
       		  <a href="${ pageContext.servletContext.contextPath }/intro">유기동물 소개글</a>
          </li>
          <li><a href="${ pageContext.servletContext.contextPath }/daily">동물들의 일상</a></li>
          <li><a href="${ pageContext.servletContext.contextPath }/review/list">입양후기</a></li>
          <li><a href="${ pageContext.servletContext.contextPath }/disappear/list">실종 동물 게시판</a></li>
          <li><a href="${ pageContext.servletContext.contextPath }/rescue/list">구조 동물 게시판</a>
          <li><a href="${ pageContext.servletContext.contextPath }/board/list">고객센터</a></li>
          <li><a href="${ pageContext.servletContext.contextPath }/donation/main">후원하기</a></li>
        </ul>
      </div>
    </div>
  </body>
</html>