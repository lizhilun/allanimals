<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/noticelist.css">
	
 <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

</head>
<body>
	<jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
	<jsp:include page="/WEB-INF/views/menubar/sidebar.jsp"/>
	
	<container>
	  <div class="container">
      <div class="page-title">
      <h3>문의사항</h3>
      
      </div>
       
       

        <!-- 게시판 검색창 -->
        <div id="board-search">
          <div class="content">
            <div class="search-window">
              <%-- <form id="loginForm" method="get"  action="${ pageContext.servletContext.contextPath }/board/inquirylist">
                  <input type="hidden" name="currentPage" value="1">
			    <select id="searchCondition" name="searchCondition">
			    	<option value="option" ${ requestScope.selectCriteria.searchCondition eq "option"? "selected": "" }>▼선택</option>
					<option value="writer" ${ requestScope.selectCriteria.searchCondition eq "writer"? "selected": "" }>작성자</option>
					<option value="title" ${ requestScope.selectCriteria.searchCondition eq "title"? "selected": "" }>제목</option>
					<option value="content" ${ requestScope.selectCriteria.searchCondition eq "content"? "selected": "" }>내용</option>
				</select> --%>
				<div class="search-wrap">
				<label for="search" class="blind"></label>
               <%--   <input type="search" id="searchValue" name="searchValue" value="<c:out value="${ requestScope.selectCriteria.searchValue }"/>" placeholder="검색어를 입력해주세요">
                  <button type="submit" class="btn btn-dark">검색</button> --%>
                </div>
              <!-- </form> -->
            </div>
          </div>
        </div>
             
        <!-- 게시판 글 목록 -->
        <div id="board-list">
          <div class="content">
            <table class="board-table">
            <thead>
              <tr>
                <th scope="col" colspan="1">번호</th>
                <th scope="col" colspan="2">제목</th>
                <th scope="col" colspan="1">작성자</th>
                <th scope="col" colspan="1">작성날짜</th>
              </tr>
            </thead>

            <tbody>
               <c:forEach var="board" items="${ requestScope.boardList }">
     		<c:if test="${ board.memId eq sessionScope.loginMember.memId }">
			<tr>
				<td scope="col" colspan="1"><c:out value="${ board.no }"/></td>
				<td scope="col" colspan="2">
				<a href="${ pageContext.servletContext.contextPath }/board/inquirycontent?boardNo=${ board.no }"><c:out value="${ board.title }"/></a></td>
				<td scope="col" colspan="1"><c:out value="${ board.memId }"/></td>
				<td scope="col" colspan="1"><c:out value="${ board.date }"/></td>
			</tr>
			</c:if>
			</c:forEach>
            </tbody>
            </table>
          </div>
        </div>         
	<script>
		
	</script>
      <!-- 페이지네이션 -->
     <c:if test="${ !empty sessionScope.loginMember }">
      <button class="btnMenu btn1"  onclick="location.href='${ pageContext.servletContext.contextPath }/board/inquiryinsert'">문의하기</button>
 	</c:if> 
      <div class="pagination">
       <jsp:include page="../common/paging.jsp"/>
      </div>
  </div>
	</container>
	
	<jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>
	 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>