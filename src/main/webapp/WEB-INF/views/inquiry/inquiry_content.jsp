<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
       <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
       <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="http://code.jquery.com/jquery-3.6.0.min.js"></script>
  <script src="https://kit.fontawesome.com/da4b1b94bb.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/inq/inqcontent.css">
 <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
 	<jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
	<jsp:include page="/WEB-INF/views/menubar/sidebar.jsp"/>
	<container>
		 <div class="container">
        <div class="row">
            <li class="empty">
                <span data-feather="file"></span>
            </li>
        </div>

        <table class="table" id="maintable">
            <thead>
              <tr> 
                <th class="titleText"scope="col"> <c:out value="${ requestScope.board.title }"/></th>
                <th scope="col" class="btn-group dropstart" id="etcmenu"> 
                <c:if test="${ sessionScope.loginMember.memId eq requestScope.board.memId }">
                  <a class=""  href="#" id="etc"  data-bs-toggle="dropdown" aria-expanded="false">▶</a>
                  <ul class="drop-cont">
                    <li><a class="dropdown-item" onclick="location.href='${ pageContext.servletContext.contextPath }/board/inquiryupdate?inqNo=${ requestScope.board.no }'">수정하기</a></li>
                    <li><a class="dropdown-item" onclick="location.href='${ pageContext.servletContext.contextPath }/board/inquirydelete?inqNo=${ requestScope.board.no }'">삭제하기</a></li>
                    </c:if>
                </th>
              </tr>
            </thead>

            <tbody>
              <tr class="inner">
                <td colspan="6"><br><br><br>
                <c:out value="${ requestScope.board.content }" escapeXml="false"/>
                
                  </td>
              </tr>
            </tbody>
          </table>

          <table class="table" id="maintable2">
            <thead>
              <tr>
                <th class="replyTitle">댓글</th>
                <th class="countreply">댓글 ${fn:length(rep)}</th>
              </tr>
            </thead>

            <tbody class="bottom">
            <!--   <tr>
                <td colspan="3">강형욱<br>
                  <span class="modify" OnClick="location.href='#'" style="cursor:pointer;" ><i class="fa-solid fa-eraser" title="수정"></i></span>
                  <span class="delete" OnClick="deletAlert()" style="cursor:pointer;" ><i class="fa-solid fa-trash-can" title="삭제"></i></span> </td>
                <td colspan="9">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam, suscipit omnis. Cum quas, inventore saepe deleniti expedita ullam impedit, hic deserunt eaque et repellat ratione neque. Iste id expedita nisi?</td>
              </tr> -->
              
             <c:forEach var="repList" items="${ requestScope.rep }">
			<tr>
				<td colspan="3"><c:out value="${ repList.memId }"/><br>
				<c:if test="${ sessionScope.loginMember.memId eq repList.memId }">
                  <span class="delete" OnClick="location.href='${ pageContext.servletContext.contextPath }/board/inquiryrepdelete?inqNo=${ requestScope.board.no }&&repNo=${ repList.repNo }'" style="cursor:pointer;"><i class="fa-solid fa-trash-can" title="삭제"></i></span>
                  </c:if>
                  
                   </td>
				<td colspan="9"><c:out value="${ repList.repCont }"/></td>
			</tr>
			</c:forEach>
            </tbody>
          </table>
		<c:if test="${ !empty sessionScope.loginMember.memSup }">
		<form action="${ pageContext.servletContext.contextPath }/board/inquiryrepwhite">
          <table id="reply">
            <tr>
              <td>
          <div class="mb-3" id="replyarea">
            <textarea class="form-control" name="exampleFormControlTextarea1" id="exampleFormControlTextarea1" rows="3" placeholder="댓글 내용을 입력해주세요"></textarea>
          </div>
             </td>
             <td>
             <input type="hidden" name="inqNo" value="${ requestScope.board.no }"/>
              <br>
            <div class="write">
            <button class="btnMenu btn1" type="submit">댓글 작성</button>
            </div>
          </td>
          </tr>
          </table>
          </form>
   
          <div class="write"> 
            <button class="btnMenu2 btn1" onclick="location.href='${ pageContext.servletContext.contextPath }/AsklistServlet';">관리자 문의사항으로 이동</button>
            </div>
          </c:if>
  </div>

	</container>
	

	<jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>
	
	 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>