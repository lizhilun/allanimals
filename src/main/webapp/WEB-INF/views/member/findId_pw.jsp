<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/findId_pw/findId_pw.css" />
  </head>
  <body>
  <header>
    <a href="${ pageContext.servletContext.contextPath }/main">
   		 <img class="logo" src="${ pageContext.servletContext.contextPath }/resources/upload/images/semiLogo2.PNG" alt=""/>
    </a>
    </header>

    <div class="tab">
      <div class="tabmenu">
        <ul>
          <li id="tab1" class="btnCon">
            <input
              type="radio"
              checked
              name="tabmenu"
              id="tabmenu1"
              class="btncon"
            />
            <label for="tabmenu1" id="tabmenu1" class="tm">아이디 찾기</label>
            <div class="tabCon">
              <form action="${ pageContext.servletContext.contextPath }/member/findid" method="post">
                <div class="input-box">
                  <input
                    id="memPhone"
                    type="text"
                    name="memPhone"
                    class="ld"
                    oninput="autoHypen(this)"
                    placeholder="전화번호"
                    maxlength="13"
                    required
                  />
                  <label for="userid" class="ls">전화번호 입력</label>
                  <div class="input-box">
                  <input
                    id="memEmail"
                    type="text"
                    name="memEmail"
                    class="ld"
                    placeholder="이메일"
                    required
                  />
                  <label for="userid" class="ls">이메일</label>
                  
                </div>
                <button class="findId" type="submit">
     			 입력하기
   				 </button>

              </form>
              
            </div>
          </li>
          <li id="tab2" class="btnCon">
            <input type="radio" name="tabmenu" id="tabmenu2" class="btncon" />
            <label for="tabmenu2" class="tm">비밀번호 재설정</label>
            <div class="tabCon">
              <form action="${ pageContext.servletContext.contextPath }/memsendemail" method="get">
                <div class="input-box">
                  <input
                    id="memId"
                    type="text"
                    name="memId"
                    class="ld"
                    placeholder="아이디"
                    required/>
                  <label for="userid" class="ls">아이디</label>

                </div>
    			
    			<button class="findPw" type="submit">
     			 입력하기
    			</button>
    			
              </form>
              
            </div>
          </li>
        </ul>
      </div>
    </div>
    
				<button class="returntologin" onclick="location.href='${ pageContext.servletContext.contextPath }/member/login';">
     			 돌아가기
   				 </button>
   				 
     
     <script>
//      전화번호 입력시 하이픈 자동 입력 코드
     const autoHypen = (target) => {
    	 target.value = target.value
    	 .replace(/[^0-9]/g, '')
    	 .replace(/^(\d{0,3})(\d{0,4})(\d{0,4})$/g, "$1-$2-$3").replace(/(\-{1,2})$/g, "");
     }
     </script>
     
     <script>
    	// 아이디 유효성 검사
    	let userId = document.querySelector('#memId')
     userId.addEventListener('change',(e)=>{
         validId(e.target)
     })

     function validId(obj){
         console.log(obj)
         if(validIdCheck(obj)==false){
             alert('4~12자의 영문 대소문자와 숫자로 입력해주세요.')
             obj.value='';
             obj.focus();
             return false;
         }
     }

     function validIdCheck(obj){
         let pattern =  /^[a-zA-Z0-9]{4,12}$/;
         return (obj.value.match(pattern)!=null)
     }
   	//휴대폰 번호 유효성 검사
   	let userPhone = document.querySelector('#memPhone')
    userPhone.addEventListener('change',(e)=>{
        validPhone(e.target)
    })

    function validPhone(obj){
        console.log(obj)
        if(validPhoneCheck(obj)==false){
            alert('올바른 번호를 입력하세요.')
            obj.value='';
            obj.focus();
            return false;
        }
    }

    function validPhoneCheck(obj){
        let pattern = /^01([0|1|6|7|8|9]?)-?([0-9]{3,4})-?([0-9]{4})$/;
        return (obj.value.match(pattern)!=null)
    }
   	
    // 이메일 유효성 검사
    let userEmail = document.querySelector('#memEmail')
    userEmail.addEventListener('change',(e)=>{
        validEmail(e.target)
    })

    function validEmail(obj){
        console.log(obj)
        if(validEmailCheck(obj)==false){
            alert('올바른 이메일 주소를 입력해주세요.')
            obj.value='';
            obj.focus();
            return false;
        }
    }

    function validEmailCheck(obj){
        let pattern =  /([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return (obj.value.match(pattern)!=null)
    }
    
   	</script>

  </body>
</html>