<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/login/login.css" />
</head>
<body>
    <header>
     <a href="${ pageContext.servletContext.contextPath }/main"
          ><img class="logo" src="${ pageContext.servletContext.contextPath }/resources/upload/images/semiLogo2.PNG" alt=""
        /></a>
    </header>

    <form action="" method="post">
      <div class="input-box">
        <input id="memId" type="text" name="memId" placeholder="아이디" required/>
        <label for="memId">아이디</label>
      </div>
      <div class="input-box">
        <input
          id="memPwd"
          type="password"
          name="memPwd"
          placeholder="비밀번호"
          required
        />
        <label for="password">비밀번호</label>
      </div>
      
      <div id="forgot">
      <a class="findIdPw" href="${ pageContext.servletContext.contextPath }/member/find">계정을 잊으셨나요?</a>
    </div>
      
      <button
      type="submit"
      class="logIn"
      id="login"
    >
      로그인
    </button>
    </form>
    
    <button class="join" id="regist" onclick="location.href='${ pageContext.servletContext.contextPath }/member/regist';">
      회원가입
    </button>
    
    
   

   
  </body>
  
<script>
   	// 아이디 유효성 검사
   	let userId = document.querySelector('#memId')
    userId.addEventListener('change',(e)=>{
        validId(e.target)
    })

    function validId(obj){
        console.log(obj)
        if(validIdCheck(obj)==false){
            alert('4~12자의 영문 대소문자와 숫자로 입력해주세요.')
            obj.value='';
            obj.focus();
            return false;
        }
    }

    function validIdCheck(obj){
        let pattern =  /^[a-zA-Z0-9]{4,12}$/;
        return (obj.value.match(pattern)!=null)
    }
   	
  //비밀번호 유효성 검사
   	let userPwd = document.querySelector('#memPwd')
    userPwd.addEventListener('change',(e)=>{
        validPwd(e.target)
    })

    function validPwd(obj){
        console.log(obj)
        if(validPwdCheck(obj)==false){
            alert('비밀번호는 8~20자리 숫자/문자/특수문자를 포함해야합니다.')
            obj.value='';
            obj.focus();
            return false;
        }
    }

    function validPwdCheck(obj){
        let pattern = /^(?=.*[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*[0-9]).{8,25}$/;
        return (obj.value.match(pattern)!=null)
    }
   	
   	</script>
   		
</html>