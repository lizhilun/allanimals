<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/findIdResult/findIdResult.css">
</head>
<body>
    <header>
      <h2><img src="${ pageContext.servletContext.contextPath }/resources/upload/images/semiLogo2.PNG" alt="logo" /></h2>
    </header>
    <div class="announce">
      <p>회원님의 이메일을 확인해주세요!</p>
    </div>

    <button class="returnMain" type="submit" onclick="location.href='${ pageContext.servletContext.contextPath }/main';">
      메인화면으로
    </button>
  </body>
</html>