<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/findIdResult/findIdResult.css">
</head>
<body>
    <header>
        <h2><img src="${ pageContext.servletContext.contextPath }/resources/upload/images/semiLogo2.PNG" alt="logo" /></h2>
      </header>
      <div class="announce">
      <p>
        회원님의 아이디는 <br>
        <u>${ requestScope.memfindId.memId }</u>입니다.
      </p>
    </div>

    <button class="returnLogin" onclick="location.href='${ pageContext.servletContext.contextPath }/member/login';">
      로그인하기
    </button>
  </body>
</html>