<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/join/join.css" />
  <script src="http://code.jquery.com/jquery-3.6.0.min.js"></script>
  <script src="//t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
  <body>
    <header>
      <a href="${ pageContext.servletContext.contextPath }/main"
          ><img class="logo" src="${ pageContext.servletContext.contextPath }/resources/upload/images/semiLogo2.PNG" alt=""
        /></a>
    </header>
    <form action="${ pageContext.servletContext.contextPath }/member/regist" id="registForm" method="post">
      <div class="input-box">
        <input id="memId" type="text" name="memId" placeholder="아이디" required/>
        <label for="memId">아이디</label>
        <button type="submit" class="overlapCheck" id="overlapCheckId" >
      아이디 중복체크
    </button>
      </div>
      
      
      <div class="input-box">
        <input
          id="memPwd"
          type="password"
          name="memPwd"
          placeholder="비밀번호"
          required
        />
        <label for="password">비밀번호</label>
      </div>

      <div class="input-box">
        <input id="memName" type="text" name="memName" placeholder="이름" required/>
        <label for="username">이름</label>
      </div>
      <div class="input-box">
        <input
          id="memPhone"
          type="text"
          name="memPhone"
          placeholder="전화번호"
          oninput="autoHypen(this)"
          maxlength="13"
          required
        />
        <label for="memPhone">전화번호</label>
      </div>
      <div class="input-box">
        <input id="memEmail" type="text" name="memEmail" placeholder="이메일" required/>
        <label for="memEmail">EMAIL</label>
      </div>
      <div class="input-box">
        <input id="memBirth" type="text" name="memBirth" placeholder="생년월일" oninput="autoHypen2(this)" maxlength="10" required />
        <label for="memBirth">생년월일 8자리 입력</label>
      </div>
      <div class="input-box">
        <input type="text" id="sample6_postcode" placeholder="우편번호" required/>
        <label for="">우편번호</label>
        <button class="postcode" onclick="sample6_execDaumPostcode()" value="우편번호 찾기">우편번호 찾기</button>
      </div>
      <div class="input-box">
        <input type="text" id="memAddre" name="memAddre" placeholder="주소">
        <label for="memAddre">주소</label>
      </div>
      <div class="input-box">
        <input type="text" id="sample6_detailAddress" name="memAddre2" placeholder="상세주소" required>
        <label for="sample6_detailAddress">상세주소</label>
     </div>
     <div class="input-box">
        <input type="text" id="sample6_extraAddress" placeholder="참고항목">
        <label for="sample6_extraAddress">참고항목</label>
      </div>
      
      
    
    <button type="submit" class="join">가입하기</button>
    
    </form>
    
    <button class="logIn" onclick="location.href='${ pageContext.servletContext.contextPath }/member/login';">
      로그인 화면으로
    </button>
    
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    
    <script>
//     아이디 중복체크 기능
    	$("#overlapCheckId").click(function(){
    		
    		const memId = $("#memId").val();
    		
    		$.ajax({
    			url: "${ pageContext.servletContext.contextPath }/member/overlapCheck",
    			type: "get",
    			data: {
    				memId: memId
    			},
    			success: function(data){
    				
    				console.log(data);
    				if(data == "true"){
    					alert("사용가능한 아이디입니다.");
    					
    				} else {
    					alert("이미 사용중인 아이디입니다.");
    					return false;
    				}
    			},
    			error: function(error){
    				console.log(error);
    			}
    		});
    	});
    </script>
    
    <script>
//     전화번호 입력시 하이픈을 넣는 코드
    const autoHypen = (target) => {
    	 target.value = target.value
    	   .replace(/[^0-9]/g, '')
    	  .replace(/^(\d{0,3})(\d{0,4})(\d{0,4})$/g, "$1-$2-$3").replace(/(\-{1,2})$/g, "");
    	}
    
//  생년월일 입력시 하이픈을 넣는 코드
    const autoHypen2 = (target) => {
    	 target.value = target.value
    	   .replace(/[^0-9]/g, '')
    	  .replace(/^(\d{0,4})(\d{0,2})(\d{0,2})$/g, "$1-$2-$3").replace(/(\-{1,2})$/g, "");
    	}
    </script>
    
   	<script>
   	// 아이디 유효성 검사
   	let userId = document.querySelector('#memId')
    userId.addEventListener('change',(e)=>{
        validId(e.target)
    })

    function validId(obj){
        console.log(obj)
        if(validIdCheck(obj)==false){
            alert('4~12자의 영문 대소문자와 숫자로 입력해주세요.')
            obj.value='';
            obj.focus();
            return false;
        }
    }

    function validIdCheck(obj){
        let pattern =  /^[a-zA-Z0-9]{4,12}$/;
        return (obj.value.match(pattern)!=null)
    }
   	
   	//비밀번호 유효성 검사
   	let userPwd = document.querySelector('#memPwd')
    userPwd.addEventListener('change',(e)=>{
        validPwd(e.target)
    })

    function validPwd(obj){
        console.log(obj)
        if(validPwdCheck(obj)==false){
            alert('비밀번호는 8~20자리 숫자/문자/특수문자를 포함해야합니다.')
            obj.value='';
            obj.focus();
            return false;
        }
    }

    function validPwdCheck(obj){
        let pattern = /^(?=.*[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*[0-9]).{8,25}$/;
        return (obj.value.match(pattern)!=null)
    }
    
	//이름 유효성 검사
   	let userName = document.querySelector('#memName')
    userName.addEventListener('change',(e)=>{
        validName(e.target)
    })

    function validName(obj){
        console.log(obj)
        if(validNameCheck(obj)==false){
            alert('한글 2글자이상 5글자 이하/영문 4글자 이상 입력하세요.')
            obj.value='';
            obj.focus();
            return false;
        }
    }

    function validNameCheck(obj){
        let pattern = /^[가-힣]{2,5}$|[a-zA-Z]{4,}$/;
        return (obj.value.match(pattern)!=null)
    }
   	
  //휴대폰 번호 유효성 검사
   	let userPhone = document.querySelector('#memPhone')
    userPhone.addEventListener('change',(e)=>{
        validPhone(e.target)
    })

    function validPhone(obj){
        console.log(obj)
        if(validPhoneCheck(obj)==false){
            alert('올바른 번호를 입력하세요.')
            obj.value='';
            obj.focus();
            return false;
        }
    }

    function validPhoneCheck(obj){
        let pattern = /^01([0|1|6|7|8|9]?)-?([0-9]{3,4})-?([0-9]{4})$/;
        return (obj.value.match(pattern)!=null)
    }
   	
 // 이메일 유효성 검사
    let userEmail = document.querySelector('#memEmail')
    userEmail.addEventListener('change',(e)=>{
        validEmail(e.target)
    })

    function validEmail(obj){
        console.log(obj)
        if(validEmailCheck(obj)==false){
            alert('올바른 이메일 주소를 입력해주세요.')
            obj.value='';
            obj.focus();
            return false;
        }
    }

    function validEmailCheck(obj){
        let pattern =  /([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return (obj.value.match(pattern)!=null)
    }
    
 // 생년월일 8자리 유효성 검사
    let userBirth = document.querySelector('#memBirth')
    userBirth.addEventListener('change',(e)=>{
        validBirth(e.target)
    })

    function validBirth(obj){
        console.log(obj)
        if(validBirthCheck(obj)==false){
            alert('올바른 생년월일을 입력해주세요.')
            obj.value='';
            obj.focus();
            return false;
        }
    }

    function validBirthCheck(obj){
        let pattern =  /^(19[0-9][0-9]|20\d{2})-(0[0-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/
        return (obj.value.match(pattern)!=null)
    }
    
   	</script>
   	
    <script>
    // 우편번호 검색 API 코드
    function sample6_execDaumPostcode() {
        new daum.Postcode({
        	
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 각 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var addr = ''; // 주소 변수
                var extraAddr = ''; // 참고항목 변수

                //사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
                if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                    addr = data.roadAddress;
                } else { // 사용자가 지번 주소를 선택했을 경우(J)
                    addr = data.jibunAddress;
                }

                // 사용자가 선택한 주소가 도로명 타입일때 참고항목을 조합한다.
                if(data.userSelectedType === 'R'){
                    // 법정동명이 있을 경우 추가한다. (법정리는 제외)
                    // 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
                    if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                        extraAddr += data.bname;
                    }
                    // 건물명이 있고, 공동주택일 경우 추가한다.
                    if(data.buildingName !== '' && data.apartment === 'Y'){
                        extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                    }
                    // 표시할 참고항목이 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
                    if(extraAddr !== ''){
                        extraAddr = ' (' + extraAddr + ')';
                    }
                    // 조합된 참고항목을 해당 필드에 넣는다.
                    document.getElementById("sample6_extraAddress").value = extraAddr;
                
                } else {
                    document.getElementById("sample6_extraAddress").value = '';
                }

                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                document.getElementById('sample6_postcode').value = data.zonecode;
                document.getElementById("memAddre").value = addr;
                // 커서를 상세주소 필드로 이동한다.
                document.getElementById("sample6_detailAddress").focus();
            },
            
            theme: {
            	   bgColor: "#DAE5D0", //바탕 배경색
            	   //searchBgColor: "", //검색창 배경색
            	   //contentBgColor: "", //본문 배경색(검색결과,결과없음,첫화면,검색서제스트)
            	   //pageBgColor: "", //페이지 배경색
            	   textColor: "#333333", //기본 글자색
  				   queryTextColor: "#333333", //검색창 글자색
            	   postcodeTextColor: "#FD8585", //우편번호 글자색
            	   emphTextColor: "#C01160", //강조 글자색
            	   outlineColor: "#8AA1A1" //테두리
            }
        }).open();
    }
    
	
</script>

<script>
	
</script>

    
</body>
</html>