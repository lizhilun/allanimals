<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/complete_join/complete_join.css">
</head>
<body>
    <header>
        <h2><img src="${ pageContext.servletContext.contextPath }/resources/upload/images/semiLogo2.PNG" alt="logo" /></h2>
      </header>
      <div class="announce">
        <p>회원가입이 완료되었습니다!<br></p>
      </div>

      <button class="returnMain" onclick="location.href='${ pageContext.servletContext.contextPath }/main'">
        메인화면으로
      </button>

</body>
</html>