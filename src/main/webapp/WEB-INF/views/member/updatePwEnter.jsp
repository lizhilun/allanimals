<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/join/join.css" />
  <body>
    <header>
      <a href="${ pageContext.servletContext.contextPath }/main"
          ><img class="logo" src="${ pageContext.servletContext.contextPath }/resources/upload/images/semiLogo2.PNG" alt=""/>
      </a>
    </header>
    <form action="${ pageContext.servletContext.contextPath }/member/updatepwd" method="post">
      <div class="input-box">
        <input id="memId" type="hidden" name="memId" placeholder="아이디" required value="${ requestScope.memId }"/>
       </div>
       
		<div class="input-box">
          <input
            id="memPwd"
            type="password"
            name="memPwd"
            class="ld"
            placeholder="새 비밀번호"
            required/>
        	<label for="newMemPwd" >새 비밀번호</label>
         </div>
         
         <div class="input-box">
            <input
              id="memPwdChk"
              type="password"
              name="memPwdChk"
              class="ld"
              placeholder="새 비밀번호 확인"
              required/>
            <label for="newMemPwdCheck" >비밀번호 확인</label>
         </div>
         
    	<button class="findPw" type="submit" onclick="return checkPwd();">
     			 입력하기
    	</button>
    			
	</form>
	
	 <script>
//    	새 비밀번호, 비밀번호 확인 일치여부 확인용 코드
      function checkPwd(){
        
        let memPwd = document.getElementById('memPwd').value;
        let memPwdChk = document.getElementById('memPwdChk').value;
        
        if(memPwd != memPwdChk){
          alert("비밀번호 불일치");
          return false;

        } else {
          alert("비밀번호가 일치합니다");
          return true;
        }
      }
     </script>
     
    <script>
   	// 아이디 유효성 검사
   	let userId = document.querySelector('#memId')
    userId.addEventListener('change',(e)=>{
        validId(e.target)
    })

    function validId(obj){
        console.log(obj)
        if(validIdCheck(obj)==false){
            alert('4~12자의 영문 대소문자와 숫자로 입력해주세요.')
            obj.value='';
            obj.focus();
            return false;
        }
    }

    function validIdCheck(obj){
        let pattern =  /^[a-zA-Z0-9]{4,12}$/;
        return (obj.value.match(pattern)!=null)
    }
   	
   	//비밀번호 유효성 검사
   	let userPwd = document.querySelector('#memPwd')
    userPwd.addEventListener('change',(e)=>{
        validPwd(e.target)
    })

    function validPwd(obj){
        console.log(obj)
        if(validPwdCheck(obj)==false){
            alert('비밀번호는 8~20자리 숫자/문자/특수문자를 포함해야합니다.')
            obj.value='';
            obj.focus();
            return false;
        }
    }

    function validPwdCheck(obj){
        let pattern = /^(?=.*[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*[0-9]).{8,25}$/;
        return (obj.value.match(pattern)!=null)
    }
   	
   	</script>
   	
</body>
</html>