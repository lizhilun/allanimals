<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
       <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
 <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/reviewWrite.css">
 
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
 	<jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
	<jsp:include page="/WEB-INF/views/menubar/sidebar.jsp"/>
	
	<container>
	<div class="container">
	<form id="inquiryInsert" action="${ pageContext.servletContext.contextPath }/board/noticeupdate" method="post">
    <div id="table">
      <div id="pName"><h1>공지사항</h1></div>
      <input name="no" value="${ requestScope.board.no }" type="hidden">

      <div class="titlearea">
      <input type="text" name="title" class="title" placeholder="제목을 입력하세요." value="${ requestScope.board.title }"> 
      </div>

      <textarea id="summernote" name="content" placeholder="문의사항을 입력해주세요.">
      <c:out value="${ requestScope.board.content }" escapeXml="false"/>
      </textarea>
      <div class="write">
        <button class="btn btn1" type="submit">작성하기</button>
        </div>
    </div>
    </form>
    <script>
      $(document).ready(function() {
    	  $('#summernote').summernote({
  
              height:600 , 
            minHeight: 600,             // set minimum height of editor
              maxHeight: 600,             // set maximum height of editor
               focus: true   ,             
              toolbar: [
                  
  
            // [groupName, [list of button]]
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['style', ['bold', 'italic', 'underline','strikethrough', 'clear']],
            ['color', ['forecolor','color']],
            ['table', ['table']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['view', ['fullscreen', 'help']]
          ],
        fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New','맑은 고딕','궁서','굴림체','굴림','돋움체','바탕체'],
        fontSizes: ['8','9','10','11','12','14','16','18','20','22','24','28','30','36','50','72']
              

          });
  
      });
     
    </script>
	
	
	</container>
	<jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>
		 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>