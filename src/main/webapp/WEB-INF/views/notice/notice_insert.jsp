<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
       <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
 <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/reviewWrite.css">
 <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

</head>
<body>
 	<jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
	<jsp:include page="/WEB-INF/views/menubar/sidebar.jsp"/>
	
	<container>
	<div class="container">
	<form id="inquiryInsert" action="${ pageContext.servletContext.contextPath }/board/noticeinsert" encType="multipart/form-data" method="post" >
    <div id="table">
      <div id="pName"><h1>공지사항</h1></div>

      <div class="titlearea">
      <input type="text" name="title" class="title" placeholder="제목을 입력하세요."> 
      </div>
		 <textarea id="board_content" name="content"class="content"></textarea> 
<!-- 	<input id="summernote" name="content" class="content"> -->
 		<br>
      	<div class="mb-3">
  		<input class="form-control" name="notefiles" type="file" id="formFileMultiple" multiple>
		</div>
      	
      <div class="write">
        <button class="btn btn1" type="submit">작성하기</button>
        </div>
    </div>
    </form>
 </div>
 </container>
    <script>
    
      $(document).ready(function() {
    	  $('#board_content').val("${board_data.BOARD_CONTENT}");
    	  $('#board_content').summernote({
  
              height:600 , 
            minHeight: 600,             // set minimum height of editor
              maxHeight: 600,             // set maximum height of editor
               focus: true   ,             
              toolbar: [
            
            // [groupName, [list of button]]
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['style', ['bold', 'italic', 'underline','strikethrough', 'clear']],
            ['color', ['forecolor','color']],
            ['table', ['table']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['view', ['fullscreen', 'help']]
          ],
          
        fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New','맑은 고딕','궁서','굴림체','굴림','돋움체','바탕체'],
        fontSizes: ['8','9','10','11','12','14','16','18','20','22','24','28','30','36','50','72'],
        lang: 'ko-KR',
        callbacks: {
            onImageUpload: function(files, editor, welEditable) {
               for(var i = files.length -1; i>=0; i--) {
                  sendFile(files[i], this);
               }
            }
         },
         


          });
  
      });
      
  
    </script>
	
	
	</container>
	<jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>
		 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>