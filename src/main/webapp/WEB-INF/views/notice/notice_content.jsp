<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
       <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
       <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="http://code.jquery.com/jquery-3.6.0.min.js"></script>
  <script src="https://kit.fontawesome.com/da4b1b94bb.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/inq/inqcontent.css">
 <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
 	<jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
	<jsp:include page="/WEB-INF/views/menubar/sidebar.jsp"/>
	<container>
		 <div class="container">
        <div class="row">
            <li class="empty">
                <span data-feather="file"></span>
            </li>
        </div>

        <table class="table" id="maintable">
            <thead>
              <tr> 
                <th class="titleText"scope="col"><c:out value="${ requestScope.board.title }" escapeXml="false"/></th>
                <th scope="col" class="btn-group dropstart" id="etcmenu"> 
                <c:if test="${ !empty sessionScope.loginMember.memSup }">
                  <a class=""  href="#" id="etc"  data-bs-toggle="dropdown" aria-expanded="false">▶</a>
                  <ul class="drop-cont">
                    <li><a class="dropdown-item" onclick="location.href='${ pageContext.servletContext.contextPath }/board/noticeupdate?notNo=${ requestScope.board.no }'">수정하기</a></li>
                    <li><a class="dropdown-item" onclick="location.href='${ pageContext.servletContext.contextPath }/board/noticedelete?notNo=${ requestScope.board.no }'">삭제하기</a></li>
                    </c:if>
                </th>
              </tr>
            </thead>

            <tbody>
              <tr class="inner">
                <td colspan="6">
                <br><br><br>
                <c:out value="${ requestScope.board.content }"  escapeXml="false"/>
                
                
                  </td>
                  
              </tr>
             
            </tbody>
          </table>
			<table id="replydown">
            <tr>
              <td>
        		 <c:forEach var="attach" items="${ requestScope.attachList }">
                 	<a href="${ pageContext.servletContext.contextPath }/resources/upload/notice/${ attach.savedName }" download><c:out value="${ attach.originalName }"/></a><br>
				</c:forEach> 
				<hr>
          	</td>
          </tr>
          </table>
  </div>
  
	</container>


	<jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>
	
	 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>