<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/sidebar.css" />
  </head>
  <body>
    <input type="checkbox" id="menuicon" checked />
    <!--checked 속성은 버튼을 누르지 않고도 사이드바가 나온 상태로 화면에 띄움-->
    <label for="menuicon">
      <span></span>
      <span></span>
      <span></span>
      <!-- span 이 있어야 버튼이 나옵니다-->
    </label>
    <div class="sidebar">
      <!--sidebar menu-->
      <a href="${ pageContext.servletContext.contextPath }/main"
        ><img
          src="${ pageContext.servletContext.contextPath }/resources/upload/images/KakaoTalk_20220713_121409010.png"
          alt="logo"
        />
      </a>

      <div class="accordianMenu">
        <button class="accordion">전체 회원관리</button>
        <div class="panel">
          <li><a href="${ pageContext.servletContext.contextPath}/MemberViewServlet" class="accmenu">전체회원조회</a></li>
          <li><a href="${ pageContext.servletContext.contextPath}/MemberDeleteServlet" class="accmenu">회원정보삭제</a></li>
        </div>
        <button class="accordion">문의사항 관리</button>
        <div class="panel">
          <li><a href="${ pageContext.servletContext.contextPath}/AsklistServlet" class="accmenu">문의사항 답변</a></li> 
        </div>
        
        <button class="accordion">구조요청 가능업체 관리</button>
        <div class="panel">
          <li><a href="${ pageContext.servletContext.contextPath}/HelpRegisterServlet" class="accmenu">업체 등록</a></li>
          <li><a href="${ pageContext.servletContext.contextPath}/HelpEditServlet" class="accmenu">업체 수정</a></li>
          <li><a href="${ pageContext.servletContext.contextPath}/HelpDeleteServlet" class="accmenu">업체 삭제</a></li>
        </div>    
        <button class="accordion">동물병원 리스트 관리</button>
        <div class="panel">
          <li><a href="${ pageContext.servletContext.contextPath}/HospitalRegisterServlet" class="accmenu">동물병원 리스트 입력</a></li>
          <li><a href="${ pageContext.servletContext.contextPath}/HospitalEditServlet" class="accmenu">동물병원 리스트 수정</a></li>
          <li><a href="${ pageContext.servletContext.contextPath}/HospitalDeleteServlet" class="accmenu">동물병원 리스트 삭제</a></li>
          
        </div>
      </div>

      <!-- accordion menu javascript -->
      <script>
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
          acc[i].addEventListener("click", function () {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
              panel.style.maxHeight = null;
            } else {
              panel.style.maxHeight = panel.scrollHeight + "px";
            }
          });
        }
      </script>
      <!-- accordion menu javascript end -->
    </div>
    <!--sidebar menu end-->
  </body>
</html>
