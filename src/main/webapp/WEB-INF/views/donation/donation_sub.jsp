<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
       <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>Insert title here</title>
 <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/donation/donation.css">
<script src="http://code.jquery.com/jquery-3.6.0.min.js"></script>
 <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
 	<jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
	<jsp:include page="/WEB-INF/views/menubar/sidebar.jsp"/>
	<container>
	 <div class="container">
        <div class="page-title">
            <h3>정기후원</h3>
            </div>

            <div id="maintable">
                <br> <br> <br>
                <h3>정기후원은 사지 않고 입양하는 AA의 활동을 </h3>
                <h3>지원할 수 있습니다.</h3>
                 <h2>&nbsp</h2>   
        
                <h3 id="text1">후원 신청시 정기적인 금액 후원을 통해 건전한 입양 문화를</h3>
                <h3 id="text1">만들어가기 위한 AA의 활동을 지지하고 동참할 수 있습니다.</h3>
                <h3 id="text1">후원금은 더 많은 유기동물들이 가족을 만날 수 있도록</h3>
                <h3 id="text1">돕기 위해 사용됩니다.</h2>
                <br> <br>
                <h3 id="text2">후원문의 : AllAnimals@gmail.com</h3>
            </div>
    

    </div>
	</container>
	<jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>
	 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>