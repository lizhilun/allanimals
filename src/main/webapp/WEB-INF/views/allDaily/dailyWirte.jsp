<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/allDaily/template.css">
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/header.css">
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/sidebar.css">
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/footer.css">

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
    <style>
      * {
        margin: 0 auto;
      } 

      .container{
        height: 1200px;
      }
      #pName {
        margin-top: -100px;
        height: 120px;
      }

      .nameBox{
        width: 800px;
        height: 50px;
        margin-bottom: 30px;
        padding: 0 0 0 15px;
        font-size: 21px;
      }

      #table{  position:absolute;
      left: 30%;
      top:300px;
      width: 800px; height: 630px;}

      #btn {
        float: right;
        margin-top: 40px;
      }

      #btn button {
        background-color: white; 
        color: black; 
        border: 2px solid #DAE5D0;
        text-decoration: none;
        color: black;
        width: 100px;
        height: 40px;
        font-size: 16px;
        margin: 4px 2px;
        text-decoration: none;
        cursor: pointer;
        padding: 0px;
        text-align: center;
      }

      #btn button:hover {
        background-color: #DAE5D0;
        color: white;
      }

      input[type=text] {
        margin-left: 0px !important;
        margin-top:  0px !important;
        width: 537px !important;
      }

      .note-btn-primary {
        margin-right: 10px !important;
        margin-top: -25px !important;
      }

      /* 검색창 */
      input[type=text] {
        width: auto;
        box-sizing: border-box;
        border: 1px solid #000;
        border-radius: 4px;
        font-size: 16px;
        color: #000;
        background-color: white;
        /* background-image: url('searchicon.png'); */
        /* background-position: 10px 10px;  */
        background-repeat: no-repeat;
        padding: 8px 20px 8px 40px;
        -webkit-transition: width 0.4s ease-in-out;
        transition: width 0.4s ease-in-out;
        margin-left: -80px;
        margin-bottom: 80px;
        margin-top: 50px;
      }
      
      input[type=text]:focus {
        width: 200px;
      }

      label {
        margin-right: 10px;
      }
      
    </style>

</head>
<body>
<jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
<jsp:include page="/WEB-INF/views/menubar/sidebar.jsp"/>

    <div class="container">

      <div id="table">
        <div id="pName"><h1>동물들의 일상 글작성</h1></div>
        <hr><br>
		<form name="addIntro" id="addIntro" action="${ pageContext.servletContext.contextPath }/daily/write" method="post" enctype="multipart/form-data">
	        <p style="font-size: 24px; margin-bottom: 15px;">동물을 선택해주세요</p>
	        <select name="list" style="margin-bottom: 30px">
	        	<c:forEach items="${ requestScope.introList }" var="intro">
	        	<option value="">이름2</option>
	        	</c:forEach>
	        </select>
<!-- 	        <div style="margin: 15px 0 15px 0;"> -->
<!-- 	            <label for="pet1"><input style="margin-right: 1px;" type="radio" id="pet1" name="aniSpec" value="D" required checked="checked">강아지</label> -->
<!-- 	            <label for="pet2"><input style="margin-right: 1px; margin-left: 16px;" type="radio" id="pet2" name="aniSpec" value="C" required>고양이</label> -->
<!-- 	        </div> -->
	        <br><hr><br>
  
        <div>

        <input type="email" class="nameBox" name="dailyTitle" placeholder="글제목을 입력하세요."> 
        </div>
        <textarea id="summernote" name="dailyCont" >
      
        </textarea>
        <br>
        <div style="margin-left:50px; width: 800px; float: left;">
        	<input style="width: 250px" type='file' id="introFile1" name='introFile1' onchange="loadImg(this,1)"/>
        	<input style="width: 250px" type='file' id="introFile2" name='introFile2' onchange="loadImg(this,2)"/>
        	<input style="width: 250px" type='file' id="introFile3" name='introFile3' onchange="loadImg(this,3)"/>
        </div>
		</form>

        <div id="btn">
          <button onclick="insert()">작성하기</button>
          <script type="text/javascript">
              function insert() {
            	  var cont = $("#introCont").text();
            	  $("#Cont").val(cont);
            	  
            	  $("#addIntro").submit();
              }
          </script>
        </div>
      </div>
      <script>
        $(document).ready(function() {
            $('#summernote').summernote({
    
                height:600 , 
              minHeight: 600,             // set minimum height of editor
                maxHeight: 600,             // set maximum height of editor
                 focus: true   ,             
                toolbar: [
                    
    
              // [groupName, [list of button]]
              ['fontname', ['fontname']],
              ['fontsize', ['fontsize']],
              ['style', ['bold', 'italic', 'underline','strikethrough', 'clear']],
              ['color', ['forecolor','color']],
              ['table', ['table']],
              ['para', ['ul', 'ol', 'paragraph']],
              ['height', ['height']],
              ['insert',['picture']],
              ['view', ['fullscreen', 'help']]
            ],
          fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New','맑은 고딕','궁서','굴림체','굴림','돋움체','바탕체'],
          fontSizes: ['8','9','10','11','12','14','16','18','20','22','24','28','30','36','50','72']
                
            });
            
    
        });
       
      </script>
      
    </div>

<jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>


</body>
</html>