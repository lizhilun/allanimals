<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/allDaily/template.css">
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/header.css">
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/sidebar.css">
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/footer.css">
	
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
    <style>
      * {
        margin: 0 auto;
      } 
      
      .container{
        height: 1600px;
      }
      #pName {
        margin-top: -100px;
        height: 120px;
      }

      .nameBox{
        width: 800px;
        height: 50px;
        margin-bottom: 30px;
        padding: 0 0 0 15px;
        font-size: 21px;
      }
      
      .petName{
        width: 300px;
        height: 50px;
        margin-bottom: 30px;
        padding: 0 0 0 15px;
        font-size: 21px;
      }
      

      #table{  position:absolute;
      left: 30%;
      top:300px;
      width: 800px; height: 630px;}

      #btn {
        float: right;
        margin-top: 40px;
      }

      #btn button {
        background-color: white; 
        color: black; 
        border: 2px solid #DAE5D0;
        text-decoration: none;
        color: black;
        width: 100px;
        height: 40px;
        font-size: 16px;
        margin: 4px 2px;
        text-decoration: none;
        cursor: pointer;
        padding: 0px;
        text-align: center;
      }

      #btn button:hover {
        background-color: #DAE5D0;
        color: white;
      }

      input[type=text] {
        margin-left: 0px !important;
        margin-top:  0px !important;
        width: 537px !important;
      }

      .note-btn-primary {
        margin-right: 10px !important;
        margin-top: -25px !important;
      }

      /* 검색창 */
      input[type=text] {
        width: auto;
        box-sizing: border-box;
        border: 1px solid #000;
        border-radius: 4px;
        font-size: 16px;
        color: #000;
        background-color: white;
        /* background-image: url('searchicon.png'); */
        /* background-position: 10px 10px;  */
        background-repeat: no-repeat;
        padding: 8px 20px 8px 40px;
        -webkit-transition: width 0.4s ease-in-out;
        transition: width 0.4s ease-in-out;
        margin-left: -80px;
        margin-bottom: 80px;
        margin-top: 50px;
      }
      
      input[type=text]:focus {
        width: 200px;
      }
    </style>


</head>
<body>
<jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
<jsp:include page="/WEB-INF/views/menubar/sidebar.jsp"/>

    <div class="container">

      <div id="table">
        <div id="pName"><h1>유기동물 소개글 작성</h1></div>
		<form name="addIntro" id="addIntro" action="${ pageContext.servletContext.contextPath }/update/list" method="post">
	        <p style="font-size: 24px; margin-bottom: 15px;">동물 이름을 작성해주세요.</p>
	        <div>
	          <input type="email" class="petName" name="aniName" style="width: 400px; height: 50px;" placeholder="여기에 작성해주세요." value="${ requestScope.thumbnail.aniName }">
	        </div>
	        <hr><br>
	        
	        <p style="font-size: 24px; margin-bottom: 15px;" >나이를 입력해주세요 </p>
        	<input type="text" class="" name="introAge" style="width: 80px; height: 40px; margin-bottom: 20px;"  placeholder="숫자만 입력해주세요" value="${ requestScope.thumbnail.introAge }">
        	<hr><br>
	
	        <p style="font-size: 24px; margin-bottom: 15px;">동물을 선택해주세요</p>
	        <div style="margin: 15px 0 15px 0;">
	        <c:if test="${ requestScope.thumbnail.aniSpec eq 'D'}">
	            <label for="pet1"><input style="margin-right: 1px;" type="radio" id="pet1" name="aniSpec" value="D" required checked="checked">강아지</label>
	            <label for="pet2"><input style="margin-right: 1px; margin-left: 16px;" type="radio" id="pet2" name="aniSpec" value="C" required>고양이</label>
	            </c:if>
	        <c:if test="${ requestScope.thumbnail.aniSpec eq 'C'}">
	            <label for="pet1"><input style="margin-right: 1px;" type="radio" id="pet1" name="aniSpec" value="D" required>강아지</label>
	            <label for="pet2"><input style="margin-right: 1px; margin-left: 16px;" type="radio" id="pet2" name="aniSpec" value="C" required checked="checked">고양이</label>
	            </c:if>
	        </div>
	        <hr><br>
	        <p style="font-size: 24px; margin-bottom: 15px;">성별을 체크해주세요.</p>
	        <div style="margin: 15px 0 15px 0;">
	         <c:if test="${ requestScope.thumbnail.introGen eq 'M'}">
	            <label for="gender1"><input style="margin-right: 1px;" type="radio" id="gender1" name="introGen" value="M" required checked="checked">남자</label>
	            <label for="gender2"><input style="margin-right: 1px; margin-left: 16px;" type="radio" id="gender2" name="introGen" value="F" required>여자</label>
	            </c:if>
	         <c:if test="${ requestScope.thumbnail.introGen eq 'F'}">
	            <label for="gender1"><input style="margin-right: 1px;" type="radio" id="gender1" name="introGen" value="M" required >남자</label>
	            <label for="gender2"><input style="margin-right: 1px; margin-left: 16px;" type="radio" id="gender2" name="introGen" value="F" required checked="checked">여자</label>
	            </c:if>
	        </div>
	        <hr><br>
	        <p style="font-size: 24px; margin-bottom: 15px;">예방접종 유무를 선택해주세요.</p>
	        <div style="margin: 15px 0 25px 0;">
	         <c:if test="${ requestScope.thumbnail.introVac eq 'Y'}">
	          <label for="infor2"><input style="margin-right: 1px; margin-left: 16px;" type="radio" id="infor2" name="introVac" value="Y" required checked="checked">맞음</label>
	          <label for="infor3"><input style="margin-right: 1px; margin-left: 16px;" type="radio" id="infor3" name="introVac" value="N" required>맞지않음</label>
	          </c:if>
	         <c:if test="${ requestScope.thumbnail.introVac eq 'N'}">
	          <label for="infor2"><input style="margin-right: 1px; margin-left: 16px;" type="radio" id="infor2" name="introVac" value="Y" required >맞음</label>
	          <label for="infor3"><input style="margin-right: 1px; margin-left: 16px;" type="radio" id="infor3" name="introVac" value="N" required checked="checked">맞지않음</label>
	          </c:if>
	        </div>
	        <hr><br>
	        <p style="font-size: 24px; margin-bottom: 15px;">입양 유무를 선택해주세요.</p>
	        <div style="margin: 15px 0 25px 0;">
	         <c:if test="${ requestScope.thumbnail.aniAdopt eq 'N'}">
	          <label for="dopt1"><input style="margin-right: 1px; margin-left: 16px;" type="radio" id="dopt1" name="aniAdopt" value="N" required checked="checked">입양희망</label>
	          <label for="dopt2"><input style="margin-right: 1px; margin-left: 16px;" type="radio" id="dopt2" name="aniAdopt" value="Y" required>입양완료</label>
	          </c:if>
	         <c:if test="${ requestScope.thumbnail.aniAdopt eq 'Y'}">
	          <label for="dopt1"><input style="margin-right: 1px; margin-left: 16px;" type="radio" id="dopt1" name="aniAdopt" value="N" required >입양희망</label>
	          <label for="dopt2"><input style="margin-right: 1px; margin-left: 16px;" type="radio" id="dopt2" name="aniAdopt" value="Y" required checked="checked">입양완료</label>
	          </c:if>
	        </div>
  
        <div>

        <input type="email" class="nameBox" name="introTitle" placeholder="글제목을 입력하세요." value="${ requestScope.thumbnail.introTitle }" > 
        </div>
        <textarea id="summernote" name="introCont">
      	<c:out value="${ requestScope.thumbnail.introCont }" escapeXml= "false"/>
        </textarea>
		<input type="hidden" name="introNum" value="${ requestScope.thumbnail.introNum }">
		<input type="hidden" name="aniNum" value="${ requestScope.thumbnail.aniNum }">
		</form>

        <div id="btn">
          <button onclick="insert()">작성하기</button>
          <script type="text/javascript">
              function insert() {
            	  var cont = $("#introCont").text();
            	  $("#Cont").val(cont);
            	  
            	  $("#addIntro").submit();
              }
          </script>
        </div>
      </div>
      <script>
        $(document).ready(function() {
            $('#summernote').summernote({
    
                height:600 , 
              minHeight: 600,             // set minimum height of editor
                maxHeight: 600,             // set maximum height of editor
                 focus: true   ,             
                toolbar: [
                    
    
              // [groupName, [list of button]]
              ['fontname', ['fontname']],
              ['fontsize', ['fontsize']],
              ['style', ['bold', 'italic', 'underline','strikethrough', 'clear']],
              ['color', ['forecolor','color']],
              ['table', ['table']],
              ['para', ['ul', 'ol', 'paragraph']],
              ['height', ['height']],
              ['insert',['picture']],
              ['view', ['fullscreen', 'help']]
            ],
          fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New','맑은 고딕','궁서','굴림체','굴림','돋움체','바탕체'],
          fontSizes: ['8','9','10','11','12','14','16','18','20','22','24','28','30','36','50','72']
                
            });
            
    
        });
       
      </script>
      <script>
		function loadImg(value, num) {
			if (value.files && value.files[0]) {
				const reader = new FileReader();
				reader.onload = function(e) {
					switch(num){
					case 1:
						document.getElementById("introFile1").src = e.target.result;
						break;
					case 2:
						document.getElementById("introFile2").src = e.target.result;
						break;
					case 3:
						document.getElementById("introFile3").src = e.target.result;
						break;
					}
				}
				reader.readAsDataURL(value.files[0]);
			}
		}
		
	  </script>
      
      
    </div>

<jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>


</body>
</html>