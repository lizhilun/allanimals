<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%-- 	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/allDaily/template.css"> --%>
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/allDaily/reviewList.css">
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/header.css">
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/sidebar.css">
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/footer.css">
<%-- 	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/button.css"> --%>
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/button.css" />

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<script src="https://kit.fontawesome.com/da4b1b94bb.js" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
<style>
    a { text-decoration : none; color: black; }
    a:hover { text-decoration : none; color: black; }
    
    .container button {
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
}

.container button {
  margin: 0;
  margin-left: 1%;
  margin-top: 5%;

  padding: 0.4rem 1.9rem;

  font-family: "Noto Sans KR", sans-serif;
  font-size: 1.2rem;
  font-weight: 400;
  text-align: center;
  text-decoration: none;

  display: inline-block;
  width: auto;

  border: none;
  border-radius: 6px;
}

.container button {
  box-shadow: 0 4px 6px -1px rgba(0, 0, 0, 0.1),
    0 2px 4px -1px rgba(0, 0, 0, 0.06);

  cursor: pointer;

  transition: 0.6s;
}

.container button:active,
.container button:hover,
.container button:focus {
  background: var(--button-hover-bg-color);
  outline: 0;
}
.container button:disabled {
  opacity: 0.5;
}

:root {
  --button-color: #8aa1a1;
  --button-bg-color: #dae5d0;
  --button-hover-bg-color: #a6bbc0;
}

.container button {
  background: var(--button-bg-color);
  color: var(--button-color);
}

.ok{
	
	margin: 0;
  margin-left: 1%;
  margin-top: 5%;

  padding: 0.4rem 1.9rem;

  font-family: "Noto Sans KR", sans-serif;
  font-size: 1.2rem;
  font-weight: 400;
  text-align: center;
  text-decoration: none;

  display: inline-block;
  width: auto;

  border: none;
  border-radius: 6px;
   -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  box-shadow: 0 4px 6px -1px rgba(0, 0, 0, 0.1),
    0 2px 4px -1px rgba(0, 0, 0, 0.06);

  cursor: pointer;

  transition: 0.6s;
  background: var(--button-bg-color);
  color: var(--button-color);
}
.ok button:active,
.ok button:hover,
.ok button:focus {
  background: var(--button-hover-bg-color);
  outline: 0;
}
.ok button:disabled {
  opacity: 0.5;
}
.pagingArea{
	width: 1500px;
	margin-left: -600px;
}
</style>

</head>
<body>
<jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
<jsp:include page="/WEB-INF/views/menubar/sidebar.jsp"/>

  <div class="container">
    <div class="features">
        <div class="content">
          <div class="page-title">
            <h3>유기동물 소개글</h3>
            </div>

        <!-- 메뉴 선택 -->
        <div class="menu">
          <c:if test="${ !empty sessionScope.loginMember }">
          <div class="write">
            <button class="btnMenu btn1" onclick="addForm()">글쓰기</button>
            </div>
            </c:if>
				<script type="text/javascript">
					function addForm() {
						location.href="${ pageContext.servletContext.contextPath }/intro/write";
					}
				</script>
          <div class="mine">
          	<c:if test="${ !empty sessionScope.loginMember }">
            <button class="btnMenu btn2" onclick="location.href='${ pageContext.servletContext.contextPath }/intro?type=myT'">나의 글</button>
            <!-- &&login=Y -->
            </c:if>
          </div>
        </div>

        <!-- 게시판 검색창 -->
        <div id="board-search">
          <div class="content">
            <div class="search-window">
              <form action="${ pageContext.servletContext.contextPath }/intro">
                <div class="search-wrap">
                  <label for="search" class="blind"></label>
                  <input type="search" id="searchValue" name="searchValue" placeholder="원하시는 글의 제목을 입력해주세요." value="<c:out value="${ requestScope.selectCriteria.searchValue }"/>"/>
                  <button type="submit" class="btn btn-dark" style="margin-top: 0px">검색</button>
                </div>
              </form>
            </div>
          </div>
        </div>

          <ul class="clearfix" >
        <c:forEach items="${ requestScope.introList }" var="intro">
            <li>
              <div class="item">
                <div class="thumb">
                <!-- 사진 -->
                  <a onclick="href='${ pageContext.servletContext.contextPath }/write/list?introNum='${ intro.introNum }">
                  <img src="<c:out value='/allanimals/${ intro.thumPath }'/>" alt="1"></a> <!-- 동물 사진을 넣어주세요. -->
                </div>
                <div class="desc">
                <input type="hidden" name="introNum" value="${ intro.introNum }">
                  <!-- 제목 -->
                  <h5><a onclick="href='${ pageContext.servletContext.contextPath }/write/list?introNum=${ intro.introNum }'"><c:out value="${ intro.introTitle }"/></a></h5>
                  <!-- 내용 -->
                  <p><a onclick="href='${ pageContext.servletContext.contextPath }/write/list?introNum=${ intro.introNum }'"><c:out value="자세한 내용이 궁금하면 들어와서 확인해주세요!" escapeXml="false"/></a></p>
                  <div class="detail">
                  <!-- 날짜 -->
                  <span><c:out value="${ intro.introTime }"/></span>
                  <!-- 아이디 -->
                  <span><c:out value="${ intro.memId }"/></span>
                  <!-- 관심버튼 -->
                  <span class="likes"><i class="fa fa-heart"></i></span>
                  </div>
                </div>
              </div>
            </li>
		</c:forEach>
          </ul>
		</div>
      </div>

    
      <div class="pagination" style="width: 1000px">
      <jsp:include page="../common/paging.jsp"/>
<!--         <a href="#">&laquo;</a> -->
<!--         <a href="#">1</a> -->
<!--         <a href="#" class="active">2</a> -->
<!--         <a href="#">3</a> -->
<!--         <a href="#">4</a> -->
<!--         <a href="#">5</a> -->
<!--         <a href="#">&raquo;</a> -->
      </div> 

  </div>
<jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>

	
	</body>
	</html>
	
	
	
	
	