<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>        

    
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>입양 신청 | 신청서 작성</title>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/allDaily/adoptApplication.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

   </head> 
    <body>
    
		<jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
		<jsp:include page="/WEB-INF/views/menubar/sidebar.jsp"/>
		
        <div id="container">
            <h1 id="adopt">입양하기</h1>
            <div class="accordion" id="accordionPanelsStayOpenExample">
                <div class="accordion-item">
                  <h2 class="accordion-header" id="panelsStayOpen-headingOne">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne"
                    style="background-color: #DAE5D0;color:black">
                      <p class="acc-title"><b>개인 정보 취급 방침</b></p>
                    </button>
                  </h2>
                  <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne" style="color:black">
                    <div class="accordion-body">
                        <b>All Animals 는 (이하 '단체'는) 입양신청자의 개인정보를 중요시하며, "정보통신망 이용촉진 및 정보보호"에 관한 법률을 준수하고 있습니다. <br>
                        단체는 개인정보취급방침을 통하여 입양신청자께서 제공하시는 개인정보가 어떠한 용도와 방식으로 이용되고 있으며, 개인정보보호를 위해 어떠한 조치가 취해지고 있는지 알려드립니다. </b><br><br>
                        
                        ■ 개인정보 수집에 대한 동의<br>
                        1) "단체"는 이용자의 개인정보를 수집하는 경우에는 [위의 개인정보취급방침에 동의합니다]의 체크박스에 체크하는 절차를 마련하고 있으며, [위의 개인정보취급방침에 동의합니다]의 체크박스에 체크하였을 경우 개인정보 수집에 대하여 동의한 것으로 봅니다.<br>
                        2) "단체"는 다음 사항에 해당하는 경우에 이용자의 별도 동의 없이 개인정보를 수집.이용할 수 있습니다.<br>
                        - 서비스의 제공에 관한 계약의 이행을 위하여 필요한 개인정보로서 경제적.기술적인 사유로 통상의 동의를 받는 것이 현저히 곤란한 경우<br>
                        - 서비스의 제공에 따른 요금정산을 위하여 필요한 경우<br>
                        - 이 법 또는 다른 법률에 특별한 규정이 있는 경우<br><br>
                        
                        ■ 수집하는 개인정보 항목<br>
                        단체는 회원가입, 상담, 서비스 신청 등등을 위해 아래와 같은 개인정보를 수집하고 있습니다.<br>                    
                        ο 수집항목 : 이름, 생년월일, 로그인ID, 비밀번호, 자택 전화번호, 자택 주소, 휴대전화번호, 이메일, 직업, 단체명, 단체전화번호, 주민등록번호, 은행계좌 정보, 접속 로그, 쿠키, 후원회비, 자동이체날짜, 봉사희망분야, 단체주소, 자기소개, 자기정보공개여부<br>
                        ο 개인정보 수집방법 : 홈페이지(회원가입,후원하기)<br><br>
                        
                        ■ 개인정보의 수집 및 이용목적<br>
                        단체는 수집한 개인정보를 다음의 목적을 위해 활용합니다..<br>
                        ο 서비스 제공에 관한 계약 이행 및 서비스 제공에 따른 요금정산<br>
                        - 콘텐츠 제공<br>
                        ο 회원 관리<br>
                        - 회원제 서비스 이용에 따른 본인확인 , 개인 식별 , 불만처리 등 민원처리 , 고지사항 전달<br>
                        ο 마케팅 및 광고에 활용<br>
                        - 이벤트 등 광고성 정보 전달<br><br>
                        
                        ■ 개인정보의 보유 및 이용기간<br>
                        원칙적으로, 개인정보 수집 및 이용목적이 달성된 후에는 해당 정보를 지체 없이 파기합니다. 단, 관계법령의 규정에 의하여 보존할 필요가 있는 경우 단체는 아래와 같이 관계법령에서 정한 일정한 기간 동안 회원정보를 보관합니다.<br>
                        - 보존 항목 : 이름, 휴대전화번호, 이메일, 은행계좌 정보, 후원회비, 자동이체날짜, 봉사희망분야, 단체주소, 자기소개, 자기정보공개여부<br>
                        - 보존 근거 : 전자상거래등에서의 소비자보호에 관한 법률<br>
                        - 보존 기간 : 5년
                  </div>
                </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" required/>
                <label class="form-check-label" for="flexCheckDefault">
                    위 개인정보취급방침에 동의합니다.
                </label>
            </div>
            <hr>
            
            <form action="${ pageContext.servletContext.contextPath }/adopt/application" method="POST">
				<input type="hidden" name="aniNum" value="${ requestScope.aniNum }">
                <p class="info"><b>* 인적사항 조사</b></p>
                
                    <div class="text-box">
                        <label for="name"><b>1. 성명</b></label>
                        <c:out value="${ sessionScope.loginMember.memName }"/>                       
                    </div>
        
                    <div class="radio-box">
                        <p><b>2. 결혼 여부</b>
                        <label><input type="radio" name="marry" value="기혼" required/>기혼</label>
                        <label><input type="radio" name="marry" value="미혼">미혼</label>
                        </p>
                    </div>
        
                    <div class="text-box">
                        <label for="birthday"><b>3. 생년월일</b></label>
                        <c:out value="${ sessionScope.loginMember.memBirth }"/>
                    </div>
        
                    <div class="text-box">
                        <label for="phone"><b>4. 전화번호</b></label>
                        <c:out value="${ sessionScope.loginMember.memPhone }"/>
                    </div>
        
                    <div class="text-box">
                        <label for="email"><b>5. 이메일</b></label>
                        <c:out value="${ sessionScope.loginMember.memEmail }"/>
                    </div>
        
                    <div class="text-box">
                        <label for="address"><b>6. 거주 지역</b></label>
                        <c:out value="${ sessionScope.loginMember.memAddre }"/>
                    </div>
        
                    <div class="radio-box">
                        <p><b>7. 통화가 가능한 시간에 체크해주세요.</b><br>
                        <label><input type="radio" name="callTime" value="06:00~08:00" required/>06:00~08:00</label>
                        <label><input type="radio" name="callTime" value="08:00~10:00">08:00~10:00</label>
                        <label><input type="radio" name="callTime" value="10:00~12:00">10:00~12:00</label>
                        <label><input type="radio" name="callTime" value="12:00~14:00">12:00~14:00</label>
                        <label><input type="radio" name="callTime" value="14:00~16:00">14:00~16:00</label><br>
                        <label><input type="radio" name="callTime" value="16:00~18:00">16:00~18:00</label>
                        <label><input type="radio" name="callTime" value="18:00~20:00">18:00~20:00</label>
                        <label><input type="radio" name="callTime" value="20:00~22:00">20:00~22:00</label>
                        <label><input type="radio" name="callTime" value="22:00~24:00">22:00~24:00</label>
                        </p>
                    </div>
                    
                    <div class="text-box">
                        <label for="mention"><b>8. 신청인의 현재 직업을 입력해주세요.</b></label><br><br>
                        <p><textarea name="memJob" cols="30" rows="2" required/></textarea></p>
                    </div>
        
                    <br><br>
        
                    <p class="info"><b>* 지금부터 설문을 시작하겠습니다.</b></p>
        
                    <div class="radio-box">
                        <p><b>1. 동거 가족의 동의 여부를 선택해주세요.</b>
                        <label><input type="radio" name="ques1" value="전체 동의" required/>전체 동의</label>
                        <label><input type="radio" name="ques1" value="비동의">비동의</label>
                        <label><input type="radio" name="ques1" value="일부 동의">일부 동의</label>
                        </p>
                    </div>
        
                    <div class="radio-box">
                        <p><b>2. 현재 집에서 다른 동물을 키우고 계십니까?</b>
                        <label><input type="radio" name="ques2" value="네" required/>네</label>
                        <label><input type="radio" name="ques2" value="아니오">아니오</label>
                        </p>
                    </div>
        
                    <div class="radio-box">
                        <p><b>3. 거주하고 계신 주택 형태에 표시해주세요.</b>
                        <label><input type="radio" name="ques3" value="아파트" required/>아파트</label>
                        <label><input type="radio" name="ques3" value="단독주택">단독주택</label>
                        <label><input type="radio" name="ques3" value="빌라/다세대">빌라/다세대</label>
                        <label><input type="radio" name="ques3" value="원룸">원룸</label>
                        <label><input type="radio" name="ques3" value="기타">기타</label>
                        </p>
                    </div>
        
                    <div class="radio-box">
                        <p><b>4. 중성화 수술에 대한 의견에 표시해주세요.</b>
                        <label><input type="radio" name="ques4" value="찬성" required/>찬성</label>
                        <label><input type="radio" name="ques4" value="반대">반대</label>
                        <label><input type="radio" name="ques4" value="잘 모르겠다">잘 모르겠다</label>
                        </p>
                    </div>
        
                    <div class="radio-box">
                        <p><b>5. 반려동물을 키워보신 적이 있으십니까?</b>
                        <label><input type="radio" name="ques5" value="네" required/>네</label>
                        <label><input type="radio" name="ques5" value="아니오">아니오</label>                
                    </p>
                    </div>
                    
                    <div class="text-box">
                        <label for="mention"><b>6. 하고 싶은 말씀이 있다면 자유롭게 적어주세요.</b></label><br><br>
                        <p><textarea name="appCont" cols="100" rows="10"></textarea></p>
                    </div>

				<input type="submit" value="전송" />

                </form>
            </div>
          </div><!--box-->
        </div> <!--container close-->
        
        
<jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>   
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>
