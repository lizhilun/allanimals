<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
    
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>입양 신청 | 제출 완료</title>
    <link rel="stylesheet" href="css/template.css" />
    <link rel="stylesheet" href="css/adoptApplicationDone.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <style>
.result {width: 400px;height: 100px;margin: 0 auto;text-align: center;padding : 100px 0;}
.result p {font-size: 15px;font-family: "나눔고딕";}
.resultBtn {width: 150px;height: 40px;background-color: #DAE5D0;border: 0;border-radius: 40px 40px 40px 40px;font-size: 15px;font-family: "나눔고딕";}
  </style>
  
  </head>
  <body>
  <jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
  <jsp:include page="/WEB-INF/views/menubar/sidebar.jsp"/>
  
    <div class="container">
        <div class="result">
            <p>
                <strong>감사합니다.</strong><br>
                입양 신청이 접수되었습니다.<br>
                심사 후 개별 연락 예정입니다.
            </p>

            <button type="button" class="resultBtn" onclick="location.href='${ pageContext.servletContext.contextPath }/intro'">더 많은 동물 보기</button>


        </div>
    </div>
    
   <jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>
    
  </body>
</html>
