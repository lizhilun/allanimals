<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/allDaily/template.css">
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/header.css">
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/sidebar.css">
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/footer.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/da4b1b94bb.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>

    <style>
      .container{
        width: 1000px;
        height: auto;
      }
      .title{
        margin-bottom: 50px;
      }
      .petImg{
        margin-left: 80px;
        margin-top: 50px;
      }
      /* .petImg, .info .btn-group {
        margin: center;
      } */
      .info {
        margin-top: -290px;
        margin-left: 610px;
        margin-bottom: 60px;
        padding: 10px;
        width: 400px;
      }
      .btn{
        margin-top: 20px;
        margin-bottom: 10px;
      }
      .carousel{
        width: 500px;height: 300px;
      }
      .aimg{
        width: 500px;height: 300px;
      }
      .status {
        background-color : #DAE5D0;border-radius: 40%;width: 80px;height: 20px;
        display: table-cell;vertical-align: middle;text-align:center;
      }
      table {
        margin: 10px;line-height: 30px;
      }
      .btn-group {
        width: 500px;clear: both;
      }

      .textBtn {
        width: 100px;
        height: 40px;
        background-color: #DAE5D0;
        border: none;
        color: white;
        padding: 0px;
        text-align: center;
        text-decoration: none;
        display: inline;
        font-size: 16px;
        margin: 4px 2px;
        transition-duration: 0.2s;
        cursor: pointer;
        text-decoration: none;
        color: black;
        margin-top: 20px;
        float: left;
        margin-right:20px;
      }
    
      .textBtn{
        background-color: white; 
        color: black; 
        border: 2px solid #DAE5D0;
        text-decoration: none;
        color: black;
      }

      .textBtn:hover {
        background-color: #DAE5D0;
        color: white;
      }
      .text{
        margin-bottom: -67px;
      }
      .textMap{
        margin-left: 1050px;
      }
      /* 글내용 div */
      .wText{
        margin-top: 60px;
        width: 800px;

      }
      /* 글내용 */
      .pText{
        margin-top: 60px;
        margin-left: 50px;
        margin-bottom: 60px;
        width: 900px;
        
      }

      .replyTitle{
        width: 100px;
      }

      .countreply {
        position: absolute; 
        width: 100px; 
        left: 1350px;
        height: 46px;
      }

      /* 댓글작성 */
      #exampleFormControlTextarea1 {
        width: 850px;        
      }

      /* 댓글이랑 댓글 작성 사이 */
      #replyarea {
        margin-top: 50px;
      }

      .btnMenu {
        width: 100px;
        height: 40px;
        background-color: #DAE5D0;
        border: none;
        color: white;
        padding: 0px;
        text-align: center;
        text-decoration: none;
        display: inline;
        font-size: 16px;
        margin: 4px 2px;
        transition-duration: 0.2s;
        cursor: pointer;
        text-decoration: none;
        color: black;
        /* margin-top: 20px; */
        float: left;
        /* margin-right:20px; */
        position: absolute;
        /* left: 1100px;
        top: 950px; */
        float: left;
      }

    .btn1{
      background-color: white; 
      color: black; 
      border: 2px solid #DAE5D0;
      text-decoration: none;
      color: black;
      margin-top: -55px;
      margin-left: 400px;
    }
    .tBtn{
      background-color: white; 
      color: black; 
      border: 2px solid #DAE5D0;
      text-decoration: none;
      color: black;
      margin-left: -173px;
      margin-top: 20px;
    }
    .gBtn{
      background-color: white; 
      color: black; 
      border: 2px solid #DAE5D0;
      text-decoration: none;
      color: black;
      margin-left: -75px;
      margin-top: 20px;
    }
    .bBtn{
      background-color: white; 
      color: black; 
      border: 2px solid #DAE5D0;
      text-decoration: none;
      color: black;
      margin-left: 22px;
      margin-top: 20px;
      }

    .btn1:hover, .tBtn:hover, .gBtn:hover, .bBtn:hover, .plus:hover, .reWrite:hover, .dlt:hover {
      background-color: #DAE5D0;
      color: white;
    }
    
    .plus{
      background-color: white; 
      color: black; 
      border: 2px solid #DAE5D0;
      text-decoration: none;
      color: black;
      margin-left: -490px;
    }
    .reWrite{
      background-color: white; 
      color: black; 
      border: 2px solid #DAE5D0;
      text-decoration: none;
      color: black;
      margin-left: -392px;
    }
    
    .dlt{
      background-color: white; 
      color: black; 
      border: 2px solid #DAE5D0;
      text-decoration: none;
      color: black;
      margin-left: -294px;
    }

    #icon {position: absolute; 
      top: 960px;
      left: 500px; 
      z-index: 100px;
      width: 40px;
      height: 40px;
      float: left;
    }

    .lastBtn{
      height: 200px;
    }
    
    .carousel-inner img {
    width: 500px;
    height: 300px;
   }

    </style>

</head>
<body>
<jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
<jsp:include page="/WEB-INF/views/menubar/sidebar.jsp"/>

    <div class="container">
      <div style="border: 1px;">
        <div class="title"><!-- 제목 -->
          <h3><c:out value="${ requestScope.thumbnail.introTitle }"/></h3>
        </div>
        
        <hr>
        <!-- 이미지 -->
        <div class="petImg carousel slide" data-bs-ride="carousel" id="demo" class="carousel slide">
        
    <div id="demo" class="carousel slide" data-ride="carousel">
    
      <!-- Indicators -->
      <ul class="carousel-indicators">
        <li data-target="#demo" data-slide-to="0" class="active"></li>
        <li data-target="#demo" data-slide-to="1"></li>
        <li data-target="#demo" data-slide-to="2"></li>
      </ul>
      
      <!-- The slideshow -->
        <div class="carousel-inner">
        <c:forEach items="${ requestScope.thumbnail.introFile }" var="intro" varStatus="stu">
        <c:if test="${ stu.count == 1 }">
          <div class="carousel-item active">
            <img src="<c:out value='/allanimals${ intro.phoAdder }${ intro.phoFile } '/>" alt="${ stu.count }" width="500" height="300">
          </div>
        </c:if>
        <c:if test="${ stu.count != 1 }">
          <div class="carousel-item">
            <img src="<c:out value='/allanimals${ intro.phoAdder }${ intro.phoFile } '/>" alt="${ stu.count }" width="500" height="300">
          </div>
        </c:if>
        </c:forEach>
        </div>

        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#demo" data-slide="prev">
          <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next">
          <span class="carousel-control-next-icon"></span>
        </a>
    </div>
    <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
    </a>
      </div>
      
      <div class="info">
        <table>
          <tr><!-- 동물번호 -->
            <td style="width : 100px"><b>동물등록번호</b></td>
            <td style="width : 100px">
            <c:out value="${ requestScope.thumbnail.aniNum }"/>
            </td>
          </tr>
          <tr><!-- 보호유무 -->
            <td><b>입양 유무</b></td>
            <td>
	            <c:choose>
				<c:when test="${ requestScope.thumbnail.aniAdopt eq 'N' }">
					미입양
				</c:when>
				<c:when test="${ requestScope.thumbnail.aniAdopt eq 'Y' }">
					입양 완료
				</c:when>
				<c:otherwise>
					입양 절차 오류
				</c:otherwise>
				</c:choose>
            </td>
          </tr>
          <tr><!-- 이름 -->
            <td><b>이름</b></td>
            <td><c:out value="${ requestScope.thumbnail.aniName }"/></td>
          </tr>
            <tr><!-- 종류 -->
                <td><b>종</b></td>
                <td style="width: 200px;">
                    <c:choose>
				        <c:when test="${ requestScope.thumbnail.aniSpec eq 'D' }">
				            강아지
				        </c:when>
				        <c:when test="${ requestScope.thumbnail.aniSpec eq 'C' }">
				            고양이
				        </c:when>
				        <c:otherwise>
				            품종 선택 오류
				         </c:otherwise>
				     </c:choose>
                </td>
              </tr>
              <tr><!-- 나이 -->
                <td><b>나이</b></td>
                <td><c:out value="${ requestScope.thumbnail.introAge }"/> 살</td>
              </tr>
              <tr><!-- 성별 -->
                <td><b>성별</b></td>
                <td>
                <c:choose>
					<c:when test="${ requestScope.thumbnail.introGen eq 'F' }">
						여자
					</c:when>
					<c:when test="${ requestScope.thumbnail.introGen eq 'M' }">
						남자
					</c:when>
					<c:otherwise>
						성별 미선택
					</c:otherwise>
					</c:choose>
                </td>
              </tr>
            </table>
            <c:if test="${ !empty sessionScope.loginMember }">
            <button class="btnMenu tBtn" onclick="location.href='${ pageContext.servletContext.contextPath }/adopt/application?aniNum=${ requestScope.thumbnail.aniNum }'">입양하기</button>
            
            <button class="btnMenu gBtn" onclick="location.href='${ pageContext.servletContext.contextPath }/donation/main'">후원하기</button>
            
            <c:choose>
				<c:when test="${ requestScope.interYn eq 'Y' }">
            		<button class="btnMenu bBtn" onclick="location.href='${ pageContext.servletContext.contextPath }/interest/insert?introNum=${ requestScope.thumbnail.introNum }'">관심추가</button>
				</c:when>
				<c:when test="${ requestScope.interYn eq 'N' }">
            		<button class="btnMenu bBtn" onclick="location.href='${ pageContext.servletContext.contextPath }/interest/insert?introNum=${ requestScope.thumbnail.introNum }'">관심삭제</button>
				</c:when>
				<c:otherwise>
				 오류
				</c:otherwise>
			</c:choose>
				     

            </c:if>
          </div><br><br>
          
          <hr>
          
          <div id="wText"><!-- 글내용 -->
            <p class="pText"><c:out value="${ requestScope.thumbnail.introCont }" escapeXml="false"/></p>
            </div>
            
          <table class="table" id="maintable2">
            <thead>
              <tr>
                <th class="replyTitle">댓글</th>
		  		<th class="countreply">댓글 ${fn:length(rep)}</th>              
			  </tr>
            </thead>

		<tbody class="bottom">      
             <c:forEach var="repList" items="${ requestScope.rep }">
			<tr>
				<td colspan="3"><c:out value="${ repList.memId }"/><br>
				<c:if test="${ sessionScope.loginMember.memId eq repList.memId }">
                  <span class="delete" OnClick="location.href='${ pageContext.servletContext.contextPath }/intro/rep/delete?introNum=${ repList.introNum }&&repNum=${ repList.repNum }'" style="cursor:pointer;"><i class="fa-solid fa-trash-can" title="삭제"></i></span>
                  </c:if>
                  
                   </td>
				<td colspan="9"><c:out value="${ repList.repCont }"/></td>
			</tr>
			</c:forEach>
            </tbody>
          </table>
		<c:if test="${ !empty sessionScope.loginMember }">
		<form action="${ pageContext.servletContext.contextPath }/intro/rep/insert">
          <table id="reply">
            <tr>
              <td>
          <div class="mb-3" id="replyarea">
            <textarea class="form-control" name="exampleFormControlTextarea1" id="exampleFormControlTextarea1" rows="3" placeholder="댓글 내용을 입력해주세요"></textarea>
          </div>
             </td>
             <td>
             <input type="hidden" name="introNum" value="${ requestScope.thumbnail.introNum }"/>
              <br>
            <div class="write" style="margin-left: -780px; margin-top: 70px">
            <button class="btnMenu btn1" type="submit">댓글 작성</button>
            </div>
          </td>
          </tr>
          </table>
          </form>
          </c:if>
  </div>	
            <div class="lastBtn">
        		<button class="btnMenu plus" onclick="location.href='${ pageContext.servletContext.contextPath }/intro'">목록으로</button>
            
            <c:if test="${ sessionScope.loginMember.memId eq thumbnail.memId }">
            <button class="btnMenu reWrite" onclick="location.href='${ pageContext.servletContext.contextPath }/update/list?introNum=${ requestScope.thumbnail.introNum }'">수정하기</button>
            
            <button class="btnMenu dlt" onclick="location.href='${ pageContext.servletContext.contextPath }/delete/list?introNum=${ thumbnail.introNum }'">삭제하기</button>
            
            </c:if>
            </div>
				
    
    <jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>
    
    <script>
  		
   		
   		// httpRequest 객체 생성
   		function getXMLHttpRequest(){
   			var httpRequest = null;
   			
   			if(window.ActiveXObject){
   				
   				try {
   				
   				httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
 
   			} catch(e) {
   				
   				try {
   					
   					httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
   				
   				} catch (e2) { httpRequest = null; }
   				
   			}
   				
   		}
   			
   			else if (window.XMLHttpRequest) {
   				
   				httpRequest = new window.XMLHttpRequest();
   			}
   			
   			return httpRequest;
   		}
   		
   		
   		//댓글 등록
   		function writeCmt()
   		{
   			var for = document.getElementById("commentForm");
   			
   			var board = form.commentBoard.value
   			var id = form.memId.value
   			var content = form.commentContent.value;
   			
   			if(!content)
   			{
   				alert("내용을 입력하세요.");
   				return false;
   			}
   			else
   			{
   				var param = "comentBoard = " + board + "&memId = " + id + "&repCont = " + content;
   				
   				httpRequest = getXMLHttpRequest();
   				httpRequest.onreadystatechange = checkFunc;
   				httpRequest.open("POST", "ComentWriteAction.co", true);
   				httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;chardet=EUC-KR')
   				httpRequest.send(param);   			
   			}	
   		}
   		
   		function checkFunc()
   		{
   			if(httpRequest.readyState == 4){
   				
   				var resultText = httpRequest.responseText;
   				if(resultText == 1){
   					document.location.reload();
   				}
   			}
   			
   		}
   		
   		

   		
    </script>

</body>
</html>