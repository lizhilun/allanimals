<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/mainPage/mainPage.css" />
 
  </head>
  <body>
    <header>
     <!-- c:if문을 이용해서 로그인했을 떄와 로그인 안했을 떄의 구분을 나중에 추가해야 -->
     <c:if test="${ empty sessionScope.loginMember }">
      <div class="menulist">
        <ul class="headermenu">
          <li><a href="${ pageContext.servletContext.contextPath }/member/menu">메뉴</a></li>
          <li><a href="${ pageContext.servletContext.contextPath }/member/login">로그인</a></li>
          <li><a href="${ pageContext.servletContext.contextPath }/member/regist">회원가입</a></li>
        </ul>
      </div>
      </c:if>
      <c:if test="${ !empty sessionScope.loginMember }">
      <div class="menulist">
        <ul class="headermenu">
        <li><a href="${ pageContext.servletContext.contextPath }/member/menu">메뉴</a></li>
          <li><a href="${ pageContext.servletContext.contextPath }/member/userInfo">마이페이지</a></li>
          <li><a href="${ pageContext.servletContext.contextPath }/member/logout">로그아웃</a></li>
          <c:if test="${ !empty sessionScope.loginMember.memSup }">
          <li><a href="${ pageContext.servletContext.contextPath }/admin/main">관리자 메뉴</a></li> 
          </c:if>
        </ul>
      </div>
      </c:if>
    </header>
    <div class="container">
      <div class="logoClass">
        <img
          class="shake-vertical"
          src="${ pageContext.servletContext.contextPath}/resources/upload/images/logo_boxless.png"
          alt="logo"
        />
      </div>
    </div>
    <div class="footer">
      <button
        class="help-btn"
        type="submit"
        onclick="location.href='${ pageContext.servletContext.contextPath }/rescueRequest';"
  	  >
        Help!
      </button>
    </div>
  </body>
</html>
    