<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/mypage/privateUserInfo.css" />
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/button.css" />
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    
  </head>

  <body>
  	<jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
	<jsp:include page="/WEB-INF/views/mypage/mypageSidebar.jsp"/>

    <!-- 이 밑에서부터 작업하시면 됩니다.-->
    <div class="container">
      <div class="box1">wp
        <div class="box3"> 
        
          		<table class="table" id="table">
                  <tbody>
                    <tr>
                      <td style="border: none; text-align: center;" class="col-4" ><img src="${ pageContext.servletContext.contextPath}/resources/upload/images/mypage/user.png"></td>
                      <td style="text-align: center;" class="col-5"><c:out value="${ requestScope.memberInfo.memName }"/></td>
                      <td class="col-3" style="border: none;"></td>
                    </tr>
                    <tr>
                      <td style="border: none; text-align: center;"><img src="${ pageContext.servletContext.contextPath}/resources/upload/images/mypage/calendar.png"></td>
                      <td style="text-align: center;"><c:out value="${ requestScope.memberInfo.memBirth }"/></td>
                    </tr>
                    <tr>
                      <td style="border: none; text-align: center;"><img src="${ pageContext.servletContext.contextPath}/resources/upload/images/mypage/email.png"></td>
                      <td style="text-align: center;"><c:out value="${ requestScope.memberInfo.memEmail }"/></td>
                    </tr>
                    <tr>
                      <td style="border: none; text-align: center;"><img src="${ pageContext.servletContext.contextPath}/resources/upload/images/mypage/phone.png"></td>
                      <td style="text-align: center;"><c:out value="${ requestScope.memberInfo.memPhone }"/></td>
                    </tr>
                    <tr>
                      <td style="border: none; text-align: center;"><img src="${ pageContext.servletContext.contextPath}/resources/upload/images/mypage/address.png"></td>
                      <td style="text-align: center;"><c:out value="${ requestScope.memberInfo.memAddre }"/></td>
                    </tr>
                    </tbody>
                    </table>
                    <div>
                        <button id="revise" type="submit" onclick="location.href='${ pageContext.servletContext.contextPath}/revise'">개인정보 수정</button>
                    </div>
         
                   
        </div>
        <div>
          <button id="password" type="button"  data-bs-toggle="modal" data-bs-target="#staticBackdrop">비밀번호 변경</button>
        </div>
        <form action="${ pageContext.servletContext.contextPath }/member/userInfo" method="post">
          <!-- Modal -->
         <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
             <div class="modal-dialog">
             <div class="modal-content">
                 <div class="modal-header">
                 <h5 class="modal-title" id="staticBackdropLabel">비밀번호 변경</h5>
                 <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                 </div>
         
                 <div class="modal-body">
                 <p>새로운 비밀번호를 입력해주세요</p>
                 <div class="mb-3 row">
                   <label for="inputNewPass" class="col-sm-4 col-form-label">새로운 Password</label>
                   <div class="col-sm-8">
                     <input type="password" class="form-control" id="memPwd" name="memPwd">
                   </div>
                 </div>
                 <p>확인을 위해 새로운 비밀번호를 한번 더 입력해주세요</p>
                 <div class="mb-3 row">
                   <label for="inputNewPass" class="col-sm-4 col-form-label">새로운 Password</label>
                   <div class="col-sm-8">
                     <input type="password" class="form-control" id="memPwdChk">
                   </div>
                 </div>
                 </div>
                 
                 <!-- 모달 바닥 -->
                 <div class="modal-footer">
                 <button id="mbutton" type="button" class="btn btn-secondary" data-bs-dismiss="modal">취소</button>
                 <button id="mbutton" type="submit" class="btn btn-primary" onclick="return checkPwd();">확인</button>
                 </div>
             </div>
             </div>
         </div><!--modal close-->
         
     </form>  
    </div> <!-- box1 close -->
  </div><!-- container close -->

	<jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>
   
    <script>
      let myModal = document.getElementById('myModal')
      let myInput = document.getElementById('myInput')
      
      myModal.addEventListener('shown.bs.modal', function () {
        myInput.focus()
      })

    function checkPwd(){
    	  
    	  const newPwd = $("#memPwd").val();
    	  const newPwdCheck = $("#memPwdChk").val();
    	  
    	 if(newPwd != newPwdCheck){
    		 alert("비밀번호 불일치");
             return false;

           } else {
             alert("비밀번호가 일치합니다");
             alert("비밀번호 변경이 완료되었습니다.");
          
             return true;
           }
    	 } 
      
    
    </script>
    
    <script>
   	//비밀번호 유효성 검사
   	let userPwd = document.querySelector('#memPwd')
    userPwd.addEventListener('change',(e)=>{
        validPwd(e.target)
    })

    function validPwd(obj){
        console.log(obj)
        if(validPwdCheck(obj)==false){
            alert('비밀번호는 8~20자리 숫자/문자/특수문자를 포함해야합니다.')
            obj.value='';
            obj.focus();
            return false;
        }
    }

    function validPwdCheck(obj){
        let pattern = /^(?=.*[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*[0-9]).{8,25}$/;
        return (obj.value.match(pattern)!=null)
    }
   	
   	</script>
    
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>
