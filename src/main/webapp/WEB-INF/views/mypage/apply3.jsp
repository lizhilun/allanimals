<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>        
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/button.css" />
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/mypage/apply3.css" />
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    
  </head>

  <body>
 
   	<jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
	<jsp:include page="/WEB-INF/views/mypage/mypageSidebar.jsp"/>
 

    <!-- 이 밑에서부터 작업하시면 됩니다.-->
    <div id="container">
        <div id="box">
            <div id="box2">
                <form action="" method="POST">
                <hr>
        
                <p class="info"><b>* 인적사항 조사</b></p>
                
                    <div class="text-box">
                        <label for="name"><b>1. 성명</b></label>
                        <c:out value="${ requestScope.adoList[0].memList[0].memName }"/>
                    </div>
        
            		<div class="radio-box">
                        <p><b>2. 결혼 여부</b>
                        <c:if test="${  requestScope.adoList[0].marry eq '기혼' }">
	                        <label><input type="radio" name="marry" value="기혼" checked="checked"/>기혼</label>
	                        <label><input type="radio" name="marry" value="미혼">미혼</label>
                        </c:if>
                        <c:if test="${ requestScope.adoList[0].marry eq '미혼' }">
	                        <label><input type="radio" name="marry" value="기혼"/>기혼</label>
	                        <label><input type="radio" name="marry" value="미혼" checked="checked">미혼</label>
                        </c:if>
                        </p>
                    </div>
        
                    <div class="text-box">
                        <label for="birthday"><b>3. 생년월일</b></label>
                        <c:out value="${ requestScope.adoList[0].memList[0].memBirth }"/>
                    </div>
        
                    <div class="text-box">
                        <label for="phone"><b>4. 전화번호</b></label>
                        <c:out value="${ requestScope.adoList[0].memList[0].memPhone }"/>
                    </div>
        
                    <div class="text-box">
                        <label for="emal"><b>5. 이메일</b></label>
                        <c:out value="${ requestScope.adoList[0].memList[0].memEmail }"/>
                    </div>
        
                    <div class="text-box">
                        <label for="address"><b>6. 거주 지역</b></label>
                        <c:out value="${ requestScope.adoList[0].memList[0].memAddre }"/>
                    </div>
        
                    <div class="radio-box">
                        <p><b>7. 통화가 가능한 시간에 모두 체크해주세요.</b><br>
                        <c:if test="${ requestScope.adoList[0].callTime eq '1' }">
	                        <label><input type="radio" name="callTime" value="06:00~08:00" checked="checked">06:00~08:00</label>
	                        <label><input type="radio" name="callTime" value="08:00~10:00">08:00~10:00</label>
	                        <label><input type="radio" name="callTime" value="10:00~12:00">10:00~12:00</label>
	                        <label><input type="radio" name="callTime" value="12:00~14:00">12:00~14:00</label>
	                        <label><input type="radio" name="callTime" value="14:00~16:00">14:00~16:00</label><br>
	                        <label><input type="radio" name="callTime" value="16:00~18:00">16:00~18:00</label>
	                        <label><input type="radio" name="callTime" value="18:00~20:00">18:00~20:00</label>
	                        <label><input type="radio" name="callTime" value="20:00~22:00">20:00~22:00</label>
	                        <label><input type="radio" name="callTime" value="22:00~24:00">22:00~24:00</label>
                        </c:if>
                        <c:if test="${ requestScope.adoList[0].callTime eq '2' }">
	                        <label><input type="radio" name="callTime" value="06:00~08:00">06:00~08:00</label>
	                        <label><input type="radio" name="callTime" value="08:00~10:00" checked="checked">08:00~10:00</label>
	                        <label><input type="radio" name="callTime" value="10:00~12:00">10:00~12:00</label>
	                        <label><input type="radio" name="callTime" value="12:00~14:00">12:00~14:00</label>
	                        <label><input type="radio" name="callTime" value="14:00~16:00">14:00~16:00</label><br>
	                        <label><input type="radio" name="callTime" value="16:00~18:00">16:00~18:00</label>
	                        <label><input type="radio" name="callTime" value="18:00~20:00">18:00~20:00</label>
	                        <label><input type="radio" name="callTime" value="20:00~22:00">20:00~22:00</label>
	                        <label><input type="radio" name="callTime" value="22:00~24:00">22:00~24:00</label>
                        </c:if>
                        <c:if test="${ requestScope.adoList[0].callTime eq '3' }">
	                        <label><input type="radio" name="callTime" value="06:00~08:00">06:00~08:00</label>
	                        <label><input type="radio" name="callTime" value="08:00~10:00">08:00~10:00</label>
	                        <label><input type="radio" name="callTime" value="10:00~12:00" checked="checked">10:00~12:00</label>
	                        <label><input type="radio" name="callTime" value="12:00~14:00">12:00~14:00</label>
	                        <label><input type="radio" name="callTime" value="14:00~16:00">14:00~16:00</label><br>
	                        <label><input type="radio" name="callTime" value="16:00~18:00">16:00~18:00</label>
	                        <label><input type="radio" name="callTime" value="18:00~20:00">18:00~20:00</label>
	                        <label><input type="radio" name="callTime" value="20:00~22:00">20:00~22:00</label>
	                        <label><input type="radio" name="callTime" value="22:00~24:00">22:00~24:00</label>
                        </c:if>
                        <c:if test="${ requestScope.adoList[0].callTime eq '4' }">
	                        <label><input type="radio" name="callTime" value="06:00~08:00">06:00~08:00</label>
	                        <label><input type="radio" name="callTime" value="08:00~10:00">08:00~10:00</label>
	                        <label><input type="radio" name="callTime" value="10:00~12:00">10:00~12:00</label>
	                        <label><input type="radio" name="callTime" value="12:00~14:00" checked="checked">12:00~14:00</label>
	                        <label><input type="radio" name="callTime" value="14:00~16:00">14:00~16:00</label><br>
	                        <label><input type="radio" name="callTime" value="16:00~18:00">16:00~18:00</label>
	                        <label><input type="radio" name="callTime" value="18:00~20:00">18:00~20:00</label>
	                        <label><input type="radio" name="callTime" value="20:00~22:00">20:00~22:00</label>
	                        <label><input type="radio" name="callTime" value="22:00~24:00">22:00~24:00</label>
                        </c:if>
                        <c:if test="${ requestScope.adoList[0].callTime eq '5' }">
	                        <label><input type="radio" name="callTime" value="06:00~08:00">06:00~08:00</label>
	                        <label><input type="radio" name="callTime" value="08:00~10:00">08:00~10:00</label>
	                        <label><input type="radio" name="callTime" value="10:00~12:00">10:00~12:00</label>
	                        <label><input type="radio" name="callTime" value="12:00~14:00">12:00~14:00</label>
	                        <label><input type="radio" name="callTime" value="14:00~16:00" checked="checked">14:00~16:00</label><br>
	                        <label><input type="radio" name="callTime" value="16:00~18:00">16:00~18:00</label>
	                        <label><input type="radio" name="callTime" value="18:00~20:00">18:00~20:00</label>
	                        <label><input type="radio" name="callTime" value="20:00~22:00">20:00~22:00</label>
	                        <label><input type="radio" name="callTime" value="22:00~24:00">22:00~24:00</label>
                        </c:if>
                        <c:if test="${ requestScope.adoList[0].callTime eq '6' }">
	                        <label><input type="radio" name="callTime" value="06:00~08:00">06:00~08:00</label>
	                        <label><input type="radio" name="callTime" value="08:00~10:00">08:00~10:00</label>
	                        <label><input type="radio" name="callTime" value="10:00~12:00">10:00~12:00</label>
	                        <label><input type="radio" name="callTime" value="12:00~14:00">12:00~14:00</label>
	                        <label><input type="radio" name="callTime" value="14:00~16:00">14:00~16:00</label><br>
	                        <label><input type="radio" name="callTime" value="16:00~18:00"  checked="checked">16:00~18:00</label>
	                        <label><input type="radio" name="callTime" value="18:00~20:00">18:00~20:00</label>
	                        <label><input type="radio" name="callTime" value="20:00~22:00">20:00~22:00</label>
	                        <label><input type="radio" name="callTime" value="22:00~24:00">22:00~24:00</label>
                        </c:if>
                        <c:if test="${ requestScope.adoList[0].callTime eq '7' }">
	                        <label><input type="radio" name="callTime" value="06:00~08:00">06:00~08:00</label>
	                        <label><input type="radio" name="callTime" value="08:00~10:00">08:00~10:00</label>
	                        <label><input type="radio" name="callTime" value="10:00~12:00">10:00~12:00</label>
	                        <label><input type="radio" name="callTime" value="12:00~14:00">12:00~14:00</label>
	                        <label><input type="radio" name="callTime" value="14:00~16:00">14:00~16:00</label><br>
	                        <label><input type="radio" name="callTime" value="16:00~18:00">16:00~18:00</label>
	                        <label><input type="radio" name="callTime" value="18:00~20:00" checked="checked">18:00~20:00</label>
	                        <label><input type="radio" name="callTime" value="20:00~22:00">20:00~22:00</label>
	                        <label><input type="radio" name="callTime" value="22:00~24:00">22:00~24:00</label>
                        </c:if>
                        <c:if test="${ requestScope.adoList[0].callTime eq '8' }">
	                        <label><input type="radio" name="callTime" value="06:00~08:00">06:00~08:00</label>
	                        <label><input type="radio" name="callTime" value="08:00~10:00">08:00~10:00</label>
	                        <label><input type="radio" name="callTime" value="10:00~12:00">10:00~12:00</label>
	                        <label><input type="radio" name="callTime" value="12:00~14:00">12:00~14:00</label>
	                        <label><input type="radio" name="callTime" value="14:00~16:00">14:00~16:00</label><br>
	                        <label><input type="radio" name="callTime" value="16:00~18:00">16:00~18:00</label>
	                        <label><input type="radio" name="callTime" value="18:00~20:00">18:00~20:00</label>
	                        <label><input type="radio" name="callTime" value="20:00~22:00" checked="checked">20:00~22:00</label>
	                        <label><input type="radio" name="callTime" value="22:00~24:00">22:00~24:00</label>
                        </c:if>
                        <c:if test="${ requestScope.adoList[0].callTime eq '9' }">
	                        <label><input type="radio" name="callTime" value="06:00~08:00">06:00~08:00</label>
	                        <label><input type="radio" name="callTime" value="08:00~10:00">08:00~10:00</label>
	                        <label><input type="radio" name="callTime" value="10:00~12:00">10:00~12:00</label>
	                        <label><input type="radio" name="callTime" value="12:00~14:00">12:00~14:00</label>
	                        <label><input type="radio" name="callTime" value="14:00~16:00">14:00~16:00</label><br>
	                        <label><input type="radio" name="callTime" value="16:00~18:00">16:00~18:00</label>
	                        <label><input type="radio" name="callTime" value="18:00~20:00">18:00~20:00</label>
	                        <label><input type="radio" name="callTime" value="20:00~22:00">20:00~22:00</label>
	                        <label><input type="radio" name="callTime" value="22:00~24:00" checked="checked">22:00~24:00</label>
                        </c:if>
                        </p>
                    </div>
                    
        		    <div class="text-box">
                        <label for="mention"><b>8. 신청인의 현재 직업을 입력해주세요.</b></label><br><br>
                        <p><textarea name="memJob" cols="30" rows="2" required><c:out value="${ requestScope.adoList[0].memJob }"/></textarea></p>
                    </div>
        
                    <br><br>
        
                    <p class="info"><b>* 지금부터 설문을 시작하겠습니다.</b></p>
        
                    <div class="radio-box">
                        <p><b>1. 동거 가족의 동의 여부를 선택해주세요.</b>
                        <c:if test="${ requestScope.adoList[0].ques1 eq '전체동의' }">
	                        <label><input type="radio" name="ques1" value="전체동의" required  checked="checked"/>전체 동의</label>
	                        <label><input type="radio" name="ques1" value="비동의">비동의</label>
	                        <label><input type="radio" name="ques1" value="일부동의">일부 동의</label>
                        </c:if>
                        <c:if test="${ requestScope.adoList[0].ques1 eq '비동의' }">
	                        <label><input type="radio" name="ques1" value="전체동의" required/>전체 동의</label>
	                        <label><input type="radio" name="ques1" value="비동의" checked="checked">비동의</label>
	                        <label><input type="radio" name="ques1" value="일부동의">일부 동의</label>
                        </c:if>
                        <c:if test="${ requestScope.adoList[0].ques1 eq '일부 동의' }">
	                        <label><input type="radio" name="ques1" value="전체동의"required/>전체 동의</label>
	                        <label><input type="radio" name="ques1" value="비동의">비동의</label>
	                        <label><input type="radio" name="ques1" value="일부동의" checked="checked">일부 동의</label>
                        </c:if>
                        </p>
                    </div>
        
                    <div class="radio-box">
                        <p><b>2. 현재 집에서 다른 동물을 키우고 계십니까?</b>
                        <c:if test="${ requestScope.adoList[0].ques2 eq '네' }">
	                        <label><input type="radio" name="ques2" value="네" required checked="checked"/>네</label>
	                        <label><input type="radio" name="ques2" value="아니오">아니오</label>
                        </c:if>
                        <c:if test="${ requestScope.adoList[0].ques2 eq '아니요' }">
	                        <label><input type="radio" name="ques2" value="네" required/>네</label>
	                        <label><input type="radio" name="ques2" value="아니오" checked="checked">아니오</label>
                        </c:if>
                        </p>
                    </div>
        
                    <div class="radio-box">
                        <p><b>3. 거주하고 계신 주택 형태에 표시해주세요.</b>
                        <c:if test="${ requestScope.adoList[0].ques3 eq '아파트' }">
	                        <label><input type="radio" name="ques3" value="아파트" required checked="checked"/>아파트</label>
	                        <label><input type="radio" name="ques3" value="단독주택">단독주택</label>
	                        <label><input type="radio" name="ques3" value="빌라/다세대">빌라/다세대</label>
	                        <label><input type="radio" name="ques3" value="원룸">원룸</label>
	                        <label><input type="radio" name="ques3" value="기타">기타</label>
                        </c:if>
                        <c:if test="${ requestScope.adoList[0].ques3 eq '단독주택' }">
	                        <label><input type="radio" name="ques3" value="아파트" required/>아파트</label>
	                        <label><input type="radio" name="ques3" value="단독주택"  checked="checked">단독주택</label>
	                        <label><input type="radio" name="ques3" value="빌라/다세대">빌라/다세대</label>
	                        <label><input type="radio" name="ques3" value="원룸">원룸</label>
	                        <label><input type="radio" name="ques3" value="기타">기타</label>
                        </c:if>
                        <c:if test="${ requestScope.adoList[0].ques3 eq '빌라/다세대' }">
	                        <label><input type="radio" name="ques3" value="아파트" required />아파트</label>
	                        <label><input type="radio" name="ques3" value="단독주택">단독주택</label>
	                        <label><input type="radio" name="ques3" value="빌라/다세대" checked="checked">빌라/다세대</label>
	                        <label><input type="radio" name="ques3" value="원룸">원룸</label>
	                        <label><input type="radio" name="ques3" value="기타">기타</label>
                        </c:if>
                        <c:if test="${ requestScope.adoList[0].ques3 eq '원룸' }">
	                        <label><input type="radio" name="ques3" value="아파트" required/>아파트</label>
	                        <label><input type="radio" name="ques3" value="단독주택">단독주택</label>
	                        <label><input type="radio" name="ques3" value="빌라/다세대">빌라/다세대</label>
	                        <label><input type="radio" name="ques3" value="원룸"  checked="checked">원룸</label>
	                        <label><input type="radio" name="ques3" value="기타">기타</label>
                        </c:if>
                        <c:if test="${ requestScope.adoList[0].ques3 eq '기타' }">
	                        <label><input type="radio" name="ques3" value="아파트" required />아파트</label>
	                        <label><input type="radio" name="ques3" value="단독주택">단독주택</label>
	                        <label><input type="radio" name="ques3" value="빌라/다세대">빌라/다세대</label>
	                        <label><input type="radio" name="ques3" value="원룸">원룸</label>
	                        <label><input type="radio" name="ques3" value="기타" checked="checked">기타</label>
                        </c:if>
                        </p>
                    </div>
        
                    <div class="radio-box">
                        <p><b>4. 중성화 수술에 대한 의견에 표시해주세요.</b>
                        <c:if test="${ requestScope.adoList[0].ques4 eq '찬성' }">
	                        <label><input type="radio" name="ques4" value="찬성" required checked="checked"/>찬성</label>
	                        <label><input type="radio" name="ques4" value="반대">반대</label>
	                        <label><input type="radio" name="ques4" value="잘 모르겠다">잘 모르겠다</label>
                        </c:if>
                        <c:if test="${ requestScope.adoList[0].ques4 eq '반대' }">
	                       	<label><input type="radio" name="ques4" value="찬성" required />찬성</label>
	                        <label><input type="radio" name="ques4" value="반대" checked="checked">반대</label>
	                        <label><input type="radio" name="ques4" value="잘 모르겠다">잘 모르겠다</label>
                        </c:if>
                        <c:if test="${ requestScope.adoList[0].ques4 eq '잘모르겠다' }">
	                        <label><input type="radio" name="ques4" value="찬성" required />찬성</label>
	                        <label><input type="radio" name="ques4" value="반대">반대</label>
	                        <label><input type="radio" name="ques4" value="잘모르겠다" checked="checked">잘 모르겠다</label>
                        </c:if>
                        </p>
                    </div>
        
                    <div class="radio-box">
                         <p><b>5. 반려동물을 키워보신 적이 있으십니까?</b>
                        <c:if test="${ requestScope.adoList[0].ques5 eq '네' }">
	                        <label><input type="radio" name="ques5" value="네" required checked="checked"/>네</label>
	                        <label><input type="radio" name="ques5" value="아니오">아니오</label>    
                        </c:if> 
                         <c:if test="${ requestScope.adoList[0].ques5 eq '아니요' }">
	                        <label><input type="radio" name="ques5" value="네" required />네</label>
	                        <label><input type="radio" name="ques5" value="아니오" checked="checked">아니오</label>    
                        </c:if>           
                    </p>
                    </div>
                    
                    <div class="text-box">
                        <label for="mention"><b>6. 하고 싶은 말씀이 있다면 자유롭게 적어주세요.</b></label><br><br>
                        <p><textarea name="appCont" cols="100" rows="10"><c:out value="${ requestScope.adoList[0].appCont }"/></textarea></p>
                    </div>

                    <button class="ok" type="submit">신청서 수정완료</button>
                    <br> <br>
                </form>
            </div>
          </div><!--box-->
        </div> <!--container close-->

	<jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>
   
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>
