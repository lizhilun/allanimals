<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/button.css" />
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/mypage/apply2.css" />
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <script src="http://code.jquery.com/jquery-3.6.0.min.js"></script>  
    
  </head>

  <body>

	<jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
	<jsp:include page="/WEB-INF/views/mypage/mypageSidebar.jsp"/>

    <!-- 이 밑에서부터 작업하시면 됩니다.-->
    <div class="container">
        <div id="box">
            <div id="box2">
              <div id="title">
          
                  <h3><b>입양 신청서 조회</b></h3>
                  <h5><b>신청인 인적사항</b></h5>
          
              <table>    
                  <tr>
                      <td width="30">1</td>
                      <th width="110">신청인 성명</th>
                      <td><c:out value="${ requestScope.adoList[0].memList[0].memName }"/></td>
                  </tr>
                  <tr>
                      <td>2</td>
                      <th>성별</th>
                      <td>
                      <c:choose>
			            <c:when test="${ requestScope.adoList[0].marry eq '기혼' }">
			               기혼
			            </c:when>
			            <c:when test="${ requestScope.adoList[0].marry eq '미혼' }">
			               미혼
			            </c:when>
			          </c:choose>
                      </td>
                  </tr>
                  <tr>
                      <td>3</td>
                      <th>생년월일</th>
                      <td><c:out value="${ requestScope.adoList[0].memList[0].memBirth }"/></td>
                  </tr>
                  <tr>
                      <td>4</td>
                      <th>전화번호</th>
                      <td><c:out value="${ requestScope.adoList[0].memList[0].memPhone }"/></td>
                  </tr>
                  <tr>
                      <td>5</td>
                      <th>이메일</th>
                      <td><c:out value="${ requestScope.adoList[0].memList[0].memEmail }"/></td>
                  </tr>
                  <tr>
                      <td>6</td>
                      <th>거주 지역</th>
                      <td><c:out value="${ requestScope.adoList[0].memList[0].memAddre }"/></td>
                  </tr>
                  <tr>
                      <td>7</td>
                      <th>통화 가능 시간</th>
                      <td>
			            <c:choose>
			            <c:when test="${ requestScope.adoList[0].callTime eq '06:00~08:00' }">
			               06:00~08:00
			            </c:when>
			            <c:when test="${ requestScope.adoList[0].callTime eq '08:00~10:00' }">
			               08:00~10:00
			            </c:when>
			            <c:when test="${ requestScope.adoList[0].callTime eq '10:00~12:00' }">
			               10:00~12:00
			            </c:when>
			            <c:when test="${ requestScope.adoList[0].callTime eq '12:00~14:00' }">
			               12:00~14:00
			            </c:when>
			            <c:when test="${ requestScope.adoList[0].callTime eq '14:00~16:00' }">
			               14:00~16:00
			            </c:when>
			            <c:when test="${ requestScope.adoList[0].callTime eq '16:00~18:00' }">
			               16:00~18:00
			            </c:when>
			            <c:when test="${ requestScope.adoList[0].callTime eq '18:00~20:00' }">
			               18:00~20:00
			            </c:when>
			            <c:when test="${ requestScope.adoList[0].callTime eq '20:00~22:00' }">
			               20:00~22:00
			            </c:when>
			            <c:when test="${ requestScope.adoList[0].callTime eq '22:00~24:00' }">
			               22:00~24:00
			            </c:when>
			            <c:otherwise>
			              선택시간 없음
			            </c:otherwise>
			            </c:choose>
                      </td>
                  </tr>
                  <tr>
                      <td>8</td>
                      <th>직업</th>
                      <td>
                      <c:out value="${ requestScope.adoList[0].memJob }"/>
                      </td>
                  </tr>
              </table>
              <br><br>
      
              <div id="content">
                  <h5><b>설문 답변 내역</b></h5>
                  <table>
                          <tr>
                              <td width="30">1</td>
                              <th width="320">동거 가족의 동의 여부를 선택해주세요.</th>
                              <td>
                               <c:choose>
                               <c:when test="${ requestScope.adoList[0].ques1 eq '전체동의' }">
			             		  전체동의
			          		   </c:when>
			          		   <c:when test="${ requestScope.adoList[0].ques1 eq '비동의' }">
			             		  비동의
			          		   </c:when>
			          		   <c:when test="${ requestScope.adoList[0].ques1 eq '일부동의' }">
			             		  일부동의
			          		   </c:when>
			           		   <c:otherwise>
					              선택사항 없음
					           </c:otherwise>
					           </c:choose> 
                              </td>
                          </tr>
                          <tr>
                              <td>2</td>
                              <th>현재 집에서 다른 동물을 키우고 계십니까?</th>
                              <td>
                              <c:choose>
                               <c:when test="${ requestScope.adoList[0].ques2 eq '네' }">
			             		 네
			          		   </c:when>
			          		   <c:when test="${ requestScope.adoList[0].ques2 eq '아니요' }">
			             		 아니요
			          		   </c:when>
			          		   <c:otherwise>
					              선택사항 없음
					           </c:otherwise>
                              </c:choose>
                              </td>
                          </tr>
                          <tr>
                              <td>3</td>
                              <th>거주하고 계신 주택 형태에 표시해주세요.</th>
                              <td>
                              <c:choose>
                               <c:when test="${ requestScope.adoList[0].ques3 eq '아파트' }">
			             		  아파트
			          		   </c:when>
			          		   <c:when test="${ requestScope.adoList[0].ques3 eq '단독주택' }">
			             		  단독주택
			          		   </c:when>
			          		   <c:when test="${ requestScope.adoList[0].ques3 eq '빌라/다세대' }">
			             		 빌라/다세대
			          		   </c:when>
			          		   <c:when test="${ requestScope.adoList[0].ques3 eq '원룸' }">
			             		  원룸
			          		   </c:when>
			          		   <c:when test="${ requestScope.adoList[0].ques3 eq '기타' }">
			             		  기타
			          		   </c:when>
			          		   <c:otherwise>
					              선택사항 없음
					           </c:otherwise>
                              </c:choose>
                              </td>
                          </tr>
                          <tr>
                              <td>4</td>
                              <th>중성화 수술에 대한 의견에 표시해주세요.</th>
                              <td>
                              <c:choose>
                               <c:when test="${ requestScope.adoList[0].ques4 eq '찬성' }">
			             		  찬성
			          		   </c:when>
			          		   <c:when test="${ requestScope.adoList[0].ques4 eq '반대' }">
			             		  반대
			          		   </c:when>
			          		   <c:when test="${ requestScope.adoList[0].ques4 eq '잘 모르겠다' }">
			             		  잘 모르겠다
			          		   </c:when>
			          		   <c:otherwise>
					              선택사항 없음
					           </c:otherwise>
                              </c:choose>
                              </td>
                          </tr>
                          <tr>
                              <td>5</td>
                              <th>반려동물을 키워보신 적이 있으십니까?</th>
                              <td>
                              <c:choose>
                               <c:when test="${ requestScope.adoList[0].ques5 eq '네' }">
			             		  네
			          		   </c:when>
			          		   <c:when test="${ requestScope.adoList[0].ques5 eq '아니요' }">
			             		  아니요
			          		   </c:when>
			          		   <c:otherwise>
					              선택사항 없음
					           </c:otherwise>
                              </c:choose>
                              </td>
                          </tr>
                          <tr>
                              <td>6</td>
                              <th>하고 싶은 말씀이 있다면 자유롭게 적어주세요.</th>
                              <td><c:out value="${ requestScope.adoList[0].appCont }"/></td>
                          </tr>
                  </table>
                  <br><br>
              </div>
              <button type="button" onclick="location.href='${ pageContext.servletContext.contextPath }/apply3?appNum=${ requestScope.adoList[0].appNum }';">수정</button>
              <button type="button" id="send-delete">삭제</button>
              <button type="button" onclick="location.href='${ pageContext.servletContext.contextPath }/apply';">취소</button>
              </div>
            </div>
          </div><!--box-->
          
    </div><!-- container close -->

	<script>
		$("#send-delete").click(function(){
			
			const applyNum = '${requestScope.adoList[0].appNum}';
			
			
			 $.ajax({
				
				url : "${ pageContext.servletContext.contextPath }/deleteApply",
				type : "get",
				data : {applyNum : applyNum},
				success : function(data){
					
					alert("삭제되었습니다!");
					location.href='${ pageContext.servletContext.contextPath }/apply';
				},
				error : function(error){
					console.log(error);
				}
			}); 
		});
	</script>

   <jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>
   
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>
