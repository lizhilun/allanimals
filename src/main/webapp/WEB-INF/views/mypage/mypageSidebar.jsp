<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>     
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/sidebar.css" />
  </head>
  <body>
    <input type="checkbox" id="menuicon" checked />
    <!--checked 속성은 버튼을 누르지 않고도 사이드바가 나온 상태로 화면에 띄움-->
    <label for="menuicon">
      <span></span>
      <span></span>
      <span></span>
      <!-- span 이 있어야 버튼이 나옵니다-->
    </label>
    <div class="sidebar">
      <!--sidebar menu-->
      <a href="${ pageContext.servletContext.contextPath }/main"
        ><img
          src="${ pageContext.servletContext.contextPath }/resources/upload/images/KakaoTalk_20220713_121409010.png"
          alt="logo"
        />
      </a>

      <div class="accordianMenu">
       <c:if test="${ !empty sessionScope.loginMember }">
	        <p>
	        <c:out value="${ sessionScope.loginMember.memName }"/>
	        님</p>
        </c:if>
        <br>
        <button class="accordion" onclick="location.href='${ pageContext.servletContext.contextPath }/member/userInfo';">개인정보 수정</button>

        <button class="accordion" >책갈피</button>
        <div class="panel">
          <li><a href="${ pageContext.servletContext.contextPath }/interest/bookmark?type=interestBox" class="accmenu">관심있는 동물</a></li>
          <li><a href="${ pageContext.servletContext.contextPath }/daily/bookmark?type=interestBox" class="accmenu">관심있는 게시글</a></li>
        </div>
        <button class="accordion">입양</button>
        <div class="panel">
          <li><a href="${ pageContext.servletContext.contextPath }/protection?type=protect" class="accmenu">보호중인 동물들</a></li>
          <li><a href="${ pageContext.servletContext.contextPath }/apply" class="accmenu">신청한 신청서</a></li>
        </div>

        <button class="accordion" onclick="location.href='${ pageContext.servletContext.contextPath }/withdraw';">회원탈퇴</button>
        
        <button
        class="help-btn"
        type="submit"
        onclick="location.href='${ pageContext.servletContext.contextPath }/rescueRequest';">
        Help!
      </button>
      </div>

      <!-- accordion menu javascript -->
      <script>
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
          acc[i].addEventListener("click", function () {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
              panel.style.maxHeight = null;
            } else {
              panel.style.maxHeight = panel.scrollHeight + "px";
            }
          });
        }
      </script>
      <!-- accordion menu javascript end -->
    </div>
    <!--sidebar menu end-->
  </body>
</html>
