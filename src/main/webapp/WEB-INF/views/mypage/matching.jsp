<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>          
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/button.css" />
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/mypage/matching.css" />
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    
  </head>

  <body>
   
   <jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
	<jsp:include page="/WEB-INF/views/mypage/mypageSidebar.jsp"/>

    <!-- 이 밑에서부터 작업하시면 됩니다.-->
    <div class="container">
        <div id="box">
            <div id="box2">
              <div id="title">
                  <h3><b>신청자 개인정보</b></h3>
                  <p>정보를 확인하시고 신청자에게 연락해주세요!</p>
                  <p>현재 다른분들의 산청서는 대기상태입니다.</p>
                  <p>입양이 완료되면 다른사람들의 신청서는 자동 반려됩니다.</p>
                  <br>
                  <h4 style="text-align: left; margin-left: 170px;"><b>신청인 인적사항</b></h4>
          
              <table>    
                  <tr>
                      <td width="60">1</td>
                      <th width="200">신청인 성명</th>
                      <td><c:out value="${ requestScope.adoList[0].memList[0].memName }"/></td>
                  </tr>
                  <tr>
                      <td>2</td>
                      <th>결혼여부</th>
                      <td><c:out value="${ requestScope.adoList[0].marry }"/></td>
                  </tr>
                  <tr>
                      <td>3</td>
                      <th>생년월일</th>
                      <td><c:out value="${ requestScope.adoList[0].memList[0].memBirth }"/></td>
                  </tr>
                  <tr>
                      <td>4</td>
                      <th>전화번호</th>
                      <td><c:out value="${ requestScope.adoList[0].memList[0].memPhone }"/></td>
                  </tr>
                  <tr>
                      <td>5</td>
                      <th>이메일</th>
                      <td><c:out value="${ requestScope.adoList[0].memList[0].memEmail }"/></td>
                  </tr>
                  <tr>
                      <td>6</td>
                      <th>거주 지역</th>
                      <td><c:out value="${ requestScope.adoList[0].memList[0].memAddre }"/></td>
                  </tr>
                  <tr>
                      <td>7</td>
                      <th>통화 가능 시간</th>
                      <td><c:out value="${ requestScope.adoList[0].callTime }"/></td>
                  </tr>
                  <tr>
                      <td>8</td>
                      <th>직업</th>
                      <td><c:out value="${ requestScope.adoList[0].memJob }"/></td>
                  </tr>
              </table>
              <br><br>
              
            </div>
            <button type="button" onclick="location.href='${ pageContext.servletContext.contextPath }/perfectMaching?memId=${ requestScope.adoList[0].memList[0].memId }&appNum=${ requestScope.adoList[0].appNum }&aniNum=${ requestScope.adoList[0].aniNum }';">입양완료</button>
            <button type="button" onclick="location.href='${ pageContext.servletContext.contextPath }/deleteMaching?memId=${ requestScope.adoList[0].memList[0].memId }&appNum=${ requestScope.adoList[0].appNum }';">입양취소</button>
           
          </div>
          </div><!--box-->
          
    </div><!-- container close -->

   <jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>
   
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>
