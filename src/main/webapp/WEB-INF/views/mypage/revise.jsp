<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/button.css" />
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/mypage/revise.css" />
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="http://code.jquery.com/jquery-3.6.0.min.js"></script>
  </head>

  <body>
   
	<jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
	<jsp:include page="/WEB-INF/views/mypage/mypageSidebar.jsp"/>


    <!-- 이 밑에서부터 작업하시면 됩니다.-->
    <div class="container">
        <div class="main">
            <div class="box1">
                <div class="box3"> 
                      <ul class="im">
                        <li><img src="${ pageContext.servletContext.contextPath}/resources/upload/images/mypage/user.png"></li>
                        <li><img src="${ pageContext.servletContext.contextPath}/resources/upload/images/mypage/calendar.png"></li>
                        <li><img src="${ pageContext.servletContext.contextPath}/resources/upload/images/mypage/email.png"></li>
                        <li><img src="${ pageContext.servletContext.contextPath}/resources/upload/images/mypage/phone.png"></li>
                        <li><img src="${ pageContext.servletContext.contextPath}/resources/upload/images/mypage/address.png"></li>
                      </ul>  
                     <%--  action="<%= request.getContextPath() %>/revise" --%>
                   
                    <form id="fm1">
                        <input type="text" placeholder="이름" name="memName" value="${ requestScope.memberInfo.memName }">
                        <input type="date" placeholder="생년월일" name="memBirth" value="${ requestScope.memberInfo.memBirth }">
                        <input type="email" placeholder="이메일" name="memEmail" value="${ requestScope.memberInfo.memEmail }">
                        <input type="text" placeholder="전화번호" name="memPhone" value="${ requestScope.memberInfo.memPhone }">
                        <input type="text" placeholder="주소" name="memAddre" value="${ requestScope.memberInfo.memAddre }">
                             <div>
                            <button type="button" class="revise" data-bs-toggle="modal" data-bs-target="#staticBackdrop">수정</button>
                           
                        </div>
                        <button type="button" class="x" onclick="location.href='${ pageContext.servletContext.contextPath }/member/userInfo';">취소</button>
                        
                    </form>
  
                         <!-- Modal -->
                        <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                         <div class="modal-dialog">
                         <div class="modal-content">
                         <div class="modal-header">
                           <h5 class="modal-title" id="staticBackdropLabel">개인정보 수정을 위한 비밀번호 확인</h5>
                          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                          </div>
                         <div class="modal-body">
                            <p>개인정보 수정을 위해 <br>
                              현재 비밀번호를 한번 더 입력해주세요.
                            </p>
                            <br><br>
                            <div class="mb-3 row">
                              <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
                              <div class="col-sm-10">
                                <input type="password" class="form-control" id="inputPassword">
                                <!-- ajax로 성공이면 myinfofh 돌아가고 아니면 틀렸다는 안내창이 먼저 나오고 그쪽으로 커서를 다시 보내준다. -->
                              </div>
                            </div>
                            <br><br>
                            
                           </div>
                       <div class="modal-footer">
                     <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">취소</button>
                     <button type="submit" class="btn btn-primary"id="send-pass"  onclick="checkPassword();" > 확인</button>
                     <script>
                     
                      function checkPassword(){
                    	  
                    	  
                    	  const memPwd = $("#inputPassword").val();
                    	  
                    	  $.ajax({
                    		 url : "${ pageContext.servletContext.contextPath }/check/pwd",
                    		 type : "post",
                    		 data : {memPwd : memPwd},
                    		 success : function(data){
                    			 if(data == "true"){
                    			 alert("회원정보가 수정되었습니다.");
                    				$("#fm1").attr("action","${ pageContext.servletContext.contextPath }/revise").attr("method","post").submit();
                    			
                    			 } else {
                    			 alert("비밀번호가 일치하지 않습니다.");
                    				 $("#inputPassword").select();
                    			 }
                    		 },
                    		 error : function(error){
                    			 
                    			 console.log(error);
                    			 result = false;
                    		 }	  
                    	  });
          				}
                     
                     </script>
                       </div>
                      </div>
                    </div>
                  </div>
  
                </div>
            </div>
        </div>
    </div><!-- container close -->

   <jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>
    
    
    
    <script>
      let myModal = document.getElementById('myModal')
      let myInput = document.getElementById('myInput')
      
      myModal.addEventListener('shown.bs.modal', function () {
        myInput.focus()
      })
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>
