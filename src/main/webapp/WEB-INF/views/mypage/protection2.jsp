<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/button.css" />
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/mypage/protection2.css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="http://code.jquery.com/jquery-3.6.0.min.js"></script>  
  </head>

  <body>
   
    <jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
	<jsp:include page="/WEB-INF/views/mypage/mypageSidebar.jsp"/>

    <!-- 이 밑에서부터 작업하시면 됩니다.-->
    <div class="container">
        <div id="box">
        
            <div id="box2">
                <div class="page-title">
                <h3>입양신청 목록 조회</h3>
                </div>
                 
                  <!-- 게시판 글 목록 -->
                  <div id="board-list">
                    <div class="content">
                      <table class="board-table">
                      <thead>
                        <tr>
                          <th scope="col" class="th-num">번호</th>
                          <th scope="col" class="th-name">회원 이름</th>
                          <th scope="col" class="th-petName">회원아이디</th>
                          <th scope="col" class="th-petName">입양 진행상태</th>
                          <th scope="col" class="th-date">등록일</th>
                          <th scope="col" class="th-detail">상세보기</th>
                        </tr>
                      </thead>
          
                      <tbody>
          			<c:forEach var="apply" items="${ requestScope.applyList }"> 
                        <tr>
                          <td><c:out value="${ apply.appNum }"/></td>
                          <td><c:out value="${ apply.memList[0].memName }"/></td>
                          <td><c:out value="${ apply.memList[0].memId }"/></td>
                          <td><c:out value="${ apply.appSituation }"/></td>
                          <td><c:out value="${ apply.appTime }"/></td>
                          <td><button type="button" onclick="location.href='${ pageContext.servletContext.contextPath }/protection3?memId=${ apply.memList[0].memId }&appNum=${ apply.appNum }&aniNum=${ apply.aniList[0].aniNum }'">more</button></td>
                        </tr>
					</c:forEach>                        
                      </tbody>
                     <%--  <c:out value="${ applyList.aniList[0].aniNum }"/>  --%>
                      </table>
                    </div>
                  </div>  
                <button id="success" type="button" onclick="location.href='${ pageContext.servletContext.contextPath }/adoptionComplete?aniNum=${ requestScope.applyList[0].aniList[0].aniNum }';">입양완료 처리하기</button>
                <button id="back" type="button" onclick="location.href='${ pageContext.servletContext.contextPath }/protection?type=protect';">뒤로가기</button>
                </div>
          </div><!--box-->
    </div><!-- container close -->

	<jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>
   
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>
