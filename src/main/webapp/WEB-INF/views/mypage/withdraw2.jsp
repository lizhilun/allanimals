<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/button.css" />
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/mypage/withdraw2.css" />

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
 <script src="http://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body>
   
   	<jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
   
      <div class="container" style="margin-left: 100px">
        <div id="box">
          <br>
          <br>
          <p style="text-align: center;">그동안 감사했습니다<br>
            언제든 다시 들려주세요
          </p>
          <br>
          <br>
        </div><!-- box close -->
      <button class="bt" onclick="location.href='${ pageContext.servletContext.contextPath}/main';">안녕히 계세요</button>
      </div><!--container close-->


  
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    </body>
</html>