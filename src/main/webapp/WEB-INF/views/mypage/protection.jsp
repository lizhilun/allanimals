<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>     
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>    
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/button.css" />
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/mypage/protection.css" />
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/allDaily/template.css">
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/allDaily/reviewList.css">
    
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    
  </head>

  <body>
   
	<jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
	<jsp:include page="/WEB-INF/views/mypage/mypageSidebar.jsp"/>
	

    <!-- 이 밑에서부터 작업하시면 됩니다.-->
    <div class="container">
        <div id="box" style="height: auto;">
          <div id="box2" style="height: auto;">
             <div class="features">
                  <div class="content">
                   <div class="page-title">
           			 <h3>보호중인 동물들</h3>
           		   </div>
	         		  <ul class="clearfix" style="width: 1450px;" >
		     		   <c:forEach items="${ requestScope.introList }" var="intro">
		            	<li style="width: 450px;">
					              <div class="item">
					                <div class="thumb">
					                  <a onclick="href='${ pageContext.servletContext.contextPath }/write/list?introNum=${ intro.introNum }'"><img src="<c:out value='${ intro.thumPath }'/>" alt="1"></a> <!-- 동물 사진을 넣어주세요. -->
					                </div>
					                <div class="desc">
					<!--                 제목 -->
					                  <h5><a onclick="href='${ pageContext.servletContext.contextPath }/write/list?introNum=${ intro.introNum }'"><c:out value="${ intro.introTitle }"/></a></h5>
					<!--                   내용 -->
					                  <p><c:out value="어떤 댓글이 달렸는지 확인해보세요!"/></p>
					                  <div class="detail">
					<!--                   날짜 -->
					                    <span><c:out value="${ intro.introTime }"/></span>
					<!--                     아이디 -->
					                    <span><c:out value="${ intro.memId }"/></span>
					                    <span class="likes"><i class="fa fa-heart"></i></span>
					                  </div>
					            <button type="button" onclick="location.href='${ pageContext.servletContext.contextPath}/protection2?aniNum=${ intro.aniNum }'">들어온 신청서</button>
					                </div>
					              </div>
					            </li>
							</c:forEach>
					     </ul>
					</div>
                 <jsp:include page="../common/paging.jsp"/>
                </div>
          </div><!-- box2 -->
    
        </div><!-- box -->
    </div><!-- container close -->

	<jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>
   
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>
