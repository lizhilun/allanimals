<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
  
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/button.css" />
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/mypage/withdraw.css" />

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
   <script src="http://code.jquery.com/jquery-3.6.0.min.js"></script>
  </head>

  <body>
   
   <jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
	<jsp:include page="/WEB-INF/views/mypage/mypageSidebar.jsp"/>

    <!-- 이 밑에서부터 작업하시면 됩니다.-->
    <div class="container">
        <div id="box">
          <div id="box1" style="overflow: auto;"> 
          <h3>회원탈퇴 약관</h3>
          <p>주식회사 AA(이하 “회사”)는 「개인정보보호법」 등 관련법령에 따라 정보주체의 개인정보 및 권익을 보호하고, <br>
            개인정보와 관련한 정보주체의 고충을 원활하게 처리할 수 있도록 다음과 같은 개인정보 처리방침을 두고 있습니다.
            <br><br>
            제 1 조 개인정보 처리 항목
            
            <br><br>
            회사는 서비스를 위한 목적에 필요한 범위에서 최소한의 개인정보를 다음과 같이 처리합니다.
            
            서비스 회원가입 및 관리
            
            필수항목 : 이름, 아이디, 비밀번호, 연락처, 주소, 이메일, 생년월일
            
            민원사무 처리
            
            이름, 아이디, 주소, 연락처 등 상담을 위해 필요한 회원의 등록정보
            
            재화 또는 서비스 제공
            
            이름, 아이디, 이메일, 주소, 연락처 등 서비스 수령을 위한 정보
            
            마케팅 및 광고에의 활용
            
            이름, 연락처, 생년월일, 서비스 이용정보, 회원이 사이트 내에 게시·등록한 콘텐츠 등
            
            서비스 개발 및 개선
            
            성별, 나이, 지역, 이용후기(글, 사진, 영상 등 콘텐츠 포함), 관심글 조회 등
            회사는 위 개인정보 처리와 관련하여 특정 개인을 알아볼 수 없도록 비식별화 조치를 취한 후 조치가 완료된 정보를 이용 및 제3자에게 제공합니다.
            
            서비스 이용 과정에서 IP주소, 쿠키, 서비스 이용 기록, 기기정보, 위치정보가 생성되어 수집될 수 있습니다.
            
            
            <br><br>
            제 2 조 개인정보 수집·이용 및 목적
            <br><br>
            회사는 서비스를 위한 목적에 필요한 범위에서 최소한의 개인정보를 수집 및 이용합니다. 수집한 개인정보는 다음의 목적 외 용도로는 이용되지 않으며, 이용 목적이 변경될 경우 회원의 사전 동의를 구합니다.
            
            <br><br>
            서비스 회원가입 및 관리
            
            서비스 가입의사 확인, 회원자격 유지·관리, 서비스 부정이용 방지, 고충처리, 분쟁 조정을 위한 기록 보존 등
            
            민원사무 처리
            
            민원인의 신원 확인, 민원사항 확인, 처리결과 통보 등
            
            재화 또는 서비스 제공
            
            서비스 제공, 콘텐츠 제공, 본인인증, 맞춤 서비스 제공 등
            
            마케팅 및 광고에의 활용
            
            이벤트 및 광고성 정보 제공 및 참여기회 제공, 마케팅 및 광고 콘텐츠제작 및 게재 등
            
            서비스 개발 및 개선
            
            신규 서비스 및 제품 개발, 맞춤 서비스 제공, 서비스의 유효성 확인, 접속 빈도파악, 회원의 서비스 이용에 대한 통계 분석 등
            
            
            <br><br>
            제 3 조 개인정보의 제공 및 위탁
            
            <br><br>
            회사는 회원의 동의, 법률의 특별한 규정 등 「개인정보보호법」제17조 및 제18조에 해당하는 경우에만 개인정보를 제3자에게 제공합니다. 
            
            <br><br>
            회사는 원칙적으로 회원의 개인정보를 회원 탈퇴 시까지 보유하며, 회원 탈퇴 시 지체없이 파기합니다. 단, 이용자에게 개인정보 보관기간에 대해 별도의 동의를 얻은 경우, 또는 법령에서 일정 기간 정보보관 의무를 부과하는 경우에는 해당 기간 동안 개인정보를 안전하게 보관합니다.
            
            부정이용기록
            
            부정이용기록은 수집 시점으로부터 6개월 간 보관하고 파기합니다.
            
            「전자상거래 등에서 소비자 보호에 관한 법률」
            
            표시·광고에 관한 기록 : 6개월
            계약 또는 청약철회 등에 관한 기록 : 5년 보관
            대금결제 및 재화 등의 공급에 관한 기록 : 5년 보관
            소비자의 불만 또는 분쟁처리에 관한 기록 : 3년 보관
            
            「전자금융거래법」
            
            전자금융에 관한 기록 : 5년 보관
            
            「통신비밀보호법」
            
            로그인 기록 등 방문 기록 : 3개월
            
            회원탈퇴, 서비스 종료, 이용자에게 동의 받은 개인정보 보유기간의 도래와 같이 개인정보의 수집 및 이용목적이 달성된 개인정보는 재생이 불가능한 방법으로 파기합니다. 법령에서 보존의무를 부과한 정보는 해당 법령에서 정한 기간이 경과한 후 지체없이 재생이 불가능한 방법으로 파기합니다.
            회사는 ‘개인정보 유효기간제’에 따라 1년 간 로그인하지 않거나 서비스를 이용하지 않은 회원의 개인정보를 별도로 분리하여 보관 및 관리합니다.
            
            
            <br><br>
            제 4 조 정보주체 및 법정대리인의 권리와 행사 방법
            
            <br><br>
            회원은 회사에 대해 언제든지 서면, 전자우편 등의 방법으로 개인정보 열람·정정·삭제·처리정리 요구 등의 권리를 행사할 수 있습니다.
            제1항에 따른 권리 행사는 회원의 법정대리인이나 위임을 받은 자 등 대리인을 통하여 하실 수 있습니다.
            회원의 개인정보의 오류에 대한 정정을 요청한 경우, 정정을 완료하기 전까지 해당 개인정보를 이용 또는 제공하지 않습니다. 또한 잘못된 개인정보를 제3자에게 이미 제공한 경우에는 정정 처리결과를 제3자에게 지체 없이 통지하여 정정이 이루어지도록 하겠습니다.
            개인정보 열람 및 처리정지 요구는 「개인정보 보호법」 제37조제2항 및 관련 법령에 의하여 회원의 권리가 제한될 수 있습니다.
            
            
            <br><br>
            제 5 조 개정 전 고지의무
            <br><br>
            본 개인정보처리방침의 내용 추가, 삭제 및 수정이 있을 경우 최소 7일 전에 ‘공지사항’을 통해 사전 공지하겠습니다. 다만, 수집하는 개인정보의 항목, 이용목적의 변경 등과 같이 이용자 권리의 중대한 변경이 발생할 때에는 최소 30일 전에 공지하며, 필요 시 이용자 동의를 다시 받을 수도 있습니다.
            
            - 공고일자 : 2021년 3월 22일
            - 시행일자 : 2021년 3월 22일
            - 시행일자(2021년 3월 22일 이전 가입 회원) : 2021년 4월 22일
            
            이전 개인정보처리방침 보기</p>
            
          </div>
          <div id="box2" style="overflow: auto;"> 
          <br>  
          <h5>회원탈퇴 전 유의사항을 확인해주세요</h3>
          <br>
          <p style="text-align: left;margin-left: 10px; word-spacing: 5px; line-height: 25px; "> 
            - 부정 이용 방지를 위해 회원탈퇴 후 48시간 이내로 재가입 불가능합니다.<br>
            - 회원탈퇴 후 개인정보가 삭제되면 어떠한 방법으로도 복원할 수 없습니다.<br>
            - 진행중인 입양이 있으신 경우 해당 사유가 해소된 후 탈퇴가 가능합니다.<br>
            - 기존에 삭제하지 않은 게시글들은 따로 삭제처리 되지 않습니다. 회원탈퇴 전 미리 게시글을 지워주세요.<br>
            <br>
            ※ 상세한 내용은 사이트 내 개인정보 취급방침을 참고 해 주세요.
          </p>  
          </div>
         <form id="fm2">
          <div class="form-check">
            <input  type="checkbox" id="chk2">
            <label class="form-check-label" for="flexCheckDefault">
               해당 내용들을 모두 확인했으며, 회원 탈퇴에 동의합니다.
            </label>
          </div>
         </form>
  
          <br><br>
  			<button id="chkbox" style="margin-left: 700px; margin-bottom: 10px;" type="button" onclick="chk();">
  			확인
  			</button>
          <!-- Button trigger modal -->
            <button style="margin-left: 700px; margin-bottom: 10px; display: none" type="submit" id="chkbox2" data-bs-toggle="modal" data-bs-target="#staticBackdrop" >
              확인
            </button>
  
            <!-- Modal -->
            <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">탈퇴를 위한 본인인증</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                    <p>본인확인을 위해 비밀번호를 다시 한번 확인합니다.<br>
                       본인확인 후 최종 탈퇴가 가능합니다.
                    </p>
                    <div class="mb-3 row">
                      <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
                      <div class="col-sm-10">
                        <input type="password" class="form-control" id="inputPassword" style="font-family: 'MalgunGothic';">
                      </div>
                    </div>
                    <br><br>
                  </div>
                  <div class="modal-footer">
                    <button  type="button" data-bs-dismiss="modal">취소</button>
                    <button  type="submit" onclick="checkPassword();">확인</button>
                      <script>
                      function chk(){
                    	  const checkbox =  $("#chk2").prop("checked")

                    	  if(checkbox == true){
                    		  $("#chkbox2").click();
                    	  }else{
                    		  alert("체크박스를 체크해주세요");
                    	
                    	  }
                      }
                     
                      function checkPassword(){
                    	  
                    	  const memPwd = $("#inputPassword").val();
                    	  
                    	  $.ajax({
                    		 url : "${ pageContext.servletContext.contextPath }/check/pwd",
                    		 type : "post",
                    		 data : {memPwd : memPwd},
                    		 success : function(data){
                    			 if(data == "true"){
                    			 alert("탈퇴되었습니다.");
                    			 	$("#fm2").attr("action","${ pageContext.servletContext.contextPath }/withdraw").attr("method","post").submit();
                    
                    			 } else {
                    			 alert("비밀번호가 일치하지 않습니다.");
                    				 $("#inputPassword").select();
                    			 }
                    		 },
                    		 error : function(error){
                    			 
                    			 console.log(error);
                    			 result = false;
                    		 }	  
                    	  });
          				}
                     
                     </script>
                  </div>
  
                </div>
              </div>
            </div><!-- modal close -->
  
        </div><!-- box close -->
      </div><!--container close-->

	<jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>

    <script>
        var myModal = document.getElementById('myModal')
        var myInput = document.getElementById('myInput')
    
        myModal.addEventListener('shown.bs.modal', function () {
        myInput.focus()
        })
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>
