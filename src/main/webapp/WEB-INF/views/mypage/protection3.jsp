<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>      
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/button.css" />
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/mypage/protection3.css" />
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    
  </head>

  <body>
   
    <jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
	<jsp:include page="/WEB-INF/views/mypage/mypageSidebar.jsp"/>

    <!-- 이 밑에서부터 작업하시면 됩니다.-->
    <div class="container">
        <div id="box">
            <div id="box2">
              <div id="title">
          
                  <h3><b>입양 신청서 조회</b></h3>
              <div id="content">
                  <h5><b>설문 답변 내역</b></h5>
                  <table>
                          <tr>
                              <td width="30">1</td>
                              <th width="450">동거 가족의 동의 여부를 선택해주세요.</th>
                              <td><c:choose>
                               <c:when test="${ requestScope.adoList[0].ques1 eq '전체동의' }">
			             		  전체동의
			          		   </c:when>
			          		   <c:when test="${ requestScope.adoList[0].ques1 eq '비동의' }">
			             		  비동의
			          		   </c:when>
			          		   <c:when test="${ requestScope.adoList[0].ques1 eq '일부동의' }">
			             		  일부동의
			          		   </c:when>
			           		   <c:otherwise>
					              선택사항 없음
					           </c:otherwise>
					           </c:choose> </td>
                          </tr>
                          <tr>
                              <td>2</td>
                              <th>현재 집에서 다른 동물을 키우고 계십니까?</th>
                              <td>
                              <c:choose>
                               <c:when test="${ requestScope.adoList[0].ques2 eq '네' }">
			             		 네
			          		   </c:when>
			          		   <c:when test="${ requestScope.adoList[0].ques2 eq '아니요' }">
			             		 아니요
			          		   </c:when>
			          		   <c:otherwise>
					              선택사항 없음
					           </c:otherwise>
                              </c:choose>
                              </td>
                          </tr>
                          <tr>
                              <td>3</td>
                              <th>거주하고 계신 주택 형태에 표시해주세요.</th>
                              <td>
                              <c:choose>
                               <c:when test="${ requestScope.adoList[0].ques3 eq '아파트' }">
			             		  아파트
			          		   </c:when>
			          		   <c:when test="${ requestScope.adoList[0].ques3 eq '단독주택' }">
			             		  단독주택
			          		   </c:when>
			          		   <c:when test="${ requestScope.adoList[0].ques3 eq '빌라/다세대' }">
			             		 빌라/다세대
			          		   </c:when>
			          		   <c:when test="${ requestScope.adoList[0].ques3 eq '원룸' }">
			             		  원룸
			          		   </c:when>
			          		   <c:when test="${ requestScope.adoList[0].ques3 eq '기타' }">
			             		  기타
			          		   </c:when>
			          		   <c:otherwise>
					              선택사항 없음
					           </c:otherwise>
                              </c:choose>
                              </td>
                          </tr>
                          <tr>
                              <td>4</td>
                              <th>중성화 수술에 대한 의견에 표시해주세요.</th>
                              <td>
                              <c:choose>
                               <c:when test="${ requestScope.adoList[0].ques4 eq '찬성' }">
			             		  찬성
			          		   </c:when>
			          		   <c:when test="${ requestScope.adoList[0].ques4 eq '반대' }">
			             		  반대
			          		   </c:when>
			          		   <c:when test="${ requestScope.adoList[0].ques4 eq '잘모르겠다' }">
			             		  잘 모르겠다
			          		   </c:when>
			          		   <c:otherwise>
					              선택사항 없음
					           </c:otherwise>
                              </c:choose>
                              </td>
                          </tr>
                          <tr>
                              <td>5</td>
                              <th>반려동물을 키워보신 적이 있으십니까?</th>
                              <td>
                              <c:choose>
                               <c:when test="${ requestScope.adoList[0].ques5 eq '네' }">
			             		  네
			          		   </c:when>
			          		   <c:when test="${ requestScope.adoList[0].ques5 eq '아니요' }">
			             		  아니요
			          		   </c:when>
			          		   <c:otherwise>
					              선택사항 없음
					           </c:otherwise>
                              </c:choose>
                              </td>
                          </tr>
                          <tr>
                              <td>6</td>
                              <th>하고 싶은 말씀이 있다면 자유롭게 적어주세요.</th>
                              <td><c:out value="${ requestScope.adoList[0].appCont }"/></td>
                          </tr>
                  </table>
                  <br><br>
              </div>
                <!-- Modal -->
                <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                  <div class="modal-dialog">
                  <div class="modal-content">
                  <div class="modal-header">
                   <h5 class="modal-title" id="staticBackdropLabel">주의!!</h5>
                   <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                   </div>
                  <div class="modal-body">
                    <br><br>
                     <p>해당 회원과 입양을 진행하시겠습니까? <br>
                        입양을 진행하지 않으시면  <br>
                        해당 회원의 개인정보 조회는 불가능하며<br>
                       입양진행시 다른 신청서들은 대기상태로 변환됩니다. <br><br>
                       
                     </p>
                     <br><br>
                  
                    </div>
                <div class="modal-footer">   
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" onclick="location.href='${ pageContext.servletContext.contextPath }/protection2?aniNum=${ requestScope.adoList[0].aniList[0].aniNum }';">아니요</button>
              <button type="submit" class="btn btn-primary" onclick="location.href='${ pageContext.servletContext.contextPath }/machingMember?memId=${ requestScope.adoList[0].memId }&appNum=${ requestScope.adoList[0].appNum }';">입양을 진행하겠습니다.</button>
                </div>
                </div>
                </div>
                 </div>
                <button type="button" class="revise" data-bs-toggle="modal" data-bs-target="#staticBackdrop">개인정보 보기</button>
            </div>
            <button type="button" class="x" onclick="location.href='${ pageContext.servletContext.contextPath }/protection2?aniNum=${ requestScope.adoList[0].aniList[0].aniNum }';">뒤로가기</button>
            </div><!-- box2 -->
        </div><!--box-->
    </div><!-- container close -->
         
    

	<jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>
   
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>
