<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>     
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/button.css" />
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/mypage/bookmark2.css" />
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/allDaily/template.css">
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/allDaily/reviewList.css">
    
 	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/da4b1b94bb.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    
  </head>

  <body>

	<jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
	<jsp:include page="/WEB-INF/views/mypage/mypageSidebar.jsp"/>
	

    <!-- 이 밑에서부터 작업하시면 됩니다.-->
    <div class="container" style="margin-top: 100px;margin-bottom: 100px;">
        <h2 id="title">관심있는 게시글</h2>
        <div id="box">
          <select id="select" class="form-select form-select-lg mb-3 col-md-8 offset-md-8" aria-label=".form-select-lg" onchange="window.open(value,'_self');">
            <option selected>잃어버린 동물</option>
            <option value="${ pageContext.servletContext.contextPath }/daily/bookmark?type=interestBox">동물들의 일상</option>
            <option value="${ pageContext.servletContext.contextPath }/review/bookmark?type=interestBox">입양후기</option>
            <option value="${ pageContext.servletContext.contextPath }/rescue/bookmark?type=interestBox">구조한 동물</option>
          </select>
          <div id="box2">
             <div class="features">
                  <div class="content">
                   <div class="page-title">
           			 <h3>잃어버린 동물</h3>
           		   </div>
	         		  <ul class="clearfix" style="width: 1450px;" >
			        <c:forEach items="${ requestScope.introList }" var="intro">
			            <li style="width: 455px;">
			              <div class="item">
			                <div class="thumb">
			                <!-- 사진 -->
			                  <a onclick="href='${ pageContext.servletContext.contextPath }/write/list?introNum='${ intro.introNum }">
			                  <img src="<c:out value='/allanimals/${ intro.thumPath }'/>" alt="1"></a> <!-- 동물 사진을 넣어주세요. -->
			                </div>
			                <div class="desc">
			                <input type="hidden" name="introNum" value="${ intro.introNum }">
			                  <!-- 제목 -->
			                  <h5><a onclick="href='${ pageContext.servletContext.contextPath }/write/list?introNum=${ intro.introNum }'"><c:out value="${ intro.introTitle }"/></a></h5>
			                  <!-- 내용 -->
			                  <p><a onclick="href='${ pageContext.servletContext.contextPath }/write/list?introNum=${ intro.introNum }'"><c:out value="${ intro.introCont }" escapeXml="false"/></a></p>
			                  <div class="detail">
			                  <!-- 날짜 -->
			                  <span><c:out value="${ intro.introTime }"/></span>
			                  <!-- 아이디 -->
			                  <span><c:out value="${ intro.memId }"/></span>
			                  <!-- 관심버튼 -->
			                  <span class="likes"><i class="fa fa-heart"></i></span>
			                  </div>
			                </div>
			              </div>
			            </li>
					</c:forEach>
			          </ul>
					</div>
                 <jsp:include page="../../common/paging.jsp"/>
                </div>  
          </div><!-- box2 -->
        </div><!-- box -->
    </div><!-- container close -->

	<jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>
   
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>
