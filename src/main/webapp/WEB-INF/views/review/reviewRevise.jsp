<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>입양 후기 | 게시글 수정</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>

    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/insertRevise.css"/>
  </head>
  <body>
  
  	<jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
	<jsp:include page="/WEB-INF/views/menubar/sidebar.jsp"/>
	
    <!-- 이 밑에서부터 작업하시면 됩니다.-->
    <div class="container">
    <div id="table">
      <div id="pName"><h1>입양 후기</h1></div>
      <p style="font-size: 24px; margin-bottom: 15px;">글 작성할 동물에 체크해주세요.</p>
      <hr>
      <form method="get" action="CheckboxServlet" style="margin: 15px 0 20px 0;">
        <label for="pet1"><input style="margin-right: 5px;" type="checkbox" name="pet" value="아지" required/>아지</label>
        <label for="pet2"><input style="margin-right: 5px;" type="checkbox" name="pet" value="양이">양이</label>
      </form>

      <div>
      <input type="email" class="nameBox" placeholder="제목을 입력하세요." required pattern=".*\S.*"> 
      </div>

      <br><br><br>
      <textarea id="summernote"></textarea>
      
      <div class="write">
        <button class="btn btn1" onclick="location.href='${ pageContext.servletContext.contextPath }/review/list'">작성하기</button>
        </div>

    </div>
    <script>
      $(document).ready(function() {
          $('#summernote').summernote({

            placeholder: '입양 후기 내용을 작성해주세요.',
              height:600 , 
            minHeight: 600,             // set minimum height of editor
              maxHeight: 600,             // set maximum height of editor
               focus: true   ,             
              toolbar: [
                  
  
            // [groupName, [list of button]]
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['style', ['bold', 'italic', 'underline','strikethrough', 'clear']],
            ['color', ['forecolor','color']],
            ['table', ['table']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['insert',['picture']],
            ['view', ['fullscreen', 'help']]
          ],
        fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New','맑은 고딕','궁서','굴림체','굴림','돋움체','바탕체'],
        fontSizes: ['8','9','10','11','12','14','16','18','20','22','24','28','30','36','50','72']
              
          });
          

  
      });
     
    </script>

    </div>

   	<jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>

    <!--footer end -->
  </body>
</html>