<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>입양 후기 | 글 작성</title>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/review/insertRevise.css"/>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/header.css">
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/sidebar.css">
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/footer.css">

    
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>

  </head>
  <body>
  	<jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
	<jsp:include page="/WEB-INF/views/menubar/sidebar.jsp"/>

    <div class="container">

      <div id="table">
        <div id="pName"><h1>입양 후기 글 작성</h1></div>
        <p style="font-size: 24px; margin-bottom: 15px;">글 작성할 동물을 체크해주세요</p>
        <hr>
        <form method="post" action="saveForm.jsp" style="margin: 15px 0 20px 0;" enctype="multipart/form-data">
          <label for="pet1"><input style="margin-right: 1px; width: 10px; height: 10px;" type="radio" id="pet1" name="pet" value="등록한동물">등록한동물1</label>
          <label for="pet2"><input style="margin-right: 1px; margin-left: 16px; margin-bottom: 20px; width: 10px;height: 10px;" type="radio" id="pet2" name="pet" value="등록한동물">등록한동물2</label>
        
        <div>
        <input type="text" name="afterTitle" id="title" placeholder="제목을 입력하세요." style="width: 800px;"> 
        </div>
  
		<textarea id="summernote" name="afterCont"></textarea>
		 
		<div id="btn">
          <button class="btn1" type="submit" onclick="sendData()" value="insert">작성하기</button>
		</div>    
        
        </form>

        </div>
        

        
        <script>
          $(document).ready(function() {
              $('#summernote').summernote({

                placeholder: '내용을 입력해주세요.',
                  height:600 , 
                minHeight: 600,             // set minimum height of editor
                  maxHeight: 600,             // set maximum height of editor
                   focus: true   ,             
                  toolbar: [
                      
      
                // [groupName, [list of button]]
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['style', ['bold', 'italic', 'underline','strikethrough', 'clear']],
                ['color', ['forecolor','color']],
                ['table', ['table']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert',['picture']],
                ['view', ['fullscreen', 'help']]
              ],
            fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New','맑은 고딕','궁서','굴림체','굴림','돋움체','바탕체'],
            fontSizes: ['8','9','10','11','12','14','16','18','20','22','24','28','30','36','50','72']
                  
              });
              
      
          });
         
        </script>
            
        
      </div>
      <script>
        $(document).ready(function() {
            $('#summernote').summernote({
    
                height:600 , 
              minHeight: 600,             // set minimum height of editor
                maxHeight: 600,             // set maximum height of editor
               placeholder: '입양 후기 글을 입력해주세요.',
                 focus: true   ,             
                toolbar: [
                    
    
              // [groupName, [list of button]]
              ['fontname', ['fontname']],
              ['fontsize', ['fontsize']],
              ['style', ['bold', 'italic', 'underline','strikethrough', 'clear']],
              ['color', ['forecolor','color']],
              ['table', ['table']],
              ['para', ['ul', 'ol', 'paragraph']],
              ['height', ['height']],
              ['view', ['fullscreen', 'help']]
            ],
          fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New','맑은 고딕','궁서','굴림체','굴림','돋움체','바탕체'],
          fontSizes: ['8','9','10','11','12','14','16','18','20','22','24','28','30','36','50','72']
                
            });
            
    
        });
          
        </script>
      
	 <jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>
  </body>
</html>