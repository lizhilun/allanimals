<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ page import="com.allanimals.commnon.pagin.Pagenation" %>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>입양 후기 | 목록</title>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/reviewList.css"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/da4b1b94bb.js" crossorigin="anonymous"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
    
</head>
<body>

<jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
<jsp:include page="/WEB-INF/views/menubar/sidebar.jsp"/>

      <div class="container">
    <div class="features">
        <div class="content">
          <div class="page-title">
            <h3>입양 후기</h3>
            </div>

        <!-- 메뉴 선택 -->
        <div class="menu">
          <div class="write">
            <button class="btnMenu btn1" onclick="location.href='${ pageContext.servletContext.contextPath }/review/insert'">글쓰기</button>
            </div>
          <div class="mine">
            <button class="btnMenu btn2" onclick="location.href='#'">나의 글</button>
          </div>
        </div>

        <!-- 게시판 검색창 -->
        <div id="board-search">
          <div class="content">
            <div class="search-window">
              <form action="">
                <div class="search-wrap">
                  <label for="search" class="blind"></label>
                  <input id="search" type="search" name="search" placeholder="동물 코드를 입력해주세요." value="">
                  <button type="submit" class="btn btn-dark">검색</button>
                </div>
              </form>
            </div>
          </div>
        </div>

          <ul class="clearfix">
            <li>
              <div class="item">
                <div class="thumb">
                  <a href="${ pageContext.servletContext.contextPath }/review/content?afterNum=${ reviewList.afterNum }"><img src="${ pageContext.servletContext.contextPath }/resources/upload/images/img15.jpeg" alt="1"></a> <!-- 동물 사진을 넣어주세요. -->
                </div>
                <div class="desc">
                  <h5><a href="${ pageContext.servletContext.contextPath }/review/content?afterNum=${ reviewList.afterNum }"><c:out value="${ reviewList.saveTitle }"/></a></h5>
                    <span><c:out value="${ reviewList.afterCont }"/></span>
                  <div class="detail">
                    <span><c:out value="${ reviewList.afterTime }"/></span>
                    <span><c:out value="${ reviewList.memId }"/></span>
                    <span class="likes"><i class="fa fa-heart"></i></span>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div class="item">
                <div class="thumb">
                  <a href="${ pageContext.servletContext.contextPath }/review/content?afterNum=${ reviewList.afterNum }"><img src="${ pageContext.servletContext.contextPath }/resources/upload/images/img15.jpeg" alt="1"></a> <!-- 동물 사진을 넣어주세요. -->
                </div>
                <div class="desc">
                  <h5><a href="${ pageContext.servletContext.contextPath }/review/content?afterNum=${ reviewList.afterNum }"><c:out value="${ reviewList.afterTitle}"/></a></h5>
                    <span><c:out value="${ reviewList.afterCont }"/></span>
                  <div class="detail">
                    <span><c:out value="${ reviewList.afterTime }"/></span>
                    <span><c:out value="${ reviewList.memId }"/></span>
                    <span class="likes"><i class="fa-solid fa-heart"></i></span>
                 </div>
                </div>
              </div>
            </li>
            <li>
              <div class="item">
                <div class="thumb">
                  <a href="${ pageContext.servletContext.contextPath }/review/content?afterNum=${ reviewList.afterNum }"><img src="${ pageContext.servletContext.contextPath }/resources/upload/images/img15.jpeg" alt="1"></a> <!-- 동물 사진을 넣어주세요. -->
                </div>
                <div class="desc">
                  <h5><a href="${ pageContext.servletContext.contextPath }/review/content?afterNum=${ reviewList.afterNum }"><c:out value="${ reviewList.afterTitle}"/></a></h5>
                  <p></p>
                  <div class="detail">
                    <span><c:out value="${ review.afterTime }"/></span>
                    <span><c:out value="${ review.memId }"/></span>
                    <span class="likes"><i class="fa fa-heart"></i></span>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div class="item">
                <div class="thumb">
                  <a href="${ pageContext.servletContext.contextPath }/review/content?afterNum=${ review.afterNum }"><img src="${ pageContext.servletContext.contextPath }/resources/upload/images/img15.jpeg" alt="1"></a> <!-- 동물 사진을 넣어주세요. -->
                </div>
                <div class="desc">
                  <h5><a href="${ pageContext.servletContext.contextPath }/review/content?afterNum=${ review.afterNum }"><c:out value="${ review.saveTitle}"/></a></h5>
                    <span><c:out value="${ review.afterCont }"/></span>
                  <div class="detail">
                    <span><c:out value="${ review.afterTime }"/></span>
                    <span><c:out value="${ review.memId }"/></span>
                    <span class="likes"><i class="fa-regular fa-heart"></i></span>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div class="item">
                <div class="thumb">
                  <a href="${ pageContext.servletContext.contextPath }/review/content?afterNum=${ review.afterNum }"><img src="${ pageContext.servletContext.contextPath }/resources/upload/images/img15.jpeg" alt="1"></a> <!-- 동물 사진을 넣어주세요. -->
                </div>
                <div class="desc">
                  <h5><a href="${ pageContext.servletContext.contextPath }/review/content?afterNum=${ review.afterNum }"><c:out value="${ review.saveTitle}"/></a></h5>
                    <span><c:out value="${ review.afterCont }"/></span>
                  <div class="detail">
                    <span><c:out value="${ review.afterTime }"/></span>
                    <span><c:out value="${ review.memId }"/></span>
                    <span class="likes"><i class="fa-regular fa-heart"></i></span>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div class="item">
                <div class="thumb">
                  <a href="${ pageContext.servletContext.contextPath }/review/content?afterNum=${ review.afterNum }"><img src="${ pageContext.servletContext.contextPath }/resources/upload/images/img15.jpeg" alt="1"></a> <!-- 동물 사진을 넣어주세요. -->
                </div>
                <div class="desc">
                  <h5><a href="${ pageContext.servletContext.contextPath }/review/content?afterNum=${ review.afterNum }"><c:out value="${ review.saveTitle}"/></a></h5>
                    <span><c:out value="${ review.afterCont }"/></span>
                  <div class="detail">
                    <span><c:out value="${ review.afterTime }"/></span>
                    <span><c:out value="${ review.memId }"/></span>
                    <span class="likes"><i class="fa-regular fa-heart"></i></span>
                  </div>
                </div>
              </div>
            </li>
          </div>
        
      </div>
    
      <div class="pagination">
        <a href="#">&laquo;</a>
        <a href="#">1</a>
        <a href="#" class="active">2</a>
        <a href="#">3</a>
        <a href="#">4</a>
        <a href="#">5</a>
        <a href="#">&raquo;</a>
      </div> 

    </div>

<jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>

	<script>
		$("#thumbnailArea > div").click(function(){
			const no = $(this).find("label").text();
			console.log(no);
			location.href = "${ pageContext.servletContext.contextPath }/thumbnail/detail?no=" + no;
		});
	</script>

  </body>
</html>