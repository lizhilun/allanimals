<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
 
 
 <!-- @@@@@작업 중 -->   
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>입양 후기 | 게시글 조회</title>
    
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/content.css" />

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/da4b1b94bb.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>


  </head>

  <body>
  
    <jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
	<jsp:include page="/WEB-INF/views/menubar/sidebar.jsp"/>
  
      <div class="container">
      <div style="border: 1px;">
      <!-- 제목 -->
        <div class="title">
          <h3><c:out value="${ requestScope.thumbnail.afterTitle }"/></h3>
        </div>
        
        <hr>
        
      <!-- 이미지 -->
        <div class="petImg carousel slide" data-bs-ride="carousel" id="demo" class="carousel slide">
    	<div id="demo" class="carousel slide" data-ride="carousel">
    
      <!-- Indicators -->
      <ul class="carousel-indicators">
        <li data-target="#demo" data-slide-to="0" class="active"></li>
        <li data-target="#demo" data-slide-to="1"></li>
        <li data-target="#demo" data-slide-to="2"></li>
      </ul>
      
      <!-- The slideshow -->
        <div class="carousel-inner">
        <c:forEach items="${ requestScope.thumbnail.introFile }" var="intro" varStatus="stu">
        <c:if test="${ stu.count == 1 }">
          <div class="carousel-item active">
            <img src="<c:out value='/allanimals${ intro.phoAdder }${ intro.phoFile } '/>" alt="${ stu.count }" width="500" height="300">
          </div>
        </c:if>
        <c:if test="${ stu.count != 1 }">
          <div class="carousel-item">
            <img src="<c:out value='/allanimals${ intro.phoAdder }${ intro.phoFile } '/>" alt="${ stu.count }" width="500" height="300">
          </div>
        </c:if>
        </c:forEach>
        </div>

        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#demo" data-slide="prev">
          <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next">
          <span class="carousel-control-next-icon"></span>
        </a>
    </div>
    <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
    </a>
      </div>
      
      <button class="btnMenu tBtn" onclick="location.href='${ pageContext.servletContext.contextPath }/adopt/application?aniNum=${ requestScope.thumbnail.aniNum }'">입양하기</button>
      <button class="btnMenu gBtn" onclick="location.href='#'">후원하기</button>
         
   <c:choose>
   <c:when test="${ requestScope.interYn eq 'Y' }">
       <button class="btnMenu bBtn" onclick="location.href='${ pageContext.servletContext.contextPath }/interest/insert?introNum=${ requestScope.thumbnail.introNum }'">관심추가</button>
   </c:when>
   <c:when test="${ requestScope.interYn eq 'N' }">
       <button class="btnMenu bBtn" onclick="location.href='${ pageContext.servletContext.contextPath }/interest/insert?introNum=${ requestScope.thumbnail.introNum }'">관심삭제</button>
   </c:when>
   <c:otherwise> 오류 발생 </c:otherwise>
   </c:choose>
   
   </div><br><br>
          
          <hr>
          
          <div id="wText"><!-- 글내용 -->
            <p class="pText"><c:out value="${ requestScope.thumbnail.afterCont }" escapeXml="false"/></p>
            </div>
            
          <table class="table" id="maintable2">
            <thead>
              <tr>
                <th class="replyTitle">댓글</th>
		  		<th class="countreply">댓글 ${fn:length(rep)}</th>              
			  </tr>
            </thead>

		<tbody class="bottom">      
             <c:forEach var="repList" items="${ requestScope.rep }">
			<tr>
				<td colspan="3"><c:out value="${ repList.memId }"/><br>
				<c:if test="${ sessionScope.loginMember.memId eq repList.memId }">
                  <span class="delete" OnClick="deletAlert();location.href='${ pageContext.servletContext.contextPath }/intro/rep/delete?introNum=${ repList.introNum }&&repNum=${ repList.repNum }'" style="cursor:pointer;"><i class="fa-solid fa-trash-can" title="삭제"></i></span>
                  </c:if>
                  
                   </td>
				<td colspan="9"><c:out value="${ repList.repCont }"/></td>
			</tr>
			</c:forEach>
            </tbody>
          </table>
		<c:if test="${ !empty sessionScope.loginMember }">
		<form action="${ pageContext.servletContext.contextPath }/intro/rep/insert">
          <table id="reply">
            <tr>
              <td>
          <div class="mb-3" id="replyarea">
            <textarea class="form-control" name="exampleFormControlTextarea1" id="exampleFormControlTextarea1" rows="3" placeholder="댓글 내용을 입력해주세요"></textarea>
          </div>
             </td>
             <td>
             <input type="hidden" name="introNum" value="${ requestScope.thumbnail.introNum }"/>
              <br>
            <div class="write">
            <button class="btnMenu btn1" type="submit">댓글 작성</button>
            </div>
          </td>
          </tr>
          </table>
          </form>
          </c:if>
  </div>

  </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script>
      function deletAlert()  {
        if (confirm("정말 삭제하시겠습니까?") == true){    //확인

          document.removefrm.submit();

          }else{   //취소

          return false;

}
}

function like()  {
  alert('관심 추가 되었습니다.');
}
    </script>
    
     	<jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>
  
  
  </body>
</html>
