<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/template.css" />
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/rescueList.css"/>
        
    <title>구조 동물| 리스트</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  </head>

  <body>
  
    <jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
	<jsp:include page="/WEB-INF/views/menubar/sidebar.jsp"/>
	
    <!-- 이 밑에서부터 작업하시면 됩니다.-->
    <div class="container">
      <div class="page-title">
      <h3>잃어버린 동물</h3>
      </div>
     

        <!-- 게시판 검색창 -->
        <div id="board-search">
          <div class="content">
            <div class="search-window">
              <form id="" method="get">
              <input type="hidden" name="currentPage" value="1">
              
                <div class="search-wrap">
                  <label for="search" class="blind"></label>
                  <input id="searchValue" type="search" name="searchValue" placeholder="검색어를 입력해주세요." value="<c:out value="${ requestScope.selectCriteria.searchValue }"/>">
                  <button type="submit" class="btn btn-dark">검색</button>
                </div>
              </form>
            </div>
          </div>
        </div>
             
        <!-- 게시판 글 목록 -->
        <div id="board-list">
          <div class="content">
            <table class="board-table">
            <thead>
              <tr>
                <th scope="col" class="th-num">번호</th>
                <th scope="col" class="th-title">제목</th>
                <th scope="col" class="th-location">지역</th>
                <th scope="col" class="th-date">작성일</th>
              </tr>
            </thead>
		<c:forEach var="rescue" items="${ requestScope.rescueList }">
              <tbody>
              <tr>
				<td><label><c:out value="${ rescue.saveNum }"/></label></td>
				<td><a href="${ pageContext.servletContext.contextPath }/rescue/content?saveNum=${ rescue.saveNum }"><c:out value="동물을 구조했어요!"/></a></td>
				<td><c:out value="${ rescue.memAddre }"/></td>
				<td><c:out value="${ rescue.saveTime }"/></td>
              </tr>
              </tbody>
		</c:forEach>
  	</table>
          </div>
        </div>         

      <!-- 페이지네이션 -->
      <div class="pagination">
        <a href="#">&laquo;</a>
        <a href="#">1</a>
        <a href="#" class="active">2</a>
        <a href="#">3</a>
        <a href="#">4</a>
        <a href="#">5</a>
        <a href="#">&raquo;</a>
      </div>
  </div>

	<script>
		$("#title").click(function(){
			const rescueNum = $(this).find("label").text();
			console.log(rescueNum);
			location.href = "${ pageContext.servletContext.contextPath }/rescue/content?rescueNum=" + rescueNum;
		});
	</script>

	 <jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>

    
  </body>
</html>