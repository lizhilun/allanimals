<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>구조 동물 | 게시글 조회</title>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/content.css"/>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/da4b1b94bb.js" crossorigin="anonymous"></script>

  </head>

  <body>

  	<jsp:include page="/WEB-INF/views/menubar/header.jsp"/>
	<jsp:include page="/WEB-INF/views/menubar/sidebar.jsp"/>

        <!-- 이 밑에서부터 작업하시면 됩니다.-->
    <div class="container">
        <div class="row">
            <li class="empty">
                <span data-feather="file"></span>
            </li>
        </div>
        
        <table class="table" id="maintable">
        <c:forEach var="rescue" items="${ requestScope.rescueContent }">
          <thead>
            <tr> 
              <th class="titleText"scope="col"><c:out value="동물을 구조했어요!"/></th>
              <th scope="col" class="btn-group dropstart" id="etcmenu"> 
                  <a class="" href="#" id="etc" data-bs-toggle="dropdown" aria-expanded="false">▶</a>
                      <ul class="drop-cont">
                      <li><a class="dropdown-item" href="#" onclick="like()">관심추가</a></li>
                      <li><a class="dropdown-item" href="${ pageContext.servletContext.contextPath }/rescue/revise">수정하기</a></li>
                      <li><a class="dropdown-item" href="#" onclick="deletAlert()">삭제하기</a></li>
                    </ul>
              </th>
            </tr>
          </thead>

            <tbody>
              <tr class="inner">
                <td colspan="6"><br><span><c:out value="${ rescue.saveCont }"/></span></td>
              </tr>
            </tbody>
         </c:forEach>
		 </table>
		
          <table class="table" id="maintable2">
            <thead>
              <tr>
                <th class="replyTitle">댓글</th>
                <th class="countreply">댓글 2</th>
              </tr>
            </thead>

            <tbody class="bottom">
              <tr>
                <td colspan="3">강형욱</td>
                <td colspan="9">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam, suscipit omnis. Cum quas, inventore saepe deleniti expedita ullam impedit, hic deserunt eaque et repellat ratione neque. Iste id expedita nisi?</td>
              </tr>
              <tr>
                <td colspan="3">캣독</td>
                <td colspan="9">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dicta autem facere, nobis veritatis architecto odit beatae accusamus veniam cum asperiores sint ut laborum iste, modi saepe, numquam ipsa consequuntur placeat!</td>
              </tr>
            </tbody>
          </table>
		
          <div class="mb-3" id="replyarea">
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="댓글 내용을 입력해주세요"></textarea>
          </div>

          <div class="menu">
            <div class="write">
              <button class="btnMenu btn1" onclick="location.href='#'">댓글 작성</button>
              </div>
          </div>

          <div id="icon">
          <span class="modify" OnClick="location.href='#'" style="cursor:pointer;"><i class="fa-solid fa-eraser" title="수정"></i></span>
          <span class="delete" OnClick="deletAlert()" style="cursor:pointer;"><i class="fa-solid fa-trash-can" title="삭제"></i></span>
          </div>
        

  </div>

   	<jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script>
      function deletAlert()  {
        if (confirm("정말 삭제하시겠습니까?") == true){    //확인

          document.removefrm.submit();

          }else{   //취소

          return false;

}
}

function like()  {
  alert('관심 추가 되었습니다.');
}
    </script>
  
  </body>
</html>
