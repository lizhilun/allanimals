<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html>
<html lang="en">
  <head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>구조요청 가능업체 정보 수정</title>
     <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/admin/helpedit.css" />
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/header.css">
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/sidebar.css">
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/footer.css">
    
    <!-- 폰트 적용 방식은 다음과 같습니다.
    html과 연결한 css 파일 최상단에
    1. @import url("https://fonts.googleapis.com/css2?family=Sunflower:wght@500&display=swap");
      붙여넣기 해주세요.
    2. * {font-family: "Sunflower", sans-serif; } 를 붙여넣기 해주세요.
    예시) 
    @import url("https://fonts.googleapis.com/css2?family=Sunflower:wght@500&display=swap");

    * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    font-family: "Sunflower", sans-serif;
    }-->
  </head>

  <body>
   
	<jsp:include page="/WEB-INF/views/adminheader.jsp"/>
	<jsp:include page="/WEB-INF/views/adminsidebar.jsp"/>
	
    <!-- 이 밑에서부터 작업하시면 됩니다.-->
    <div class="container">
      <div class="page-title">
      <h3>구조요청 가능 업체 수정</h3>
      </div>
        <!-- 카테고리 드롭다운
         <div class="dropdown">
          <button class="dropbtn">▼ 진행상태</button>
          <div class="dropdown-content">
            <a href="#">입양 대기</a>
            <a href="#">입양 거절</a>
            <a href="#">입양 완료</a>
            <a href="#">진행중</a>
          </div>
        </div>  -->

        <!-- 게시판 검색창 -->
        <div id="board-search">
          <div class="content">
            <div class="search-window">
              <form action="">
                <div class="search-wrap">
                  <label for="search" class="blind"></label>
                  <input id="search" type="search" name="search" placeholder="검색어를 입력해주세요." value="">
                  <button type="submit" class="btn btn-dark">검색</button>
                </div>
              </form>
            </div>
          </div>
        </div>
                 
        <!-- 게시판 글 목록 -->
        <div id="board-list">
          <div class="content">
            <table class="board-table">
            <thead>
              <tr>
               
                <th scope="col" class="th-num">고유번호</th>
                <th scope="col" class="th-petName">지역</th>
                <th scope="col" class="th-name">상호명</th>
                <th scope="col" class="th-name">전화번호</th>
                <th scope="col" class="th-name">주소</th>
                <th scope="col" class="th-name">수정</th>
              </tr>

            </thead>
            <tbody>
            <c:forEach var="help" items="${ requestScope.helpList }">
              <tr>
                <td>${ help.helpNum }</td>
                <td>${ help.helpArea }</td>
                <td>${ help.helpName }</td>
                <td>${ help.helpPhone }</td>
                <td>${ help.helpAddre }</td>     
                <td><a href="${ pageContext.servletContext.contextPath }/HelpEdit2?helpNum=${ help.helpNum }">수정하기</a></td>
              </tr>
			</c:forEach>
            </tbody>
            </table>
          </div>
        </div>         

 <%-- <div class="pagination">
      <jsp:include page="../common/paging.jsp"/> --%>
      <!-- 페이지네이션 -->
      <!-- <div class="pagination">
        <a href="#">&laquo;</a>
        <a href="#">1</a>
        <a href="#" class="active">2</a>
        <a href="#">3</a>
        <a href="#">4</a>
        <a href="#">5</a>
        <a href="#">&raquo;</a>
      </div>-->
  </div> 
  <br><br>
    
<%--     <jsp:include page="/WEB-INF/views/menubar/footer.jsp"/> --%>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>
    