<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>구조요청업체 수정</title>
     <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/admin/helpedit2.css" />
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/header.css">
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/sidebar.css">
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/footer.css">
    
    <!-- 폰트 적용 방식은 다음과 같습니다.
    html과 연결한 css 파일 최상단에
    1. @import url("https://fonts.googleapis.com/css2?family=Sunflower:wght@500&display=swap");
      붙여넣기 해주세요.
    2. * {font-family: "Sunflower", sans-serif; } 를 붙여넣기 해주세요.
    예시) 
    @import url("https://fonts.googleapis.com/css2?family=Sunflower:wght@500&display=swap");

    * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    font-family: "Sunflower", sans-serif;
    }-->
  </head>

  <body>
    
	<jsp:include page="/WEB-INF/views/adminheader.jsp"/>
	<jsp:include page="/WEB-INF/views/adminsidebar.jsp"/>
	
    <!-- 이 밑에서부터 작업하시면 됩니다.-->
    <div class="container">

        <h3>구조요청 가능업체 수정</h3>
        <br><br><br>
        
      
            <form action="${ pageContext.servletContext.contextPath }/HelpEdit2" method="post">
            
            <select name="helpArea" aria-label="Default select example">
            <option selected>관할지역 선택하기</option>
            <option value="강서구">강서구</option>
            <option value="양천구">양천구</option>
            <option value="구로구">구로구</option>
            <option value="영등포구">영등포구</option>
            <option value="금천구">금천구</option>
            <option value="동작구">동작구</option>
            <option value="관악구">관악구</option>
            <option value="서초구">서초구</option>
            <option value="강남구">강남구</option>
            <option value="송파구">송파구</option>
            <option value="강동구">강동구</option>
            <option value="마포구">마포구</option>
            <option value="용산구">용산구</option>
            <option value="성동구">성동구</option>
            <option value="광진구">광진구</option>
            <option value="중구">중구</option>
            <option value="은평구">은평구</option>
            <option value="서대문구">서대문구</option>
            <option value="종로구">종로구</option>
            <option value="동대문구">동대문구</option>
            <option value="중랑구">중랑구</option>
            <option value="노원구">노원구</option>
            <option value="성북구">성북구</option>
            <option value="도봉구">도봉구</option>
            <option value="강북구">강북구</option>
         	
        </select>
        <br><br>
             <input type="hidden" name="helpNum" value="${ requestScope.helpList.helpNum }">
            <br><br>
           상호명 :  <input type="text" name="helpName" value="${ requestScope.helpList.helpName }">
            <br><br>
           주소 :  <input type="text" name="helpAddre" value="${ requestScope.helpList.helpAddre }">
            <br><br>
           전화번호 : <input type="text" name="helpPhone" value="${ requestScope.helpList.helpPhone }">
            <br><br>
            <input name="memId" value="${ sessionScope.loginMember.memId }" type="hidden">
            <button type="submit" class="okbutton">수정</button>
            </form>
        
       <br>
          

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">동물병원 수정</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
       수정이 완료되었습니다.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">닫기</button>

      </div>
    </div>  
  </div>
</div>


    </div>

    <jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>
    