<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>문의사항 답변</title>
     <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/admin/answer.css" />
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/header.css">
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/sidebar.css">
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/footer.css">
    <!-- 폰트 적용 방식은 다음과 같습니다.
    html과 연결한 css 파일 최상단에
    1. @import url("https://fonts.googleapis.com/css2?family=Sunflower:wght@500&display=swap");
      붙여넣기 해주세요.
    2. * {font-family: "Sunflower", sans-serif; } 를 붙여넣기 해주세요.
    예시) 
    @import url("https://fonts.googleapis.com/css2?family=Sunflower:wght@500&display=swap"); 

    * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    font-family: "Sunflower", sans-serif;
    }-->
  </head>
  <body>
  
	<jsp:include page="/WEB-INF/views/adminheader.jsp"/>
	<jsp:include page="/WEB-INF/views/adminsidebar.jsp"/>
	

    <!-- 이 밑에서부터 작업하시면 됩니다.-->
    <main class="col-md-9 ms-sm-auto col-lg-11 px-md-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-6 mb-3 " id="tablehead">
     
        </div>
      </div>

        <div id="textbase">
            <h1 class="h4 border-bottom p-3">문의사항 답변</h1> 

        <div class="mb-3 py-3">
            <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="제목을 입력해주세요">
          </div>
          <div class="mb-3">
            
            <div id="summernote"></div>
          </div>
          <div class="mb-3 border-bottom p-3">
            <label for="formFileSm" class="form-label">파일첨부</label>
            <input class="form-control form-control-sm" id="formFileSm" type="file">
          </div>
          
        <button class="okbutton" onclick="location.href='#';" id="okbutton">확인</button>
        
        </div>
<br><br><br>
    </main>
     
    <div id="summernote"></div>
    <script>

      $('#summernote').summernote({
        placeholder: '내용을 입력해주세요.',
        height:600 , 
	        minHeight: 600,             // set minimum height of editor
            maxHeight: 600,             // set maximum height of editor
             focus: true   ,             
            toolbar: [
                

			    // [groupName, [list of button]]
			    ['fontname', ['fontname']],
			    ['fontsize', ['fontsize']],
			    ['style', ['bold', 'italic', 'underline','strikethrough', 'clear']],
			    ['color', ['forecolor','color']],
			    ['table', ['table']],
			    ['para', ['ul', 'ol', 'paragraph']],
			    ['height', ['height']],
			    ['insert',['picture']],
			    ['view', ['fullscreen', 'help']]
			  ],
			fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New','맑은 고딕','궁서','굴림체','굴림','돋움체','바탕체'],
			fontSizes: ['8','9','10','11','12','14','16','18','20','22','24','28','30','36','50','72']
        
      });
  
    
    </script>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">공지사항 작성</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
       작성이 완료되었습니다.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">닫기</button>

      </div>
    </div>  
  </div>
</div>

    </div>

   <jsp:include page="/WEB-INF/views/menubar/footer.jsp"/>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>
    