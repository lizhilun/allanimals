package com.allanimals.inquiry.service;


import static com.allanimals.commnon.mybatis.Template.getSqlSession;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.allanimals.commnon.pagin.SelectCriteria;
import com.allanimals.inquiry.dao.NoticeMapper;
import com.allanimals.inquiry.dto.AttachmentDTO;
import com.allanimals.inquiry.dto.NoticeBoardDTO;


public class InquiryService {
	private NoticeMapper noticeMapper;
	
	
	/**
	 * <pre>
	 * 	페이징 처리를 위한 전체 게시물 수 조회용 메소드
	 * </pre>
	 * @param searchMap
	 * @return
	 */
	public int selectTotalCount(Map<String, String> searchMap) {
		
		SqlSession sqlSession = getSqlSession();
		noticeMapper = sqlSession.getMapper(NoticeMapper.class);
		
		int totalCount = noticeMapper.selectTotalCount(searchMap);
		
		sqlSession.close();
		
		return totalCount;
	}
	
	/**
	 * <pre>
	 *   게시물 전체 조회용 메소드
	 * </pre>
	 * @param selectCriteria
	 * @return
	 */
	public List<NoticeBoardDTO> selectBoardList(SelectCriteria selectCriteria) {
		
		SqlSession sqlSession = getSqlSession();
		noticeMapper = sqlSession.getMapper(NoticeMapper.class);
		
		List<NoticeBoardDTO> boardList = noticeMapper.selectBoardList(selectCriteria);
		
		sqlSession.close();
		
		return boardList;
	}

	public List<AttachmentDTO> selectInquiryRep(String no) {
		SqlSession sqlSession = getSqlSession();
		noticeMapper = sqlSession.getMapper(NoticeMapper.class);
		List<AttachmentDTO> rep = noticeMapper.selectInquiryRep(no);
		
		sqlSession.close();
		
		return rep;
	}

	public NoticeBoardDTO selectInquiryBoard(String boardNo) {
		SqlSession sqlSession = getSqlSession();
		noticeMapper = sqlSession.getMapper(NoticeMapper.class);
		NoticeBoardDTO board = noticeMapper.selectInquiryBoard(boardNo);
		
		sqlSession.close();
		return board;
	}

	
	public int insertInquiryRep(AttachmentDTO rep) {
		SqlSession sqlSession = getSqlSession();
		noticeMapper = sqlSession.getMapper(NoticeMapper.class);
		int result =  noticeMapper.insertInquiryRep(rep);
		int result2 = 0;

		if(result > 0) {
			result2 = noticeMapper.inquirysolve(rep.getInqNo());
			
			if( result2 > 0 ){
			sqlSession.commit();
			
			}else {
				sqlSession.rollback();
			}
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	public int deleteInquiryRep(AttachmentDTO rep) {
		SqlSession sqlSession = getSqlSession();
		noticeMapper = sqlSession.getMapper(NoticeMapper.class);
		int result =  noticeMapper.deleteInquiryRep(rep);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	public int insertInquiry(NoticeBoardDTO board) {
		SqlSession sqlSession = getSqlSession();
		noticeMapper = sqlSession.getMapper(NoticeMapper.class);
		int result = noticeMapper.insertInquiry(board);
		

		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	public int updateInquiry(NoticeBoardDTO board) {
		SqlSession sqlSession = getSqlSession();
		noticeMapper = sqlSession.getMapper(NoticeMapper.class);
		int result = noticeMapper.updateInquiry(board);
		
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	public int deleteInquiry(String boardNo) {
		SqlSession sqlSession = getSqlSession();
		noticeMapper = sqlSession.getMapper(NoticeMapper.class);
		int result = noticeMapper.deleteInquiry(boardNo);
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}
}
