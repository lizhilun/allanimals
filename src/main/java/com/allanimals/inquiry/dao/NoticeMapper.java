package com.allanimals.inquiry.dao;

import java.util.List;
import java.util.Map;

import com.allanimals.commnon.pagin.SelectCriteria;
import com.allanimals.inquiry.dto.AttachmentDTO;
import com.allanimals.inquiry.dto.NoticeBoardDTO;


public interface NoticeMapper {

	int selectTotalCount(Map<String, String> searchMap);

	List<NoticeBoardDTO> selectBoardList(SelectCriteria selectCriteria);

	List<AttachmentDTO> selectInquiryRep(String no);

	NoticeBoardDTO selectInquiryBoard(String boardNo);

	int insertInquiryRep(AttachmentDTO rep);

	int deleteInquiryRep(AttachmentDTO rep);

	int insertInquiry(NoticeBoardDTO board);

	int updateInquiry(NoticeBoardDTO board);

	int deleteInquiry(String boardNo);

	int inquirysolve(String boardNo);

	
}
