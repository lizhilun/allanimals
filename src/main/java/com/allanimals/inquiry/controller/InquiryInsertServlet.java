package com.allanimals.inquiry.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.inquiry.dto.NoticeBoardDTO;
import com.allanimals.inquiry.service.InquiryService;
import com.allanimals.member.model.dto.MemberDTO;

/**
 * Servlet implementation class InquiryInsertServlet
 */
@WebServlet("/board/inquiryinsert")
public class InquiryInsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InquiryInsertServlet() {

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String path =  "/WEB-INF/views/inquiry/inquiry_insert.jsp";
		
		request.getRequestDispatcher(path).forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		
		String title = request.getParameter("title");
		String content = request.getParameter("content");
		String memId =((MemberDTO) request.getSession().getAttribute("loginMember")).getMemId();
		
		InquiryService service = new InquiryService();
		NoticeBoardDTO board = new NoticeBoardDTO();
		
		board.setMemId(memId);
		board.setTitle(title);
		board.setContent(content);
		System.out.println(board);
		
		int result = service.insertInquiry(board);
		
		String path="";
		
		if(result > 0) {
			response.setContentType("text/html;charset=utf-8");
			path ="/board/inquirylist";
			response.sendRedirect(request.getContextPath()+path);
			
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "게시글 등록에 실패하셨습니다.");
			request.getRequestDispatcher(path).forward(request, response);
		}
		
		
		
		
		
	}
}
