package com.allanimals.inquiry.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.inquiry.dto.AttachmentDTO;
import com.allanimals.inquiry.dto.NoticeBoardDTO;
import com.allanimals.inquiry.service.InquiryService;

/**
 * Servlet implementation class InquirySelectContentServlet
 */
@WebServlet("/board/inquirycontent")
public class InquirySelectContentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InquirySelectContentServlet() {
   
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String boardNo = request.getParameter("boardNo");
		System.out.println(boardNo);
		
		NoticeBoardDTO board = new NoticeBoardDTO();
		
//		board.setTitle(request.getParameter("board.title"));
//		board.setContent(request.getParameter("board.content"));
//		board.setNo("board.no");
		
		InquiryService service = new InquiryService();
		board = service.selectInquiryBoard(boardNo);
		
		List<AttachmentDTO> rep = service.selectInquiryRep(boardNo);
		System.out.println(rep);
		System.out.println(board);
		String path="";
		
		if(board != null) {
			if(rep !=null) {
			
				path = "/WEB-INF/views/inquiry/inquiry_content.jsp";
				request.setAttribute("board", board);
				request.setAttribute("rep", rep);
		
							}else {
								path = "/WEB-INF/views/inquiry/inquiry_content.jsp";
								request.setAttribute("board", board);
								  }
		}else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "게시물 목록 조회 실패!");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
	}



	
}
