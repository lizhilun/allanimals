package com.allanimals.inquiry.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.inquiry.dto.AttachmentDTO;
import com.allanimals.inquiry.service.InquiryService;

/**
 * Servlet implementation class InquiryRepDeleteServlet
 */
@WebServlet("/board/inquiryrepdelete")
public class InquiryRepDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
    public InquiryRepDeleteServlet() {
  
    }

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String inqNo = request.getParameter("inqNo");
		String repNo = request.getParameter("repNo");
		AttachmentDTO rep = new AttachmentDTO();
		
		rep.setInqNo(inqNo);
		rep.setRepNo(repNo);
		System.out.println(rep);
		
		InquiryService boardService = new InquiryService();
		int result = boardService.deleteInquiryRep(rep);
		
		String path="";
		
		if(result > 0) {
//			path = "/WEB-INF/views/inquiry/inquiry_content.jsp";
			response.setContentType("text/html;charset=utf-8");
			path ="/board/inquirycontent?boardNo="+inqNo;
			response.sendRedirect(request.getContextPath()+path);
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "댓글 삭제에 실패하셨습니다.");
			request.getRequestDispatcher(path).forward(request, response);
		}
		
		
	}

}
