package com.allanimals.inquiry.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.inquiry.dto.AttachmentDTO;
import com.allanimals.inquiry.service.InquiryService;
import com.allanimals.member.model.dto.MemberDTO;

/**
 * Servlet implementation class InquiryRepUpdateServlet
 */
@WebServlet("/board/inquiryrepwhite")
public class InquiryRepUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InquiryRepUpdateServlet() {
       
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String memId =((MemberDTO) request.getSession().getAttribute("loginMember")).getMemId();
		String repCont = request.getParameter("exampleFormControlTextarea1");
		String inqNo = request.getParameter("inqNo");
		AttachmentDTO rep = new AttachmentDTO();
		
		rep.setMemId(memId);
		rep.setRepCont(repCont);
		rep.setInqNo(inqNo);
		System.out.println(rep);
		
		InquiryService boardService = new InquiryService();
		int result1 = boardService.insertInquiryRep(rep);
		String path="";
		

		
		if(result1 > 0) {

//			path = "/WEB-INF/views/inquiry/inquiry_content.jsp";
			
			response.setContentType("text/html;charset=utf-8");
			path ="/board/inquirycontent?boardNo="+inqNo;
			response.sendRedirect(request.getContextPath()+path);
			
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "댓글 등록에 실패하셨습니다.");
			request.getRequestDispatcher(path).forward(request, response);
		}
		
		
	}
}
