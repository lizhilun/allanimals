package com.allanimals.inquiry.dto;

import java.sql.Date;

import com.allanimals.member.model.dto.MemberDTO;

public class AttachmentDTO {
	private String repNo;
	private String inqNo;
	private String repCont;
	private java.sql.Date repDate;
	private String yn;
	private String memId;
	
	public AttachmentDTO(String repNo, String inqNo, String repCont, Date repDate, String yn, String memId) {

		this.repNo = repNo;
		this.inqNo = inqNo;
		this.repCont = repCont;
		this.repDate = repDate;
		this.yn = yn;
		this.memId = memId;
	}

	public AttachmentDTO() {

	}

	public String getRepNo() {
		return repNo;
	}

	public void setRepNo(String repNo) {
		this.repNo = repNo;
	}

	public String getInqNo() {
		return inqNo;
	}

	public void setInqNo(String inqNo) {
		this.inqNo = inqNo;
	}

	public String getRepCont() {
		return repCont;
	}

	public void setRepCont(String repCont) {
		this.repCont = repCont;
	}

	public java.sql.Date getRepDate() {
		return repDate;
	}

	public void setRepDate(java.sql.Date repDate) {
		this.repDate = repDate;
	}

	public String getYn() {
		return yn;
	}

	public void setYn(String yn) {
		this.yn = yn;
	}

	public String getMemId() {
		return memId;
	}

	public void setMemId(String memId) {
		this.memId = memId;
	}

	@Override
	public String toString() {
		return "AttachmentDTO [repNo=" + repNo + ", inqNo=" + inqNo + ", repCont=" + repCont + ", repDate=" + repDate
				+ ", yn=" + yn + ", memId=" + memId + "]";
	}
	
	
	
	
	
	
	
	
	
	
}
