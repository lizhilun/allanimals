package com.allanimals.allDaily.service;

import static com.allanimals.commnon.mybatis.Template.getSqlSession;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.allanimals.allDaily.model.dao.AllDailyMapper;
import com.allanimals.allDaily.model.dto.ApplicationDTO;
import com.allanimals.allDaily.model.dto.DailyDTO;
import com.allanimals.allDaily.model.dto.DailyPhoDTO;
import com.allanimals.allDaily.model.dto.DailyRepDTO;
import com.allanimals.allDaily.model.dto.IntroDTO;
import com.allanimals.allDaily.model.dto.IntroInterestDTO;
import com.allanimals.allDaily.model.dto.IntroPhoDTO;
import com.allanimals.allDaily.model.dto.IntroRepDTO;
import com.allanimals.member.model.dto.MemberDTO;

public class AllDailyService {

	private AllDailyMapper alldailymapper;

	
	/**
	 * <pre>
	 *   유기동물 소개글 게시판 전제 조회 메소드
	 * </pre>
	 * @return
	 */
	public List<IntroDTO> IntroAnimalsList() {

		SqlSession sqlSession = getSqlSession();
		
		alldailymapper = sqlSession.getMapper(AllDailyMapper.class);
		List<IntroDTO> introList = alldailymapper.IntroAnimalsList();
		
		sqlSession.close();
		
		return introList;
	}


	/**
	 * <pre>
	 *   유기동물글 전체 조회 메소드
	 * </pre>
	 * @param searchMap
	 * @return
	 */
	public int selectTotalCount(Map<String, Object> searchMap) {

		SqlSession sqlSession = getSqlSession();
		alldailymapper = sqlSession.getMapper(AllDailyMapper.class);
		
		int totalCount = alldailymapper.selectTotalCount(searchMap);
		
		sqlSession.close();
		
		return totalCount;
	}


	/**
	 * <pre>
	 *   유기동물 페이징 메소드
	 * </pre>
	 * @param selectCriteria
	 * @return
	 */
	public List<IntroDTO> selectIntroList(Map<String, Object> searchMap) {

		SqlSession sqlSession = getSqlSession();
		alldailymapper = sqlSession.getMapper(AllDailyMapper.class);
		
		List<IntroDTO> introList = alldailymapper.selectIntroList(searchMap);
		
		sqlSession.close();
		
		return introList;
	}


	/**
	 * <pre>
	 *   상세조회 메소드
	 * </pre>
	 * @param introNum
	 * @return
	 */
	public IntroDTO selectOneThumbnail(String introNum) {
		
		
		SqlSession sqlSession = getSqlSession();
		alldailymapper = sqlSession.getMapper(AllDailyMapper.class);
		
		IntroDTO thumbnail = alldailymapper.selectOneThumbnail(introNum);
			
			if(thumbnail != null) {
				
				sqlSession.commit();
			} else {
				
				sqlSession.rollback();
			}
		
		sqlSession.close();
		
		return thumbnail;
	}


	/**
	 * <pre>
	 *   글쓰기 메소드
	 * </pre>
	 * @param intro
	 * @return
	 */
	public int insertIntro(IntroDTO intro) {

		SqlSession sqlSession = getSqlSession();
		alldailymapper = sqlSession.getMapper(AllDailyMapper.class);
		
		int result = 0;
		int memInsert = 0;
		
		int introResult = alldailymapper.insertThumbnailContent(intro);
		intro.getIntroNum();
		System.out.println("intro.getIntroNum() : " + intro.getIntroNum());
		
		if(introResult > 0) {
			
			memInsert = alldailymapper.insertIntroId(intro);
		}
		System.out.println("memInsert : " + memInsert);
		List<IntroPhoDTO> fileList = intro.getIntroFile();
		
		for(int i = 0; i < fileList.size(); i++) {
			
			fileList.get(i).setIntroNum(intro.getIntroNum());
		}
		
		int introPhoResult = 0;
		for(int i = 0; i < fileList.size(); i++) {
			
			introPhoResult += alldailymapper.insertIntroPho(fileList.get(i));
		}
		System.out.println("intro 확인용 : " + intro);
		
		if((introResult > 0) && (introPhoResult == fileList.size())) {
			
			
			int aniInsert = 0;
			System.out.println("intro 확인용2 : " + intro);
			
			aniInsert = alldailymapper.insertIntroAni(intro); 
			

			if(memInsert > 0 && aniInsert >0) {
				
				
			}
			
			
			sqlSession.commit();
			result = 1;
		} else {
			
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}


	/** <pre>
	 * 입양신청서 작성을 위한 메소드
	 * </pre>
	 * @param apply
	 * @return
	 */
	public int insertApplication(ApplicationDTO apply) {
		
		SqlSession sqlSession = getSqlSession();
		
		alldailymapper = sqlSession.getMapper(AllDailyMapper.class);
		
		int result =  alldailymapper.insertApplication(apply);
		
		if(result > 0) {

			sqlSession.commit();

		} else {

			sqlSession.rollback();

		}
		
		sqlSession.close();
		
		return result;

	}

	/**
	 * <pre>
	 * 댓글 작성을 위한 메소드입니다.
	 * </pre>
	 * @param rep
	 * @return
	 */

	public int insertIntroRep(IntroRepDTO rep) {

		SqlSession sqlSession = getSqlSession();

		alldailymapper = sqlSession.getMapper(AllDailyMapper.class);

		int result =  alldailymapper.insertIntroRep(rep);

		if(result > 0) {

			sqlSession.commit();
			
		} else {

			sqlSession.rollback();

		}
		
		sqlSession.close();
	
		return result;

	}

	/**
	 * <pre>
	 * 댓글 삭제를 위한 메소드입니다.
	 * </pre>
	 * @param rep
	 * @return
	 */

	public int deleteIntroRep(IntroRepDTO rep) {
		
		SqlSession sqlSession = getSqlSession();

		alldailymapper = sqlSession.getMapper(AllDailyMapper.class);

		int result =  alldailymapper.deleteIntroRep(rep);
		
		if(result > 0) {

			sqlSession.commit();

		} else {

			sqlSession.rollback();

		}

		sqlSession.close();
		
		return result;
		
	}


	
	/**
	 * <pre>
	 * 댓글 조회를 위한 메소드 입니다.
	 * </pre>
	 * @param introNum
	 * @return
	 */
	public List<IntroRepDTO> selectIntroRep(String introNum) {
		
		SqlSession sqlSession = getSqlSession();

		alldailymapper = sqlSession.getMapper(AllDailyMapper.class);

		List<IntroRepDTO> rep = alldailymapper.selectIntroRep(introNum);
		
		sqlSession.close();
		
		return rep;
	}

	
	/**
	 * <pre>
	 * 소개글 관심 설정을 위한 메소드입니다.
	 * </pre>
	 * @param introInterest
	 * @return
	 */

	public int insertIntroInterest(IntroInterestDTO introInterest) {
		
		SqlSession sqlSession = getSqlSession();

		alldailymapper = sqlSession.getMapper(AllDailyMapper.class);

		int result =  alldailymapper.insertIntroInterest(introInterest);

		if(result > 0) {

			sqlSession.commit();
			
		} else {

			sqlSession.rollback();

		}
		
		sqlSession.close();
	
		return result;
		
	}

	/**
	 * <pre>
	 * 관심 설정 조회를 위한 메소드입니다.
	 * </pre>
	 * @param introInterest
	 * @return
	 */

	public int selectInter(IntroInterestDTO introInterest) {
		
		SqlSession sqlSession = getSqlSession();
		alldailymapper = sqlSession.getMapper(AllDailyMapper.class);
		
		int selcetCheck = alldailymapper.selectInter(introInterest);
		
		sqlSession.close();
		
		return selcetCheck;

		
	}


	/**
	 * <pre>
	 * 관심 설정 삭제를 위한 메소드입니다.
	 * </pre>
	 * @param introInterest
	 * @return
	 */
	public int deleteIntroInterest(IntroInterestDTO introInterest) {
		
		
		SqlSession sqlSession = getSqlSession();

		alldailymapper = sqlSession.getMapper(AllDailyMapper.class);

		int result =  alldailymapper.deleteIntroInterest(introInterest);
		
		if(result > 0) {

			sqlSession.commit();

		} else {

			sqlSession.rollback();

		}

		sqlSession.close();
		
		return result;
		
	}


	/**
	 * <pre>
	 *   게시글 삭제용 메소드
	 * </pre>
	 * @param introDto
	 * @return
	 */
	public int deleteList(IntroDTO introDto) {
		
		SqlSession sqlSession = getSqlSession();
		
		alldailymapper = sqlSession.getMapper(AllDailyMapper.class);
		
		int result = alldailymapper.deleteList(introDto);
		
		if(result > 0 ) {
			
			sqlSession.commit();
		} else {
			
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}


	/**
	 * <pre>
	 *   수정하기 메소드
	 * </pre>
	 * @param intro
	 * @return
	 */
	public int writeListUpdate(IntroDTO intro) {
		
		SqlSession sqlSession = getSqlSession();
		
		alldailymapper = sqlSession.getMapper(AllDailyMapper.class);
		
		int result = alldailymapper.writeListUpdate(intro);
		
		if(result > 0) {
			
			int result2 = alldailymapper.aniUpdate(intro);
			
			if(result2 > 0) {
				
				sqlSession.commit();

			}
			
		} else {
			
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	/**
	 * <pre>
	 *   일상동물글 전체 조회 메소드
	 * </pre>
	 * @param searchMap
	 * @return
	 */
	public int selectTotalDailyCount(Map<String, Object> searchMap) {

		SqlSession sqlSession = getSqlSession();
		alldailymapper = sqlSession.getMapper(AllDailyMapper.class);
		
		int totalCount = alldailymapper.selectTotalDailyCount(searchMap);
		
		sqlSession.close();
		
		return totalCount;
	}

	/**
	 * <pre>
	 *   일상동물 페이징 메소드
	 * </pre>
	 * @param searchMap
	 * @return
	 */
	public List<DailyDTO> selectDailyList(Map<String, Object> searchMap) {

		SqlSession sqlSession = getSqlSession();
		alldailymapper = sqlSession.getMapper(AllDailyMapper.class);
		
		List<DailyDTO> dailyList = alldailymapper.selectDailyList(searchMap);
		
		sqlSession.close();
		
		return dailyList;
	}


	/**
	 * <pre>
	 *   일상동물글쓰기 메소드
	 * </pre>
	 * @param thumbnail
	 * @return
	 */
	public int insertDaily(DailyDTO daily) {
		
		SqlSession sqlSession = getSqlSession();
		alldailymapper = sqlSession.getMapper(AllDailyMapper.class);
		
		int result = 0;
		int memInsert = 0;
		
		int introResult = alldailymapper.insertDailyThumbnailContent(daily);
		daily.getDailyNum();
		System.out.println("intro.getIntroNum() : " + daily.getDailyNum());
		
		if(introResult > 0) {
			
			memInsert = alldailymapper.insertDailyId(daily);
		}
		System.out.println("memInsert : " + memInsert);
		List<DailyPhoDTO> fileList = daily.getIntroFile();
		
		for(int i = 0; i < fileList.size(); i++) {
			
			fileList.get(i).setDailyNum(daily.getDailyNum());
		}
		
		int introPhoResult = 0;
		for(int i = 0; i < fileList.size(); i++) {
			
			introPhoResult += alldailymapper.insertDailyPho(fileList.get(i));
		}
		System.out.println("daily 확인용 : " + daily);
		
		if((introResult > 0) && (introPhoResult == fileList.size())) {
			
			
			int aniInsert = 0;
			System.out.println("daily 확인용2 : " + daily);
			
			aniInsert = alldailymapper.insertDailyAni(daily); 
			

			if(memInsert > 0 && aniInsert >0) {
				
				
			}
			
			
			sqlSession.commit();
			result = 1;
		} else {
			
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}


	/**
	 * <pre>
	 *   일상글 상세보기
	 * </pre>
	 * @param dailyNum
	 * @return
	 */
	public DailyDTO selectOneDailyThumbnail(String dailyNum) {
		
		SqlSession sqlSession = getSqlSession();
		alldailymapper = sqlSession.getMapper(AllDailyMapper.class);
		
		DailyDTO thumbnail = alldailymapper.selectOneDailyThumbnail(dailyNum);
			
			if(thumbnail != null) {
				
				sqlSession.commit();
			} else {
				
				sqlSession.rollback();
			}
		
		sqlSession.close();
		
		return thumbnail;	
		
	}


	/**
	 * <pre>
	 *   일상글 댓글조회
	 * </pre>
	 * @param dailyNum
	 * @return
	 */
	public List<DailyRepDTO> selectDailyRep(String dailyNum) {

		SqlSession sqlSession = getSqlSession();

		alldailymapper = sqlSession.getMapper(AllDailyMapper.class);

		List<DailyRepDTO> rep = alldailymapper.selectDailyRep(dailyNum);
		
		sqlSession.close();
		
		return rep;
	}


//	/**
//	 * <pre>
//	 *   일상글 작성용 동물번호 조회
//	 * </pre>
//	 * @param memId
//	 * @return
//	 */
//	public IntroDTO selectAniNum(String mId) {
//		
//		SqlSession sqlSession = getSqlSession();
//		alldailymapper = sqlSession.getMapper(AllDailyMapper.class);
//		
//		IntroDTO catchAni = alldailymapper.selectAniNum(mId);
//		
//		sqlSession.close();
//		
//		return catchAni;
//	}




	
}
