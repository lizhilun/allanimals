package com.allanimals.allDaily.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.allDaily.model.dto.IntroRepDTO;
import com.allanimals.allDaily.service.AllDailyService;

/**
 * Servlet implementation class IntroRepDeleteServlet
 */
@WebServlet("/intro/rep/delete")
public class IntroRepDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IntroRepDeleteServlet() { }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String introNum = request.getParameter("introNum");
		String repNum = request.getParameter("repNum");
		IntroRepDTO rep = new IntroRepDTO();
		
		rep.setIntroNum(introNum);
		rep.setRepNum(repNum);
		
		AllDailyService introService = new AllDailyService();
		int result = introService.deleteIntroRep(rep);
		
		String path="";
		
		if(result > 0) {
			response.setContentType("text/html;charset=utf-8");
			path ="/write/list?introNum="+introNum;
			response.sendRedirect(request.getContextPath()+path);
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "댓글 삭제에 실패하셨습니다.");
			request.getRequestDispatcher(path).forward(request, response);
		}
		
		
	}


}
