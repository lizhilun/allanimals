package com.allanimals.allDaily.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.allDaily.model.dto.IntroRepDTO;
import com.allanimals.allDaily.service.AllDailyService;
import com.allanimals.member.model.dto.MemberDTO;

/**
 * Servlet implementation class IntroRepServlet
 */
@WebServlet("/intro/rep/insert")
public class IntroRepInsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public IntroRepInsertServlet() {
	       
    }
       
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String memId =((MemberDTO) request.getSession().getAttribute("loginMember")).getMemId();
		
		String repCont = request.getParameter("exampleFormControlTextarea1");
		String introNum = request.getParameter("introNum");
		
		IntroRepDTO rep = new IntroRepDTO();
		
		rep.setMemId(memId);
		rep.setRepCont(repCont);
		rep.setIntroNum(introNum);
		
		AllDailyService introService = new AllDailyService();
		int result = introService.insertIntroRep(rep);
		
		String path="";
		
		if(result > 0) {
			
			response.setContentType("text/html;charset=utf-8");
			path ="/write/list?introNum="+introNum;
			response.sendRedirect(request.getContextPath()+path);
			
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "댓글 등록에 실패하셨습니다.");
			request.getRequestDispatcher(path).forward(request, response);
		}
		
		
	}


}
