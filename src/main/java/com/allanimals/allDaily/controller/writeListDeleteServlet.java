package com.allanimals.allDaily.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.allanimals.allDaily.model.dto.IntroDTO;
import com.allanimals.allDaily.service.AllDailyService;
import com.allanimals.member.model.dto.MemberDTO;

/**
 * Servlet implementation class writeListServlet
 */
@WebServlet("/delete/list")
public class writeListDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String introNum = request.getParameter("introNum");
		
		IntroDTO introDto = new IntroDTO();
		introDto.setIntroNum(introNum);
		
		
		System.out.println("글번호 확인 : " + introNum);
		System.out.println("아이디 글번호 둘다 들어왔는지 확인 : " + introDto);
		
		AllDailyService introService = new AllDailyService();
		int result = introService.deleteList(introDto);
		
		String path="";
		
		if(result > 0) {
			
//			response.setContentType("text/html;charset=utf-8");
			path = "/intro";
			response.sendRedirect(request.getContextPath()+path);
		} else {
			
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "글 삭제에 실패하셨습니다.");
			request.getRequestDispatcher(path).forward(request, response);
		}
		
		
		
		
	}

}
