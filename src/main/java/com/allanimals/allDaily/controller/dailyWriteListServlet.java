package com.allanimals.allDaily.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.allanimals.allDaily.model.dto.DailyDTO;
import com.allanimals.allDaily.model.dto.DailyRepDTO;
import com.allanimals.allDaily.model.dto.IntroInterestDTO;
import com.allanimals.allDaily.service.AllDailyService;
import com.allanimals.member.model.dto.MemberDTO;

/**
 * Servlet implementation class writeListServlet
 */
@WebServlet("/daily/write/list")
public class dailyWriteListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		MemberDTO member = (MemberDTO) session.getAttribute("loginMember");
		
		String mId = "";
		
		if(member != null) {
			
			mId = member.getMemId();				
		}

		String dailyNum = request.getParameter("dailyNum");
		
		DailyDTO thumbnail = new DailyDTO();
		
		AllDailyService introService = new AllDailyService();
		thumbnail = introService.selectOneDailyThumbnail(dailyNum);
		
//		IntroInterestDTO introInterest = new IntroInterestDTO();

//		introInterest.setMemId(mId);
//		introInterest.setIntroNum(dailyNum);
		
//		int introCheck = introService.selectInter(introInterest);

		
		String interYn ="";
		
//		if(introCheck == 0) {
			
//			interYn = "Y";
			
//		} else {
			
//			interYn = "N";
//		}
		

		List<DailyRepDTO> rep = introService.selectDailyRep(dailyNum);
		System.out.println(rep);
		System.out.println(thumbnail);
		String path="";

		if(thumbnail != null) {
			if(rep !=null) {
			
				path = "/WEB-INF/views/allDaily/dailyWriteList.jsp";
				request.setAttribute("thumbnail", thumbnail);
				request.setAttribute("rep", rep);
				request.setAttribute("introNum", dailyNum);
				request.setAttribute("interYn", interYn);

							}else {
								path = "/WEB-INF/views/allDaily/dailyWriteList.jsp";
								request.setAttribute("thumbnail", thumbnail);
								request.setAttribute("introNum", dailyNum);
								request.setAttribute("interYn", interYn);


								  }
		}else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "게시물 목록 조회 실패!");
		}

		request.getRequestDispatcher(path).forward(request, response);
		}

	
}
		
