package com.allanimals.allDaily.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.allDaily.model.dto.IntroInterestDTO;
import com.allanimals.allDaily.service.AllDailyService;
import com.allanimals.member.model.dto.MemberDTO;

/**
 * Servlet implementation class IntroAnimalsInterestInsertServlet
 */
@WebServlet("/interest/insert")
public class IntroAnimalsInterestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IntroAnimalsInterestServlet() { }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	String memId = ((MemberDTO) request.getSession().getAttribute("loginMember")).getMemId();				
		String introNum = request.getParameter("introNum");
		
		IntroInterestDTO introInterest = new IntroInterestDTO();
		
		introInterest.setMemId(memId);
		introInterest.setIntroNum(introNum);		
		
		AllDailyService introInterestService = new AllDailyService();
		int introCheck = introInterestService.selectInter(introInterest);

		int result = 0;
		String interYn = "";
		
		if(introCheck == 0) {
			
			result = introInterestService.insertIntroInterest(introInterest);
			interYn = "Y";
			
		} else if(introCheck != 0) {
			
			result = introInterestService.deleteIntroInterest(introInterest);
			interYn = "N";
		}
		
		String path = "";
		
		if(result > 0) {
						
			response.setContentType("text/html;charset=utf-8");
			path = "/write/list?introNum="+introNum+"&&interYn="+interYn;
			response.sendRedirect(request.getContextPath()+path);
			
			
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "관심 등록에 실패하셨습니다.");
			request.getRequestDispatcher(path).forward(request, response);
		}

		
	}

}
