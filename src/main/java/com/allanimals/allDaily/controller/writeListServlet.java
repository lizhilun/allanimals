package com.allanimals.allDaily.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.allanimals.allDaily.model.dto.IntroDTO;
import com.allanimals.allDaily.model.dto.IntroInterestDTO;
import com.allanimals.allDaily.model.dto.IntroRepDTO;
import com.allanimals.allDaily.service.AllDailyService;
import com.allanimals.member.model.dto.MemberDTO;

/**
 * Servlet implementation class writeListServlet
 */
@WebServlet("/write/list")
public class writeListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		MemberDTO member = (MemberDTO) session.getAttribute("loginMember");
		
		String mId = "";
		
		if(member != null) {
			
			mId = member.getMemId();				
		}

		String introNum = request.getParameter("introNum");
		
		IntroDTO thumbnail = new IntroDTO();
		
		AllDailyService introService = new AllDailyService();
		thumbnail = introService.selectOneThumbnail(introNum);
		
		IntroInterestDTO introInterest = new IntroInterestDTO();

		introInterest.setMemId(mId);
		introInterest.setIntroNum(introNum);
		
		int introCheck = introService.selectInter(introInterest);

		
		String interYn ="";
		
		if(introCheck == 0) {
			
			interYn = "Y";
			
		} else {
			
			interYn = "N";
		}
		

		List<IntroRepDTO> rep = introService.selectIntroRep(introNum);
		System.out.println(rep);
		System.out.println(thumbnail);
		String path="";

		if(thumbnail != null) {
			if(rep !=null) {
			
				path = "/WEB-INF/views/allDaily/writeList.jsp";
				request.setAttribute("thumbnail", thumbnail);
				request.setAttribute("rep", rep);
				request.setAttribute("introNum", introNum);
				request.setAttribute("interYn", interYn);

							}else {
								path = "/WEB-INF/views/allDaily/writeList.jsp";
								request.setAttribute("thumbnail", thumbnail);
								request.setAttribute("introNum", introNum);
								request.setAttribute("interYn", interYn);


								  }
		}else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "게시물 목록 조회 실패!");
		}

		request.getRequestDispatcher(path).forward(request, response);
		}

	
}
		
