package com.allanimals.allDaily.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.allDaily.model.dto.ApplicationDTO;
import com.allanimals.allDaily.service.AllDailyService;
import com.allanimals.member.model.dto.MemberDTO;

/**
 * Servlet implementation class adoptApplicationServlet
 */
@WebServlet("/adopt/application")
public class AdoptApplicationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    
    	request.setAttribute("aniNum", request.getParameter("aniNum"));
    	request.getRequestDispatcher("/WEB-INF/views/allDaily/adoptApplication.jsp").forward(request, response);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		
		// 아이디 들고다니기		
		String memId = ((MemberDTO) request.getSession().getAttribute("loginMember")).getMemId();

		String aniNum = request.getParameter("aniNum");
        String callTime = request.getParameter("callTime");
        String memJob = request.getParameter("memJob");
        String marry = request.getParameter("marry");
        String ques1 = request.getParameter("ques1");
        String ques2 = request.getParameter("ques2");
        String ques3 = request.getParameter("ques3");
        String ques4 = request.getParameter("ques4");
        String ques5 = request.getParameter("ques5");
        String appCont = request.getParameter("appCont");
        
        AllDailyService service = new AllDailyService();
        ApplicationDTO apply = new ApplicationDTO();
		        
        apply.setMemId(memId);
        apply.setCallTime(callTime);
        apply.setMemJob(memJob);
        apply.setMarry(marry);
        apply.setQues1(ques1);
        apply.setQues2(ques2);
        apply.setQues3(ques3);
        apply.setQues4(ques4);
        apply.setQues5(ques5);
        apply.setAppCont(appCont);
        apply.setAniNum(aniNum);
 
		System.out.println(apply);
		
		int result = service.insertApplication(apply);
		
		String path="";
		
		if(result > 0) {
			response.setContentType("text/html;charset=utf-8");
			path ="/WEB-INF/views/allDaily/introAnimals.jsp";
			response.sendRedirect(request.getContextPath()+"/adopt/success");
			
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message","신청서 입력에 실패하셨습니다.");
			request.getRequestDispatcher(path).forward(request, response);
		}


    }

}
