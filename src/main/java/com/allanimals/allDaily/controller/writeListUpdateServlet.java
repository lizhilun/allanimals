package com.allanimals.allDaily.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.allanimals.allDaily.model.dto.IntroDTO;
import com.allanimals.allDaily.service.AllDailyService;
import com.allanimals.member.model.dto.MemberDTO;

/**
 * Servlet implementation class writeListServlet
 */
@WebServlet("/update/list")
public class writeListUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
//		String introNum = Integer.parseInt(request.getParameter("introNum"));
		String introNum = (request.getParameter("introNum"));
		
		
		IntroDTO thumbnail = new AllDailyService().selectOneThumbnail(introNum);
		
		System.out.println("thumbnail : " + thumbnail);

		
		
		String path = "";
		if(thumbnail != null) {
			
			path = "/WEB-INF/views/allDaily/introUpdate.jsp";
			request.setAttribute("thumbnail", thumbnail);
		} else {
			
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "글 수정 실패!");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		IntroDTO intro = new IntroDTO();

		
		intro.setIntroNum(request.getParameter("introNum"));
		intro.setIntroTitle(request.getParameter("introTitle"));
		intro.setIntroCont(request.getParameter("introCont"));
		intro.setIntroGen(request.getParameter("introGen"));
		intro.setIntroVac(request.getParameter("introVac"));
		intro.setIntroAge(request.getParameter("introAge"));
		intro.setAniName(request.getParameter("aniName"));
		intro.setAniSpec(request.getParameter("aniSpec"));
		intro.setAniAdopt(request.getParameter("aniAdopt"));
		intro.setAniNum(request.getParameter("aniNum"));
		
		System.out.println(intro);
		int result = new AllDailyService().writeListUpdate(intro);
		
		String path = "";
		if(result > 0) {
			
			path = "/write/list?introNum="+request.getParameter("introNum");
			response.sendRedirect(request.getContextPath()+path);
		} else {
			
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "글 수정 실패!");
			request.getRequestDispatcher(path).forward(request, response);
		}
		
		
		
	}

	
}
