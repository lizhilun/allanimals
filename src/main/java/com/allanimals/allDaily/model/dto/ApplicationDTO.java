package com.allanimals.allDaily.model.dto;

import java.sql.Date;

public class ApplicationDTO {
	
	private String appNum;
	private String memId;
	private String memName;
	private String aniNum;
	private Date appTime;
	private String appYn;
	private String situation;
	private String callTime;
	private String memJob;
	private String marry;
	private String ques1;
	private String ques2;
	private String ques3;
	private String ques4;
	private String ques5;
	private String appCont;
	private String memAddree;
	private String memPhone;
	private String memBirth;
	private String memEmail;
	
	public ApplicationDTO() {}

	public ApplicationDTO(String appNum, String memId, String memName, String aniNum, Date appTime, String appYn,
			String situation, String callTime, String memJob, String marry, String ques1, String ques2, String ques3,
			String ques4, String ques5, String appCont, String memAddree, String memPhone, String memBirth,
			String memEmail) {
		super();
		this.appNum = appNum;
		this.memId = memId;
		this.memName = memName;
		this.aniNum = aniNum;
		this.appTime = appTime;
		this.appYn = appYn;
		this.situation = situation;
		this.callTime = callTime;
		this.memJob = memJob;
		this.marry = marry;
		this.ques1 = ques1;
		this.ques2 = ques2;
		this.ques3 = ques3;
		this.ques4 = ques4;
		this.ques5 = ques5;
		this.appCont = appCont;
		this.memAddree = memAddree;
		this.memPhone = memPhone;
		this.memBirth = memBirth;
		this.memEmail = memEmail;
	}

	public String getAppNum() {
		return appNum;
	}

	public void setAppNum(String appNum) {
		this.appNum = appNum;
	}

	public String getMemId() {
		return memId;
	}

	public void setMemId(String memId) {
		this.memId = memId;
	}

	public String getMemName() {
		return memName;
	}

	public void setMemName(String memName) {
		this.memName = memName;
	}

	public String getAniNum() {
		return aniNum;
	}

	public void setAniNum(String aniNum) {
		this.aniNum = aniNum;
	}

	public Date getAppTime() {
		return appTime;
	}

	public void setAppTime(Date appTime) {
		this.appTime = appTime;
	}

	public String getAppYn() {
		return appYn;
	}

	public void setAppYn(String appYn) {
		this.appYn = appYn;
	}

	public String getSituation() {
		return situation;
	}

	public void setSituation(String situation) {
		this.situation = situation;
	}

	public String getCallTime() {
		return callTime;
	}

	public void setCallTime(String callTime) {
		this.callTime = callTime;
	}

	public String getMemJob() {
		return memJob;
	}

	public void setMemJob(String memJob) {
		this.memJob = memJob;
	}

	public String getMarry() {
		return marry;
	}

	public void setMarry(String marry) {
		this.marry = marry;
	}

	public String getQues1() {
		return ques1;
	}

	public void setQues1(String ques1) {
		this.ques1 = ques1;
	}

	public String getQues2() {
		return ques2;
	}

	public void setQues2(String ques2) {
		this.ques2 = ques2;
	}

	public String getQues3() {
		return ques3;
	}

	public void setQues3(String ques3) {
		this.ques3 = ques3;
	}

	public String getQues4() {
		return ques4;
	}

	public void setQues4(String ques4) {
		this.ques4 = ques4;
	}

	public String getQues5() {
		return ques5;
	}

	public void setQues5(String ques5) {
		this.ques5 = ques5;
	}

	public String getAppCont() {
		return appCont;
	}

	public void setAppCont(String appCont) {
		this.appCont = appCont;
	}

	public String getMemAddree() {
		return memAddree;
	}

	public void setMemAddree(String memAddree) {
		this.memAddree = memAddree;
	}

	public String getMemPhone() {
		return memPhone;
	}

	public void setMemPhone(String memPhone) {
		this.memPhone = memPhone;
	}

	public String getMemBirth() {
		return memBirth;
	}

	public void setMemBirth(String memBirth) {
		this.memBirth = memBirth;
	}

	public String getMemEmail() {
		return memEmail;
	}

	public void setMemEmail(String memEmail) {
		this.memEmail = memEmail;
	}

	@Override
	public String toString() {
		return "ApplicationDTO [appNum=" + appNum + ", memId=" + memId + ", memName=" + memName + ", aniNum=" + aniNum
				+ ", appTime=" + appTime + ", appYn=" + appYn + ", situation=" + situation + ", callTime=" + callTime
				+ ", memJob=" + memJob + ", marry=" + marry + ", ques1=" + ques1 + ", ques2=" + ques2 + ", ques3="
				+ ques3 + ", ques4=" + ques4 + ", ques5=" + ques5 + ", appCont=" + appCont + ", memAddree=" + memAddree
				+ ", memPhone=" + memPhone + ", memBirth=" + memBirth + ", memEmail=" + memEmail + "]";
	}
	
	
	
	
	
}