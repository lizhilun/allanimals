package com.allanimals.allDaily.model.dto;

import java.sql.Date;

public class DailyPhoDTO {

	private String phoNum;
	private String phoName;
	private String phoAdder;
	private String phoDel;
	private java.sql.Date phoDate;
	private String phoFile;
	private String dailyNum;
	private String thumPath;
	
	public DailyPhoDTO() {

	}

	public DailyPhoDTO(String phoNum, String phoName, String phoAdder, String phoDel, Date phoDate, String phoFile,
			String dailyNum, String thumPath) {
		super();
		this.phoNum = phoNum;
		this.phoName = phoName;
		this.phoAdder = phoAdder;
		this.phoDel = phoDel;
		this.phoDate = phoDate;
		this.phoFile = phoFile;
		this.dailyNum = dailyNum;
		this.thumPath = thumPath;
	}

	public String getPhoNum() {
		return phoNum;
	}

	public void setPhoNum(String phoNum) {
		this.phoNum = phoNum;
	}

	public String getPhoName() {
		return phoName;
	}

	public void setPhoName(String phoName) {
		this.phoName = phoName;
	}

	public String getPhoAdder() {
		return phoAdder;
	}

	public void setPhoAdder(String phoAdder) {
		this.phoAdder = phoAdder;
	}

	public String getPhoDel() {
		return phoDel;
	}

	public void setPhoDel(String phoDel) {
		this.phoDel = phoDel;
	}

	public java.sql.Date getPhoDate() {
		return phoDate;
	}

	public void setPhoDate(java.sql.Date phoDate) {
		this.phoDate = phoDate;
	}

	public String getPhoFile() {
		return phoFile;
	}

	public void setPhoFile(String phoFile) {
		this.phoFile = phoFile;
	}

	public String getDailyNum() {
		return dailyNum;
	}

	public void setDailyNum(String dailyNum) {
		this.dailyNum = dailyNum;
	}

	public String getThumPath() {
		return thumPath;
	}

	public void setThumPath(String thumPath) {
		this.thumPath = thumPath;
	}

	@Override
	public String toString() {
		return "DailyPhoDTO [phoNum=" + phoNum + ", phoName=" + phoName + ", phoAdder=" + phoAdder + ", phoDel="
				+ phoDel + ", phoDate=" + phoDate + ", phoFile=" + phoFile + ", dailyNum=" + dailyNum + ", thumPath="
				+ thumPath + "]";
	}

}
