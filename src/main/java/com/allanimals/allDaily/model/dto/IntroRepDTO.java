package com.allanimals.allDaily.model.dto;

import java.sql.Date;

public class IntroRepDTO {

	private String repNum;
	private String repCont;
	private Date repTime;
	private String repYn;
	private String introNum;
	private String memId;
	
	public IntroRepDTO() {}

	public IntroRepDTO(String repNum, String repCont, Date repTime, String repYn, String introNum, String memId) {
		super();
		this.repNum = repNum;
		this.repCont = repCont;
		this.repTime = repTime;
		this.repYn = repYn;
		this.introNum = introNum;
		this.memId = memId;
	}

	public String getRepNum() {
		return repNum;
	}

	public void setRepNum(String repNum) {
		this.repNum = repNum;
	}

	public String getRepCont() {
		return repCont;
	}

	public void setRepCont(String repCont) {
		this.repCont = repCont;
	}

	public Date getRepTime() {
		return repTime;
	}

	public void setRepTime(Date repTime) {
		this.repTime = repTime;
	}

	public String getRepYn() {
		return repYn;
	}

	public void setRepYn(String repYn) {
		this.repYn = repYn;
	}

	public String getIntroNum() {
		return introNum;
	}

	public void setIntroNum(String introNum) {
		this.introNum = introNum;
	}

	public String getMemId() {
		return memId;
	}

	public void setMemId(String memId) {
		this.memId = memId;
	}

	@Override
	public String toString() {
		return "IntroRepDTO [repNum=" + repNum + ", repCont=" + repCont + ", repTime=" + repTime + ", repYn=" + repYn
				+ ", introNum=" + introNum + ", memId=" + memId + "]";
	}
	
	
	
	
}
