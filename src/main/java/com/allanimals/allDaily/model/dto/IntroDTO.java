package com.allanimals.allDaily.model.dto;

import java.sql.Date;
import java.util.List;

import com.allanimals.member.model.dto.MemberDTO;

public class IntroDTO {
	
	private String introNum;
	private String introTitle;
	private String introCont;
	private String introGen;
	private String introVac;
	private String introAge;	
	private String aniNum;
	private String introYn;
	private java.sql.Date introTime;
	private String memId;
	private String thumPath;
	private String aniAdopt;
	private String aniSpec;
	private String aniName;
	private List<IntroPhoDTO> introFile;
	private List<MemberDTO> memList;
	
	public IntroDTO() {

	}

	public IntroDTO(String introNum, String introTitle, String introCont, String introGen, String introVac,
			String introAge, String aniNum, String introYn, Date introTime, String memId, String thumPath,
			String aniAdopt, String aniSpec, String aniName, List<IntroPhoDTO> introFile, List<MemberDTO> memList) {
		super();
		this.introNum = introNum;
		this.introTitle = introTitle;
		this.introCont = introCont;
		this.introGen = introGen;
		this.introVac = introVac;
		this.introAge = introAge;
		this.aniNum = aniNum;
		this.introYn = introYn;
		this.introTime = introTime;
		this.memId = memId;
		this.thumPath = thumPath;
		this.aniAdopt = aniAdopt;
		this.aniSpec = aniSpec;
		this.aniName = aniName;
		this.introFile = introFile;
		this.memList = memList;
	}

	public String getIntroNum() {
		return introNum;
	}

	public void setIntroNum(String introNum) {
		this.introNum = introNum;
	}

	public String getIntroTitle() {
		return introTitle;
	}

	public void setIntroTitle(String introTitle) {
		this.introTitle = introTitle;
	}

	public String getIntroCont() {
		return introCont;
	}

	public void setIntroCont(String introCont) {
		this.introCont = introCont;
	}

	public String getIntroGen() {
		return introGen;
	}

	public void setIntroGen(String introGen) {
		this.introGen = introGen;
	}

	public String getIntroVac() {
		return introVac;
	}

	public void setIntroVac(String introVac) {
		this.introVac = introVac;
	}

	public String getIntroAge() {
		return introAge;
	}

	public void setIntroAge(String introAge) {
		this.introAge = introAge;
	}

	public String getAniNum() {
		return aniNum;
	}

	public void setAniNum(String aniNum) {
		this.aniNum = aniNum;
	}

	public String getIntroYn() {
		return introYn;
	}

	public void setIntroYn(String introYn) {
		this.introYn = introYn;
	}

	public java.sql.Date getIntroTime() {
		return introTime;
	}

	public void setIntroTime(java.sql.Date introTime) {
		this.introTime = introTime;
	}

	public String getMemId() {
		return memId;
	}

	public void setMemId(String memId) {
		this.memId = memId;
	}

	public String getThumPath() {
		return thumPath;
	}

	public void setThumPath(String thumPath) {
		this.thumPath = thumPath;
	}

	public String getAniAdopt() {
		return aniAdopt;
	}

	public void setAniAdopt(String aniAdopt) {
		this.aniAdopt = aniAdopt;
	}

	public String getAniSpec() {
		return aniSpec;
	}

	public void setAniSpec(String aniSpec) {
		this.aniSpec = aniSpec;
	}

	public String getAniName() {
		return aniName;
	}

	public void setAniName(String aniName) {
		this.aniName = aniName;
	}

	public List<IntroPhoDTO> getIntroFile() {
		return introFile;
	}

	public void setIntroFile(List<IntroPhoDTO> introFile) {
		this.introFile = introFile;
	}

	public List<MemberDTO> getMemList() {
		return memList;
	}

	public void setMemList(List<MemberDTO> memList) {
		this.memList = memList;
	}

	@Override
	public String toString() {
		return "IntroDTO [introNum=" + introNum + ", introTitle=" + introTitle + ", introCont=" + introCont
				+ ", introGen=" + introGen + ", introVac=" + introVac + ", introAge=" + introAge + ", aniNum=" + aniNum
				+ ", introYn=" + introYn + ", introTime=" + introTime + ", memId=" + memId + ", thumPath=" + thumPath
				+ ", aniAdopt=" + aniAdopt + ", aniSpec=" + aniSpec + ", aniName=" + aniName + ", introFile="
				+ introFile + ", memList=" + memList + "]";
	}
	
}
