package com.allanimals.allDaily.model.dto;

import java.sql.Date;
import java.util.List;

import com.allanimals.member.model.dto.MemberDTO;

public class DailyDTO {

	private String dailyNum;
	private String dailyTitle;
	private String dailyCont;
	private java.sql.Date dailyTime;
	private String aniNum;
	private String dailyYn;
	private String memId;
	private String thumPath;
	private String aniAdopt;
	private String aniSpec;
	private String aniName;
	private String introAge;
	private String introGen;
	private List<DailyPhoDTO> introFile;
	private List<MemberDTO> memList;
	
	public DailyDTO() {

	}

	public DailyDTO(String dailyNum, String dailyTitle, String dailyCont, Date dailyTime, String aniNum, String dailyYn,
			String memId, String thumPath, String aniAdopt, String aniSpec, String aniName, String introAge,
			String introGen, List<DailyPhoDTO> introFile, List<MemberDTO> memList) {
		super();
		this.dailyNum = dailyNum;
		this.dailyTitle = dailyTitle;
		this.dailyCont = dailyCont;
		this.dailyTime = dailyTime;
		this.aniNum = aniNum;
		this.dailyYn = dailyYn;
		this.memId = memId;
		this.thumPath = thumPath;
		this.aniAdopt = aniAdopt;
		this.aniSpec = aniSpec;
		this.aniName = aniName;
		this.introAge = introAge;
		this.introGen = introGen;
		this.introFile = introFile;
		this.memList = memList;
	}

	public String getDailyNum() {
		return dailyNum;
	}

	public void setDailyNum(String dailyNum) {
		this.dailyNum = dailyNum;
	}

	public String getDailyTitle() {
		return dailyTitle;
	}

	public void setDailyTitle(String dailyTitle) {
		this.dailyTitle = dailyTitle;
	}

	public String getDailyCont() {
		return dailyCont;
	}

	public void setDailyCont(String dailyCont) {
		this.dailyCont = dailyCont;
	}

	public java.sql.Date getDailyTime() {
		return dailyTime;
	}

	public void setDailyTime(java.sql.Date dailyTime) {
		this.dailyTime = dailyTime;
	}

	public String getAniNum() {
		return aniNum;
	}

	public void setAniNum(String aniNum) {
		this.aniNum = aniNum;
	}

	public String getDailyYn() {
		return dailyYn;
	}

	public void setDailyYn(String dailyYn) {
		this.dailyYn = dailyYn;
	}

	public String getMemId() {
		return memId;
	}

	public void setMemId(String memId) {
		this.memId = memId;
	}

	public String getThumPath() {
		return thumPath;
	}

	public void setThumPath(String thumPath) {
		this.thumPath = thumPath;
	}

	public String getAniAdopt() {
		return aniAdopt;
	}

	public void setAniAdopt(String aniAdopt) {
		this.aniAdopt = aniAdopt;
	}

	public String getAniSpec() {
		return aniSpec;
	}

	public void setAniSpec(String aniSpec) {
		this.aniSpec = aniSpec;
	}

	public String getAniName() {
		return aniName;
	}

	public void setAniName(String aniName) {
		this.aniName = aniName;
	}

	public String getIntroAge() {
		return introAge;
	}

	public void setIntroAge(String introAge) {
		this.introAge = introAge;
	}

	public String getIntroGen() {
		return introGen;
	}

	public void setIntroGen(String introGen) {
		this.introGen = introGen;
	}

	public List<DailyPhoDTO> getIntroFile() {
		return introFile;
	}

	public void setIntroFile(List<DailyPhoDTO> introFile) {
		this.introFile = introFile;
	}

	public List<MemberDTO> getMemList() {
		return memList;
	}

	public void setMemList(List<MemberDTO> memList) {
		this.memList = memList;
	}

	@Override
	public String toString() {
		return "DailyDTO [dailyNum=" + dailyNum + ", dailyTitle=" + dailyTitle + ", dailyCont=" + dailyCont
				+ ", dailyTime=" + dailyTime + ", aniNum=" + aniNum + ", dailyYn=" + dailyYn + ", memId=" + memId
				+ ", thumPath=" + thumPath + ", aniAdopt=" + aniAdopt + ", aniSpec=" + aniSpec + ", aniName=" + aniName
				+ ", introAge=" + introAge + ", introGen=" + introGen + ", introFile=" + introFile + ", memList="
				+ memList + "]";
	}

}
