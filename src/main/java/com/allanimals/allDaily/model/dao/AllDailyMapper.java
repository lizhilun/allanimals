package com.allanimals.allDaily.model.dao;

import java.util.List;
import java.util.Map;

import com.allanimals.allDaily.model.dto.ApplicationDTO;
import com.allanimals.allDaily.model.dto.DailyDTO;
import com.allanimals.allDaily.model.dto.DailyPhoDTO;
import com.allanimals.allDaily.model.dto.DailyRepDTO;
import com.allanimals.allDaily.model.dto.IntroDTO;
import com.allanimals.allDaily.model.dto.IntroInterestDTO;
import com.allanimals.allDaily.model.dto.IntroPhoDTO;
import com.allanimals.allDaily.model.dto.IntroRepDTO;
import com.allanimals.commnon.pagin.SelectCriteria;
import com.allanimals.member.model.dto.MemberDTO;

public interface AllDailyMapper {

	List<IntroDTO> IntroAnimalsList();

	List<DailyDTO> DailyAnimalsList();

	// 페이징
	List<IntroDTO> selectIntroList(Map<String, Object> searchMap);
	// 일상글 전체조회
	int selectTotalCount(Map<String, Object> searchMap);
	
	// 상세조회
	IntroDTO selectOneThumbnail(String introNum);

	// 글등록 - 썸네일	
	int insertIntroContent(IntroDTO intro);

	// 글작성
	int insertThumbnailContent(IntroDTO intro);
	// 글번호 값이 들어오는지 확인
	String selectIntroNum();

	int insertIntroPho(IntroPhoDTO introPhoDTO);

	// 정보 추가 INSERT문
	int insertIntroId(IntroDTO intro);
	
	// 정보 추가 INSERT문2
	int insertIntroAni(IntroDTO intro);
	
	// 게시글 삭제
	int deleteList(IntroDTO introDto);
	// 게시글 수정
	int writeListUpdate(IntroDTO intro);
	int aniUpdate(IntroDTO intro);
	
	// 입양 신청서 작성

	int insertApplication(ApplicationDTO apply);
	
	// 소개글 댓글 작성
	int insertIntroRep(IntroRepDTO rep);

	// 소개글 댓글 삭제 
	int deleteIntroRep(IntroRepDTO rep);

	// 소개글 댓글 조회
	List<IntroRepDTO> selectIntroRep(String introNum);

	// 소개글 관심 설정
	int insertIntroInterest(IntroInterestDTO introInterest);

	int selectInter(IntroInterestDTO introInterest);

	int deleteIntroInterest(IntroInterestDTO introInterest);

	// 일상글 전제초회
	int selectTotalDailyCount(Map<String, Object> searchMap);
	// 일상글 페이징
	List<DailyDTO> selectDailyList(Map<String, Object> searchMap);
	
	// 일상글 글쓰기 - 썸네일
	int insertDailyThumbnailContent(DailyDTO daily);
	// 일상글 정보 추가 - 아이디
	int insertDailyId(DailyDTO daily);
	// 일상글 정보 추가 - 사진
	int insertDailyPho(DailyPhoDTO dailyPhoDTO);
	// 일상글 정보 추가 - 동물 -> 동물 정보는 있어서 받아오는 방식을 사용해야함
	int insertDailyAni(DailyDTO daily);
	// 일상글 상세조회
	DailyDTO selectOneDailyThumbnail(String dailyNum);
	// 일상글 댓글조회
	List<DailyRepDTO> selectDailyRep(String dailyNum);
	// 일상글 동물번호 가져오기
//	IntroDTO selectAniNum(String mId);


	
	







}
