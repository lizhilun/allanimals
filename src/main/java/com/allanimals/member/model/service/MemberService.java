package com.allanimals.member.model.service;

import static com.allanimals.commnon.mybatis.Template.getSqlSession;

import org.apache.ibatis.session.SqlSession;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.allanimals.member.model.dao.MemberMapper;
import com.allanimals.member.model.dto.MemberDTO;

public class MemberService {
	
	private MemberMapper memberMapper;

	/**
	 * <pre>
	 *	 로그인 메소드
	 * </pre>
	 * @param requestMember
	 * @return
	 */
	public MemberDTO loginCheck(MemberDTO requestMember) {
		
		SqlSession sqlSession = getSqlSession();
		memberMapper = sqlSession.getMapper(MemberMapper.class);
		
		MemberDTO loginMember = null;
		String encPwd = memberMapper.selectEncryptedPwd(requestMember);
		
		System.out.println("encPwd 확인 : " + encPwd);
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		
		if(passwordEncoder.matches(requestMember.getMemPwd(), encPwd)) {
			
			loginMember = memberMapper.selectLoginMember(requestMember);
		}
		
		sqlSession.close();
		
		
		return loginMember;
	}
	
	/**<pre>
	 * 회원가입용 메소드
	 * </pre>
	 * @param requestMem
	 * @return
	 */
	public int registMember(MemberDTO requestMem) {
		
		SqlSession sqlSession = getSqlSession();
		memberMapper = sqlSession.getMapper(MemberMapper.class);
		
		int result = memberMapper.insertMem(requestMem);
		
		if(result > 0) {
			
			sqlSession.commit();
		
		} else {
			
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}
	
	/**
	 * <pre>
	 * 사용자 아이디 중복 체크용 메소드
	 * </pre>
	 * @param memId
	 * @return
	 */
	public int overlapCheckId(String memId) {
		
		SqlSession sqlSession = getSqlSession();
		memberMapper = sqlSession.getMapper(MemberMapper.class);
		
		int result = memberMapper.overlapCheckId(memId);
		
		sqlSession.close();
		
		return result;
	}
	
	/**<pre>
	 * 사용자 아이디 찾기 메소드
	 * </pre>
	 * @param requestMember
	 * @return
	 */
	public MemberDTO findId(MemberDTO requestMember) {
		
		SqlSession sqlSession = getSqlSession();
		memberMapper = sqlSession.getMapper(MemberMapper.class);
		MemberDTO memfindId = memberMapper.findId(requestMember);
		
		sqlSession.close();
		
		return memfindId;
	}
	
	/** <pre>
	 * 사용자 비밀번호 재설정 메소드
	 * </pre>
	 * @param requestMember
	 * @return
	 */
	public int updatePw(MemberDTO requestMember) {
		
		SqlSession sqlSession = getSqlSession();
		memberMapper = sqlSession.getMapper(MemberMapper.class);
		
		int result = memberMapper.updatePw(requestMember);
		System.out.println("왜 안나올까? " + requestMember);
		if(result > 0) {
			
			sqlSession.commit();
		} else {
			
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}
	
	/**
	 * <pre>
	 *    회원정보 조회
	 * </pre>
	 * @param memId
	 * @return
	 */
	public MemberDTO selectOneMemById(String memId) {
		
		SqlSession sqlSession = getSqlSession();
		
		memberMapper = sqlSession.getMapper(MemberMapper.class);
		MemberDTO memInfo = memberMapper.selectMemberInfoList(memId);
		
		sqlSession.close();
		
		return memInfo;
	}
	
	/**
	 * <pre>
	 * 	회원정보 변경
	 * </pre>
	 * @param reviseMember
	 * @return
	 */
	public int updateMemInfo(MemberDTO reviseMember) {
		
		SqlSession sqlSession = getSqlSession();
		
		memberMapper = sqlSession.getMapper(MemberMapper.class);
		int result = memberMapper.updateMemInfo(reviseMember);
		
		if(result > 0 ) {
			
			sqlSession.commit();
		}else {
			
			sqlSession.rollback();
		}
		sqlSession.close();
		
		return result;
	}
	/**
	 * <pre>
	 * 	기존 비밀번호 맞는지 확인하는 확인용 서블릿
	 * </pre>
	 * @param passWordCheck
	 * @return
	 */
	public Boolean checkPassword(MemberDTO passWordCheck) {
		//id를 이용해서  pwd를 조회 조회된결과로 입력받은 비번과 매치로 비교를ㅐ서 같으면 비밀번호ㅓㅏ 인증완료.(암호화가 되어있지 않으니까 매치메소드 말고 그냥 equls로 비교해두기.)  
		SqlSession sqlSession = getSqlSession();
		memberMapper = sqlSession.getMapper(MemberMapper.class);
	
		Boolean passCheck = false;
		String encPwd = memberMapper.checkPassword(passWordCheck);
//		
		System.out.println("encPwd 확인 : " + encPwd);
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		
		if(passwordEncoder.matches(passWordCheck.getMemPwd(), encPwd)) {
			passCheck = true;
			System.out.println("비밀번호 확인완료");
		}
	
		sqlSession.close();
		
		return passCheck;
		
	}

	/**
	 * <pre>
	 *  회원탈퇴
	 * </pre>
	 * @param deleteMember
	 * @return
	 */
	
	public int deleteMember(MemberDTO deleteMember) {
		
		SqlSession sqlSession = getSqlSession();
		
		memberMapper = sqlSession.getMapper(MemberMapper.class);
		int result = memberMapper.deleteMember(deleteMember);
		
		if(result > 0 ) {
			
			sqlSession.commit();
		}else {
			
			sqlSession.rollback();
		}
		sqlSession.close();
		
		return result;
		
	}

	/**<pre>
	 * 저주스러운 이메일 전송 메소드
	 * </pre>
	 * @param memId
	 * @return
	 */
	public MemberDTO selectMember(String memId) {
		
		SqlSession sqlSession = getSqlSession();
		memberMapper = sqlSession.getMapper(MemberMapper.class);
		
		MemberDTO member = memberMapper.selectMember(memId);
		
		sqlSession.close();
		
		return member;
	}

	

}
