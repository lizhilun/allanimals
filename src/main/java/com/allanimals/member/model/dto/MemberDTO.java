package com.allanimals.member.model.dto;

import java.sql.Date;

public class MemberDTO {
	
	private String memId;
	private String memPwd;
	private String memName;
	private String memAddre;
	private String memEmail;
	private String memPhone;
	private java.sql.Date memBirth;
	private String memSup;
	private String introYN;
	
	public MemberDTO(){}

	public MemberDTO(String memId, String memPwd, String memName, String memAddre, String memEmail, String memPhone,
			Date memBirth, String memSup, String introYN) {
		super();
		this.memId = memId;
		this.memPwd = memPwd;
		this.memName = memName;
		this.memAddre = memAddre;
		this.memEmail = memEmail;
		this.memPhone = memPhone;
		this.memBirth = memBirth;
		this.memSup = memSup;
		this.introYN = introYN;
	}

	public String getMemId() {
		return memId;
	}

	public void setMemId(String memId) {
		this.memId = memId;
	}

	public String getMemPwd() {
		return memPwd;
	}

	public void setMemPwd(String memPwd) {
		this.memPwd = memPwd;
	}

	public String getMemName() {
		return memName;
	}

	public void setMemName(String memName) {
		this.memName = memName;
	}

	public String getMemAddre() {
		return memAddre;
	}

	public void setMemAddre(String memAddre) {
		this.memAddre = memAddre;
	}

	public String getMemEmail() {
		return memEmail;
	}

	public void setMemEmail(String memEmail) {
		this.memEmail = memEmail;
	}

	public String getMemPhone() {
		return memPhone;
	}

	public void setMemPhone(String memPhone) {
		this.memPhone = memPhone;
	}

	public java.sql.Date getMemBirth() {
		return memBirth;
	}

	public void setMemBirth(java.sql.Date memBirth) {
		this.memBirth = memBirth;
	}

	public String getMemSup() {
		return memSup;
	}

	public void setMemSup(String memSup) {
		this.memSup = memSup;
	}

	public String getIntroYN() {
		return introYN;
	}

	public void setIntroYN(String introYN) {
		this.introYN = introYN;
	}

	@Override
	public String toString() {
		return "MemberDTO [memId=" + memId + ", memPwd=" + memPwd + ", memName=" + memName + ", memAddre=" + memAddre
				+ ", memEmail=" + memEmail + ", memPhone=" + memPhone + ", memBirth=" + memBirth + ", memSup=" + memSup
				+ ", introYN=" + introYN + "]";
	}

}
