package com.allanimals.member.model.dao;

import com.allanimals.member.model.dto.MemberDTO;

public interface MemberMapper {

	String selectEncryptedPwd(MemberDTO requestMember);

	MemberDTO selectLoginMember(MemberDTO requestMember);

	MemberDTO selectMemberInfoList(String memId);

	int updateMemInfo(MemberDTO reviseMember);

	String checkPassword(MemberDTO passWordCheck);

	int insertMem(MemberDTO requestMem);

	int overlapCheckId(String memId);


	int deleteMember(MemberDTO deleteMember);

	MemberDTO findId(MemberDTO requestMember);

	int updatePw(MemberDTO requestMember);

	MemberDTO selectMember(String memId);

	
}
