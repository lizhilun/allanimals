package com.allanimals.member.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.allanimals.member.model.dto.MemberDTO;
import com.allanimals.member.model.service.MemberService;

/**
 * Servlet implementation class MemberLoginServelt
 */
@WebServlet("/member/login")
public class MemberLoginServelt extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/views/member/login.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String memId = request.getParameter("memId");
		String memPwd = request.getParameter("memPwd");
		
		System.out.println("member id : " + memId);
		System.out.println("member pw : " + memPwd);
		
		MemberDTO requestMember = new MemberDTO();
		requestMember.setMemId(memId);
		requestMember.setMemPwd(memPwd);
		
		MemberService memberService = new MemberService();
		
		MemberDTO loginMember = memberService.loginCheck(requestMember);
		System.out.println(loginMember);
		
		if(loginMember != null) {
			
			HttpSession session = request.getSession();
			session.setAttribute("loginMember", loginMember);
			System.out.println("loginmember: " + loginMember);
			response.sendRedirect(request.getContextPath()+"/main");
			
		}else {
			
			request.setAttribute("message", "아이디와 비밀번호를 다시 확인해주세요");
			request.getRequestDispatcher("/WEB-INF/views/common/failed.jsp").forward(request, response);
		}
	}
}
