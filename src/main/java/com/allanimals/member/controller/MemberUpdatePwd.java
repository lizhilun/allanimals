package com.allanimals.member.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.member.model.dto.MemberDTO;
import com.allanimals.member.model.service.MemberService;

/**
 * Servlet implementation class MemberFindPw
 */
@WebServlet("/member/updatepwd")
public class MemberUpdatePwd extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String memId = request.getParameter("memId");
		request.setAttribute("memId", memId);
		request.getRequestDispatcher("/WEB-INF/views/member/updatePwEnter.jsp").forward(request, response);
	}
       
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		MemberDTO requestMember = new MemberDTO();
		requestMember.setMemId(request.getParameter("memId"));
		requestMember.setMemPwd(request.getParameter("memPwd"));
		
		System.out.println(requestMember);
		
		int result = new MemberService().updatePw(requestMember);
		System.out.println("result 확인 : " + result);
		
		String Path = "";
		
		if( result > 0){
			
			request.setAttribute("memUpdatePwd", result);
//			Path = "/WEB-INF/views/member/updatePwResult.jsp";
			request.getRequestDispatcher("/WEB-INF/views/member/updatePwResult.jsp").forward(request, response);
			
		} else {
			 request.setAttribute("Message", "요청실패. 뒤로가기를 눌러주세요");
			 Path = "/WEB-INF/views/common/failed.jsp";
			 request.getRequestDispatcher(Path).forward(request, response);
		}
		
		
//		request.getRequestDispatcher(Path).forward(request, response);
	}

}
