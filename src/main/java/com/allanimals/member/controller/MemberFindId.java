package com.allanimals.member.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.allanimals.member.model.dto.MemberDTO;
import com.allanimals.member.model.service.MemberService;

/**
 * Servlet implementation class MemberFindId
 */
@WebServlet("/member/findid")
public class MemberFindId extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		MemberDTO requestMember = new MemberDTO();
		requestMember.setMemPhone(request.getParameter("memPhone"));
		requestMember.setMemEmail(request.getParameter("memEmail"));
		
		System.out.println(requestMember);
		
		MemberService memberService = new MemberService();
		
		MemberDTO memfindId = memberService.findId(requestMember);
		System.out.println(memfindId);
		
		String path = "";
		
		if(memfindId != null) {
			
			request.setAttribute("memfindId", memfindId);
			path = "/WEB-INF/views/member/findidResult.jsp";
			
		} else {
			
			request.setAttribute("message", "요청 실패. 뒤로가기를 눌러주세요.");
			path = "/WEB-INF/views/common/failed.jsp";
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
	}
	
	

}
