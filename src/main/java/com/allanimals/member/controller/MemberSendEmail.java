package com.allanimals.member.controller;

import java.io.IOException;
import javax.mail.PasswordAuthentication;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.member.model.dto.MemberDTO;
import com.allanimals.member.model.service.MemberService;

/**
 * Servlet implementation class MemberSendEmail
 */
@WebServlet("/memsendemail")
public class MemberSendEmail extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String memId = request.getParameter("memId");
//		String memEmail = request.getParameter("memEmail");
		
		MemberDTO member = new MemberDTO();
		MemberService memberService = new MemberService();
		member = memberService.selectMember(memId);
		
		String host = "smtp.gmail.com";
//		String host = "smtp.naver.com";
		String user = "yhc102155@gmail.com";
//		String user = "yhc10215@naver.com";
		String password = "cptpppxwayskompx";
//		String password = "dbgmlckd92^^!";
//		String password = "RPCEWYVHZB79";
		
		
		String to_email = member.getMemEmail();
		
		Properties props = new Properties();
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", 465);
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.ssl.enable", "true");
		props.put("mail.smtp.ssl.trust", host);
		
//		props.put("mail.smtp.starttls.required", "true");
//		props.put("mail.transport.protocol", "smtp");
//		props.put("mail.smtp.ssl.protocols", "TLSv1.2");
		
		
		
		
		 Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
             protected PasswordAuthentication getPasswordAuthentication() {
                 return new PasswordAuthentication(user, password);
			}
		});
		
		 
		 try {
			 
			 MimeMessage message = new MimeMessage(session);
			 System.out.println("갔니?");
			 message.setFrom(new InternetAddress(user, "ALLANIMALS"));
			 message.addRecipient(Message.RecipientType.TO, new InternetAddress(to_email));
			 
			 //메일 제목
			 message.setSubject("안녕하세요 ALLANIMALS입니다.");
			 
			 //메일 내용
			 message.setText("해당 링크를 클릭해주세요 : " + "http://localhost:8080/allanimals/member/updatepwd?memId="+memId );
			 Transport.send(message);
			 System.out.println("이메일 전송");
			 
		 } catch (Exception e) {
			 
			 e.printStackTrace();
		 }
		 
		 request.getRequestDispatcher("/WEB-INF/views/member/idsendEmailResult.jsp").forward(request, response);
		
		
		
		
	}

}
