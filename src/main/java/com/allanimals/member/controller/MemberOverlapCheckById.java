package com.allanimals.member.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.member.model.service.MemberService;

/**
 * Servlet implementation class MemberOverlapCheckById
 */
@WebServlet("/member/overlapCheck")
public class MemberOverlapCheckById extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String memId = request.getParameter("memId");
		System.out.println("memId : " + memId);
		
		MemberService memberService = new MemberService();
		
		int result = memberService.overlapCheckId(memId);
		System.out.println("result : " + result);
		
		boolean overlapCheck = false;
		
		if(result > 0) {
			
			overlapCheck = false;
			
		} else {
			
			overlapCheck = true;
		}
		
		response.setCharacterEncoding("UTF-8");
		
		PrintWriter out = response.getWriter();
		out.print(overlapCheck);
		
		out.flush();
		out.close();
		
		
	}

}
