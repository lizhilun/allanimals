package com.allanimals.member.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.member.model.dto.MemberDTO;
import com.allanimals.member.model.service.MemberService;

/**
 * Servlet implementation class MemberRegist
 */
@WebServlet("/member/regist")
public class MemberRegist extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/views/member/join.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String memId = request.getParameter("memId");
		String memPwd = request.getParameter("memPwd");
		String memName = request.getParameter("memName");
		String memAddre = request.getParameter("memAddre") + request.getParameter("memAddre2");
		String memPhone = request.getParameter("memPhone");
		String memEmail = request.getParameter("memEmail");
		java.sql.Date memBirth = java.sql.Date.valueOf(request.getParameter("memBirth"));
		
		MemberDTO requestMem = new MemberDTO();
		requestMem.setMemId(memId);
		requestMem.setMemPwd(memPwd);
		requestMem.setMemName(memName);
		requestMem.setMemBirth(memBirth);
		requestMem.setMemPhone(memPhone);
		requestMem.setMemEmail(memEmail);
		requestMem.setMemAddre(memAddre);
		
		System.out.println("requestMember : " + requestMem);
		int result = new MemberService().registMember(requestMem);
		System.out.println("result 확인 : " + result);
		String page = "";
		
		if(result > 0) {
			request.getRequestDispatcher("/WEB-INF/views/member/complete_join.jsp").forward(request, response);
			
		} else {
			page = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "다시 입력해주세요");
			
		}
	}

}
