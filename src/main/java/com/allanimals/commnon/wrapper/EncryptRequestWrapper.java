package com.allanimals.commnon.wrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class EncryptRequestWrapper extends HttpServletRequestWrapper {

	public EncryptRequestWrapper(HttpServletRequest request) {
		super(request);
	}

	@Override
	public String getParameter(String key) {
	
		String value = "";
		if("memPwd".equals(key)) {

			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			value = passwordEncoder.encode(super.getParameter(key));
			System.out.println("비밀번호 확인용 출력" + value);
			
		}else {
			value = super.getParameter(key);
		}
		
		return value;
	}

	
}
