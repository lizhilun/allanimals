package com.allanimals.review.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.review.model.dto.ReviewBoardDTO;
import com.allanimals.review.model.service.ReviewBoardService;

/**
 * Servlet implementation class ReviewDeleteServlet
 */
@WebServlet("/review/delete")
public class ReviewDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	String afterNum = request.getParameter("afterNum");
		
		ReviewBoardService reviewService = new ReviewBoardService();
		ReviewBoardDTO reviewDTO = new ReviewBoardDTO();
		
		System.out.println(afterNum);
		
		int result = reviewService.deleteReview(afterNum);
		
		String path="";
		
		if(result > 0) {
			path = "/WEB-INF/views/inquiry/reviewList.jsp";
			
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "게시글 삭제에 실패하셨습니다.");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
    	
	}

}
