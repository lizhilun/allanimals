package com.allanimals.review.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.review.model.dto.ReviewBoardDTO;
import com.allanimals.review.model.service.ReviewBoardService;

/**
 * Servlet implementation class AdoptReviewRevise
 */
@WebServlet("/review/revise")
public class ReviewReviseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String afterNum = request.getParameter("afterNum");
		System.out.println(afterNum);
		
		ReviewBoardDTO reviewDTO = new ReviewBoardDTO();
		
		ReviewBoardService reviewService = new ReviewBoardService();
		reviewDTO = reviewService.selectOneReviewBoard(afterNum);
		
		System.out.println(reviewDTO);
		
		
		String path="";
		if(reviewDTO != null) {
		path = "/WEB-INF/views/inquiry/reviewList.jsp";
		request.setAttribute("reviewDTO", reviewDTO); 
		}else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "게시물 조회 실패!");
		}
		
		request.getRequestDispatcher(path).forward(request, response);

	}
	
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		
		String afterTitle = request.getParameter("afterTitle");
		String afterCont = request.getParameter("afterCont");
		String afterNum = request.getParameter("afterNum");
		//String memId =((MemberDTO) request.getSession().getAttribute("loginMember")).getMemId();
		//DATE 삽입
		
		ReviewBoardService reviewService = new ReviewBoardService();
		ReviewBoardDTO reviewDTO = new ReviewBoardDTO();
		
		reviewDTO.setAfterNum(afterNum);
		//reviewDTO.setMemId(memId);
		reviewDTO.setAfterTitle(afterTitle);
		reviewDTO.setAfterCont(afterCont);
		System.out.println(reviewDTO);
		
		int result = reviewService.reviseReview(reviewDTO);
		
		String path="";
		
		if(result > 0) {
			path = "/WEB-INF/views/inquiry/reviewList.jsp";
			
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "게시글 수정에 실패하셨습니다.");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
	}


}
