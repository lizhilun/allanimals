package com.allanimals.review.model.dao;

import java.util.List;
import java.util.Map;

import com.allanimals.commnon.pagin.SelectCriteria;
import com.allanimals.review.model.dto.AttachmentDTO;
import com.allanimals.review.model.dto.ReviewBoardDTO;

public interface ReviewBoardMapper {

	List<ReviewBoardDTO> selectReviewList(SelectCriteria selectCriteria);

	ReviewBoardDTO selectOneReviewBoard(String afterNum);

	int selectTotalCount(Map<String, String> searchMap);

	int insertReview(ReviewBoardDTO thumbnail);

	List<ReviewBoardDTO> selectReviewList();

	int insertReviewContent(ReviewBoardDTO review);

	int insertAfterPho(AttachmentDTO afterPhoDTO);

	int reviseReview(ReviewBoardDTO reviewDTO);

	int deleteReview(String afterNum);

}
