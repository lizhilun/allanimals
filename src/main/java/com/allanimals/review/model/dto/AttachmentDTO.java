package com.allanimals.review.model.dto;

import java.sql.Date;

public class AttachmentDTO {
	
	private String phoNum;
	private String phoName;
	private String phoAddre;
	private String phoDel;
	private Date phoDate;
	private String phoFile;
	private String afterNum;
	private String thumPath;
	
	public AttachmentDTO() {}

	public AttachmentDTO(String phoNum, String phoName, String phoAddre, String phoDel, Date phoDate, String phoFile,
			String afterNum, String thumPath) {
		super();
		this.phoNum = phoNum;
		this.phoName = phoName;
		this.phoAddre = phoAddre;
		this.phoDel = phoDel;
		this.phoDate = phoDate;
		this.phoFile = phoFile;
		this.afterNum = afterNum;
		this.thumPath = thumPath;
	}

	public String getPhoNum() {
		return phoNum;
	}

	public void setPhoNum(String phoNum) {
		this.phoNum = phoNum;
	}

	public String getPhoName() {
		return phoName;
	}

	public void setPhoName(String phoName) {
		this.phoName = phoName;
	}

	public String getPhoAddre() {
		return phoAddre;
	}

	public void setPhoAddre(String phoAddre) {
		this.phoAddre = phoAddre;
	}

	public String getPhoDel() {
		return phoDel;
	}

	public void setPhoDel(String phoDel) {
		this.phoDel = phoDel;
	}

	public Date getPhoDate() {
		return phoDate;
	}

	public void setPhoDate(Date phoDate) {
		this.phoDate = phoDate;
	}

	public String getPhoFile() {
		return phoFile;
	}

	public void setPhoFile(String phoFile) {
		this.phoFile = phoFile;
	}

	public String getAfterNum() {
		return afterNum;
	}

	public void setAfterNum(String afterNum) {
		this.afterNum = afterNum;
	}

	public String getThumPath() {
		return thumPath;
	}

	public void setThumPath(String thumPath) {
		this.thumPath = thumPath;
	}

	@Override
	public String toString() {
		return "AttachmentDTO [phoNum=" + phoNum + ", phoName=" + phoName + ", phoAddre=" + phoAddre + ", phoDel="
				+ phoDel + ", phoDate=" + phoDate + ", phoFile=" + phoFile + ", afterNum=" + afterNum + ", thumPath="
				+ thumPath + "]";
	}

	
	
}