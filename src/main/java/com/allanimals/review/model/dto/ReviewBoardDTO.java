package com.allanimals.review.model.dto;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class ReviewBoardDTO {

	private String afterTitle;
	private String afterCont;
    private Date afterTime;
    private String afterNum;
    private String afterYn;
    private String memId;
    private String repNum;
    private String repCont;
    private Date repTime;
    private String repYn;
	private List<AttachmentDTO> attachmentList;
	
	public ReviewBoardDTO() {}

	public ReviewBoardDTO(String afterTitle, String afterCont, Date afterTime, String afterNum, String afterYn,
			String memId, String repNum, String repCont, Date repTime, String repYn,
			List<AttachmentDTO> attachmentList) {
		super();
		this.afterTitle = afterTitle;
		this.afterCont = afterCont;
		this.afterTime = afterTime;
		this.afterNum = afterNum;
		this.afterYn = afterYn;
		this.memId = memId;
		this.repNum = repNum;
		this.repCont = repCont;
		this.repTime = repTime;
		this.repYn = repYn;
		this.attachmentList = attachmentList;
	}

	public String getAfterTitle() {
		return afterTitle;
	}

	public void setAfterTitle(String afterTitle) {
		this.afterTitle = afterTitle;
	}

	public String getAfterCont() {
		return afterCont;
	}

	public void setAfterCont(String afterCont) {
		this.afterCont = afterCont;
	}

	public Date getAfterTime() {
		return afterTime;
	}

	public void setAfterTime(Date afterTime) {
		this.afterTime = afterTime;
	}

	public String getAfterNum() {
		return afterNum;
	}

	public void setAfterNum(String afterNum) {
		this.afterNum = afterNum;
	}

	public String getAfterYn() {
		return afterYn;
	}

	public void setAfterYn(String afterYn) {
		this.afterYn = afterYn;
	}

	public String getMemId() {
		return memId;
	}

	public void setMemId(String memId) {
		this.memId = memId;
	}

	public String getRepNum() {
		return repNum;
	}

	public void setRepNum(String repNum) {
		this.repNum = repNum;
	}

	public String getRepCont() {
		return repCont;
	}

	public void setRepCont(String repCont) {
		this.repCont = repCont;
	}

	public Date getRepTime() {
		return repTime;
	}

	public void setRepTime(Date repTime) {
		this.repTime = repTime;
	}

	public String getRepYn() {
		return repYn;
	}

	public void setRepYn(String repYn) {
		this.repYn = repYn;
	}

	public List<AttachmentDTO> getAttachmentList() {
		return attachmentList;
	}

	public void setAttachmentList(List<AttachmentDTO> attachmentList) {
		this.attachmentList = attachmentList;
	}

	@Override
	public String toString() {
		return "ReviewBoardDTO [afterTitle=" + afterTitle + ", afterCont=" + afterCont + ", afterTime=" + afterTime
				+ ", afterNum=" + afterNum + ", afterYn=" + afterYn + ", memId=" + memId + ", repNum=" + repNum
				+ ", repCont=" + repCont + ", repTime=" + repTime + ", repYn=" + repYn + ", attachmentList="
				+ attachmentList + "]";
	}

	


}