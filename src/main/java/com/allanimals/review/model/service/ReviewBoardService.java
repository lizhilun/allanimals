package com.allanimals.review.model.service;

import static com.allanimals.commnon.mybatis.Template.getSqlSession;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.allanimals.review.model.dao.ReviewBoardMapper;
import com.allanimals.review.model.dto.AttachmentDTO;
import com.allanimals.review.model.dto.ReviewBoardDTO;



public class ReviewBoardService {
	
	private ReviewBoardMapper reviewMapper;
	
	/**
	 * <pre>
	 * 	페이징 처리를 위한 전체 게시물 수 조회용 메소드
	 * </pre>
	 * @param searchMap
	 * @return
	 */
	public int selectTotalCount(Map<String, String> searchMap) {
		
		SqlSession sqlSession = getSqlSession();
		reviewMapper = sqlSession.getMapper(ReviewBoardMapper.class);
		
		int totalCount = reviewMapper.selectTotalCount(searchMap);
		
		sqlSession.close();
		
		return totalCount;
	}
	
	
	/**
	 * <pre>
	 * 입양 후기 게시판 조회용 메소드
	 * </pre>
	 * @return
	 */
	
	public List<ReviewBoardDTO> selectReviewList() {
		
		/* Connection 생성 */
		SqlSession sqlSession = getSqlSession();
		reviewMapper = sqlSession.getMapper(ReviewBoardMapper.class);
		
		/* List 조회 */
		List<ReviewBoardDTO> reviewList = reviewMapper.selectReviewList();
		
		/* Connection 닫기 */
		sqlSession.close();
		
		/* 조회 결과 반환 */
		return reviewList;
	}

	
	/**
	 * <pre>
	 * 입양 후기 조회
	 * </pre>
	 * @param no
	 * @return
	 */

	public ReviewBoardDTO selectOneReviewBoard(String afterNum) {
		
		SqlSession sqlSession = getSqlSession();
		
		reviewMapper = sqlSession.getMapper(ReviewBoardMapper.class);
		
		ReviewBoardDTO review = null;

		review = reviewMapper.selectOneReviewBoard(afterNum);
			
			if(review != null) {
				
				sqlSession.commit();
				
			} else {
				
				sqlSession.rollback();
			}
		
		sqlSession.close();
		
		return review;
		
	}


	public int insertReview(ReviewBoardDTO thumbnail) {
		
		/* Connection 생성 */
		SqlSession sqlSession = getSqlSession();
		reviewMapper = sqlSession.getMapper(ReviewBoardMapper.class);
		
		/* 최종적으로 반환할 result 선언 */
		int result = 0;
		
		/* 먼저 board 테이블부터 thumbnail 내용부터 insert 한다. */
		int reviewResult = reviewMapper.insertReviewContent(thumbnail);
		
		System.out.println("reviewResult : " + thumbnail);
	
		/* Attachment 리스트를 불러온다. */
		List<AttachmentDTO> fileList = thumbnail.getAttachmentList();
		
		/* fileList에 boardNo값을 넣는다. */
		for(int i = 0; i < fileList.size(); i++) {
			fileList.get(i).setAfterNum(thumbnail.getAfterNum());
		}
		
		/* Attachment 테이블에 list size만큼 insert 한다. */
		int attachmentResult = 0;
		for(int i = 0; i < fileList.size(); i++) {
			attachmentResult += reviewMapper.insertAfterPho(fileList.get(i));
		}
		
		/* 모든 결과가 성공이면 트랜젝션을 완료한다. */
		if(reviewResult > 0 && attachmentResult == fileList.size()) {
			sqlSession.commit();
			result = 1;
		} else {
			sqlSession.rollback();
		}
		
		/* Connection을 닫는다. */
		sqlSession.close();
		
		/* 수행 결과를 리턴한다. */
		return result;
		
	}
	
	/**
	 * <pre>
	 * 사진을 넣기 위한 메소드입니다.	
	 * </pre>
	 * @param photo
	 * @return
	 */
	
	public int insertAfterPho(AttachmentDTO photo) {
		SqlSession sqlSession = getSqlSession();
		
		reviewMapper = sqlSession.getMapper(ReviewBoardMapper.class);
		int result = 0;
		result = reviewMapper.insertAfterPho(photo);
		
		if(result == 1) {
			
			//success
		} else {
			//fail
		}
		return 0;
	}


	
	/**
	 * <pre>
	 * 게시글 삭제를 위한 메소드입니다.
	 * </pre>
	 * @param afterNum
	 * @return
	 */
	public int deleteReview(String afterNum) {
		
		SqlSession sqlSession = getSqlSession();
		
		reviewMapper = sqlSession.getMapper(ReviewBoardMapper.class);

		int result =  reviewMapper.deleteReview(afterNum);

		if(result > 0) {

			sqlSession.commit();

		} else {

			sqlSession.rollback();

		}
		
		sqlSession.close();

		
		return result;

		
	}


	
	/**
	 * <pre>
	 * 게시글 수정을 위한 메소드입니다.
	 * </pre>
	 * @param reviewDTO
	 * @return
	 */
	public int reviseReview(ReviewBoardDTO reviewDTO) {
		
		SqlSession sqlSession = getSqlSession();

		reviewMapper = sqlSession.getMapper(ReviewBoardMapper.class);

		int result = reviewMapper.reviseReview(reviewDTO);
		
		// 추가 필요

		return 0;
		
	}

	

}
