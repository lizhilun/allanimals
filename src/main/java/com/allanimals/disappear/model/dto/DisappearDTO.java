package com.allanimals.disappear.model.dto;

import java.sql.Date;
import java.util.List;

import com.allanimals.member.model.dto.MemberDTO;

public class DisappearDTO {
	
	private String memId;
	private String disNum; //글 번호
	private String disCont; //글 내용
	private Date disTime;
	private String disYn;
	private List<DisappearPhoDTO> disFile;
	private List<MemberDTO> memList;
	private String thumPath;
	
	public DisappearDTO() { }
	
	public DisappearDTO(String memId, String disNum, String disCont, Date disTime, String disYn,
			List<DisappearPhoDTO> disFile, List<MemberDTO> memList, String thumPath) {
		super();
		this.memId = memId;
		this.disNum = disNum;
		this.disCont = disCont;
		this.disTime = disTime;
		this.disYn = disYn;
		this.disFile = disFile;
		this.memList = memList;
		this.thumPath = thumPath;
	}

	public String getMemId() {
		return memId;
	}

	public void setMemId(String memId) {
		this.memId = memId;
	}

	public String getDisNum() {
		return disNum;
	}

	public void setDisNum(String disNum) {
		this.disNum = disNum;
	}

	public String getDisCont() {
		return disCont;
	}

	public void setDisCont(String disCont) {
		this.disCont = disCont;
	}

	public Date getDisTime() {
		return disTime;
	}

	public void setDisTime(Date disTime) {
		this.disTime = disTime;
	}

	public String getDisYn() {
		return disYn;
	}

	public void setDisYn(String disYn) {
		this.disYn = disYn;
	}

	public List<DisappearPhoDTO> getDisFile() {
		return disFile;
	}

	public void setDisFile(List<DisappearPhoDTO> disFile) {
		this.disFile = disFile;
	}

	public List<MemberDTO> getMemList() {
		return memList;
	}

	public void setMemList(List<MemberDTO> memList) {
		this.memList = memList;
	}

	public String getThumPath() {
		return thumPath;
	}

	public void setThumPath(String thumPath) {
		this.thumPath = thumPath;
	}

	@Override
	public String toString() {
		return "DisappearDTO [memId=" + memId + ", disNum=" + disNum + ", disCont=" + disCont + ", disTime=" + disTime
				+ ", disYn=" + disYn + ", disFile=" + disFile + ", memList=" + memList + ", thumPath=" + thumPath + "]";
	}
	
	
	
}
