package com.allanimals.disappear.model.dto;

public class DisappearInterestDTO {
	
	private String interestNum;
	private String memId;
	private String saveNum;
	private String disNum;
	private String dailyNum;
	private String introNum;
	
	public DisappearInterestDTO() { }
	
	public DisappearInterestDTO(String interestNum, String memId, String saveNum, String disNum, String dailyNum,
			String introNum) {
		super();
		this.interestNum = interestNum;
		this.memId = memId;
		this.saveNum = saveNum;
		this.disNum = disNum;
		this.dailyNum = dailyNum;
		this.introNum = introNum;
	}
	public String getInterestNum() {
		return interestNum;
	}
	public void setInterestNum(String interestNum) {
		this.interestNum = interestNum;
	}
	public String getMemId() {
		return memId;
	}
	public void setMemId(String memId) {
		this.memId = memId;
	}
	public String getSaveNum() {
		return saveNum;
	}
	public void setSaveNum(String saveNum) {
		this.saveNum = saveNum;
	}
	public String getDisNum() {
		return disNum;
	}
	public void setDisNum(String disNum) {
		this.disNum = disNum;
	}
	public String getDailyNum() {
		return dailyNum;
	}
	public void setDailyNum(String dailyNum) {
		this.dailyNum = dailyNum;
	}
	public String getIntroNum() {
		return introNum;
	}
	public void setIntroNum(String introNum) {
		this.introNum = introNum;
	}
	
	@Override
	public String toString() {
		return "DisappearInterestDTO [interestNum=" + interestNum + ", memId=" + memId + ", saveNum=" + saveNum
				+ ", disNum=" + disNum + ", dailyNum=" + dailyNum + ", introNum=" + introNum + "]";
	}

	

}
