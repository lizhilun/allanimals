package com.allanimals.disappear.model.dto;

import java.sql.Date;

public class DisappearRepDTO {

	private String repNum;
	private String repCont;
	private Date repTime;
	private String repYn;
	private String disNum;
	private String memId;
	
	public DisappearRepDTO() { }

	public DisappearRepDTO(String repNum, String repCont, Date repTime, String repYn, String disNum, String memId) {
		super();
		this.repNum = repNum;
		this.repCont = repCont;
		this.repTime = repTime;
		this.repYn = repYn;
		this.disNum = disNum;
		this.memId = memId;
	}

	public String getRepNum() {
		return repNum;
	}

	public void setRepNum(String repNum) {
		this.repNum = repNum;
	}

	public String getRepCont() {
		return repCont;
	}

	public void setRepCont(String repCont) {
		this.repCont = repCont;
	}

	public Date getRepTime() {
		return repTime;
	}

	public void setRepTime(Date repTime) {
		this.repTime = repTime;
	}

	public String getRepYn() {
		return repYn;
	}

	public void setRepYn(String repYn) {
		this.repYn = repYn;
	}

	public String getDisNum() {
		return disNum;
	}

	public void setDisNum(String disNum) {
		this.disNum = disNum;
	}

	public String getMemId() {
		return memId;
	}

	public void setMemId(String memId) {
		this.memId = memId;
	}

	@Override
	public String toString() {
		return "DisappearRepDTO [repNum=" + repNum + ", repCont=" + repCont + ", repTime=" + repTime + ", repYn="
				+ repYn + ", disNum=" + disNum + ", memId=" + memId + "]";
	}

	
	
}
