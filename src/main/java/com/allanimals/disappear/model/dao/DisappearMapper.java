package com.allanimals.disappear.model.dao;

import java.util.List;
import java.util.Map;

import com.allanimals.disappear.model.dto.DisappearDTO;
import com.allanimals.disappear.model.dto.DisappearInterestDTO;
import com.allanimals.disappear.model.dto.DisappearPhoDTO;
import com.allanimals.disappear.model.dto.DisappearRepDTO;

public interface DisappearMapper {
	
	List<DisappearDTO> DisappearList();

	// 관심 설정 저장
	int insertDisappearInterest(DisappearInterestDTO disappearInterest);
	
	// 관심 설정 삭제
	int deleteDisappearInterest(DisappearInterestDTO disappearInterest);

	// 관심 설정 조회
	int selectInter(DisappearInterestDTO disappearInterest);

	// 페이징
	List<DisappearDTO> selectDisappearList(Map<String, Object> searchMap);

	// 상세 조회
	DisappearDTO selectOneThumbnail(String disNum);

	// 글쓰기
	int insertThumbnailContent(DisappearDTO thumbnail);

	// 회원 정보 추가
	int insertDisappearId(DisappearDTO thumbnail);
	
	// 사진 저장
	int insertDisappearPho(DisappearPhoDTO disappearPhoDTO);

	// 댓글 작성
	int insertDisappearRep(DisappearRepDTO rep);

	// 댓글 삭제
	int deleteDisappearRep(DisappearRepDTO rep);

	// 댓글 조회
	List<DisappearRepDTO> selectDisappearRep(String disNum);

	// 게시글 삭제
	int deleteDisappearContent(DisappearDTO disappear);

	// 게시글 수정
	int updateDisappearContent(DisappearDTO disappear);

	int selectTotalCount(Map<String, Object> searchMap);

		
	

}
