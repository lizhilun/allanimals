package com.allanimals.disappear.model.service;

import static com.allanimals.commnon.mybatis.Template.getSqlSession;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.allanimals.disappear.model.dao.DisappearMapper;
import com.allanimals.disappear.model.dto.DisappearDTO;
import com.allanimals.disappear.model.dto.DisappearInterestDTO;
import com.allanimals.disappear.model.dto.DisappearPhoDTO;
import com.allanimals.disappear.model.dto.DisappearRepDTO;


public class DisappearService {
	
	private DisappearMapper disappearMapper;
	

	public int selectTotalCount(Map<String, Object> searchMap) {
		
		SqlSession sqlSession = getSqlSession();
		disappearMapper = sqlSession.getMapper(DisappearMapper.class);
		
		int totalCount = disappearMapper.selectTotalCount(searchMap);
		
		sqlSession.close();
		
		return totalCount;
	}

	/**
	 * <pre>
	 * 페이징 메소드
	 * </pre>
	 * @param searchMap
	 * @return
	 */

	public List<DisappearDTO> selectDisappearList(Map<String, Object> searchMap) {
		
		SqlSession sqlSession = getSqlSession();
		disappearMapper = sqlSession.getMapper(DisappearMapper.class);
		
		List<DisappearDTO> introList = disappearMapper.selectDisappearList(searchMap);
		
		sqlSession.close();
		
		return introList;
	}

	/**
	 * <pre>
	 * 상세 조회 메소드
	 * </pre>
	 * @param disNum
	 * @return
	 */

	public DisappearDTO selectOneThumbnail(String disNum) {
		
		SqlSession sqlSession = getSqlSession();
		
		disappearMapper = sqlSession.getMapper(DisappearMapper.class);
		
		DisappearDTO thumbnail = disappearMapper.selectOneThumbnail(disNum);
			
			if(thumbnail != null) {
				
				sqlSession.commit();
				
			} else {
				
				sqlSession.rollback();
			}
		
		sqlSession.close();
		
		return thumbnail;
		
	}


	/**
	 * <pre>
	 * 댓글 조회를 위한 메소드
	 * </pre>
	 * @param disNum
	 * @return
	 */
	public List<DisappearRepDTO> selectDisappearRep(String disNum) {
		
		SqlSession sqlSession = getSqlSession();

		disappearMapper = sqlSession.getMapper(DisappearMapper.class);

		List<DisappearRepDTO> rep = disappearMapper.selectDisappearRep(disNum);
		
		sqlSession.close();
		
		return rep;
		
	}



	/**
	 * <pre>
	 * 댓글 등록을 위한 메소드
	 * </pre>
	 */

	public int insertDisappearRep(DisappearRepDTO rep) {
		
		SqlSession sqlSession = getSqlSession();

		disappearMapper = sqlSession.getMapper(DisappearMapper.class);

		int result =  disappearMapper.insertDisappearRep(rep);

		if(result > 0) {

			sqlSession.commit();
			
		} else {

			sqlSession.rollback();

		}
		
		sqlSession.close();
	
		return result;
		
	}
	
	/**
	 * <pre>
	 * 댓글 삭제를 위한 메소드
	 * </pre>
	 * @param rep
	 * @return
	 */


	public int deletDisappearRep(DisappearRepDTO rep) {
		
		SqlSession sqlSession = getSqlSession();

		disappearMapper = sqlSession.getMapper(DisappearMapper.class);

		int result =  disappearMapper.deleteDisappearRep(rep);
		
		if(result > 0) {

			sqlSession.commit();

		} else {

			sqlSession.rollback();

		}

		sqlSession.close();
		
		return result;
	}

	/** 
	 * <pre>
	 * 관심 설정 상태를 거르기 위한 메소드
	 * </pre>
	 * @param disappearInterest
	 * @return
	 */

	public int selectInter(DisappearInterestDTO disappearInterest) {
		
		SqlSession sqlSession = getSqlSession();
		disappearMapper = sqlSession.getMapper(DisappearMapper.class);
		
		int selcetCheck = disappearMapper.selectInter(disappearInterest);
		
		sqlSession.close();
		
		return selcetCheck;
		
	}


	/**
	 * <pre>
	 * 관심 설정 등록을 위한 메소드
	 * </pre>
	 * @param disappearInterest
	 * @return
	 */
	public int insertDisappearInterest(DisappearInterestDTO disappearInterest) {
		
		SqlSession sqlSession = getSqlSession();

		disappearMapper = sqlSession.getMapper(DisappearMapper.class);

		int result =  disappearMapper.insertDisappearInterest(disappearInterest);

		if(result > 0) {

			sqlSession.commit();
			
		} else {

			sqlSession.rollback();

		}
		
		sqlSession.close();
	
		return result;
		
	}

	
	/**
	 * <pre>
	 * 관심 설정 삭제를 위한 메소드
	 * </pre>
	 * @param disappearInterest
	 * @return
	 */

	public int deleteDisappearInterest(DisappearInterestDTO disappearInterest) {
		
		SqlSession sqlSession = getSqlSession();

		disappearMapper = sqlSession.getMapper(DisappearMapper.class);

		int result = disappearMapper.deleteDisappearInterest(disappearInterest);
		
		if(result > 0) {

			sqlSession.commit();

		} else {

			sqlSession.rollback();

		}

		sqlSession.close();
		
		return result;
	}

	
	/**
	 * <pre>
	 * 게시글 삭제를 위한 메소드
	 * </pre>
	 * @param disappear
	 * @return
	 */

	public int deleteDisappearContent(DisappearDTO disappear) {
		
		SqlSession sqlSession = getSqlSession();
		
		disappearMapper = sqlSession.getMapper(DisappearMapper.class);
		
		int result = disappearMapper.deleteDisappearContent(disappear);
		
		if(result > 0 ) {
			
			sqlSession.commit();
			
		} else {
			
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}


	/**
	 * <pre>
	 * 게시글 수정을 위한 메소드
	 * </pre>
	 * @param disappear
	 * @return
	 */
	public int updateDisappearContent(DisappearDTO disappear) {
		
		SqlSession sqlSession = getSqlSession();
		
		disappearMapper = sqlSession.getMapper(DisappearMapper.class);
		
		int result = disappearMapper.updateDisappearContent(disappear);
		
		if(result > 0) {
		
			sqlSession.commit();

			} else {
			
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}


	/**
	 * <pre>
	 * 게시글 등록을 위한 메소드
	 * </pre>
	 * @param thumbnail
	 * @return
	 */
	public int insertDisappearContent(DisappearDTO thumbnail) {
		
		SqlSession sqlSession = getSqlSession();
		disappearMapper = sqlSession.getMapper(DisappearMapper.class);
		
		int result = 0;
		int memInsert = 0;
		
		int disappearResult = disappearMapper.insertThumbnailContent(thumbnail);
		thumbnail.getDisNum();
		System.out.println("intro.getIntroNum() : " + thumbnail.getDisNum());
		
		if(disappearResult > 0) {
			
			memInsert = disappearMapper.insertDisappearId(thumbnail);
			
		}
		
		System.out.println("memInsert : " + memInsert);
		List<DisappearPhoDTO> fileList = thumbnail.getDisFile();
		
		for(int i = 0; i < fileList.size(); i++) {
			
			fileList.get(i).setDisNum(thumbnail.getDisNum());
		}
		
		int disappearPhoResult = 0;
		
		for(int i = 0; i < fileList.size(); i++) {
			
			disappearPhoResult += disappearMapper.insertDisappearPho(fileList.get(i));
		}
		
		System.out.println("disappear 확인용 : " + thumbnail);
		
		if((disappearResult > 0) && (disappearPhoResult == fileList.size())) {
			
			if(memInsert > 0) {
				
				
			}
			
			sqlSession.commit();
			result = 1;
		} else {
			
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}


	
}
