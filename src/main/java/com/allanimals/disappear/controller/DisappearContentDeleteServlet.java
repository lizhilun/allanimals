package com.allanimals.disappear.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.disappear.model.dto.DisappearDTO;
import com.allanimals.disappear.model.service.DisappearService;

/**
 * Servlet implementation class DisappearContentDeleteServlet
 */
@WebServlet("/disappear/content/delete")
public class DisappearContentDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String disNum = request.getParameter("disNum");
		
		DisappearDTO disappear = new DisappearDTO();
		disappear.setDisNum(disNum);
		
		System.out.println("글번호 확인 : " + disNum);
		System.out.println("아이디 글번호 둘다 들어왔는지 확인 : " + disappear);
		
		DisappearService disappearService = new DisappearService();
		int result = disappearService.deleteDisappearContent(disappear);
		
		String path="";
		
		if(result > 0) {
			
			path = "/disappear/list";
			response.sendRedirect(request.getContextPath()+path);
		} else {
			
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "글 삭제에 실패하셨습니다.");
			request.getRequestDispatcher(path).forward(request, response);
		}
		
		
		
		
	}

}
