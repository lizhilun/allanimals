package com.allanimals.disappear.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.disappear.model.dto.DisappearInterestDTO;
import com.allanimals.disappear.model.service.DisappearService;
import com.allanimals.member.model.dto.MemberDTO;

/**
 * Servlet implementation class DisappearInterestServlet
 */
@WebServlet("/disappear/interest")
public class DisappearInterestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisappearInterestServlet() { }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	String memId = ((MemberDTO) request.getSession().getAttribute("loginMember")).getMemId();				
		String disNum = request.getParameter("disNum");
		
		DisappearInterestDTO disappearInterest = new DisappearInterestDTO();
		
		disappearInterest.setMemId(memId);
		disappearInterest.setDisNum(disNum);
		
		System.out.println("=========================================================");
		System.out.println("InterestDTO : " + disappearInterest);
		System.out.println("=========================================================");
		
		DisappearService disappearInterestService = new DisappearService();
		int interCheck = disappearInterestService.selectInter(disappearInterest);
		
		System.out.println("interCheck : " + interCheck);
		
		int result = 0;
		String interYn = "";
		
		if(interCheck == 0) {
			
			result = disappearInterestService.insertDisappearInterest(disappearInterest);
			interYn = "Y";
			
		} else {
			
			result = disappearInterestService.deleteDisappearInterest(disappearInterest);
			interYn = "N";
		}
		
		String path = "";
		
		if(result > 0) {
						
			response.setContentType("text/html;charset=utf-8");
			path = "/disappear/content?disNum="+disNum+"&&interYn="+interYn;
			response.sendRedirect(request.getContextPath()+path);
			
			
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "관심 등록에 실패하셨습니다.");
			request.getRequestDispatcher(path).forward(request, response);
		}	
		
	}

}
