package com.allanimals.disappear.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.allanimals.allDaily.model.dto.IntroDTO;
import com.allanimals.allDaily.service.AllDailyService;
import com.allanimals.commnon.pagin.Pagenation;
import com.allanimals.commnon.pagin.SelectCriteria;
import com.allanimals.disappear.model.dto.DisappearDTO;
import com.allanimals.disappear.model.service.DisappearService;
import com.allanimals.member.model.dto.MemberDTO;

/**
 * Servlet implementation class DisappearList
 */
@WebServlet("/disappear/list")
public class DisappearListServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String nono = request.getContextPath();
		System.out.println(nono);
		
		String currentPage = request.getParameter("currentPage");
		int pageNo = 1;
		
		if(currentPage != null && !"".equals("currentPage")) {
			
			pageNo = Integer.parseInt(currentPage);
		}
		
		if(pageNo <= 0) {
			
			pageNo = 1;
		}
		
		String type = request.getParameter("type");
		System.out.println("들어오는 type : " + type);
		

		String memId = "";
		
		HttpSession session = request.getSession();
		MemberDTO member = (MemberDTO)session.getAttribute("loginMember");
		
		if(member != null) {
		 
			memId = member.getMemId();
		}
		
		// 검색기능
		String searchCondition = null;
		String searchValue = request.getParameter("searchValue");
		// 제목으로만 검색하겠다고 선언
		if(searchValue != null) {
			searchCondition = "disTitle";
		}
		
		
		Map<String, Object> searchMap = new HashMap<>();
		
		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);
		
		searchMap.put("type", type);
		
		if(member != null) {
			 
			searchMap.put("memId", memId);
		}
		
		System.out.println("searchMap : " + searchMap);
		
		// 전체 게시물 수 필요
		DisappearService disappearService = new DisappearService();
		int totalCount = disappearService.selectTotalCount(searchMap);
		
		System.out.println("totalCount : " + totalCount);

		
		// 페이지당 글 수 
		int limit = 6;
		// 버튼 수
		int buttonAmount = 5;
		
		SelectCriteria selectCriteria = null;
		
		
		if(searchCondition != null && !"".equals(searchCondition)) {
			
			selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount, searchCondition, searchValue);
		} else {
			
			selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
		}
		
		
		System.out.println("selectCriteria : " + selectCriteria);
		
		searchMap.put("selectCriteria", selectCriteria);
		
		// 조회
	
		List<DisappearDTO> disappearList = disappearService.selectDisappearList(searchMap);
		
		System.out.println("disappearList : " + disappearList);
		
		String path ="";
		
		System.out.println("type체크 : " + type);
		if(disappearList != null) {
			
			path = "/WEB-INF/views/disappear/disappear.jsp";
			request.setAttribute("disappearList", disappearList);
			request.setAttribute("selectCriteria", selectCriteria);
			request.setAttribute("boardType", "disappear");
			request.setAttribute("typeValue", type);
			
		} else {
			
			path ="/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "게시물 목록 조회 실패!");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
		
		
	}


}
