package com.allanimals.disappear.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.disappear.model.dto.DisappearDTO;
import com.allanimals.disappear.model.service.DisappearService;

/**
 * Servlet implementation class DisappearContentUpdateServlet
 */
@WebServlet("/disappear/content/update")
public class DisappearContentUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String disNum = (request.getParameter("disNum"));
		
		DisappearDTO thumbnail = new DisappearService().selectOneThumbnail(disNum);
		
		System.out.println("thumbnail : " + thumbnail);
		
		String path = "";
		if(thumbnail != null) {
			
			path = "/WEB-INF/views/disappear/disappearContentUpdate.jsp";
			request.setAttribute("thumbnail", thumbnail);
		} else {
			
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "상세 조회 실패!");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		DisappearDTO disappear = new DisappearDTO();
		
		disappear.setDisNum(request.getParameter("disNum"));
		disappear.setDisCont(request.getParameter("disCont"));

		System.out.println(disappear);
		int result = new DisappearService().updateDisappearContent(disappear);
		
		String path = "";
		
		if(result > 0) {
			
			path = "/disappear/content?disNum="+request.getParameter("disNum");
			response.sendRedirect(request.getContextPath()+path);
		} else {
			
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "상세 조회 실패!");
			request.getRequestDispatcher(path).forward(request, response);
		}
		
		
		
	}

}
