package com.allanimals.disappear.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.allanimals.allDaily.model.dto.IntroRepDTO;
import com.allanimals.disappear.model.dto.DisappearDTO;
import com.allanimals.disappear.model.dto.DisappearInterestDTO;
import com.allanimals.disappear.model.dto.DisappearRepDTO;
import com.allanimals.disappear.model.service.DisappearService;
import com.allanimals.member.model.dto.MemberDTO;

/**
 * Servlet implementation class DisappearContent
 */
@WebServlet("/disappear/content")
public class DisappearContentServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		MemberDTO member = (MemberDTO) session.getAttribute("loginMember");

		String mId = "";

		if (member != null) {

			mId = member.getMemId();
		}

		String disNum = request.getParameter("disNum");

		DisappearDTO thumbnail = new DisappearDTO();

		DisappearService disappearService = new DisappearService();
		thumbnail = disappearService.selectOneThumbnail(disNum);

		DisappearInterestDTO diesappearInterest = new DisappearInterestDTO();

		diesappearInterest.setMemId(mId);
		diesappearInterest.setIntroNum(disNum);

		int introCheck = disappearService.selectInter(diesappearInterest);

		String interYn = "";

		if (introCheck == 0) {

			interYn = "Y";

		} else {

			interYn = "N";
		}

		List<DisappearRepDTO> rep = disappearService.selectDisappearRep(disNum);
		System.out.println(rep);
		System.out.println(thumbnail);
		String path = "";

		if (thumbnail != null) {
			if (rep != null) {

				path = "/WEB-INF/views/disappear/disappearContent.jsp";
				request.setAttribute("thumbnail", thumbnail);
				request.setAttribute("rep", rep);
				request.setAttribute("disNum", disNum);
				request.setAttribute("interYn", interYn);

			} else {
				path = "/WEB-INF/views/disappear/disappearContent.jsp";
				request.setAttribute("thumbnail", thumbnail);
				request.setAttribute("disNum", disNum);
				request.setAttribute("interYn", interYn);

			}
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "게시물 목록 조회 실패!");
		}

		request.getRequestDispatcher(path).forward(request, response);
	}

}
