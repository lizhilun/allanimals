package com.allanimals.disappear.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.disappear.model.dto.DisappearRepDTO;
import com.allanimals.disappear.model.service.DisappearService;

/**
 * Servlet implementation class DisappearRepDeleteServlet
 */
@WebServlet("/disappear/rep/delete")
public class DisappearRepDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisappearRepDeleteServlet() { }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String disNum = request.getParameter("disNum");
		String repNum = request.getParameter("repNum");
		DisappearRepDTO rep = new DisappearRepDTO();
		
		rep.setDisNum(disNum);
		rep.setRepNum(repNum);
		System.out.println("================================================");
		System.out.println(rep);
		
		DisappearService disappearService = new DisappearService();
		int result = disappearService.deletDisappearRep(rep);
		
		String path="";
		
		if(result > 0) {
			response.setContentType("text/html;charset=utf-8");
			path ="/disappear/content?disNum="+disNum;
			response.sendRedirect(request.getContextPath()+path);
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "댓글 삭제에 실패하셨습니다.");
			request.getRequestDispatcher(path).forward(request, response);
		}
		
		
	}

}
