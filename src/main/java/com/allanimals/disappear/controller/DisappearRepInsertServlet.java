package com.allanimals.disappear.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.disappear.model.dto.DisappearRepDTO;
import com.allanimals.disappear.model.service.DisappearService;
import com.allanimals.member.model.dto.MemberDTO;

/**
 * Servlet implementation class DisappearRepInsertServlet
 */
@WebServlet("/disappear/rep/Insert")
public class DisappearRepInsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisappearRepInsertServlet() { }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String memId =((MemberDTO) request.getSession().getAttribute("loginMember")).getMemId();
		String repCont = request.getParameter("exampleFormControlTextarea1");
		String disNum = request.getParameter("disNum");
		
		DisappearRepDTO rep = new DisappearRepDTO();
		
		rep.setMemId(memId);
		rep.setRepCont(repCont);
		rep.setDisNum(disNum);
		System.out.println(rep);
		
		DisappearService disappearService = new DisappearService();
		int result = disappearService.insertDisappearRep(rep);
		String path="";
		
		if(result > 0) {
			
			response.setContentType("text/html;charset=utf-8");
			path ="/disappear/content?disNum="+disNum;
			response.sendRedirect(request.getContextPath()+path);
			
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "댓글 등록에 실패하셨습니다.");
			request.getRequestDispatcher(path).forward(request, response);
		}
	}

}
