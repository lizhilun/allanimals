package com.allanimals.user.mypage.adoption.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.allanimals.member.model.dto.MemberDTO;
import com.allanimals.user.adoption.model.dto.AdoptApplicationDTO;
import com.allanimals.user.adoption.model.service.AdoptionService;


/**
 * Servlet implementation class MypageProtection2Servlet
 */
@WebServlet("/protection2")
public class MypageProtection2Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
//		String mId = ((MemberDTO)session.getAttribute("loginMember")).getMemId();
		
	//	request.getRequestDispatcher("/WEB-INF/views/mypage/protection2.jsp").forward(request, response);
	//	String aniNum = "45";
	
		String aniNum = request.getParameter("aniNum");
		
		AdoptionService adoService = new AdoptionService();
		List<AdoptApplicationDTO> ani = adoService.selectApplyList(aniNum);
		
		System.out.println(ani);
		
		String path="";
		if(ani != null){
			
			path = "/WEB-INF/views/mypage/protection2.jsp";
			request.setAttribute("applyList", ani);
		}else {
			
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "내가 쓴 신청서 조회에 실패하였습니다.");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
	}

}
