package com.allanimals.user.mypage.adoption.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.user.adoption.model.dto.AdoptDTO;
import com.allanimals.user.adoption.model.service.AdoptionService;

/**
 * Servlet implementation class DeleteMaching
 */
@WebServlet("/deleteMaching")
public class DeleteMaching extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String memId = request.getParameter("memId");
		String appNum = request.getParameter("appNum");
		
		AdoptDTO machingDelete = new AdoptDTO();
		machingDelete.setMemId(memId);
		machingDelete.setAppNum(appNum);
	
		System.out.println(machingDelete);
		
		int result = new AdoptionService().machingDelete(machingDelete);
	
		String path="";
		 if(result > 0){
			 path = "/WEB-INF/views/common/success.jsp";
			 request.setAttribute("successCode", "deleteMaching");
			 
		 }else {
			 
			 path = "/WEB-INF/views/common/failed.jsp";
			 request.setAttribute("message", "매칭원하시는 회원의 개인정보 조회에 실패하였습니다.");
		 }
		 request.getRequestDispatcher(path).forward(request, response);
	}

}
