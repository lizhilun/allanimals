package com.allanimals.user.mypage.adoption.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.introduce.model.service.AnimalService;

/**
 * Servlet implementation class adoptionComplete
 */
@WebServlet("/adoptionComplete")
public class AdoptionComplete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		 String aniNum = request.getParameter("aniNum");
		 System.out.println("입력받은 동물번호 확인 : " + aniNum);
		 
		 AnimalService animalService = new AnimalService();
		 int aniAdoptionComplte = animalService.completeAdopt(aniNum);
		 
		 String path="";
		 if(aniAdoptionComplte > 0){
			 path = "/WEB-INF/views/common/success.jsp";
			 request.setAttribute("successCode", "complteAdopt");
			 
		 }else {
			 
			 path = "/WEB-INF/views/common/failed.jsp";
			 request.setAttribute("message", "동물 입양완료 처리에 실패하였습니다.");
		 }
		 request.getRequestDispatcher(path).forward(request, response);
	}
}
