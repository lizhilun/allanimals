package com.allanimals.user.mypage.adoption.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.user.adoption.model.dto.AdoptDTO;
import com.allanimals.user.adoption.model.service.AdoptionService;

/**
 * Servlet implementation class MypageProtection3Servlet
 */
@WebServlet("/protection3")
public class MypageProtection3Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	//	request.getRequestDispatcher("/WEB-INF/views/mypage/protection3.jsp").forward(request, response);
	
		String memId = request.getParameter("memId");
		String appNum = request.getParameter("appNum");

		System.out.println(memId);
		System.out.println(appNum);
		
		AdoptDTO ad = new AdoptDTO();
		ad.setMemId(memId);
		ad.setAppNum(appNum);
		
		AdoptionService adoService = new AdoptionService();
		List<AdoptDTO> selectAnimalApply = adoService.selectAllAnimalApply(ad);
		
		System.out.println(selectAnimalApply);
		
		String path="";
		if(selectAnimalApply != null) {
			
			path="/WEB-INF/views/mypage/protection3.jsp";
			request.setAttribute("adoList", selectAnimalApply);
		}else {
			
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "신청서 조회에 실패하였습니다.");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
	}
	

}
