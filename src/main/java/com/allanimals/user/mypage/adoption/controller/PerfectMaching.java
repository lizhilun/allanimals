package com.allanimals.user.mypage.adoption.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.introduce.model.dto.AnimalDTO;
import com.allanimals.introduce.model.service.AnimalService;
import com.allanimals.user.adoption.model.dto.AdopMemberDTO;
import com.allanimals.user.adoption.model.dto.MemberMachingDTO;
import com.allanimals.user.adoption.model.service.AdoptionService;

/**
 * Servlet implementation class PerfectMaching
 */
@WebServlet("/perfectMaching")
public class PerfectMaching extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String memId = request.getParameter("memId");
		String appNum = request.getParameter("appNum");
		String aniNum = request.getParameter("aniNum");
		
		MemberMachingDTO aniDTO = new MemberMachingDTO();
		aniDTO.setMemId(memId);
		aniDTO.setAppNum(appNum);
		aniDTO.setAniNum(aniNum);
		
		System.out.println(aniDTO);
		
		int result = new AdoptionService().perfectMaching(aniDTO);
		
		String path="";
		 if(result > 0){
			 path = "/WEB-INF/views/common/success.jsp";
			 request.setAttribute("successCode", "machingSuccess");
			 request.setAttribute("aniNum", aniNum);
			 
		 }else {
			 
			 path = "/WEB-INF/views/common/failed.jsp";
			 request.setAttribute("message", "매칭에 실패하였습니다.");
		 }
		 request.getRequestDispatcher(path).forward(request, response);
	}

}
