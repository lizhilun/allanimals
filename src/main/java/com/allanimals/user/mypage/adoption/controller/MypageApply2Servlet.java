package com.allanimals.user.mypage.adoption.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.ibatis.io.ResolverUtil.Test;

import com.allanimals.member.model.dto.MemberDTO;
import com.allanimals.user.adoption.model.dto.AdoptApplicationDTO;
import com.allanimals.user.adoption.model.service.AdoptionService;

/**
 * Servlet implementation class MypageApply2Servlet
 */
@WebServlet("/apply2")
public class MypageApply2Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	//	request.getRequestDispatcher("/WEB-INF/views/mypage/apply2.jsp").forward(request, response);
	
		HttpSession session = request.getSession();
		String mId = ((MemberDTO)session.getAttribute("loginMember")).getMemId();
		
		String appNum = request.getParameter("appNum");
		
		AdoptionService adoService = new AdoptionService();
		List<Test> selectApply = adoService.selectApply(mId,appNum);
		
		System.out.println(selectApply);
		
		String path="";
		if(selectApply != null){
			
			path = "/WEB-INF/views/mypage/apply2.jsp";
			request.setAttribute("adoList", selectApply);
		}else {
			
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "내가 쓴 신청서 조회에 실패하였습니다.");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
	}

}
