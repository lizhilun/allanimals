package com.allanimals.user.mypage.adoption.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.ibatis.io.ResolverUtil.Test;

import com.allanimals.member.model.dto.MemberDTO;
import com.allanimals.user.adoption.model.dto.AdoptApplicationDTO;
import com.allanimals.user.adoption.model.service.AdoptionService;

/**
 * Servlet implementation class MypageApply3Servlet
 */
@WebServlet("/apply3")
public class MypageApply3Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	//	request.getRequestDispatcher("/WEB-INF/views/mypage/apply3.jsp").forward(request, response);
		
		HttpSession session = request.getSession();
		String mId = ((MemberDTO)session.getAttribute("loginMember")).getMemId();
		
		String appNum = request.getParameter("appNum");
		
		AdoptionService adoService = new AdoptionService();
		List<Test> selectApply = adoService.selectApply(mId,appNum);
		
		System.out.println(selectApply);
		
		String path="";
		if(selectApply != null){
			
			path = "/WEB-INF/views/mypage/apply3.jsp";
			request.setAttribute("adoList", selectApply);
		}else {
			
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "내가 쓴 신청서 조회에 실패하였습니다.");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String appNum = request.getParameter("appNum");
		String callTime = request.getParameter("callTime");
		String memJob = request.getParameter("memJob");
		String ques1 = request.getParameter("ques1");
		String ques2 = request.getParameter("ques2");
		String ques3 = request.getParameter("ques3");
		String ques4 = request.getParameter("ques4");
		String ques5 = request.getParameter("ques5");
		String appCont = request.getParameter("appCont");
		String marry = request.getParameter("marry");
		
		AdoptApplicationDTO appDTO = new AdoptApplicationDTO();
		appDTO.setCallTime(callTime);
		appDTO.setMemJob(memJob);
		appDTO.setQues1(ques1);
		appDTO.setQues2(ques2);
		appDTO.setQues3(ques3);
		appDTO.setQues4(ques4);
		appDTO.setQues5(ques5);
		appDTO.setAppCont(appCont);
		appDTO.setAppNum(appNum);
		appDTO.setMarry(marry);
		
		System.out.println(appDTO);
		
		int result = new AdoptionService().updateApply(appDTO);
		
		System.out.println("result 확인 : " + result);
	
		String page= "";
		if(result > 0) {
			page = "/WEB-INF/views/common/success.jsp";
			request.setAttribute("successCode", "updateApply");
			response.sendRedirect(request.getContextPath() + "/apply");
		} else {
			
			page = "/WEB-INF/views/common/failed.jsp";
			
			request.setAttribute("message", "신청서 수정 실패!");
			request.getRequestDispatcher(page).forward(request, response);
		}
	}

}
