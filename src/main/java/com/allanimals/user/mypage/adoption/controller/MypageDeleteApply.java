package com.allanimals.user.mypage.adoption.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import com.allanimals.member.model.dto.MemberDTO;
import com.allanimals.user.adoption.model.service.AdoptionService;

/**
 * Servlet implementation class MypageDeleteApply
 */
@WebServlet("/deleteApply")
public class MypageDeleteApply extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	//	request.getRequestDispatcher("/WEB-INF/views/mypage/deleteApply.jsp").forward(request, response);
		
		HttpSession session = request.getSession();
		String mId = ((MemberDTO)session.getAttribute("loginMember")).getMemId();
		
		String appNum = request.getParameter("applyNum");
		
		AdoptionService adoService = new AdoptionService();
		int selectApply = adoService.deleteApply(mId,appNum);
		
		System.out.println("확인 한번 해보자고! : " + selectApply);
		
		String page = "";
		
		if(selectApply > 0) {
			
			PrintWriter pri = new PrintWriter(appNum);
			pri.print(appNum); 
			page = "/WEB-INF/views/mypage/apply.jsp";
		//	request.setAttribute("applyNum", appNum);
			pri.close();

		} else {
			
			page = "/WEB-INF/views/common/failed.jsp";
			
			request.setAttribute("message", "신청서 삭제 실패!");
			request.getRequestDispatcher(page).forward(request, response);
		}
	}


}
