package com.allanimals.user.mypage.adoption.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.user.adoption.model.dto.AdopMemberDTO;
import com.allanimals.user.adoption.model.dto.AdoptDTO;
import com.allanimals.user.adoption.model.service.AdoptionService;

/**
 * Servlet implementation class MypageProtectionMatchingServlet
 */
@WebServlet("/matching")
public class MypageProtectionMatchingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	//	request.getRequestDispatcher("/WEB-INF/views/mypage/matching.jsp").forward(request, response);
	
		String memId = request.getParameter("memId");
		String appNum = request.getParameter("appNum");

		System.out.println(memId);
		System.out.println(appNum);
		
		AdoptDTO ad = new AdoptDTO();
		ad.setMemId(memId);
		ad.setAppNum(appNum);
		
		AdoptionService adoService = new AdoptionService();
		List<AdopMemberDTO> selectAnimalApply = adoService.selectMem(ad);
		
		System.out.println("확인한번 해보자고 : " + selectAnimalApply);
		
		String path="";
		if(selectAnimalApply != null) {
			
			path="/WEB-INF/views/mypage/matching.jsp";
			request.setAttribute("adoList", selectAnimalApply);
		}else {
			
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "회원의 개인정보 조회에 실패하였습니다.");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
	}

}
