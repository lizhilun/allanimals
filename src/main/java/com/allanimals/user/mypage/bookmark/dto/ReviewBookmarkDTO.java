package com.allanimals.user.mypage.bookmark.dto;

import java.util.List;

import com.allanimals.review.model.dto.ReviewBoardDTO;

public class ReviewBookmarkDTO {

	private int interestNum;
	private String memId;
	private List<ReviewBoardDTO> reviewBookList;

	public ReviewBookmarkDTO() {}

	public ReviewBookmarkDTO(int interestNum, String memId, List<ReviewBoardDTO> reviewBookList) {
		
		this.interestNum = interestNum;
		this.memId = memId;
		this.reviewBookList = reviewBookList;
	}

	public int getInterestNum() {
		return interestNum;
	}

	public void setInterestNum(int interestNum) {
		this.interestNum = interestNum;
	}

	public String getMemId() {
		return memId;
	}

	public void setMemId(String memId) {
		this.memId = memId;
	}

	public List<ReviewBoardDTO> getReviewBookList() {
		return reviewBookList;
	}

	public void setReviewBookList(List<ReviewBoardDTO> reviewBookList) {
		this.reviewBookList = reviewBookList;
	}

	@Override
	public String toString() {
		return "ReviewBookmarkDTO [interestNum=" + interestNum + ", memId=" + memId + ", reviewBookList="
				+ reviewBookList + "]";
	}
	
	
}
