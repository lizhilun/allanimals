package com.allanimals.user.mypage.withdraw.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.allanimals.member.model.dto.MemberDTO;
import com.allanimals.member.model.service.MemberService;

/**
 * Servlet implementation class MypageWithdrawServlet
 */
@WebServlet("/withdraw")
public class MypageWithdrawServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	//	request.getRequestDispatcher("/WEB-INF/views/mypage/withdraw.jsp").forward(request, response);
	
		HttpSession session = request.getSession();
		String mId = ((MemberDTO)session.getAttribute("loginMember")).getMemId();
		
		MemberService memService = new MemberService();
		MemberDTO selectMem = memService.selectOneMemById(mId);
		
		System.out.println(selectMem);
		
		String path="";
		if(selectMem != null){
			
			path = "/WEB-INF/views/mypage/withdraw.jsp";
			request.setAttribute("memberInfo", selectMem);
		}else {
			
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "탈퇴조회에 실패하였습니다.");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		String mId = ((MemberDTO)session.getAttribute("loginMember")).getMemId();
		
		MemberDTO deleteMember = new MemberDTO();
		deleteMember.setMemId(mId);
		
		System.out.println("delete 확인 : " + deleteMember);
		
		int result = new MemberService().deleteMember(deleteMember);
		
		System.out.println("result 확인 : " + result);
		
		String page = "";
		
		if(result > 0) {
			session.invalidate();
			response.sendRedirect(request.getContextPath() + "/withdraw2");
			
		} else {
			
			page = "/WEB-INF/views/common/failed.jsp";
			
			request.setAttribute("message", "회원 가입 실패!");
			request.getRequestDispatcher(page).forward(request, response);
		}
		
		
	}
}
