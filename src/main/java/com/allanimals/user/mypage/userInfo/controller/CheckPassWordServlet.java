package com.allanimals.user.mypage.userInfo.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.allanimals.member.model.dto.MemberDTO;
import com.allanimals.member.model.service.MemberService;

/**
 * Servlet implementation class CheckPassWordServlet
 */
@WebServlet("/check/pwd")
public class CheckPassWordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		String mId = ((MemberDTO)session.getAttribute("loginMember")).getMemId();
		
		String mPw = request.getParameter("memPwd");
		System.out.println("입력받은 비밀번호 확인 : " + mPw);
		
		MemberDTO passWordCheck = new MemberDTO();
		passWordCheck.setMemId(mId);
		passWordCheck.setMemPwd(mPw);
		
		Boolean result = new MemberService().checkPassword(passWordCheck);
		
		System.out.println(result);
		
		PrintWriter out = response.getWriter();
		out.print(result);
		
		out.flush();
		out.close();
		
	}

}
