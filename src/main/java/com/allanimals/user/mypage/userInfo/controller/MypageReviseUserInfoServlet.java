package com.allanimals.user.mypage.userInfo.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.allanimals.member.model.dto.MemberDTO;
import com.allanimals.member.model.service.MemberService;

/**
 * Servlet implementation class MypageReviseUserInfoServlet
 */
@WebServlet("/revise")
public class MypageReviseUserInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//세션값
		HttpSession session = request.getSession();
		String mId = ((MemberDTO)session.getAttribute("loginMember")).getMemId();
		
		//db조회 
		MemberService memService = new MemberService();
		MemberDTO selectMem = memService.selectOneMemById(mId);
		
		System.out.println(selectMem);
		
		String path="";
		if(selectMem != null){
			
			path = "/WEB-INF/views/mypage/revise.jsp";
			request.setAttribute("memberInfo", selectMem);
		}else {
			
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "개인정보 조회에 실패하였습니다.");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
//		
//		//session으로 값을 계속 담아둘거고 그 session에서 필요한 정보만 내가 가져와서 뿌리는것.
//		HttpSession session = request.getSession();
//		String mId = (MemberDTO)session.getAttribute(/*세션 아이디 값*/).getId();
//		System.out.println(session.getId());
	
	
		
	}
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		String mId = ((MemberDTO)session.getAttribute("loginMember")).getMemId();
		
		String memName = request.getParameter("memName");
		java.sql.Date memBirth = java.sql.Date.valueOf(request.getParameter("memBirth"));
		String memEmail = request.getParameter("memEmail");
		String memPhone = request.getParameter("memPhone");
		String memAddre = request.getParameter("memAddre");
	
		MemberDTO reviseMember = new MemberDTO();
		reviseMember.setMemName(memName);
		reviseMember.setMemBirth(memBirth);
		reviseMember.setMemEmail(memEmail);
		reviseMember.setMemPhone(memPhone);
		reviseMember.setMemAddre(memAddre);
		reviseMember.setMemId(mId);
		
		System.out.println(reviseMember);
		
		int result = new MemberService().updateMemInfo(reviseMember);
		
		System.out.println("result 확인 : " + result);
		
		String page = "";
		
		if(result > 0) {
			
			response.sendRedirect(request.getContextPath() + "/member/userInfo");
		} else {
			
			page = "/WEB-INF/views/common/failed.jsp";
			
			request.setAttribute("message", "회원 정보 수정 실패!");
			request.getRequestDispatcher(page).forward(request, response);
		}
		
	}

}
