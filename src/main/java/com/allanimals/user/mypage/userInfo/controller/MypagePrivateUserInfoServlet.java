package com.allanimals.user.mypage.userInfo.controller;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.allanimals.member.model.dto.MemberDTO;
import com.allanimals.member.model.service.MemberService;

/**
 * Servlet implementation class MypagePrivateUserInfoServlet
 */
@WebServlet("/member/userInfo")
public class MypagePrivateUserInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		String mId = ((MemberDTO)session.getAttribute("loginMember")).getMemId();

		MemberService memService = new MemberService();
		MemberDTO selectMem = memService.selectOneMemById(mId);
		
		System.out.println(selectMem);
		
		String path="";
		if(selectMem != null){
			
			path = "/WEB-INF/views/mypage/privateUserInfo.jsp";
			request.setAttribute("memberInfo", selectMem);
		}else {
			
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "개인정보 조회에 실패하였습니다.");
		}
		
		request.getRequestDispatcher(path).forward(request, response);

	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		String mId = ((MemberDTO)session.getAttribute("loginMember")).getMemId();
		
		MemberDTO requestMember = new MemberDTO();
		requestMember.setMemId(mId);
		requestMember.setMemPwd(request.getParameter("memPwd"));
		
		System.out.println(requestMember);
		
		int result = new MemberService().updatePw(requestMember);
		System.out.println("result 확인 : " + result);
		
		String Path = "";
		
		if( result > 0){
			
		//	Path = "/WEB-INF/views/mainpage/mainPage.jsp";
		//	Path = "/WEB-INF/views/mapage/privateUserInfo.jsp";
			response.sendRedirect(request.getContextPath() + "/member/userInfo");
			
			
		} else {
			 request.setAttribute("Message", "요청실패. 뒤로가기를 눌러주세요");
			 Path = "/WEB-INF/views/common/failed.jsp";
			 request.getRequestDispatcher(Path).forward(request, response);
		}
		
	}
	

}
