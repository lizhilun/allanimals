package com.allanimals.user.adoption.model.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.io.ResolverUtil.Test;

import com.allanimals.user.adoption.model.dto.AdopMemberDTO;
import com.allanimals.user.adoption.model.dto.AdoptApplicationDTO;
import com.allanimals.user.adoption.model.dto.AdoptDTO;
import com.allanimals.user.adoption.model.dto.MemberMachingDTO;

public interface AdoptionMapper {

	List<AdoptApplicationDTO> selectAllAdoptionList(String mId);

	List<Test> selectAdoption(
			@Param("memId") String mId,
			@Param("appNum") String appNum
			);

	int deleteAdoption(Map<String, String> mmm);

	int updateApply(AdoptApplicationDTO appDTO);

	List<AdoptApplicationDTO> selectAllApplyList(String aniNum);

	List<AdoptDTO> selectMemberApplyList(AdoptDTO ad);

	List<AdopMemberDTO> selectMem(AdoptDTO ad);

	int machingMem(AdoptDTO machingMember);

	int deleteMaching(AdoptDTO machingDelete);

	int machingSuccess(MemberMachingDTO aniDTO);

	int machingPerfect(MemberMachingDTO aniDTO);

	int machingFail(MemberMachingDTO aniDTO);



}
