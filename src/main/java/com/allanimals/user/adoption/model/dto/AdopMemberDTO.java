package com.allanimals.user.adoption.model.dto;

import java.util.List;

import com.allanimals.member.model.dto.MemberDTO;

public class AdopMemberDTO {
	
	private String appNum; //신청서번호
	private List<MemberDTO> memList; //회원아이디 회원리스트
	private String callTime; //통화가능시간.
	private String memJob; //신청인 직업
	private String aniNum; //동물번호
	private String marry; //결혼여부
	
	public AdopMemberDTO() {}

	public AdopMemberDTO(String appNum, List<MemberDTO> memList, String callTime, String memJob, String aniNum,
			String marry) {

		this.appNum = appNum;
		this.memList = memList;
		this.callTime = callTime;
		this.memJob = memJob;
		this.aniNum = aniNum;
		this.marry = marry;
	}

	public String getAppNum() {
		return appNum;
	}

	public void setAppNum(String appNum) {
		this.appNum = appNum;
	}

	public List<MemberDTO> getMemList() {
		return memList;
	}

	public void setMemList(List<MemberDTO> memList) {
		this.memList = memList;
	}

	public String getCallTime() {
		return callTime;
	}

	public void setCallTime(String callTime) {
		this.callTime = callTime;
	}

	public String getMemJob() {
		return memJob;
	}

	public void setMemJob(String memJob) {
		this.memJob = memJob;
	}

	public String getAniNum() {
		return aniNum;
	}

	public void setAniNum(String aniNum) {
		this.aniNum = aniNum;
	}

	public String getMarry() {
		return marry;
	}

	public void setMarry(String marry) {
		this.marry = marry;
	}

	@Override
	public String toString() {
		return "AdopMemberDTO [appNum=" + appNum + ", memList=" + memList + ", callTime=" + callTime + ", memJob="
				+ memJob + ", aniNum=" + aniNum + ", marry=" + marry + "]";
	}

	
		
}
