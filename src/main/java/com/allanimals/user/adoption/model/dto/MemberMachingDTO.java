package com.allanimals.user.adoption.model.dto;

import java.util.List;

import com.allanimals.introduce.model.dto.AnimalDTO;

public class MemberMachingDTO {
	
	private String memId;
	private String appNum;
	private String aniNum;
	private String appSituation;
	private List<AnimalDTO> aniList;

	public MemberMachingDTO() {}

	public MemberMachingDTO(String memId, String appNum, String aniNum, String appSituation, List<AnimalDTO> aniList) {
	
		this.memId = memId;
		this.appNum = appNum;
		this.aniNum = aniNum;
		this.appSituation = appSituation;
		this.aniList = aniList;
	}

	public String getMemId() {
		return memId;
	}

	public void setMemId(String memId) {
		this.memId = memId;
	}

	public String getAppNum() {
		return appNum;
	}

	public void setAppNum(String appNum) {
		this.appNum = appNum;
	}

	public String getAniNum() {
		return aniNum;
	}

	public void setAniNum(String aniNum) {
		this.aniNum = aniNum;
	}

	public String getAppSituation() {
		return appSituation;
	}

	public void setAppSituation(String appSituation) {
		this.appSituation = appSituation;
	}

	public List<AnimalDTO> getAniList() {
		return aniList;
	}

	public void setAniList(List<AnimalDTO> aniList) {
		this.aniList = aniList;
	}

	@Override
	public String toString() {
		return "MemberMachingDTO [memId=" + memId + ", appNum=" + appNum + ", aniNum=" + aniNum + ", appSituation="
				+ appSituation + ", aniList=" + aniList + "]";
	}

	

}
