package com.allanimals.user.adoption.model.dto;

import java.sql.Date;
import java.util.List;

import com.allanimals.introduce.model.dto.AnimalDTO;
import com.allanimals.member.model.dto.MemberDTO;

public class AdoptDTO {
	
	private String appNum; //신청서번호
	private String appCont; //글(내용)
	private String memId; //회원아이디 회원리스트
	private List<AnimalDTO> aniList; //동물번호 동물리스트
	private java.sql.Date appTime; //신청일
	private String appYn; //매칭여부
	private String appSituation; //입양신청서 상태
	private String ques1; //동거가족의 동의가 있었는지
	private String ques2; //현재다른돌물을 키우고 있는지
	private String ques3; //거주하고있는 주택형태 무엇인지
	private String ques4; //중성화 수술에 대한 의견은 어떤지
	private String ques5; //반려동물을 키워본 경험이 있는지.
	private String callTime; //통화가능시간.
	private String memJob; //신청인 직업
	
	
	public AdoptDTO() {}


	public AdoptDTO(String appNum, String appCont, String memId, List<AnimalDTO> aniList, Date appTime, String appYn,
			String appSituation, String ques1, String ques2, String ques3, String ques4, String ques5, String callTime,
			String memJob) {
	
		this.appNum = appNum;
		this.appCont = appCont;
		this.memId = memId;
		this.aniList = aniList;
		this.appTime = appTime;
		this.appYn = appYn;
		this.appSituation = appSituation;
		this.ques1 = ques1;
		this.ques2 = ques2;
		this.ques3 = ques3;
		this.ques4 = ques4;
		this.ques5 = ques5;
		this.callTime = callTime;
		this.memJob = memJob;
	}


	public String getAppNum() {
		return appNum;
	}


	public void setAppNum(String appNum) {
		this.appNum = appNum;
	}


	public String getAppCont() {
		return appCont;
	}


	public void setAppCont(String appCont) {
		this.appCont = appCont;
	}


	public String getMemId() {
		return memId;
	}


	public void setMemId(String memId) {
		this.memId = memId;
	}


	public List<AnimalDTO> getAniList() {
		return aniList;
	}


	public void setAniList(List<AnimalDTO> aniList) {
		this.aniList = aniList;
	}


	public java.sql.Date getAppTime() {
		return appTime;
	}


	public void setAppTime(java.sql.Date appTime) {
		this.appTime = appTime;
	}


	public String getAppYn() {
		return appYn;
	}


	public void setAppYn(String appYn) {
		this.appYn = appYn;
	}


	public String getAppSituation() {
		return appSituation;
	}


	public void setAppSituation(String appSituation) {
		this.appSituation = appSituation;
	}


	public String getQues1() {
		return ques1;
	}


	public void setQues1(String ques1) {
		this.ques1 = ques1;
	}


	public String getQues2() {
		return ques2;
	}


	public void setQues2(String ques2) {
		this.ques2 = ques2;
	}


	public String getQues3() {
		return ques3;
	}


	public void setQues3(String ques3) {
		this.ques3 = ques3;
	}


	public String getQues4() {
		return ques4;
	}


	public void setQues4(String ques4) {
		this.ques4 = ques4;
	}


	public String getQues5() {
		return ques5;
	}


	public void setQues5(String ques5) {
		this.ques5 = ques5;
	}


	public String getCallTime() {
		return callTime;
	}


	public void setCallTime(String callTime) {
		this.callTime = callTime;
	}


	public String getMemJob() {
		return memJob;
	}


	public void setMemJob(String memJob) {
		this.memJob = memJob;
	}


	@Override
	public String toString() {
		return "AdoptDTO [appNum=" + appNum + ", appCont=" + appCont + ", memId=" + memId + ", aniList=" + aniList
				+ ", appTime=" + appTime + ", appYn=" + appYn + ", appSituation=" + appSituation + ", ques1=" + ques1
				+ ", ques2=" + ques2 + ", ques3=" + ques3 + ", ques4=" + ques4 + ", ques5=" + ques5 + ", callTime="
				+ callTime + ", memJob=" + memJob + "]";
	}

	
	
	
}
