package com.allanimals.user.adoption.model.service;

import static com.allanimals.commnon.mybatis.Template.getSqlSession;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.io.ResolverUtil.Test;
import org.apache.ibatis.session.SqlSession;

import com.allanimals.user.adoption.model.dao.AdoptionMapper;
import com.allanimals.user.adoption.model.dto.AdopMemberDTO;
import com.allanimals.user.adoption.model.dto.AdoptApplicationDTO;
import com.allanimals.user.adoption.model.dto.AdoptDTO;
import com.allanimals.user.adoption.model.dto.MemberMachingDTO;

public class AdoptionService {

	private AdoptionMapper adoptionMapper;

	/**
	 * <pre>
	 *  회원일 경우 신청한 신청서 전체 조회	
	 * </pre>
	 * @param mId
	 * @return
	 */
	public List<AdoptApplicationDTO> selectAllApply(String mId) {
		
		SqlSession sqlSession = getSqlSession();
		adoptionMapper = sqlSession.getMapper(AdoptionMapper.class);
		
		List<AdoptApplicationDTO> adoList = adoptionMapper.selectAllAdoptionList(mId);
		
		sqlSession.close();
		
		return adoList;
	}

	/**
	 * <pre>
	 * 	내가 쓴 신청서 상세조회 
	 * </pre>
	 * @param mId
	 * @param appNum 
	 * @return
	 */
	public List<Test> selectApply(String mId, String appNum) {
		
		SqlSession sqlSession = getSqlSession();
		adoptionMapper = sqlSession.getMapper(AdoptionMapper.class);
		
		List<Test> adoList = adoptionMapper.selectAdoption(mId, appNum);
		
		sqlSession.close();
		
		return adoList;
	}

	/**
	 * <pre>
	 * 	내가 쓴 신청서 삭제하기
	 * </pre>
	 * @param mId
	 * @param appNum
	 * @return
	 */
	public int deleteApply(String mId, String appNum) {
		
		SqlSession sqlSession = getSqlSession();
		adoptionMapper = sqlSession.getMapper(AdoptionMapper.class);
		Map<String, String> mmm = new HashMap<String, String>();
		mmm.put("memId", mId);
		mmm.put("appNum", appNum);
	

		int adoList = adoptionMapper.deleteAdoption(mmm);
		
		if(adoList > 0 ) {
			
			sqlSession.commit();
		}else {
			
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return adoList;
	}

	/**
	 * <pre>
	 * 	내가 쓴 신청서 수정하기
	 * </pre>
	 * @param appDTO
	 * @return
	 */
	public int updateApply(AdoptApplicationDTO appDTO) {
		
		SqlSession sqlSession = getSqlSession();
		adoptionMapper = sqlSession.getMapper(AdoptionMapper.class);
		
		int result = adoptionMapper.updateApply(appDTO);
		
		if(result > 0 ) {
			
			sqlSession.commit();
		}else {
			
			sqlSession.rollback();
		}
		sqlSession.close();
		
		return result;
	}

	/**
	 * <pre>
	 * 	내가 데리고 있는 동물에 들어온 신청서 리스트 조회하기
	 * </pre>
	 * @param aniNum
	 * @return
	 */
	public List<AdoptApplicationDTO> selectApplyList(String aniNum) {
		
		SqlSession sqlSession = getSqlSession();
		adoptionMapper = sqlSession.getMapper(AdoptionMapper.class);
		
		List<AdoptApplicationDTO> myApplyList = adoptionMapper.selectAllApplyList(aniNum);
		
		sqlSession.close();
		
		return myApplyList;
	}

	/**
	 * <pre>
	 * 	내가 데리고 있는 동물에게 들어온 신청서 질의조회
	 * </pre>
	 * @param memId
	 * @param appNum
	 * @return
	 */
	public List<AdoptDTO> selectAllAnimalApply(AdoptDTO ad) {
		
		SqlSession sqlSession = getSqlSession();
		adoptionMapper = sqlSession.getMapper(AdoptionMapper.class);
		
		List<AdoptDTO> adoList = adoptionMapper.selectMemberApplyList(ad);
		
		sqlSession.close();
		
		return adoList;
	}

	/**
	 * <pre>
	 * 	매칭하려는 회원 정보조회
	 * </pre>
	 * @param ad
	 * @return
	 */
	public List<AdopMemberDTO> selectMem(AdoptDTO ad) {
		
		SqlSession sqlSession = getSqlSession();
		adoptionMapper = sqlSession.getMapper(AdoptionMapper.class);
		
		List<AdopMemberDTO> adoList = adoptionMapper.selectMem(ad);
		
		sqlSession.close();
		
		return adoList;
	}

	/**
	 * <pre>
	 * 	개인정보 볼 때 입양진행중으로 상태 변환
	 * </pre>
	 * @param machingMember
	 * @return
	 */
	public int machingMember(AdoptDTO machingMember) {
		
		SqlSession sqlSession = getSqlSession();
		adoptionMapper = sqlSession.getMapper(AdoptionMapper.class);
		
		int result = adoptionMapper.machingMem(machingMember);
		
		if(result > 0 ) {
			
			sqlSession.commit();
		}else {
			
			sqlSession.rollback();
		}
		sqlSession.close();
		
		return result;
	}

	/**
	 * <pre>
	 *  매칭 취소하기
	 * </pre>
	 * @param machingDelete
	 * @return
	 */
	public int machingDelete(AdoptDTO machingDelete) {
		
		SqlSession sqlSession = getSqlSession();
		adoptionMapper = sqlSession.getMapper(AdoptionMapper.class);
		
		int result = adoptionMapper.deleteMaching(machingDelete);
		
		if(result > 0 ) {
			
			sqlSession.commit();
		}else {
			
			sqlSession.rollback();
		}
		sqlSession.close();
		
		return result;
	}

	/**
	 * <pre>
	 * 	입양 매칭 성공
	 * </pre>
	 * @param aniDTO
	 * @return
	 */
	public int perfectMaching(MemberMachingDTO aniDTO) {
		
		SqlSession sqlSession = getSqlSession();
		adoptionMapper = sqlSession.getMapper(AdoptionMapper.class);
		
		int result = adoptionMapper.machingSuccess(aniDTO);
		int result2 = 0;
		int result3 = 0;
		
		if(result > 0 ) {
			
			result2 = adoptionMapper.machingPerfect(aniDTO);
			if(result2 > 0) {
				
				result3 = adoptionMapper.machingFail(aniDTO);
			}
			
		}	
		
		if(result > 0 && result2 > 0 && result3 > 0) {
			
			sqlSession.commit();
			
		}else {
			
			sqlSession.rollback();
		}
		sqlSession.close();
		
		return result;
	}

	
}
