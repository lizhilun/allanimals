package com.allanimals.rescue.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.allanimals.member.model.dto.MemberDTO;
import com.allanimals.rescue.model.dto.RescueDTO;
import com.allanimals.rescue.model.service.RescueService;

/**
 * Servlet implementation class RescueContent
 */
@WebServlet("/rescue/content")
public class RescueContentServlet extends HttpServlet {
       
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// 아이디 들고다니기
		HttpSession session = request.getSession();
		String mId = ((MemberDTO)session.getAttribute("loginMember")).getMemId();
		
    	
		int saveNum = Integer.parseInt(request.getParameter("saveNum"));
	
		System.out.println("saveNum : " + saveNum);
		
		RescueDTO rescue = new RescueService().selectOneRescueBoard(saveNum);
		System.out.println(rescue);
		
		String path = "";
		if(rescue != null) {
			path = "/WEB-INF/views/rescue/rescueContent.jsp";
			request.setAttribute("rescue", rescue);
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "구조 동물 게시판 상세 조회 실패!");
		}
		
		request.getRequestDispatcher(path).forward(request, response);

}
    
}
