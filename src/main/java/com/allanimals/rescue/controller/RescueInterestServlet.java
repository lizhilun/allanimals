package com.allanimals.rescue.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.member.model.dto.MemberDTO;
import com.allanimals.rescue.model.dto.RescueInterestDTO;
import com.allanimals.rescue.model.service.RescueService;

/**
 * Servlet implementation class RescueInterestServlet
 */
@WebServlet("/rescue/interest")
public class RescueInterestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RescueInterestServlet() { }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    	String memId = ((MemberDTO) request.getSession().getAttribute("loginMember")).getMemId();				
		String saveNum = request.getParameter("saveNum");
		
		RescueInterestDTO rescueInterest = new RescueInterestDTO();
		
		rescueInterest.setMemId(memId);
		rescueInterest.setSaveNum(saveNum);
		
		System.out.println("=========================================================");
		System.out.println("InterestDTO : " + rescueInterest);
		System.out.println("=========================================================");
		
		RescueService rescueInterestService = new RescueService();
		
		int interCheck = rescueInterestService.selectInterest(rescueInterest);
		
		System.out.println("interCheck : " + interCheck);
		
		int result = 0;
		String interYn = "";
		
		if(interCheck == 0) {
			
			result = rescueInterestService.insertRescueInterest(rescueInterest);
			interYn = "Y";
			
		} else {
			
			result = rescueInterestService.deleteRescueInterest(rescueInterest);
			interYn = "N";
		}
		
		String path = "";
		
		if(result > 0) {
						
			response.setContentType("text/html;charset=utf-8");
			path = "/rescue/content?saveNum="+saveNum+"&&interYn="+interYn;
			response.sendRedirect(request.getContextPath()+path);
			
			
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "관심 등록에 실패하셨습니다.");
			request.getRequestDispatcher(path).forward(request, response);
		}	
		
	}

}
