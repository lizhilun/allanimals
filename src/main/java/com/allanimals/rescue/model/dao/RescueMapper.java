package com.allanimals.rescue.model.dao;

import java.util.List;
import java.util.Map;

import com.allanimals.commnon.pagin.SelectCriteria;
import com.allanimals.rescue.model.dto.RescueDTO;
import com.allanimals.rescue.model.dto.RescueInterestDTO;

public interface RescueMapper {

	List<RescueDTO> selectRescueList(SelectCriteria selectCriteria);

	int selectTotalCount(Map<String, String> searchMap);

	List<RescueDTO> selectOneRescueContent(int saveNum);

	int incrementRescueCount(int saveNum);

	RescueDTO selectOneRescueBoard(int saveNum);
	
	// 관심 저장 또는 삭제 여부 구별
	int selectInterest(RescueInterestDTO rescueInterest);
	
	// 관심 설정 저장
	int insertRescueInterest(RescueInterestDTO rescueInterest);

	// 관심 설정 삭제
	int deleteRescueInterest(RescueInterestDTO rescueInterest);


	
	

}
