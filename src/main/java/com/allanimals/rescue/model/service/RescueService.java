package com.allanimals.rescue.model.service;

import static com.allanimals.commnon.mybatis.Template.getSqlSession;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.session.SqlSession;

import com.allanimals.commnon.pagin.SelectCriteria;
import com.allanimals.rescue.model.dao.RescueMapper;
import com.allanimals.rescue.model.dto.RescueDTO;
import com.allanimals.rescue.model.dto.RescueInterestDTO;



public class RescueService {
	
	private RescueMapper rescueMapper;

	
	/**
	 * <pre>
	 * 	페이징 처리를 위한 전체 게시물 수 조회용 메소드
	 * </pre>
	 * @param searchMap
	 * @return
	 */
	
	public int selectTotalCount(Map<String, String> searchMap) {

		SqlSession sqlSession = getSqlSession();
		
		rescueMapper = sqlSession.getMapper(RescueMapper.class);
		
		int totalCount = rescueMapper.selectTotalCount(searchMap);
		
		sqlSession.close();
		
		return totalCount;
	
	}

	
	/**
	 * <pre>
	 *   게시물 전체 조회용 메소드
	 * </pre>
	 * @param selectCriteria
	 * @return
	 */
	
	public List<RescueDTO> selectRescueList(SelectCriteria selectCriteria) {
		
		SqlSession sqlSession = getSqlSession();
		
		rescueMapper = sqlSession.getMapper(RescueMapper.class);
		
		List<RescueDTO> rescueList = rescueMapper.selectRescueList(selectCriteria);
		
		sqlSession.close();
		
		return rescueList;
		
	}
	

	/**
	 * <pre>
	 * 구조 동물 게시물 상세 보기를 위한 메소드
	 * </pre>
	 * @param saveNum
	 * @return
	 */


	public RescueDTO selectOneRescueBoard(int saveNum) {
		
		SqlSession sqlSession = getSqlSession();
		
		rescueMapper = sqlSession.getMapper(RescueMapper.class);
		
		RescueDTO rescue = null;
		
		rescue = rescueMapper.selectOneRescueBoard(saveNum);
			
			if(rescue != null) {
				
				sqlSession.commit();	
				
			} else {				
				
				sqlSession.rollback();
			}			 
	
		sqlSession.close();
		
		return rescue;
	}


	/**
	 * <pre>
	 * 관심 설정 / 삭제 여부를 결정하기 위한 메소드
	 * </pre>
	 * @param rescueInterest
	 * @return
	 */
	public int selectInterest(RescueInterestDTO rescueInterest) {
		
		SqlSession sqlSession = getSqlSession();
		
		rescueMapper = sqlSession.getMapper(RescueMapper.class);
		
		int selcetCheck = rescueMapper.selectInterest(rescueInterest);
		
		sqlSession.close();
		
		return selcetCheck;
	}


	/**
	 * <pre>
	 * 관심 설정 저장을 위한 메소드
	 * </pre>
	 * @param rescueInterest
	 * @return
	 */
	public int insertRescueInterest(RescueInterestDTO rescueInterest) {
		
		SqlSession sqlSession = getSqlSession();

		rescueMapper = sqlSession.getMapper(RescueMapper.class);

		int result =  rescueMapper.insertRescueInterest(rescueInterest);

		if(result > 0) {

			sqlSession.commit();
			
		} else {

			sqlSession.rollback();

		}
		
		sqlSession.close();
	
		return result;
		
	}

	

	/**
	 * <pre>
	 * 관심 설정 삭제를 위한 메소드
	 * </pre>
	 * @param rescueInterest
	 * @return
	 */
	public int deleteRescueInterest(RescueInterestDTO rescueInterest) {
		
		SqlSession sqlSession = getSqlSession();

		rescueMapper = sqlSession.getMapper(RescueMapper.class);

		int result =  rescueMapper.deleteRescueInterest(rescueInterest);
		
		if(result > 0) {

			sqlSession.commit();

		} else {

			sqlSession.rollback();

		}

		sqlSession.close();
		
		return result;
		
	}


}
