package com.allanimals.rescue.model.dto;

public class RescueInterestDTO {
	
	private String interestNum;
	private String memId;
	private String saveNum;
	private String disNum;
	private String dailyNum;
	private String afterNum;
	private String introNum;
	
	public RescueInterestDTO() {}

	public RescueInterestDTO(String interestNum, String memId, String saveNum, String disNum, String dailyNum,
			String afterNum, String introNum) {
		super();
		this.interestNum = interestNum;
		this.memId = memId;
		this.saveNum = saveNum;
		this.disNum = disNum;
		this.dailyNum = dailyNum;
		this.afterNum = afterNum;
		this.introNum = introNum;
	}

	public String getInterestNum() {
		return interestNum;
	}

	public void setInterestNum(String interestNum) {
		this.interestNum = interestNum;
	}

	public String getMemId() {
		return memId;
	}

	public void setMemId(String memId) {
		this.memId = memId;
	}

	public String getSaveNum() {
		return saveNum;
	}

	public void setSaveNum(String saveNum) {
		this.saveNum = saveNum;
	}

	public String getDisNum() {
		return disNum;
	}

	public void setDisNum(String disNum) {
		this.disNum = disNum;
	}

	public String getDailyNum() {
		return dailyNum;
	}

	public void setDailyNum(String dailyNum) {
		this.dailyNum = dailyNum;
	}

	public String getAfterNum() {
		return afterNum;
	}

	public void setAfterNum(String afterNum) {
		this.afterNum = afterNum;
	}

	public String getIntroNum() {
		return introNum;
	}

	public void setIntroNum(String introNum) {
		this.introNum = introNum;
	}

	@Override
	public String toString() {
		return "RescueInterestDTO [interestNum=" + interestNum + ", memId=" + memId + ", saveNum=" + saveNum
				+ ", disNum=" + disNum + ", dailyNum=" + dailyNum + ", afterNum=" + afterNum + ", introNum=" + introNum
				+ "]";
	}
	
	

}
