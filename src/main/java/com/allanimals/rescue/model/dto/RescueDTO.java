package com.allanimals.rescue.model.dto;

import java.sql.Date;

public class RescueDTO {
	
	private int saveNum;
	private String saveCont;
	private Date saveTime;
	private char saveYn;
	private String memId;
	private char repNum;
	private String repCont;
	private Date repTime;
	private char repYn;
	private String memAddre;
	private char aniAdopt;
	private char aniSpec;
	
	public RescueDTO() {}

	public RescueDTO(int saveNum, String saveCont, Date saveTime, char saveYn, String memId, char repNum,
			String repCont, Date repTime, char repYn, String memAddre, char aniAdopt, char aniSpec) {
		super();
		this.saveNum = saveNum;
		this.saveCont = saveCont;
		this.saveTime = saveTime;
		this.saveYn = saveYn;
		this.memId = memId;
		this.repNum = repNum;
		this.repCont = repCont;
		this.repTime = repTime;
		this.repYn = repYn;
		this.memAddre = memAddre;
		this.aniAdopt = aniAdopt;
		this.aniSpec = aniSpec;
	}

	public int getSaveNum() {
		return saveNum;
	}

	public void setSaveNum(int saveNum) {
		this.saveNum = saveNum;
	}

	public String getSaveCont() {
		return saveCont;
	}

	public void setSaveCont(String saveCont) {
		this.saveCont = saveCont;
	}

	public Date getSaveTime() {
		return saveTime;
	}

	public void setSaveTime(Date saveTime) {
		this.saveTime = saveTime;
	}

	public char getSaveYn() {
		return saveYn;
	}

	public void setSaveYn(char saveYn) {
		this.saveYn = saveYn;
	}

	public String getMemId() {
		return memId;
	}

	public void setMemId(String memId) {
		this.memId = memId;
	}

	public char getRepNum() {
		return repNum;
	}

	public void setRepNum(char repNum) {
		this.repNum = repNum;
	}

	public String getRepCont() {
		return repCont;
	}

	public void setRepCont(String repCont) {
		this.repCont = repCont;
	}

	public Date getRepTime() {
		return repTime;
	}

	public void setRepTime(Date repTime) {
		this.repTime = repTime;
	}

	public char getRepYn() {
		return repYn;
	}

	public void setRepYn(char repYn) {
		this.repYn = repYn;
	}

	public String getMemAddre() {
		return memAddre;
	}

	public void setMemAddre(String memAddre) {
		this.memAddre = memAddre;
	}

	public char getAniAdopt() {
		return aniAdopt;
	}

	public void setAniAdopt(char aniAdopt) {
		this.aniAdopt = aniAdopt;
	}

	public char getAniSpec() {
		return aniSpec;
	}

	public void setAniSpec(char aniSpec) {
		this.aniSpec = aniSpec;
	}

	@Override
	public String toString() {
		return "RescueDTO [saveNum=" + saveNum + ", saveCont=" + saveCont + ", saveTime=" + saveTime + ", saveYn="
				+ saveYn + ", memId=" + memId + ", repNum=" + repNum + ", repCont=" + repCont + ", repTime=" + repTime
				+ ", repYn=" + repYn + ", memAddre=" + memAddre + ", aniAdopt=" + aniAdopt + ", aniSpec=" + aniSpec
				+ "]";
	}
	
	
}
