package com.allanimals.introduce.model.service;

import static com.allanimals.commnon.mybatis.Template.getSqlSession;

import org.apache.ibatis.session.SqlSession;

import com.allanimals.introduce.model.dao.AnimalMapper;
import com.allanimals.user.adoption.model.dao.AdoptionMapper;
import com.allanimals.user.adoption.model.dto.MemberMachingDTO;

public class AnimalService {

	private AnimalMapper animalMapper;
	private AdoptionMapper adoptionMapper; 
	/**
	 * <pre>
	 * 	동물 입양처리 완료시
	 * </pre>
	 * @param aniNum
	 * @return
	 */
	public int completeAdopt(String aniNum) {
		
		SqlSession sqlSession = getSqlSession();
		animalMapper = sqlSession.getMapper(AnimalMapper.class);
		
		int result = animalMapper.updateComplte(aniNum);
		
		if(result > 0 ) {
			
			sqlSession.commit();
		}else {
			
			sqlSession.rollback();
		}
		sqlSession.close();
		
		return result; 
	}



}
