package com.allanimals.introduce.model.dto;

public class AnimalDTO {
	
	private String aniName; //동물이름
	private String aniSpec; //동물품종
	private String aniNum; //동물번호
	private char aniAdopt; //입양여부 (Y.N)
	private String memId; // 동물을 데리고있는 회원 아이디
	
	
	public AnimalDTO() {}


	public AnimalDTO(String aniName, String aniSpec, String aniNum, char aniAdopt, String memId) {

		this.aniName = aniName;
		this.aniSpec = aniSpec;
		this.aniNum = aniNum;
		this.aniAdopt = aniAdopt;
		this.memId = memId;
	}


	public String getAniName() {
		return aniName;
	}


	public void setAniName(String aniName) {
		this.aniName = aniName;
	}


	public String getAniSpec() {
		return aniSpec;
	}


	public void setAniSpec(String aniSpec) {
		this.aniSpec = aniSpec;
	}


	public String getAniNum() {
		return aniNum;
	}


	public void setAniNum(String aniNum) {
		this.aniNum = aniNum;
	}


	public char getAniAdopt() {
		return aniAdopt;
	}


	public void setAniAdopt(char aniAdopt) {
		this.aniAdopt = aniAdopt;
	}


	public String getMemId() {
		return memId;
	}


	public void setMemId(String memId) {
		this.memId = memId;
	}


	@Override
	public String toString() {
		return "AnimalDTO [aniName=" + aniName + ", aniSpec=" + aniSpec + ", aniNum=" + aniNum + ", aniAdopt="
				+ aniAdopt + ", memId=" + memId + "]";
	}

	
}