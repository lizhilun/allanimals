package com.allanimals.admin.ask.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.admin.model.dto.AsklistDTO;
import com.allanimals.admin.service.AdminService;


/**
 * Servlet implementation class AsklistServlet
 */
@WebServlet("/AsklistServlet")
public class AsklistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		AdminService adminService = new AdminService();
		List<AsklistDTO> askList = adminService.selectask();
		
		for(AsklistDTO ask : askList) {
			
			System.out.println(ask);
		}
		
		/* 조회 결과 성공 여부에 따른 뷰 결정 */
		String path = "";
		
		if(askList != null) {
			
			path = "/WEB-INF/views/admin/asklist.jsp";
			request.setAttribute("askList", askList);
					
		} else {
			
			path = "/WEB-INF/views/common/errorPage.jsp";
			request.setAttribute("message", " 목록 조회 실패!!");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
	}

}


