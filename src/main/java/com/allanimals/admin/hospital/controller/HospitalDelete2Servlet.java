package com.allanimals.admin.hospital.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.admin.service.AdminService;

/**
 * Servlet implementation class HospitalDelete2Servlet
 */
@WebServlet("/HospitalDelete2Servlet")
public class HospitalDelete2Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		String hosNum = request.getParameter("hosNum");
		
		System.out.println("hosNum : " + hosNum);
		
		AdminService adminservice = new AdminService();
		
		int result = adminservice.hospitaldelete(hosNum);
		
		System.out.println("수행결과 확인 : " + result);
		
		String path = "";
		
		if(result > 0) {
			
			path = "/WEB-INF/views/common/success.jsp";
			request.setAttribute("successCode", "deleteHelp");
		} else {
			
			path = "/WEB-INF/views/common/errorPage.jsp";
			request.setAttribute("message", "삭제 실패!");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
	}
}

