package com.allanimals.admin.hospital.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.admin.model.dto.HelpDTO;
import com.allanimals.admin.model.dto.HospitalDTO;
import com.allanimals.admin.service.AdminService;

/**
 * Servlet implementation class HospitalEdit2
 */
@WebServlet("/HospitalEdit2")
public class HospitalEdit2Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		String hosNum = request.getParameter("hosNum");
		
		AdminService adminService = new AdminService();
		HospitalDTO hosOne = adminService.hosOnelist(hosNum);
		
		System.out.println(hosOne);
		
		/* 조회 결과 성공 여부에 따른 뷰 결정 */
		String path = "";
		
		if(hosOne != null) {
			
			path = "/WEB-INF/views/admin/hospitaledit2.jsp";
			request.setAttribute("hosList", hosOne);
					
		} else {
			
			path = "/WEB-INF/views/common/errorPage.jsp";
			request.setAttribute("message", " 목록 조회 실패!!");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		String hosName = request.getParameter("hosName");
		String hosAddre = request.getParameter("hosAddre");
		String hosPhone = request.getParameter("hosPhone");
		String hosTime = request.getParameter("hosTime");
		String hosNum = request.getParameter("hosNum");
		
		
		
		
		HospitalDTO hospital = new HospitalDTO();
		
		hospital.setHosName(hosName);
		hospital.setHosAddre(hosAddre);
		hospital.setHosPhone(hosPhone);
		hospital.setHosTime(hosTime);
		hospital.setHosNum(hosNum);
		
		
		
		
		System.out.println("수정 정보 확인 : " + hospital);
		
		int result = new AdminService().updatehospital(hospital);
		
		System.out.println("수정 결과 확인 : " + result);
		
		String path = "";
		
		if(result > 0) {
			
			path = "/WEB-INF/views/common/success.jsp";
			request.setAttribute("successCode", "updateHelp");
		} else {
			
			path = "/WEB-INF/views/common/errorPage.jsp";
			request.setAttribute("message", "회원 정보 수정 실패!");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
		}

}
