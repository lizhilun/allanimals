package com.allanimals.admin.hospital.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.admin.model.dto.HospitalDTO;
import com.allanimals.admin.service.AdminService;
import com.allanimals.member.model.dto.MemberDTO;

/**
 * Servlet implementation class HospitalRegisterServlet
 */
@WebServlet("/HospitalRegisterServlet")
public class HospitalRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/views/admin/hospitalregister.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		String hosName = request.getParameter("hosName");
		String hosAddre = request.getParameter("hosAddre");
		String hosPhone = request.getParameter("hosPhone");
		String hosTime = request.getParameter("hosTime");
		String hosNum = request.getParameter("hosNum");
	
		
		HospitalDTO hos = new HospitalDTO();
		hos.setHosName(hosName);
		hos.setHosAddre(hosAddre);
		hos.setHosPhone(hosPhone);
		hos.setHosTime(hosTime);
		hos.setHosNum(hosNum);
		hos.setMemId(((MemberDTO) request.getSession().getAttribute("loginMember")).getMemId());
		
	
		System.out.println("insert request hos : " + hos);
		
		AdminService adminService = new AdminService();
		int result = adminService.inserthospital(hos);
		
		System.out.println("추가 확인 : " + result);
		
		String path = "";
		
		if(result > 0) {
			path = "/WEB-INF/views/common/success.jsp";
			
			request.setAttribute("successCode", "insertHelp");
		} else {
			
			path = "/WEB-INF/views/common/errorPage.jsp";
			request.setAttribute("message", "신규 직원 등록에 실패하셨습니다.");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
		
	}
}
