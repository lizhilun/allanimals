package com.allanimals.admin.hospital.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.admin.model.dto.HospitalDTO;
import com.allanimals.admin.service.AdminService;

/**
 * Servlet implementation class HospitalEditServlet
 */
@WebServlet("/HospitalEditServlet")
public class HospitalEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		AdminService adminService = new AdminService();
		List<HospitalDTO> hospitalList = adminService.selecthospital();
		
		for(HospitalDTO hospital : hospitalList) {
			
			System.out.println(hospital);
		}
		
		/* 조회 결과 성공 여부에 따른 뷰 결정 */
		String path = "";
		
		if(hospitalList != null) {
			
			path = "/WEB-INF/views/admin/hospitaledit.jsp";
			request.setAttribute("hospitalList", hospitalList);
					
		} else {
			
			path = "/WEB-INF/views/common/errorPage.jsp";
			request.setAttribute("message", " 목록 조회 실패!!");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
	}
	
		
}

