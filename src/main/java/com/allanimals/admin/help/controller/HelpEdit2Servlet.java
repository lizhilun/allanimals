package com.allanimals.admin.help.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.admin.model.dto.HelpDTO;
import com.allanimals.admin.model.dto.HospitalDTO;
import com.allanimals.admin.service.AdminService;

/**
 * Servlet implementation class HelpEdit2
 */
@WebServlet("/HelpEdit2")
public class HelpEdit2Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		String helpNum = request.getParameter("helpNum");
		
		AdminService adminService = new AdminService();
		HelpDTO helpOne = adminService.helpOnelist(helpNum);
		
		System.out.println( helpOne);
		
		/* 조회 결과 성공 여부에 따른 뷰 결정 */
		String path = "";
		
		if(helpOne != null) {
			
			path = "/WEB-INF/views/admin/helpedit2.jsp";
			request.setAttribute("helpList", helpOne);
					
		} else {
			
			path = "/WEB-INF/views/common/errorPage.jsp";
			request.setAttribute("message", " 목록 조회 실패!!");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		String helpArea = request.getParameter("helpArea");
		String helpName = request.getParameter("helpName");
		String helpPhone = request.getParameter("helpPhone");
		String helpAddre = request.getParameter("helpAddre");
		String helpNum = request.getParameter("helpNum");
		String memId = request.getParameter("memId");
		
		
		
		HelpDTO help = new HelpDTO();
		
		help.setHelpArea(helpArea);
		help.setHelpName(helpName);
		help.setHelpPhone(helpPhone);
		help.setHelpAddre(helpAddre);
		help.setHelpNum(helpNum);
		help.setMemId(memId);
		
		
		
		System.out.println("수정 정보 확인 : " + help);
		
		int result = new AdminService().updateHelp(help);
		
		System.out.println("수정 결과 확인 : " + result);
		
		String path = "";
		
		if(result > 0) {
			
			path = "/WEB-INF/views/common/success.jsp";
			request.setAttribute("successCode", "updateHelp");
		} else {
			
			path = "/WEB-INF/views/common/errorPage.jsp";
			request.setAttribute("message", "회원 정보 수정 실패!");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
		}
}
	
	





