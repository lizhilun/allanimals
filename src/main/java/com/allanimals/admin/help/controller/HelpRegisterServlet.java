package com.allanimals.admin.help.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.admin.model.dto.HelpDTO;
import com.allanimals.admin.service.AdminService;
import com.allanimals.member.model.dto.MemberDTO;

/**
 * Servlet implementation class HelpRegisterServlet
 */
@WebServlet("/HelpRegisterServlet")
public class HelpRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/views/admin/helpregister.jsp").forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		String helpArea = request.getParameter("helpArea");
		String helpName = request.getParameter("helpName");
		String helpPhone = request.getParameter("helpPhone");
		String helpAddre = request.getParameter("helpAddre");
		String helpNum = request.getParameter("helpNum");
		
		HelpDTO help = new HelpDTO();
		help.setHelpArea(helpArea);
		help.setHelpName(helpName);
		help.setHelpPhone(helpPhone);
		help.setHelpAddre(helpAddre);
		help.setHelpNum(helpNum);
		help.setMemId(((MemberDTO) request.getSession().getAttribute("loginMember")).getMemId());
		
		System.out.println("insert request help : " + help);
		
		AdminService adminService = new AdminService();
		int result = adminService.insertHelp(help);
		
		System.out.println("추가 확인 : " + result);
		
		String path = "";
		
		if(result > 0) {
			path = "/WEB-INF/views/common/success.jsp";
			
			request.setAttribute("successCode", "insertHelp");
		} else {
			
			path = "/WEB-INF/views/common/errorPage.jsp";
			request.setAttribute("message", "등록에 실패하셨습니다.");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
		
	}
}


