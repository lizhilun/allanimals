package com.allanimals.admin.help.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.admin.model.dto.HelpDTO;
import com.allanimals.admin.service.AdminService;

/**
 * Servlet implementation class HelpDeleteServlet
 */
@WebServlet("/HelpDeleteServlet")
public class HelpDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminService adminService = new AdminService();
		List<HelpDTO> helpList = adminService.helpedit();
		
		for(HelpDTO help : helpList) {
			
			System.out.println(help);
		}
		
		/* 조회 결과 성공 여부에 따른 뷰 결정 */
		String path = "";
		
		if(helpList != null) {
			
			path = "/WEB-INF/views/admin/helpdelete.jsp";
			request.setAttribute("helpList", helpList);
					
		} else {
			
			path = "/WEB-INF/views/common/errorPage.jsp";
			request.setAttribute("message", " 목록 조회 실패!!");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		String helpNum = request.getParameter("helpNum");
		
		System.out.println("helpNum : " + helpNum);
		
		AdminService adminservice = new AdminService();
		
		int result = adminservice.helpdelete(helpNum);
		
		System.out.println("수행결과 확인 : " + result);
		
		String path = "";
		
		if(result > 0) {
			
			path = "/WEB-INF/views/common/success.jsp";
			request.setAttribute("successCode", "deleteHelp");
		} else {
			
			path = "/WEB-INF/views/common/errorPage.jsp";
			request.setAttribute("message", "삭제 실패!");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
	}

}


