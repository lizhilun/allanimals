//package com.allanimals.admin.pagin;
//
//import java.io.IOException;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import com.allanimals.admin.service.AdminService;
//import com.allanimals.commnon.pagin.Pagenation;
//import com.allanimals.commnon.pagin.SelectCriteria;
//import com.allanimals.inquiry.dto.NoticeBoardDTO;
//import com.allanimals.member.model.dto.MemberDTO;
//
//
///**
// * Servlet implementation class MemberPaginServlet
// */
//@WebServlet("/MemberPaginServlet")
//public class MemberPaginServlet extends HttpServlet {
//	private static final long serialVersionUID = 1L;
//
//	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		
//	
//		String currentPage = request.getParameter("currentPage");
//		int pageNo = 1;
//		
//		if(currentPage != null && !"".equals(currentPage)) {
//			pageNo = Integer.parseInt(currentPage);
//		}
//		
//		/* 0보다 작은 숫자값을 입력해도 1페이지를 보여준다 */
//		if(pageNo <= 0) {
//			pageNo = 1;
//		}
//		
//		String searchCondition = request.getParameter("searchCondition");
//		String searchValue = request.getParameter("searchValue");
//		
//		Map<String, String> searchMap = new HashMap<>();
//		searchMap.put("searchCondition", searchCondition);
//		searchMap.put("searchValue", searchValue);
//		
//		/* 전체 게시물 수가 필요하다.
//		 * 데이터베이스에서 먼저 전체 게시물 수를 조회해올 것이다.
//		 * 검색조건이 있는 경우 검색 조건에 맞는 전체 게시물 수를 조회한다.
//		 * */
//		AdminService adminService = new AdminService();
//		int totalCount = adminService.selectTotalCount(searchMap);
//		
//		System.out.println("totalBoardCount : " + totalCount);
//		
//		/* 한 페이지에 보여 줄 게시물 수 */
//		int limit = 10;		//얘도 파라미터로 전달받아도 된다.
//		/* 한 번에 보여질 페이징 버튼의 갯수 */
//		int buttonAmount = 5;
//		
//		/* 페이징 처리를 위한 로직 호출 후 페이징 처리에 관한 정보를 담고 있는 인스턴스를 반환받는다. */
//		SelectCriteria selectCriteria = null;
//		
//		if(searchCondition != null && !"".equals(searchCondition)) {
//			selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount, searchCondition, searchValue);
//		} else {
//			selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
//		}
//		
//		System.out.println(selectCriteria);
//		
//		/* 조회해온다 */
//		List<MemberDTO> boardList = adminService.selectBoardList(selectCriteria);
//		
//		System.out.println("boardList : " + boardList);
//		
//		String path = "";
//		if(boardList != null) {
//			path = "/WEB-INF/views/admin/memberview.jsp";
//			request.setAttribute("boardList",  boardList);
//			request.setAttribute("selectCriteria", selectCriteria);
//			request.setAttribute("boardType", "memlist");
//		
//		} else {
//			path = "/WEB-INF/views/common/failed.jsp";
//			request.setAttribute("message", "게시물 목록 조회 실패!");
//		}
//		
//		request.getRequestDispatcher(path).forward(request, response);
//	}
//
//	
//}
