package com.allanimals.admin.service;

import static com.allanimals.commnon.mybatis.Template.getSqlSession;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.allanimals.admin.dao.AdminMapper;
import com.allanimals.admin.model.dto.AsklistDTO;
import com.allanimals.admin.model.dto.HelpDTO;
import com.allanimals.admin.model.dto.HospitalDTO;
import com.allanimals.admin.pagin.AdminSelectCriteria;
import com.allanimals.allDaily.model.dao.AllDailyMapper;
import com.allanimals.allDaily.model.dto.IntroDTO;
import com.allanimals.commnon.pagin.SelectCriteria;
import com.allanimals.inquiry.dao.NoticeMapper;
import com.allanimals.member.model.dao.MemberMapper;
import com.allanimals.member.model.dto.MemberDTO;
import com.allanimals.notice.dto.NoticeBoardDTO;


public class AdminService {
	
		private AdminMapper adminMapper;


		
		/**
		 * <pre>
		 * 	 회원 전체조회 메소드
		 * </pre>
		 * 
		 */
		
		public List<MemberDTO> selectmemberList() {
			
			SqlSession sqlSession = getSqlSession();
			
			adminMapper = sqlSession.getMapper(AdminMapper.class);
			List<MemberDTO> memList = adminMapper.selectmemberList();
			
			sqlSession.close();
			
			return memList;
		}

		/**
		 * <pre>
		 *  회원정보삭제 메소드
		 * </pre>
		 *
		 */

		public int deleteMem(String memId) {
			SqlSession sqlSession = getSqlSession();
			
			adminMapper = sqlSession.getMapper(AdminMapper.class);
			int result = adminMapper.deleteMem(memId);
			
			System.out.println(result);
			
			if(result > 0) {
				
				sqlSession.commit();
			} else {
				
				sqlSession.rollback();
			}
			
			sqlSession.close();
			
			return result;
		}
		
		/**
		 * 
		 * <pre>
		 *  문의사항 답변 메소드
		 * </pre>
		 */
		
		public List<AsklistDTO> selectask() {
			SqlSession sqlSession = getSqlSession();
			
			adminMapper = sqlSession.getMapper(AdminMapper.class);
			List<AsklistDTO> askList = adminMapper.selectask();
			
			sqlSession.close();
			
			return askList;
		}

		/**
		 * <pre>
		 *  구조요청 가능업체 등록 메소드
		 * </pre>
		 */
		
		public int insertHelp(HelpDTO help) {
			SqlSession sqlSession = getSqlSession();
			
			adminMapper = sqlSession.getMapper(AdminMapper.class);
			int result = adminMapper.insertHelp(help);
				
			if(result > 0) {
				
				sqlSession.commit();
			} else {
				
				sqlSession.rollback();
			}
			
			sqlSession.close();
			
			return result;
		}


		/**
		 * <pre>
		 *  구조요청 가능업체 수정 메소드
		 * </pre>
		 */
		
		public List<HelpDTO> helpedit() {
			SqlSession sqlSession = getSqlSession();
			
			adminMapper = sqlSession.getMapper(AdminMapper.class);
			List<HelpDTO> helpList = adminMapper.helpedit();
			
			sqlSession.close();
			
			return helpList;
		}

		public int updateHelp(HelpDTO help) {
			
			SqlSession sqlSession = getSqlSession();
			
			adminMapper = sqlSession.getMapper(AdminMapper.class);
			int result = adminMapper.updateHelp(help);
			
			if(result > 0) {
				
				sqlSession.commit();
			} else {
				
				sqlSession.rollback();
			}
			
			sqlSession.close();
			
			return result;
		}


		/**
		 * <pre>
		 *  구조요청 가능업체 삭제 메소드
		 * </pre>
		 */
		
		public int helpdelete(String helpNum) {
			SqlSession sqlSession = getSqlSession();
			
			adminMapper = sqlSession.getMapper(AdminMapper.class);
			int result = adminMapper.helpdelete(helpNum);
			
			System.out.println(result);
			
			if(result > 0) {
				
				sqlSession.commit();
			} else {
				
				sqlSession.rollback();
			}
			
			sqlSession.close();
			
			return result;
		}

		public List<HospitalDTO> selecthospital() {
			SqlSession sqlSession = getSqlSession();
			
			adminMapper = sqlSession.getMapper(AdminMapper.class);
			List<HospitalDTO> hospitalList = adminMapper.selecthospital();
			
			sqlSession.close();
			
			return hospitalList;
		}

		public int updatehospital(HospitalDTO hospital) {

			SqlSession sqlSession = getSqlSession();
			
			adminMapper = sqlSession.getMapper(AdminMapper.class);
			int result = adminMapper.updatehospital(hospital);
			
			if(result > 0) {
				
				sqlSession.commit();
			} else {
				
				sqlSession.rollback();
			}
			
			sqlSession.close();
			
			return result;
		}

		public int hospitaldelete(String hosNum) {
			SqlSession sqlSession = getSqlSession();
			
			adminMapper = sqlSession.getMapper(AdminMapper.class);
			int result = adminMapper.hospitaldelete(hosNum);
			
			System.out.println(result);
			
			if(result > 0) {
				
				sqlSession.commit();
			} else {
				
				sqlSession.rollback();
			}
			
			sqlSession.close();
			
			return result;
		}

		public int inserthospital(HospitalDTO hos) {
			SqlSession sqlSession = getSqlSession();
			
			adminMapper = sqlSession.getMapper(AdminMapper.class);
			int result = adminMapper.inserthospital(hos);
		
			if(result > 0) {
				
				sqlSession.commit();
			} else {
				
				sqlSession.rollback();
			}
			
			sqlSession.close();
			
			return result;
		}

	

		public HelpDTO helpOnelist(String helpNum) {
			
			SqlSession sqlSession = getSqlSession();
			
			adminMapper = sqlSession.getMapper(AdminMapper.class);
			HelpDTO helpOne = adminMapper.helpOnelist(helpNum);
			
			sqlSession.close();
			
			return helpOne;
		}

		public HospitalDTO hosOnelist(String hosNum) {
			SqlSession sqlSession = getSqlSession();
			
			adminMapper = sqlSession.getMapper(AdminMapper.class);
			HospitalDTO hosOne = adminMapper.hosOnelist(hosNum);
			
			sqlSession.close();
			
			return hosOne;
		}
		
		/**
		 * <pre>
		 * 	페이징 처리를 위한 전체 게시물 수 조회용 메소드
		 * </pre>
		 * @param searchMap
		 * @return
		 */
		public int selectTotalCount(Map<String, String> searchMap) {
			
			SqlSession sqlSession = getSqlSession();
			adminMapper = sqlSession.getMapper(AdminMapper.class);
			
			int totalCount = adminMapper.selectTotalCount(searchMap);
			
			sqlSession.close();
			
			return totalCount;
		}

		public List<MemberDTO> selectBoardList(SelectCriteria selectCriteria) {
			SqlSession sqlSession = getSqlSession();
			adminMapper = sqlSession.getMapper(AdminMapper.class);
			
			List<MemberDTO> boardList = adminMapper.selectBoardList(selectCriteria);
			
			sqlSession.close();
				System.out.print(boardList);
			return boardList;
		}


	}





	


		

	
