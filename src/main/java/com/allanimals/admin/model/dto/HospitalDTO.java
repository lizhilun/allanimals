package com.allanimals.admin.model.dto;

public class HospitalDTO {
	private String hosName;
	private String hosAddre;
	private String hosPhone;
	private String hosTime;
	private String hosNum;
	private String memId;
	
	public HospitalDTO() {}

	public HospitalDTO(String hosName, String hosAddre, String hosPhone, String hosTime, String hosNum, String memId) {
		super();
		this.hosName = hosName;
		this.hosAddre = hosAddre;
		this.hosPhone = hosPhone;
		this.hosTime = hosTime;
		this.hosNum = hosNum;
		this.memId = memId;
	}

	public String getHosName() {
		return hosName;
	}

	public void setHosName(String hosName) {
		this.hosName = hosName;
	}

	public String getHosAddre() {
		return hosAddre;
	}

	public void setHosAddre(String hosAddre) {
		this.hosAddre = hosAddre;
	}

	public String getHosPhone() {
		return hosPhone;
	}

	public void setHosPhone(String hosPhone) {
		this.hosPhone = hosPhone;
	}

	public String getHosTime() {
		return hosTime;
	}

	public void setHosTime(String hosTime) {
		this.hosTime = hosTime;
	}

	public String getHosNum() {
		return hosNum;
	}

	public void setHosNum(String hosNum) {
		this.hosNum = hosNum;
	}

	public String getMemId() {
		return memId;
	}

	public void setMemId(String memId) {
		this.memId = memId;
	}

	@Override
	public String toString() {
		return "HospitalDTO [hosName=" + hosName + ", hosAddre=" + hosAddre + ", hosPhone=" + hosPhone + ", hosTime="
				+ hosTime + ", hosNum=" + hosNum + ", memId=" + memId + "]";
	}

}