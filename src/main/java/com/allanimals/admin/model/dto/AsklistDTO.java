package com.allanimals.admin.model.dto;

import java.sql.Date;

public class AsklistDTO {
	
	private String inqTitle;
	private String inqCont;
	private String inqNum;
	private java.sql.Date inqtime;
	private String inqYN;
	private String memId;
	private String inqDelYN;
	
	public AsklistDTO() {}

	public AsklistDTO(String inqTitle, String inqCont, String inqNum, Date inqtime, String inqYN, String memId,
			String inqDelYN) {
		super();
		this.inqTitle = inqTitle;
		this.inqCont = inqCont;
		this.inqNum = inqNum;
		this.inqtime = inqtime;
		this.inqYN = inqYN;
		this.memId = memId;
		this.inqDelYN = inqDelYN;
	}

	public String getInqTitle() {
		return inqTitle;
	}

	public void setInqTitle(String inqTitle) {
		this.inqTitle = inqTitle;
	}

	public String getInqCont() {
		return inqCont;
	}

	public void setInqCont(String inqCont) {
		this.inqCont = inqCont;
	}

	public String getInqNum() {
		return inqNum;
	}

	public void setInqNum(String inqNum) {
		this.inqNum = inqNum;
	}

	public java.sql.Date getInqtime() {
		return inqtime;
	}

	public void setInqtime(java.sql.Date inqtime) {
		this.inqtime = inqtime;
	}

	public String getInqYN() {
		return inqYN;
	}

	public void setInqYN(String inqYN) {
		this.inqYN = inqYN;
	}

	public String getMemId() {
		return memId;
	}

	public void setMemId(String memId) {
		this.memId = memId;
	}

	public String getInqDelYN() {
		return inqDelYN;
	}

	public void setInqDelYN(String inqDelYN) {
		this.inqDelYN = inqDelYN;
	}

	@Override
	public String toString() {
		return "AsklistDTO [inqTitle=" + inqTitle + ", inqCont=" + inqCont + ", inqNum=" + inqNum + ", inqtime="
				+ inqtime + ", inqYN=" + inqYN + ", memId=" + memId + ", inqDelYN=" + inqDelYN + "]";
	}
}

	