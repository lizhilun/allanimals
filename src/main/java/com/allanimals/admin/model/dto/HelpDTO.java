package com.allanimals.admin.model.dto;

public class HelpDTO {
	private String helpArea;
	private String helpName;
	private String helpPhone;
	private String helpAddre;
	private String helpNum;
	private String memId;
	
	public HelpDTO() {}
	
	public HelpDTO(String helpArea, String helpName, String helpPhone, String helpAddre, String helpNum, String memId) {
		super();
		this.helpArea = helpArea;
		this.helpName = helpName;
		this.helpPhone = helpPhone;
		this.helpAddre = helpAddre;
		this.helpNum = helpNum;
		this.memId = memId;
	}
	
	public String getHelpArea() {
		return helpArea;
	}

	public void setHelpArea(String helpArea) {
		this.helpArea = helpArea;
	}

	public String getHelpName() {
		return helpName;
	}

	public void setHelpName(String helpName) {
		this.helpName = helpName;
	}

	public String getHelpPhone() {
		return helpPhone;
	}

	public void setHelpPhone(String helpPhone) {
		this.helpPhone = helpPhone;
	}

	public String getHelpAddre() {
		return helpAddre;
	}

	public void setHelpAddre(String helpAddre) {
		this.helpAddre = helpAddre;
	}

	public String getHelpNum() {
		return helpNum;
	}

	public void setHelpNum(String helpNum) {
		this.helpNum = helpNum;
	}

	public String getMemId() {
		return memId;
	}

	public void setMemId(String memId) {
		this.memId = memId;
	}

	@Override
	public String toString() {
		return "HelpDTO [helpArea=" + helpArea + ", helpName=" + helpName + ", helpPhone=" + helpPhone + ", helpAddre="
				+ helpAddre + ", helpNum=" + helpNum + ", memId=" + memId + "]";
	}


	
}
	