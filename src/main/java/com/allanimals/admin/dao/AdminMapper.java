package com.allanimals.admin.dao;

import java.util.List;
import java.util.Map;

import com.allanimals.admin.model.dto.AsklistDTO;
import com.allanimals.admin.model.dto.HelpDTO;
import com.allanimals.admin.model.dto.HospitalDTO;
import com.allanimals.commnon.pagin.SelectCriteria;
import com.allanimals.member.model.dto.MemberDTO;
import com.allanimals.notice.dto.NoticeBoardDTO;


public interface AdminMapper {

	int insertHelp(HelpDTO help);

	List<AsklistDTO> selectask();
	
	int deleteMem(String memId);

	List<HelpDTO> helpedit();

	int updateHelp(HelpDTO help);

	int helpdelete(String helpNum);

	List<HospitalDTO> selecthospital();

	int updatehospital(HospitalDTO hospital);

	int hospitaldelete(String hosNum);

	int inserthospital(HospitalDTO hos);

	String helpedit2(String helpNum);

	HelpDTO helpOnelist(String helpNum);

	HospitalDTO hosOnelist(String hosNum);

	int selectTotalCount(Map<String, String> searchMap);

	List<MemberDTO> selectBoardList(SelectCriteria selectCriteria);

	List<MemberDTO> selectmemberList();

	}
	


