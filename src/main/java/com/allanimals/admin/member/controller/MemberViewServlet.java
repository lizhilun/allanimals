package com.allanimals.admin.member.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.admin.dao.AdminMapper;
import com.allanimals.admin.service.AdminService;
import com.allanimals.member.model.dto.MemberDTO;
import com.allanimals.notice.dto.NoticeBoardDTO;

/**
 * Servlet implementation class MemberViewServlet
 */
@WebServlet("/MemberViewServlet")
public class MemberViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		AdminService adminService = new AdminService();
		List<MemberDTO> memList = adminService.selectmemberList();
		
		for(MemberDTO mem : memList) {
			
			System.out.println(mem);
		}
		
		/* 조회 결과 성공 여부에 따른 뷰 결정 */
		String path = "";
		
		if(memList != null) {
			
			path = "/WEB-INF/views/admin/memberview.jsp";
			request.setAttribute("memList", memList);
					
		} else {
			
			path = "/WEB-INF/views/common/errorPage.jsp";
			request.setAttribute("message", "회원 목록 조회 실패!!");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
	}
}
