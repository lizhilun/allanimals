package com.allanimals.admin.member.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.admin.service.AdminService;
import com.allanimals.member.model.dto.MemberDTO;

/**
 * Servlet implementation class MemberDeleteServlet
 */
@WebServlet("/MemberDeleteServlet")
public class MemberDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		AdminService adminService = new AdminService();
		List<MemberDTO> memList = adminService.selectmemberList();
		
		for(MemberDTO mem : memList) {
			
			System.out.println(mem);
		}
		
		/* 조회 결과 성공 여부에 따른 뷰 결정 */
		String path = "";
		
		if(memList != null) {
			
			path = "/WEB-INF/views/admin/memberdelete.jsp";
			request.setAttribute("memList", memList);
					
		} else {
			
			path = "/WEB-INF/views/common/errorPage.jsp";
			request.setAttribute("message", "회원 목록 조회 실패!!");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
	
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		String memId = request.getParameter("memId");
		
		System.out.println("memId : " + memId);
		
		AdminService adminservice = new AdminService();
		
		int result = adminservice.deleteMem(memId);
		
		System.out.println("수행결과 확인 : " + result);
		
		String path = "";
		
		if(result > 0) {
			
			path = "/WEB-INF/views/common/success.jsp";
			request.setAttribute("successCode", "deleteHelp");
		} else {
			
			path = "/WEB-INF/views/common/errorPage.jsp";
			request.setAttribute("message", "삭제 실패!");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
	}

	
}

