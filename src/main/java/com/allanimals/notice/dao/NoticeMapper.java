package com.allanimals.notice.dao;

import java.util.List;
import java.util.Map;

import com.allanimals.commnon.pagin.SelectCriteria;
import com.allanimals.notice.dto.AttachmentDTO;
import com.allanimals.notice.dto.NoticeBoardDTO;

public interface NoticeMapper {

	int selectTotalCount(Map<String, String> searchMap);

	List<NoticeBoardDTO> selectBoardList(SelectCriteria selectCriteria);

	int insertNotice(NoticeBoardDTO board);

	NoticeBoardDTO selectNoticeBoard(String boardNo);

	int updateNotice(NoticeBoardDTO board);

	int deleteNotice(String boardNo);

	int noteFileInsert(AttachmentDTO attach);

	List<AttachmentDTO> selectNotFileList(String boardNo);

	int deleteNotFile(String boardNo);

}
