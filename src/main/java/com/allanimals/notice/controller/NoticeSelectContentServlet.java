package com.allanimals.notice.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.notice.dto.AttachmentDTO;
import com.allanimals.notice.dto.NoticeBoardDTO;
import com.allanimals.notice.service.NoticeService;



/**
 * Servlet implementation class NoticeSelectContentServlet
 */
@WebServlet("/board/noticecontent")
public class NoticeSelectContentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NoticeSelectContentServlet() {
      
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String boardNo = request.getParameter("boardNo");
		System.out.println(boardNo);
		
		NoticeBoardDTO board = new NoticeBoardDTO();
		List<AttachmentDTO> attachList = new ArrayList<>();
//		board.setTitle(request.getParameter("board.title"));
//		board.setContent(request.getParameter("board.content"));
//		board.setNo("board.no");
		
		NoticeService service = new NoticeService();
		board = service.selectNoticeBoard(boardNo);
		attachList = service.selectNotFileList(boardNo);
		System.out.println(board);
		String path="";
		
		if(board != null) {

				path = "/WEB-INF/views/notice/notice_content.jsp";
				request.setAttribute("board", board);
				request.setAttribute("attachList",attachList);
		}else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "게시물 목록 조회 실패!");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
	}

}
