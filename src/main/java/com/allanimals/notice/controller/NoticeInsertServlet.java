package com.allanimals.notice.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.allanimals.member.model.dto.MemberDTO;
import com.allanimals.notice.dto.AttachmentDTO;
import com.allanimals.notice.dto.NoticeBoardDTO;
import com.allanimals.notice.service.NoticeService;

/**
 * Servlet implementation class NoticeInsertServlet
 */
@WebServlet("/board/noticeinsert")
public class NoticeInsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String rootLocation;
	private int maxFileSize;
	private String encodingType;
	
	@Override
	public void init() throws ServletException {
	
		ServletContext context = getServletContext();
		
//		rootLocation = context.getInitParameter("upload-location");
		maxFileSize = Integer.parseInt(context.getInitParameter("max-file-size"));
		encodingType = context.getInitParameter("encoding-type");
	}
   
	
	
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NoticeInsertServlet() {
  
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String path =  "/WEB-INF/views/notice/notice_insert.jsp";
		
		request.getRequestDispatcher(path).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
//		request.setCharacterEncoding("UTF-8");
//		
//		String title = request.getParameter("title");
//		String content = request.getParameter("content");
		String memId =((MemberDTO) request.getSession().getAttribute("loginMember")).getMemId();
//		
		NoticeService service = new NoticeService();
		NoticeBoardDTO board = new NoticeBoardDTO();
//		
		board.setMemId(memId);
//		board.setTitle(title);
//		board.setContent(content);
//		System.out.println(board);
//		
		int result = 0;
		int result2 = 0;

//		//
		AttachmentDTO attach = new AttachmentDTO();
		List<AttachmentDTO> attachList = new ArrayList<>();
		String rootLocation = getServletContext().getRealPath("/");
		System.out.println(rootLocation);
		// 파일 업로드
				if(ServletFileUpload.isMultipartContent(request)) {
					
					System.out.println("파일 저장 ROOT 경로 : " + rootLocation);
					System.out.println("최대 업로드 파일 용량 : " + maxFileSize);
					System.out.println("인코딩 방식 : " + encodingType);
					
					String fileUploadDirectory = rootLocation + "/resources/upload/images/notice";
					
					File directory = new File(fileUploadDirectory);
					/* 파일 저장경로가 존재하지 않는 경우 디렉토리를 생성한다. */
					if(!directory.exists()) {
						
						System.out.println("폴더 생성 : " + directory.mkdirs());
					}
					
					/* request를 parsing하고 파일을 저장한 뒤 필요한 내용을 담을 리스트와 맵을 생성
					 * 파일에 대한 정보는 리스트에, 다른 파라미터의 정보는 모두 맵에 담을 것이다.
					 * */
					Map<String, String> parameter = new HashMap<>();
					List<Map<String, String>> fileList = new ArrayList<>();
					
					/* 파일을 업로드할 시 최대크기나 임시 저장할 폴더의 경로 등을 포함한 인스턴스 생성 */
					DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
					fileItemFactory.setRepository(new File(fileUploadDirectory)); 
					fileItemFactory.setSizeThreshold(maxFileSize);
					
					ServletFileUpload fileUpload = new ServletFileUpload(fileItemFactory);
					
					try {
						List<FileItem> fileItems = fileUpload.parseRequest(request);
						
						for(FileItem item : fileItems) {
							/* 폼 데이터 isFormField 속성이 true이고, 파일은 isFormField 속성이 false*/
							System.out.println(item);
							System.out.println(item.isFormField());
						}
						
						for(int i = 0; i < fileItems.size(); i++) {
							
							FileItem item = fileItems.get(i);
							/* 파일 데이터인 경우 */
							if(!item.isFormField()) {
								
								if(item.getSize() > 0) {
									
									/* 파일의 사이즈가 0보다 커야 전송된 파일이 있다는 의미 
									 * 0인 경우에는 무시하도록 로직을 작성
									 * */
									String filedName = item.getFieldName();
									String originFileName = item.getName();
									
									int dot = originFileName.lastIndexOf(".");
									String ext = originFileName.substring(dot);
									
									String randomFileName = UUID.randomUUID().toString().replace("-", "") + ext;
									/* 저장할 파일 정보를 담은 인스턴스 생성 */
									File storeFile = new File(fileUploadDirectory + "/" + randomFileName);
									
									/* 저장한다. */
									item.write(storeFile);
									Map<String, String> fileMap = new HashMap<>();
									fileMap.put("filedName", filedName);
									fileMap.put("originFileName", originFileName);
									fileMap.put("savedFileName", randomFileName);
									fileMap.put("savePath", fileUploadDirectory);
									fileList.add(fileMap);
									
									attach.setOriginalName(originFileName);
									attach.setSavedName(randomFileName);
									attach.setSavePath(fileUploadDirectory);
								    attachList.add(attach);
									
								}
								
							} else {
							    /* 폼 데이터인 경우 */	
								/* 전송된 폼의 name은 getFieldName()으로 받아오고, 해당 필드의 value는 
								 * getString()으로 받아온다. 하지만 인코딩 설정을 하더라도 전송되는 파라미터는
								 * ISO-8859-1로 처리된다. 별도로 ISO-8858-1로 해석된 한글을 UTF-8로 변경해주어야한다.
								 * */
								
								parameter.put(item.getFieldName(), new String(item.getString().getBytes("ISO-8859-1"), "UTF-8"));
							}
						}

						System.out.println("parameter : " + parameter);
						System.out.println("fileList : " + fileList);
						
						
					
						board.setTitle(parameter.get("title"));
						board.setContent(parameter.get("content"));
						System.out.println(board);
						
						result = service.insertNotice(board);
						if(!(attachList.isEmpty())) {
						result2 = service.noteFileInsert(attachList);}


						
					} catch (Exception e) {
						
						/* 어떤 종류의 Exception이 발생하더라도 실패 시 파일을 삭제해야 한다. */
						int cnt = 0;
						for(int i = 0; i < fileList.size(); i++) {
							
							Map<String, String> file = fileList.get(i);
							
							File deleteFile = new File(fileUploadDirectory + "/" + file.get("savedFileName"));
							boolean isDeleted = deleteFile.delete();
							
							if(isDeleted) {
								cnt++;
							}
						}
						
						if(cnt == fileList.size()) {
							
							System.out.println("업로드에 실패한 모든 사진 삭제 완료!");
						} else {
							
							e.printStackTrace();
						}
					}
				}
				
	
				
				
		
		
		String path="";
		
		
		
		
		
		
		if(result > 0 && result2 > 0) {
			response.setContentType("text/html;charset=utf-8");
			path ="/board/list";
			response.sendRedirect(request.getContextPath()+path);
			
		}else if(result > 0 && attachList.isEmpty()) 
		{
		 response.setContentType("text/html;charset=utf-8");
		 path ="/board/list";
		 response.sendRedirect(request.getContextPath()+path);
		}
		
		else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "게시글 등록에 실패하셨습니다.");
			request.getRequestDispatcher(path).forward(request, response);
		}
		
		
		
		
		
	
	}

}
