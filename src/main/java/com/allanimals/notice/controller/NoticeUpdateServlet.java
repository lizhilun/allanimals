package com.allanimals.notice.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.member.model.dto.MemberDTO;
import com.allanimals.notice.dto.NoticeBoardDTO;
import com.allanimals.notice.service.NoticeService;



/**
 * Servlet implementation class NoticeUpdateServlet
 */
@WebServlet("/board/noticeupdate")
public class NoticeUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NoticeUpdateServlet() {
   
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String boardNo = request.getParameter("notNo");
		System.out.println(boardNo);
		
		NoticeBoardDTO board = new NoticeBoardDTO();
		
		NoticeService service = new NoticeService();
		board = service.selectNoticeBoard(boardNo);
		
		System.out.println(board);
		
		
		String path="";
		if(board != null) {
		path = "/WEB-INF/views/notice/notice_update.jsp";
		request.setAttribute("board", board); 
		}else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "게시물 목록 조회 실패!");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
	
	}

	
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		
		String title = request.getParameter("title");
		String content = request.getParameter("content");
		String no = request.getParameter("no");
		String memId =((MemberDTO) request.getSession().getAttribute("loginMember")).getMemId();
		
		NoticeService service = new NoticeService();
		NoticeBoardDTO board = new NoticeBoardDTO();
		
		board.setNo(no);
		board.setMemId(memId);
		board.setTitle(title);
		board.setContent(content);
		System.out.println(board);
		
		int result = service.updateNotice(board);
		
		String path="";
		
		if(result > 0) {
			response.setContentType("text/html;charset=utf-8");
			path ="/board/list";
			response.sendRedirect(request.getContextPath()+path);
			
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "게시글 수정에 실패하셨습니다.");
			request.getRequestDispatcher(path).forward(request, response);
		}

}
}