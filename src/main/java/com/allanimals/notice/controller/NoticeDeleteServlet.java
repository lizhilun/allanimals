package com.allanimals.notice.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.notice.dto.NoticeBoardDTO;
import com.allanimals.notice.service.NoticeService;



/**
 * Servlet implementation class NoticeDeleteServlet
 */
@WebServlet(name = "NoticeDeleteBoardServlet", urlPatterns = { "/board/noticedelete" })
public class NoticeDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NoticeDeleteServlet() {
      
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String boardNo = request.getParameter("notNo");
		
		NoticeService service = new NoticeService();
		NoticeBoardDTO board = new NoticeBoardDTO();
		
		System.out.println(boardNo);
		
		int result = service.deleteNotice(boardNo);
		
		String path="";
		
		if(result > 0) {
			response.setContentType("text/html;charset=utf-8");
			path ="/board/list";
			response.sendRedirect(request.getContextPath()+path);
			
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "게시글 수정에 실패하셨습니다.");
			request.getRequestDispatcher(path).forward(request, response);
		}
	}

}
