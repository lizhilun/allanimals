package com.allanimals.notice.dto;

import java.sql.Date;
import java.util.List;

import com.allanimals.member.model.dto.MemberDTO;

public class NoticeBoardDTO {
	private String title;
	private String no;
	private String content;
	private java.sql.Date date;
	private String memId;
	private MemberDTO member;
	private String yN;
	private List<AttachmentDTO> photo;
	
	public NoticeBoardDTO(String title, String no, String content, Date date, String memId, MemberDTO member, String yN,
			List<AttachmentDTO> photo) {
		
		this.title = title;
		this.no = no;
		this.content = content;
		this.date = date;
		this.memId = memId;
		this.member = member;
		this.yN = yN;
		this.photo = photo;
	}
	public NoticeBoardDTO() {
		
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public java.sql.Date getDate() {
		return date;
	}
	public void setDate(java.sql.Date date) {
		this.date = date;
	}
	public String getMemId() {
		return memId;
	}
	public void setMemId(String memId) {
		this.memId = memId;
	}
	public MemberDTO getMember() {
		return member;
	}
	public void setMember(MemberDTO member) {
		this.member = member;
	}
	public String getyN() {
		return yN;
	}
	public void setyN(String yN) {
		this.yN = yN;
	}
	public List<AttachmentDTO> getPhoto() {
		return photo;
	}
	public void setPhoto(List<AttachmentDTO> photo) {
		this.photo = photo;
	}
	@Override
	public String toString() {
		return "NoticeBoardDTO [title=" + title + ", no=" + no + ", content=" + content + ", date=" + date + ", memId="
				+ memId + ", member=" + member + ", yN=" + yN + ", photo=" + photo + "]";
	}
	
	
	
}
