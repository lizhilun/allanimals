package com.allanimals.notice.dto;

public class AttachmentDTO {
	private String phoNo;
	private String BoardNo;
	private String originalName;
	private String savedName;
	private String savePath;
	private String thumbnailPath;
	private String yN;
	
	public AttachmentDTO(String phoNo, String boardNo, String originalName, String savedName, String savePath,
			String thumbnailPath, String yN) {

		this.phoNo = phoNo;
		BoardNo = boardNo;
		this.originalName = originalName;
		this.savedName = savedName;
		this.savePath = savePath;
		this.thumbnailPath = thumbnailPath;
		this.yN = yN;
	}

	public AttachmentDTO() {
	
	}

	public String getPhoNo() {
		return phoNo;
	}

	public void setPhoNo(String phoNo) {
		this.phoNo = phoNo;
	}

	public String getBoardNo() {
		return BoardNo;
	}

	public void setBoardNo(String boardNo) {
		BoardNo = boardNo;
	}

	public String getOriginalName() {
		return originalName;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	public String getSavedName() {
		return savedName;
	}

	public void setSavedName(String savedName) {
		this.savedName = savedName;
	}

	public String getSavePath() {
		return savePath;
	}

	public void setSavePath(String savePath) {
		this.savePath = savePath;
	}

	public String getThumbnailPath() {
		return thumbnailPath;
	}

	public void setThumbnailPath(String thumbnailPath) {
		this.thumbnailPath = thumbnailPath;
	}

	public String getyN() {
		return yN;
	}

	public void setyN(String yN) {
		this.yN = yN;
	}

	@Override
	public String toString() {
		return "AttachmentDTO [phoNo=" + phoNo + ", BoardNo=" + BoardNo + ", originalName=" + originalName
				+ ", savedName=" + savedName + ", savePath=" + savePath + ", thumbnailPath=" + thumbnailPath + ", yN="
				+ yN + "]";
	}
	
	
	
}
