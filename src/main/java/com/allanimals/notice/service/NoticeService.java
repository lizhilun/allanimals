package com.allanimals.notice.service;


import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.allanimals.commnon.pagin.SelectCriteria;
import com.allanimals.notice.dao.NoticeMapper;
import com.allanimals.notice.dto.AttachmentDTO;
import com.allanimals.notice.dto.NoticeBoardDTO;

import static com.allanimals.commnon.mybatis.Template.getSqlSession;

public class NoticeService {
	private NoticeMapper noticeMapper;
	
	
	/**
	 * <pre>
	 * 	페이징 처리를 위한 전체 게시물 수 조회용 메소드
	 * </pre>
	 * @param searchMap
	 * @return
	 */
	public int selectTotalCount(Map<String, String> searchMap) {
		
		SqlSession sqlSession = getSqlSession();
		noticeMapper = sqlSession.getMapper(NoticeMapper.class);
		
		int totalCount = noticeMapper.selectTotalCount(searchMap);
		
		sqlSession.close();
		
		return totalCount;
	}
	
	/**
	 * <pre>
	 *   게시물 전체 조회용 메소드
	 * </pre>
	 * @param selectCriteria
	 * @return
	 */
	public List<NoticeBoardDTO> selectBoardList(SelectCriteria selectCriteria) {
		
		SqlSession sqlSession = getSqlSession();
		noticeMapper = sqlSession.getMapper(NoticeMapper.class);
		
		List<NoticeBoardDTO> boardList = noticeMapper.selectBoardList(selectCriteria);
		
		sqlSession.close();
		
		return boardList;
	}
	

	public int insertNotice(NoticeBoardDTO board) {
		SqlSession sqlSession = getSqlSession();
		noticeMapper = sqlSession.getMapper(NoticeMapper.class);
		int result = noticeMapper.insertNotice(board);
				
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
				
		sqlSession.close();
				
		return result;
	}

	public NoticeBoardDTO selectNoticeBoard(String boardNo) {
		SqlSession sqlSession = getSqlSession();
		noticeMapper = sqlSession.getMapper(NoticeMapper.class);
		NoticeBoardDTO board = noticeMapper.selectNoticeBoard(boardNo);
				

		sqlSession.close();
		return board;
	}

	public int updateNotice(NoticeBoardDTO board) {
		SqlSession sqlSession = getSqlSession();
		noticeMapper = sqlSession.getMapper(NoticeMapper.class);
		int result = noticeMapper.updateNotice(board);
		
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
				
		sqlSession.close();
				
		return result;
	}

	public int deleteNotice(String boardNo) {
		SqlSession sqlSession = getSqlSession();
		noticeMapper = sqlSession.getMapper(NoticeMapper.class);
		int result = noticeMapper.deleteNotice(boardNo);
		
		
		if(result > 0) {
			int result2 = noticeMapper.deleteNotFile(boardNo);
			sqlSession.commit();

		} else {
			sqlSession.rollback();
		}
				
		sqlSession.close();
				
		return result;
	}

	public int noteFileInsert(List<AttachmentDTO> attachList) {
		SqlSession sqlSession = getSqlSession();
		noticeMapper = sqlSession.getMapper(NoticeMapper.class);
		int result2 = 0;
		int count = 0;
		for(AttachmentDTO attach : attachList) {
		int result = noticeMapper.noteFileInsert(attach);
		count++;
		if(result > 0) {
			result2 ++;
		} 
				
		}
		
		if(result2 == count) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
				
		sqlSession.close();
				
		return result2;
	}

	public List<AttachmentDTO> selectNotFileList(String boardNo) {
		SqlSession sqlSession = getSqlSession();
		noticeMapper = sqlSession.getMapper(NoticeMapper.class);
		List<AttachmentDTO> attachList = noticeMapper.selectNotFileList(boardNo);
		sqlSession.close();
		
		return attachList;
	}
}
