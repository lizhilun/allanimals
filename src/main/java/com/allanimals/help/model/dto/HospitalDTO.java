package com.allanimals.help.model.dto;

public class HospitalDTO {
	private String name;
	private String addre;
	private String phone;
	private String time;
	private String no;
	private String memId;
	
	public HospitalDTO(String name, String addre, String phone, String time, String no, String memId) {
		
		this.name = name;
		this.addre = addre;
		this.phone = phone;
		this.time = time;
		this.no = no;
		this.memId = memId;
	}

	public HospitalDTO() {
	
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddre() {
		return addre;
	}

	public void setAddre(String addre) {
		this.addre = addre;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getMemId() {
		return memId;
	}

	public void setMemId(String memId) {
		this.memId = memId;
	}

	@Override
	public String toString() {
		return "HospitalDTO [name=" + name + ", addre=" + addre + ", phone=" + phone + ", time=" + time + ", no=" + no
				+ ", memId=" + memId + "]";
	}
	
	

}
