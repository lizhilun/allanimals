package com.allanimals.help.model.service;

import static com.allanimals.commnon.mybatis.Template.getSqlSession;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.allanimals.help.model.dao.ReRequestMapper;
import com.allanimals.help.model.dto.rescueRequestDTO;

public class RescueRequestService {
	
private ReRequestMapper reRequestMapper;	

	/**
	 * <pre>
	 * 	구조요청 전체 목록 조회용 메소드
	 * </pre>
	 * @return
	 */
	public List<rescueRequestDTO> selectAllRescueRequestList(String selectRe) {
		
		SqlSession sqlSession = getSqlSession();
		reRequestMapper = sqlSession.getMapper(ReRequestMapper.class);
		List<rescueRequestDTO> resList = reRequestMapper.selectAllRescueRequestList(selectRe);
		
		sqlSession.close();
		
		return resList;
	}
	/**
	 * <pre>
	 *   지역 목록 출력용
	 * </pre>
	 * @return
	 */
	public List<rescueRequestDTO> selectAreaList() {
		
		SqlSession sqlSession = getSqlSession();
		reRequestMapper = sqlSession.getMapper(ReRequestMapper.class);
		List<rescueRequestDTO> areaList = reRequestMapper.selectAreaList();
		
		sqlSession.close();
		
		return areaList;
	}

	

	

}
