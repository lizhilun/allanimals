package com.allanimals.help.model.service;

import static com.allanimals.commnon.mybatis.Template.getSqlSession;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.allanimals.commnon.pagin.SelectCriteria;
import com.allanimals.help.model.dao.HospitalMapper;
import com.allanimals.help.model.dto.HospitalDTO;

public class HospitalService {
	private HospitalMapper hospitalMapper;
	
	public int selectTotalCount(Map<String, String> searchMap) {
		SqlSession sqlSession = getSqlSession();
		hospitalMapper = sqlSession.getMapper(HospitalMapper.class);
		int totalCount = hospitalMapper.selectTotalCount(searchMap);
		
		sqlSession.close();
		
		return totalCount;
	}

	public List<HospitalDTO> selectBoardList(SelectCriteria selectCriteria) {
		
		SqlSession sqlSession = getSqlSession();
		hospitalMapper = sqlSession.getMapper(HospitalMapper.class);
		List<HospitalDTO> boardList = hospitalMapper.selectBoardList(selectCriteria);
		
		sqlSession.close();
		
		return boardList;
	}

}
