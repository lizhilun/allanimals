package com.allanimals.help.model.dao;

import java.util.List;
import java.util.Map;

import com.allanimals.commnon.pagin.SelectCriteria;
import com.allanimals.help.model.dto.HospitalDTO;

public interface HospitalMapper {

	int selectTotalCount(Map<String, String> searchMap);

	List<HospitalDTO> selectBoardList(SelectCriteria selectCriteria);

	
	
}
