package com.allanimals.help.model.dao;

import java.util.List;

import com.allanimals.help.model.dto.rescueRequestDTO;

public interface ReRequestMapper {

	List<rescueRequestDTO> selectAllRescueRequestList(String selectRe);

	List<rescueRequestDTO> selectAreaList();


}
