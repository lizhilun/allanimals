package com.allanimals.help.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.allanimals.help.model.dto.rescueRequestDTO;
import com.allanimals.help.model.service.RescueRequestService;

/**
 * Servlet implementation class RescueRequestServlet
 */
@WebServlet("/rescueRequest")
public class RescueRequestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		List<rescueRequestDTO> areaList = new RescueRequestService().selectAreaList();

		String selectRe = request.getParameter("type");
		rescueRequestDTO res = new rescueRequestDTO();
		res.setHelpArea(selectRe);
		System.out.println("출력확인 : " + selectRe);
		
		List<rescueRequestDTO> resList = new RescueRequestService().selectAllRescueRequestList(selectRe);	
		
		String path = "";
		if(areaList != null) {
			path = "/WEB-INF/views/help/rescueRequest.jsp";
			request.setAttribute("resList", resList);
			request.setAttribute("areaList", areaList);
			request.setAttribute("type", selectRe);
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
	}
}
